/* ============================================================================
|
|   Filename:    ast.cpp
|   Dated:       21 August, 2006
|   By:          Shahid Alam (shalam@ieee.org
|
|   Description: Implementation of AST (Abstract Syntax Tree) class
|
|   TO DO:       
|
 ----------------------------------------------------------------------------*/

#include "include/ast.h"

/**
//
// Constructor function.
//
 ----------------------------------------------------------------------------*/
AST :: AST ()
{
	root = 0;
}

/**
//
// Destructor function.
//
 ----------------------------------------------------------------------------*/
AST :: ~AST ()
{
	removeAll ();
}

/**
//
// Dynamically allocates memory for the new Node.
//
 ----------------------------------------------------------------------------*/
TreeNode * AST :: insertRoot (const char *clabel)
{
	root = new TreeNode (clabel);
	root->setID (0);

	return root;
}

/**
//
// Change the lable of the node to new value
// @param node pointer to the node
// @see TreeNode:ID
// @param label pointer points to the new value
//
 ----------------------------------------------------------------------------*/
void AST :: changeValue (TreeNode *node, const char *cvalue)
{
	if (node != NULL)
		node->setValuePtr (cvalue);
}

/**
//
// Returns vlaue of the node
// @param id node id for the value
//
 ----------------------------------------------------------------------------*/
const char * AST :: getValue (IntList *id)
{
	const char *value;

	TreeNode *node = getNode (id);
	if (node != NULL)
		value = node->getValuePtr ();
	else
		value = NULL;

	return value;
}

/**
//
// Add child node to the parent node
// @param pnode pointer of the parent node where
//  child is to be added
// @param clabel pointer points to the value of child
//  node to be added
// @return ID of the node which has been added
//
 ----------------------------------------------------------------------------*/
TreeNode * AST :: addNode (TreeNode *pnode, const char *cvalue)
{
	TreeNode *cnode = NULL;

	if (pnode != NULL)
	{
		cnode = new TreeNode (cvalue);
		cnode->setParent (pnode);
		pnode->addChild (cnode);
	}
	else
		fprintf (stderr, "AST :: AddNode : Incorrect Parent Node %s\n", pnode);

	return cnode;
}

/**
//
// Get Node from the Tree using the ID
// @param id ID to be searched for the Node
// @return TreeNode with the given ID
//
 ----------------------------------------------------------------------------*/
TreeNode * AST :: getNode (IntList *id)
{
	TreeNode *node = NULL;

	if (!isempty ())
	{
		node = root;
		//
		// If not root
		//
		if (id->getFirst () > 0)
		{
			int lastIntID = id->getLast ();
			int count = id->getCount ();

			node->initChildIterator ();
			node = node->getNextChild ();
			while (node != NULL)
			{
				IntList *cID = node->getID ();
				if (count == cID->getCount ())
				{
					node = node->getChild (id);
					break;
				}
				else if (lastIntID == cID->getLast ())
					node->initChildIterator ();
				node = node->getNextChild ();
			}
		}
		else if (id->getFirst () != 0)
			node = NULL;
	}

	return node;
}

/**
//
// Remove Root Node including all the children.
//
 ----------------------------------------------------------------------------*/
void AST :: removeAll (void)
{
	if (!isempty ())
	{
		//
		// If root has children
		//
		int children = root->getNumberOfChildren ();
		if (children > 0)
		{
			TreeNode *node = NULL;
			root->initChildIterator ();
			while ((node = root->getNextChild ()) != NULL)
				removeChild (node);
		}

//fprintf (stderr, "<ID> %12s </ID> DELETED ROOT\n", root->getValuePtr());
		delete root;
		root = 0;
	}
}

/**
//
// Remove Node including all the children.
// @param mnode node to be removed from the AST
//
 ----------------------------------------------------------------------------*/
void AST :: removeNode (TreeNode *mnode)
{
	if (mnode != NULL)
	{
		if (mnode == root)
			removeAll ();
		else
		{
			//
			// If not leaf node
			//
			int children = mnode->getNumberOfChildren ();
			if (children > 0)
			{
				TreeNode *node = NULL;
				mnode->initChildIterator ();
				while ((node = mnode->getNextChild ()) != NULL)
					removeChild (node);
			}
			TreeNode *pnode = mnode->getParent();
//fprintf (stderr, "<ID> %17s </ID> PARENT ", pnode->getValuePtr());
//fprintf (stderr, "<ID> %17s </ID> NODE\n", mnode->getValuePtr());
			pnode->removeChildFromList(mnode->getID());
//			delete mnode;
		}
	}
}

/**
//
// Remove Child Node including all the children
// @param node child node to be removed from the AST
//
 ----------------------------------------------------------------------------*/
void AST :: removeChild (TreeNode *node)
{
	if (node != NULL)
	{
		//
		// If not leaf node
		//
		int children = node->getNumberOfChildren ();
		if (children > 0)
		{
			node->initChildIterator ();
			for (int n = 0; n < children; n++)
				removeChild (node->getNextChild ());
		}

//fprintf (stdout, "<ID> %12s </ID> DELETED\n", node->getID ());
		delete node;
	}
}

/**
//
// Prints the AST with Tokens.
// @param file File pointer to write to, if NULL then
//  write to stdout
// @param sym Pointer to the Symbol Table for printing tokens
//
 ----------------------------------------------------------------------------*/
void AST :: printWithToken (FILE *file, Symtable *symT) const
{
	FILE *stream = stdout;

	if (!isempty ())
	{
		if (file != NULL)
			stream = file;
		//
		// Printing root
		//
		fprintf (stream, "\n   <ROOT ID='"); root->getID ()->print (stream);
		fprintf (stream, "' value='%s' ", root->getValuePtr ());
		fflush (stream);
		int children = root->getNumberOfChildren ();
		if (children <= 0)
		{
			fprintf (stream, "children='%d'/>\n", children);
			fflush (stream);
		}
		else
		{
			LG lg;
			fprintf (stream, "children='%d'>\n", children);
			fflush (stream);
			TreeNode *node = NULL;
			root->initChildIterator ();
			while ((node = root->getNextChild ()) != NULL)
				printChildrenWithToken (3, node, file, symT, lg);
			fprintf (stream, "   </ROOT>\n");
			fflush (stream);
		}
	}
}

/**
//
// Prints the AST with Tokens.
// Recursively call this function to print all the childs.
// @param depth depth of the child node for printing spaces
// @param node node to be printed
// @param file File pointer to write to, if NULL then
//  write to stdout
// @param sym Pointer to the Symbol Table for printing tokens
//
 ----------------------------------------------------------------------------*/
void AST :: printChildrenWithToken (int depth, TreeNode *node, FILE *file, Symtable *symT, LG lg) const
{
	if (node != NULL)
	{
		FILE *stream = stdout;
		if (file != NULL)
			stream = file;
		//
		// Printing node
		//
		for (int i = 0; i < depth; i++)
			fprintf (stream, " ");
		const char *lexeme = node->getValuePtr ();
		SYMBOL *sym = symT->lookup (lexeme);
		char *token;
		if (sym != NULL)
			token = lg.getToken (sym->token);
		else
			token = (char *)lexeme;
		fprintf (stream, "   <%s name='%s'", token, lexeme);
		fflush (stream);
		int children = node->getNumberOfChildren ();
		if (children <= 0)
		{
			fprintf (stream, " />\n");
			fflush (stream);
		}
		//
		// Printing all the children
		//
		else
		{
			fprintf (stream, ">\n");
			fflush (stream);
			node->initChildIterator ();
			for (int n = 0; n < children; n++)
				printChildrenWithToken (depth + 3, node->getNextChild (), file, symT, lg);
			for (int i = 0; i < depth; i++)
				fprintf (stream, " ");
			fprintf (stream, "   </%s>\n", token);
			fflush (stream);
		}
	}
	else
		return;
}

/**
//
// Prints the AST with nimber of children for debugging using ID's.
// @param file File pointer to write to, if NULL then
//  write to stdout
//
 ----------------------------------------------------------------------------*/
void AST :: print (FILE *file) const
{
	FILE *stream = stdout;

	if (!isempty ())
	{
		if (file != NULL)
			stream = file;
		//
		// Printing root
		//
		fprintf (stream, "\n   <ROOT ID='"); root->getID ()->print (stream);
		fprintf (stream, "' value='%s' ", root->getValuePtr ());
		fflush (stream);
		int children = root->getNumberOfChildren ();
		if (children <= 0)
		{
			fprintf (stream, "children='%d'/>\n", children);
			fflush (stream);
		}
		else
		{
			fprintf (stream, "children='%d'>\n", children);
			fflush (stream);
			TreeNode *node = NULL;
			root->initChildIterator ();
			while ((node = root->getNextChild ()) != NULL)
				printChildren (3, node, file);
			fprintf (stream, "   </ROOT>\n");
			fflush (stream);
		}
	}
}

/**
//
// Prints the AST with nimber of children for debugging using ID's.
// Recursively call this function to print all the childs.
// @param depth depth of the child node for printing spaces
// @param node node to be printed
// @param file File pointer to write to, if NULL then
//  write to stdout
//
 ----------------------------------------------------------------------------*/
void AST :: printChildren (int depth, TreeNode *node, FILE *file) const
{
	if (node != NULL)
	{
		FILE *stream = stdout;
		if (file != NULL)
			stream = file;
		//
		// Printing node
		//
		for (int i = 0; i < depth; i++)
			fprintf (stream, " ");
		fprintf (stream, "   <node name='%s' ", node->getValuePtr ());
		fprintf (stream, "ID='"); node->getID ()->print (stream); fprintf (stream, "' ");
		fflush (stream);
		int children = node->getNumberOfChildren ();
		if (children <= 0)
		{
			fprintf (stream, "children='%d'/>\n", children);
			fflush (stream);
		}
		//
		// Printing all the children
		//
		else
		{
			fprintf (stream, "children='%d'>\n", children);
			fflush (stream);
			node->initChildIterator ();
			for (int n = 0; n < children; n++)
				printChildren (depth + 3, node->getNextChild (), file);
			for (int i = 0; i < depth; i++)
				fprintf (stream, " ");
			fprintf (stream, "   </node>\n");
			fflush (stream);
		}
	}
	else
		return;
}
