/* ============================================================================
|
|   Filename:    lexer.cpp
|   Dated:       31 May, 2006
|   By:          Shahid Alam (shalam@ieee.org
|
|   Description: This lexical Analyzer reads the input stream representing the
|                source program into a buffer and then , one character at a
|                time and translate it into valid tokens.
|
|   TO DO:       See below
|
 ----------------------------------------------------------------------------*/

#include "include/lexer.h"

/**
|
| Constructor
|
 ----------------------------------------------------------------------------*/
Lexer :: Lexer (int sizeOfSymbolTable)
{
	count = 1;
	currentCol = currentLine = 1;

	//
	// Initializes the symbol table
	//
	st = new Symtable (sizeOfSymbolTable);
	buffer = NULL;
	laoutNum = NULL;
	in = NULL;
	fileLA = NULL;
	fileLANum = NULL;
}

/**
|
| Destructor
|
 ----------------------------------------------------------------------------*/
Lexer :: ~Lexer ()
{
	//
	// Write Lexical Analysis number output to file
	//
	if (_PRINT_LEXICAL_ANALYSIS_ == 1)
	{
		fclose (fileLA);
		laoutNum[strlen (laoutNum)] = END_OF_STRING;
		if (fileLANum != NULL)
		{
			fputs (laoutNum, fileLANum);
			fflush (fileLANum);
			fclose (fileLANum);
		}
		delete (laoutNum);
	}

	//
	// Delete buffer and symbol table and close the file
	//
	delete (st);
	if (buffer != NULL)
		delete (buffer);
}

/**
|
| read file and store it in buffer
| @param filename Name of the file to be processed
| @return 0 on success and 1 on ERROR
| 1 = Syntax error
|
 ----------------------------------------------------------------------------*/
int Lexer :: readFile (char *filename, char *lFilename)
{
	if ((in = fopen (filename, "rb")) == NULL)
	{
		char errStr[128];
		sprintf (errStr, "Lexer->readFile: Cannot open the input file %s.\n", filename);
		err.fatal (errStr, FILE_NOT_OPEN);
		return 1;
	}
	else
	{
		//
		// Read the input file (CAL file) in buffer.
		//
		size = fileSize (in);
		if (size <= 0)
		{
			fprintf (stderr, "<FileEmpty file='%s' />\n", filename);
			return 2;
		}
		buffer = new char[size];
		//
		// Check for ERROR
		//
		if (buffer == NULL)
		{
			char errStr[128];
			sprintf (errStr, "Lexer->readFile: Not able to read complete file %s", filename);
			err.fatal (errStr, FILE_NOT_READ);
			delete (buffer);
			buffer = NULL;
			return 1;
		}
		else
		{
			int n = fread (buffer, size, 1, in);
			if (n < 1)
			{
				char errStr[128];
				sprintf ("Lexer->readFile: Not able to read complete file %s", filename);
				err.fatal (errStr, FILE_NOT_READ);
				delete (buffer);
				buffer = NULL;
				return 1;
			}
		}
		fclose (in);
	}


	//
	// Open out files for Lexical Analysis output
	//
	if (_PRINT_LEXICAL_ANALYSIS_ == 1)
	{
		if ((fileLA = fopen (lFilename, "wab")) == NULL)
		{
			char errStr[128];
			sprintf (errStr, "Lexer->readFile: Cannot open Lexical Analyser output file %s.\n", lFilename);
			err.warning (errStr, FILE_NOT_OPEN, 0, 0);
			_PRINT_LEXICAL_ANALYSIS_ = 0;
		}
	
		char *tmp, file[256];
		if ((tmp = strtok (lFilename, ".")) != NULL)
			sprintf (file, "%s", tmp);
		else
			sprintf (file, "%s", lFilename);
	
		strcat (file, ".num");
		file[strlen(file)] = END_OF_STRING;
		if ((fileLANum = fopen (file, "wab")) == NULL)
		{
			char errStr[128];
			sprintf (errStr, "Lexer->readFile: Cannot open Lexical Analyser number output file %s.\n", file);
			err.warning (errStr, FILE_NOT_OPEN, 0, 0);
		}
	
		laoutNum = new char [size];
		laoutNum[0] = END_OF_STRING;
	}

	//
	// Initialize global parameters
	//
	current = lookahead = buffer;
	buffN = 0;

	return 0;
}

/**
|
| Process the stored buffer and returns the next token
| It look ahead several characters beyond the current input position
| for a pattern before a match can be found.
|
| For string CHARACTERS = ASCII character set from 32 - 126
|
| @return pointer to the symbol table for the token and lexeme
|
 ----------------------------------------------------------------------------*/
SYMBOL * Lexer :: getNextToken (void)
{
	int BAD_CHAR_ERROR = 0;
	int STATE = 0;
	int ignore = 0;
	char ch, *lexeme = NULL, tmp[8];
	SYMBOL *sym = NULL;

	//
	// Implementing switch statements using Transition Diagrams
	// for different state machines as mentioned
	//
	// Check the size of the file to make sure count doesn't
	// pass the end of the buffer
	//
	ch = *lookahead;
	for ( ; count <= size; )
	{
		sym = NULL;
		switch (STATE)
		{
			//
			// ws, whitespace to be ignored see Readme.txt
			// TOKEN = NONE
			//
			case 0:
				buffN = count;
				if (ch == CR || ch == LF)
				{
					if (ch == LF)
					{
						currentCol = 1;
						currentLine++;
					}
					STATE = 0;
				}
				else if (ch == TAB || ch == SPACE)
				{
					ignore++;
					STATE = 0;
				}
				else if (ch == '-')
				{
					STATE = 1;
				}
				//
				// Check for ATPRE '@pre'
				//
				else if (ch == '@')
					STATE = 3;
				else if (isalpha (ch))
				{
					STATE = 3;
				}
				else if (isdigit (ch))
				{
					STATE = 4;
				}
				else if (ch == '?')
				{
					sym = st->insert ("?", lg.QMARK);
					currentCol += (1 + ignore);
					if (_PRINT_LEXICAL_ANALYSIS_ == 1)
						fillBuffLaNum (count);
					STATE = 0;
				}
				else if (ch == ',')
				{
					sym = st->insert (",", lg.COMMA);
					currentCol += (1 + ignore);
					if (_PRINT_LEXICAL_ANALYSIS_ == 1)
						fillBuffLaNum (count);
					STATE = 0;
				}
				else if (ch == ':')
				{
					STATE = 11;
				}
				else if (ch == ';')
				{
					sym = st->insert (";", lg.SCOLON);
					currentCol += (1 + ignore);
					if (_PRINT_LEXICAL_ANALYSIS_ == 1)
						fillBuffLaNum (count);
					STATE = 0;
				}
				else if (ch == '=')
				{
					sym = st->insert ("=", lg.ROP);
					currentCol += (1 + ignore);
					if (_PRINT_LEXICAL_ANALYSIS_ == 1)
						fillBuffLaNum (count);
					STATE = 0;
				}
				else if (ch == '<')
				{
					STATE = 12;
				}
				else if (ch == '>')
				{
					STATE = 13;
				}
				else if (ch == '.')
				{
					STATE = 16;
				}
				else if (ch == '*')
				{
					sym = st->insert ("*", lg.MUL);
					currentCol += (1 + ignore);
					if (_PRINT_LEXICAL_ANALYSIS_ == 1)
						fillBuffLaNum (count);
					STATE = 0;
				}
				else if (ch == '/')
				{
					sym = st->insert ("/", lg.DIV);
					currentCol += (1 + ignore);
					if (_PRINT_LEXICAL_ANALYSIS_ == 1)
						fillBuffLaNum (count);
					STATE = 0;
				}
				else if (ch == '+')
				{
					sym = st->insert ("+", lg.PLUS);
					currentCol += (1 + ignore);
					if (_PRINT_LEXICAL_ANALYSIS_ == 1)
						fillBuffLaNum (count);
					STATE = 0;
				}
				else if (ch == '^')
				{
					STATE = 15;
				}
				else if (ch == '"')
				{
					STATE = 17;
				}
				else if (ch == '(')
				{
					sym = st->insert ("(", lg.LBRKT1);
					currentCol += (1 + ignore);
					if (_PRINT_LEXICAL_ANALYSIS_ == 1)
						fillBuffLaNum (count);
					STATE = 0;
				}
				else if (ch == ')')
				{
					sym = st->insert (")", lg.RBRKT1);
					currentCol += (1 + ignore);
					if (_PRINT_LEXICAL_ANALYSIS_ == 1)
						fillBuffLaNum (count);
					STATE = 0;
				}
				else if (ch == '[')
				{
					sym = st->insert ("[", lg.LBRKT2);
					currentCol += (1 + ignore);
					if (_PRINT_LEXICAL_ANALYSIS_ == 1)
						fillBuffLaNum (count);
					STATE = 0;
				}
				else if (ch == ']')
				{
					sym = st->insert ("]", lg.RBRKT2);
					currentCol += (1 + ignore);
					if (_PRINT_LEXICAL_ANALYSIS_ == 1)
						fillBuffLaNum (count);
					STATE = 0;
				}
				else if (ch == '{')
				{
					sym = st->insert ("{", lg.LBRKT3);
					currentCol += (1 + ignore);
					if (_PRINT_LEXICAL_ANALYSIS_ == 1)
						fillBuffLaNum (count);
					STATE = 0;
				}
				else if (ch == '}')
				{
					sym = st->insert ("}", lg.RBRKT3);
					currentCol += (1 + ignore);
					if (_PRINT_LEXICAL_ANALYSIS_ == 1)
						fillBuffLaNum (count);
					STATE = 0;
				}
				else if (ch == '|')
				{
					sym = st->insert ("|", lg.PIPE);
					currentCol += (1 + ignore);
					if (_PRINT_LEXICAL_ANALYSIS_ == 1)
						fillBuffLaNum (count);
					STATE = 0;
				}
				else
				{
					ignore++;
					STATE = 0;
					//
					// ERROR
					//
					if (strcmp (lastToken, "endpackage"))
					{
						BAD_CHAR_ERROR = 1;
						err.normal ("Lexer->getNextToken: Bad character", LEXICAL_ERROR, currentLine, currentCol);
					}
				}
				current = lookahead;
				break;
			//
			// comment | -- | > | -
			// TOKEN = NONE
			// TOKEN = AOP
			//
			case 1:
				if (ch == '-')
					STATE = 2;
				else if (ch == '>')
					STATE = 14;
				else
				{
					sym = st->insert ("-", lg.MINUS);
					currentCol += (1 + ignore);
					count--;
					lookahead--;
					STATE = 0;
					if (_PRINT_LEXICAL_ANALYSIS_ == 1)
						fillBuffLaNum (count);
				}
				break;
			case 2:
				if (ch == CR || ch == LF)
				{
					ignore = 0;
					currentCol = 1;
					STATE = 0;
					if (_PRINT_LEXICAL_ANALYSIS_ == 1)
					{
						//
						// Write COMMENT to Lexical Analysis output file
						//
						if (fileLA != NULL)
						{
							buffN = count - buffN;
							lexeme = new char [buffN + 1];
							strncpy (lexeme, current, buffN);
							lexeme[buffN] = ':';
							lexeme[buffN + 1] = END_OF_STRING;
#ifdef _PRINT_TOKENS_
							fprintf (stdout, "COMMENT: %s:\n", lexeme);
#endif
							fprintf (fileLA, "COMMENT:%s", lexeme);
							lexeme = NULL;
						}
						fillBuffLaNum (count);
					}
				}
				else
				{
					STATE = 2;
				}
				break;
			//
			// TOKEN = ID, LOP, CTID, ATPTRE, TUPLE, TUPLET, CONTEXT, INIT, TRUE, FALSE
			// TOKEN = DRIVE, INV, DEF, LET, ACTION, ITERATE, IF, THEN, ELSE, ENDIF
			// PUBLIC, PRIVATE, PROTECTED, VIS
			//
			case 3:
				if (isalpha (ch))
					STATE = 3;
				else if (isdigit (ch))
					STATE = 3;
				//
				// check for allowed characters, see the grammar for list of allowed characters
				//
				else if (ch == '!' || ch == SINGLE_QUOTE || ch == '$' || ch == '%')
					STATE = 3;
				else if (ch == '#' || ch == '&' || ch == '?' || ch == '~' || ch == '_')
					STATE = 3;
				//
				// can check for SPACE here
				//
				else
				{
					buffN = count - buffN;
					lexeme = new char [buffN + 1];
					strncpy (lexeme, current, buffN);
					lexeme[buffN] = END_OF_STRING;
					//
					// checking for keywords and inserting them with proper TOKEN
					//
					int tok;
					//
					// checking for TRUE ::= 'true'
					//
					if (strcmp (lexeme, "true") == 0)
						tok = lg.TRUE;
					//
					// checking for FALSE ::= 'false
					//
					else if (strcmp (lexeme, "false") == 0)
						tok = lg.FALSE;
					//
					// checking for LOP ::= 'and' | 'or' | 'xor' | 'implies'
					//
					else if (strcmp (lexeme, "and") == 0)
						tok = lg.LOP;
					else if (strcmp (lexeme, "or") == 0)
						tok = lg.LOP;
					else if (strcmp (lexeme, "xor") == 0)
						tok = lg.LOP;
					else if (strcmp (lexeme, "implies") == 0)
						tok = lg.LOP;
					//
					// checking for NOT ::= 'not'
					//
					else if (strcmp (lexeme, "not") == 0)
						tok = lg.NOT;
					//
					// checking for CTID ::= 'Set' | 'Bag' | 'Sequence' | 'Collection'
					//                             | 'OrderedSet' | 'Dag'
					//
					else if (strcmp (lexeme, "Set") == 0)
						tok = lg.CTID;
					else if (strcmp (lexeme, "Bag") == 0)
						tok = lg.CTID;
					else if (strcmp (lexeme, "Sequence") == 0)
						tok = lg.CTID;
					else if (strcmp (lexeme, "Collection") == 0)
						tok = lg.CTID;
					else if (strcmp (lexeme, "OrderedSet") == 0)
						tok = lg.CTID;
					else if (strcmp (lexeme, "Dag") == 0)
						tok = lg.CTID;
					//
					// checking for TUPLE ::= 'Tuple'
					//
					else if (strcmp (lexeme, "Tuple") == 0)
						tok = lg.TUPLE;
					//
					// checking for TUPLET ::= 'TupleType'
					//
					else if (strcmp (lexeme, "TupleType") == 0)
						tok = lg.TUPLET;
					//
					// checking for PACKAGE ::= 'package'
					//
					else if (strcmp (lexeme, "package") == 0)
						tok = lg.PACKAGE;
					//
					// checking for ENDPACKAGE ::= 'endpackage'
					//
					else if (strcmp (lexeme, "endpackage") == 0)
					{
						lastToken[0] = END_OF_STRING;
						sprintf (lastToken, "endpackage");
						tok = lg.ENDPACKAGE;
					}
					//
					// checking for CONTEXT ::= 'context'
					//
					else if (strcmp (lexeme, "context") == 0)
						tok = lg.CONTEXT;
					//
					// checking for INIT ::= 'init'
					//
					else if (strcmp (lexeme, "init") == 0)
						tok = lg.INIT;
					//
					// checking for DRIVE ::= 'drive'
					//
					else if (strcmp (lexeme, "drive") == 0)
						tok = lg.DRIVE;
					//
					// checking for INV ::= 'inv'
					//
					else if (strcmp (lexeme, "inv") == 0)
						tok = lg.INV;
					//
					// checking for DEF ::= 'def'
					//
					else if (strcmp (lexeme, "def") == 0)
						tok = lg.DEF;
					//
					// checking for LET ::= 'let'
					//
					else if (strcmp (lexeme, "let") == 0)
						tok = lg.LET;
					//
					// checking for PRE ::= 'pre'
					//
					else if (strcmp (lexeme, "pre") == 0)
						tok = lg.PRE;
					//
					// checking for ATPRE ::= '@pre'
					//
					else if (strcmp (lexeme, "@pre") == 0)
						tok = lg.ATPRE;
					//
					// checking for POST ::= 'post'
					//
					else if (strcmp (lexeme, "post") == 0)
						tok = lg.POST;
					//
					// checking for BODY ::= 'body'
					//
					else if (strcmp (lexeme, "body") == 0)
						tok = lg.BODY;
					//
					// checking for ACTION ::= 'action'
					//
					else if (strcmp (lexeme, "action") == 0)
						tok = lg.ACTION;
					//
					// checking for ITERATE ::= 'iterate'
					//
					else if (strcmp (lexeme, "iterate") == 0)
						tok = lg.ITERATE;
					//
					// checking for IF ::= 'if'
					//
					else if (strcmp (lexeme, "if") == 0)
						tok = lg.IF;
					//
					// checking for THEN ::= 'then'
					//
					else if (strcmp (lexeme, "then") == 0)
						tok = lg.THEN;
					//
					// checking for ELSE ::= 'else'
					//
					else if (strcmp (lexeme, "else") == 0)
						tok = lg.ELSE;
					//
					// checking for ENDIF ::= 'endif'
					//
					else if (strcmp (lexeme, "endif") == 0)
						tok = lg.ENDIF;
					//
					// checking for TO ::= 'to'
					//
					else if (strcmp (lexeme, "to") == 0)
						tok = lg.TO;
					//
					// checking for OF ::= 'of'
					//
					else if (strcmp (lexeme, "of") == 0)
						tok = lg.OF;
					//
					// checking for VIS ::= 'public'
					//
					else if (strcmp (lexeme, "public") == 0)
						tok = lg.VIS;
					//
					// checking for VIS ::= 'private'
					//
					else if (strcmp (lexeme, "private") == 0)
						tok = lg.VIS;
					//
					// checking for VIS ::= 'protected'
					//
					else if (strcmp (lexeme, "protected") == 0)
						tok = lg.VIS;
					else
						tok = lg.ID;
					sym = st->insert (lexeme, tok);
					currentCol += (buffN + ignore);
					count--;
					lookahead--;
					STATE = 0;
					if (_PRINT_LEXICAL_ANALYSIS_ == 1)
						fillBuffLaNum (count);
				}
				break;
			//
			// num
			// TOKEN = NUM
			//
			case 4:
				if (isdigit (ch))
					STATE = 4;
				else if (ch == '.')
					STATE = 5;
				else if (ch == 'e' || ch == 'E')
					STATE = 7;
				else
					STATE = 10;
				break;
			case 5:
				if (isdigit (ch))
					STATE = 6;
				else
				{
					//
					// ERROR
					//
					err.warning ("Lexer->getNextToken: Incorrect number", LEXICAL_ERROR, currentLine, currentCol);
					STATE = 0;
				}
				break;
			case 6:
				if (isdigit (ch))
					STATE = 6;
				else if (ch == 'e' || ch == 'E')
					STATE = 7;
				else if (ch == SPACE)
					STATE = 6;
				else
					STATE = 10;
				break;
			case 7:
				if (ch == '+' || ch == '-')
					STATE = 8;
				else if (isdigit (ch))
					STATE = 9;
				else if (ch == SPACE)
					STATE = 7;
				else
				{
					//
					// ERROR
					//
					err.warning ("Lexer->getNextToken: Incorrect number", LEXICAL_ERROR, currentLine, currentCol);
					STATE = 0;
				}
				break;
			case 8:
				if (isdigit (ch))
					STATE = 9;
				else if (ch == SPACE)
					STATE = 8;
				else
				{
					//
					// ERROR
					//
					err.warning ("Lexer->getNextToken: Incorrect number", LEXICAL_ERROR, currentLine, currentCol);
					STATE = 0;
				}
				break;
			case 9:
				if (isdigit (ch))
					STATE = 9;
				else
					STATE = 10;
				break;
			case 10:
				count--;
				lookahead--;
				buffN = count - buffN;
				lexeme = new char [buffN + 1];
				strncpy (lexeme, current, buffN);
				lexeme[buffN] = END_OF_STRING;
				sym = st->insert (lexeme, lg.NUM);
				currentCol += (buffN + ignore);
				count--;
				lookahead--;
				STATE = 0;
				if (_PRINT_LEXICAL_ANALYSIS_ == 1)
					fillBuffLaNum (count);
				break;
			//
			// colon / dcolon
			// TOKEN = COLON / DCOLON
			//
			case 11:
				if (ch == ':')
				{
					sym = st->insert ("DCOLON", lg.DCOLON);
					currentCol += (2 + ignore);
					if (_PRINT_LEXICAL_ANALYSIS_ == 1)
						fillBuffLaNum (count);
				}
				else
				{
					sym = st->insert ("COLON", lg.COLON);
					currentCol += (1 + ignore);
					count--;
					lookahead--;
					if (_PRINT_LEXICAL_ANALYSIS_ == 1)
						fillBuffLaNum (count);
				}
				STATE = 0;
				break;
			//
			// < | <= | <>
			// TOKEN =ROP
			//
			case 12:
				if (ch == '=')
				{
					sym = st->insert ("<=", lg.ROP);
					currentCol += (2 + ignore);
				}
				else if (ch == '>')
				{
					sym = st->insert ("<>", lg.ROP);
					currentCol += (2 + ignore);
				}
				else
				{
					sym = st->insert ("<", lg.ROP);
					currentCol += (1 + ignore);
					count--;
					lookahead--;
				}
				STATE = 0;
				if (_PRINT_LEXICAL_ANALYSIS_ == 1)
					fillBuffLaNum (count);
				break;
			//
			// > | >=
			// TOKEN = ROP
			//
			case 13:
				if (ch == '=')
				{
					sym = st->insert (">=", lg.ROP);
					currentCol += (2 + ignore);
				}
				else
				{
					sym = st->insert (">", lg.ROP);
					currentCol += (1 + ignore);
					count--;
					lookahead--;
				}
				STATE = 0;
				if (_PRINT_LEXICAL_ANALYSIS_ == 1)
					fillBuffLaNum (count);
				break;
			//
			// arrow
			// TOKEN = DAOP
			//
			case 14:
				sym = st->insert ("->", lg.DAOP);
				currentCol += (1 + ignore);
				count--;
				lookahead--;
				STATE = 0;
				if (_PRINT_LEXICAL_ANALYSIS_ == 1)
					fillBuffLaNum (count);
				break;
			//
			// ^ | ^^
			// TOKEN = MSGOP
			//
			case 15:
				if (ch == '^')
				{
					sym = st->insert ("^^", lg.MSGOP);
					currentCol += (2 + ignore);
				}
				else
				{
					sym = st->insert ("^", lg.MSGOP);
					currentCol += (1 + ignore);
					count--;
					lookahead--;
				}
				STATE = 0;
				if (_PRINT_LEXICAL_ANALYSIS_ == 1)
					fillBuffLaNum (count);
				break;
			//
			// . | ..
			// TOKEN = DAOP
			// TOKEN = RANGEOP
			//
			case 16:
				if (ch == '.')
				{
					sym = st->insert ("..", lg.RANGEOP);
					currentCol += (2 + ignore);
				}
				else
				{
					sym = st->insert (".", lg.DAOP);
					currentCol += (1 + ignore);
					count--;
					lookahead--;
				}
				STATE = 0;
				if (_PRINT_LEXICAL_ANALYSIS_ == 1)
					fillBuffLaNum (count);
				break;
			//
			// string ::= "characters"
			// TOKEN = STRING
			//
			case 17:
				if (ch == CR || ch == LF)
				{
					ignore = 0;
					currentCol = 1;
					STATE = 0;
				}
				else if (ch == '"')
				{
					buffN = count - buffN - 1;
					lexeme = new char [buffN];
					strncpy (lexeme, current+1, buffN);
					lexeme[buffN] = END_OF_STRING;
					sym = st->insert (lexeme, lg.STRING);
					currentCol += (buffN + ignore);
					STATE = 0;
					if (_PRINT_LEXICAL_ANALYSIS_ == 1)
						fillBuffLaNum (count);
				}
				//
				// CHARACTERS = ASCII character set from 32 - 126
				//
				else if (ch >= 32 || ch <= 126)
					STATE = 17;
				else
					BAD_CHAR_ERROR = 1;
				break;
			default:
				break;
		}
		count++;
		lookahead++;
		ch = *lookahead;
		if (BAD_CHAR_ERROR)
			break;
		else if (sym != NULL)
		{
			if (_PRINT_LEXICAL_ANALYSIS_ == 1)
			{
				//
				// Write Lexical Analysis output to file
				//
				if (fileLA != NULL)
				{
					char *token;
					token = lg.getToken (sym->token);
					fprintf (fileLA, "%s:%s:", token, sym->lexeme);
					fflush (fileLA);
				}
			}
			return sym;
		}
	}

	return NULL;
}

/**
|
| Fills the buffer to write to fileLANum
|
 ----------------------------------------------------------------------------*/
void Lexer :: fillBuffLaNum (int count)
{
	char tmp[8];
//fprintf (stderr, "%d:%c",  count, *lookahead);
	sprintf (tmp, "%d:",  count);
	strcat (laoutNum, tmp);
}

/**
|
| Returns current column number currently processed by the Lexical Analyser
|
 ----------------------------------------------------------------------------*/
int Lexer :: getCurrentCol (void)
{
	return currentCol;
}

/**
|
| Returns current line number currently processed by the Lexical Analyser
|
 ----------------------------------------------------------------------------*/
int Lexer :: getCurrentLine (void)
{
	return currentLine;
}

/**
|
| Returns pointer to the Symbol Table
| @Return pointer to the Symbol Table
|
 ----------------------------------------------------------------------------*/
Symtable * Lexer :: getST (void)
{
	return st;
}

/**
|
| Returns the size of the file stream.
| Keeps the file pointer at the current position
|
 ----------------------------------------------------------------------------*/
long Lexer :: fileSize (FILE *stream)
{
	long curpos, length;

	curpos = ftell (stream);
	fseek (stream, 0L, SEEK_END);
	length = ftell (stream);
	fseek (stream, curpos, SEEK_SET);

	return length;
}
