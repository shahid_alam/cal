/* ============================================================================
|
|   Filename:    error.cpp
|   Dated:       31 May, 2006
|   By:          Shahid Alam (shalam@ieee.org
|
|   Description: 
|
|   TO DO:       See below
|
 ----------------------------------------------------------------------------*/

#include "include/error.h"

/**
|
| Constructor
|
 ----------------------------------------------------------------------------*/
Error :: Error ()
{
}

/**
|
| Destructor
|
 ----------------------------------------------------------------------------*/
Error :: ~Error ()
{
}

/**
|
| FATAL ERROR
| Compiler Should Exit using the main
| After deleting all the allocated memory
|
 ----------------------------------------------------------------------------*/
void Error :: fatal (char *errorStr, int errorNum)
{
#ifdef _DEBUG_
		fprintf (stderr, "<Fatal_Error %s />", errorStr);
#endif
		fprintf (stderr, "<%s />\n", getErrorType(errorNum));
}

/**
|
| NORMAL ERROR
|
 ----------------------------------------------------------------------------*/
void Error :: normal (char *errorStr, int errorNum, int currentLine, int currentCol)
{
#ifdef _DEBUG_
		fprintf (stderr, "<Error %s />", errorStr);
#endif
		fprintf (stderr, "<%s ", getErrorType(errorNum));
		if (currentLine != 0)
			fprintf (stderr, " line='%d', col='%d' />\n", currentLine, currentCol);
}

/**
|
| WARNING
|
 ----------------------------------------------------------------------------*/
void Error :: warning (char *errorStr, int errorNum, int currentLine, int currentCol)
{
#ifdef _DEBUG_
		fprintf (stderr, "<Warning ");
		fprintf (stderr, "%s />", errorStr);
		fprintf (stderr, "<%s >", getErrorType(errorNum));
		if (currentLine != 0)
			fprintf (stderr, " line='%d', col='%d' />\n", currentLine, currentCol);
#endif
}

/**
|
| Get Error String according to the type of error
|
 ----------------------------------------------------------------------------*/
char * Error :: getErrorType (int errorNumber)
{
	switch (errorNumber)
	{
		case FILE_NOT_OPEN:
			return ("FileNotOpen");
		case FILE_NOT_READ:
			return ("FileNotRead");
		case LEXICAL_ERROR:
			return ("LexicalError");
		case SYNTAX_ERROR:
			return ("SyntaxError");
		case SEMANTIC_ERROR:
			return ("SemanticError");
		default:
			return ("");
	}

	return "";
}
