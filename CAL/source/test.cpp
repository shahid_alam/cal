/* ============================================================================
|
|   Filename:    test.cpp
|   Dated:       21 August, 2006
|   By:          Shahid Alam (shalam@ieee.org
|
|   Description: For writing any test
|
|   TO DO:       
|
 ----------------------------------------------------------------------------*/

#include "include/ast.h"

/**
|
| TEMP
| TEMP
|
| TO BE DELETED
|
| TEMP
| TEMP
|
 ----------------------------------------------------------------------------*/
int testAST (void)
{
	AST *ast = new AST ();

		ast->insertRoot ("Root");

		ast->addNode ("0", "C1");
			ast->addNode ("1", "C11");
				ast->addNode ("11", "C111");
					ast->addNode ("111", "C1111");
					ast->addNode ("111", "C1112");
					ast->addNode ("111", "C1113");
				ast->addNode ("11", "C112");
				ast->addNode ("11", "C113");
			ast->addNode ("1", "C12");
				ast->addNode ("12", "C121");
				ast->addNode ("12", "C122");
				ast->addNode ("12", "C123");
			ast->addNode ("1", "C13");
				ast->addNode ("13", "C131");
				ast->addNode ("13", "C132");
					ast->addNode ("132", "C1321");
					ast->addNode ("132", "C1322");
						ast->addNode ("1322", "C13221");
						ast->addNode ("1322", "C13222");
						ast->addNode ("1322", "C13223");
					ast->addNode ("132", "C1323");
				ast->addNode ("13", "C133");

		ast->addNode ("0", "C2");
			ast->addNode ("2", "C21");
				ast->addNode ("21", "C211");
					ast->addNode ("211", "C2111");
					ast->addNode ("211", "C2112");
					ast->addNode ("211", "C2113");
				ast->addNode ("21", "C212");
				ast->addNode ("21", "C213");
			ast->addNode ("2", "C22");
				ast->addNode ("22", "C221");
				ast->addNode ("22", "C222");
				ast->addNode ("22", "C223");
			ast->addNode ("2", "C23");
				ast->addNode ("23", "C231");
				ast->addNode ("23", "C232");
					ast->addNode ("232", "C2321");
					ast->addNode ("232", "C2322");
						ast->addNode ("2322", "C23221");
						ast->addNode ("2322", "C23222");
						ast->addNode ("2322", "C23223");
					ast->addNode ("232", "C2323");
				ast->addNode ("23", "C233");

		ast->addNode ("0", "C3");
			ast->addNode ("3", "C31");
				ast->addNode ("31", "C311");
					ast->addNode ("311", "C3111");
					ast->addNode ("311", "C3112");
					ast->addNode ("311", "C3113");
				ast->addNode ("31", "C312");
				ast->addNode ("31", "C313");
			ast->addNode ("3", "C32");
				ast->addNode ("32", "C321");
				ast->addNode ("32", "C322");
				ast->addNode ("32", "C323");
			ast->addNode ("3", "C33");
				ast->addNode ("33", "C331");
				ast->addNode ("33", "C332");
					ast->addNode ("332", "C3321");
					ast->addNode ("332", "C3322");
						ast->addNode ("3322", "C33221");
						ast->addNode ("3322", "C33222");
						ast->addNode ("3322", "C33223");
					ast->addNode ("332", "C3323");
				ast->addNode ("33", "C333");

		ast->addNode ("0", "C4");
			ast->addNode ("4", "C41");
				ast->addNode ("41", "C411");
					ast->addNode ("411", "C4111");
					ast->addNode ("411", "C4112");
					ast->addNode ("411", "C4113");
				ast->addNode ("41", "C412");
				ast->addNode ("41", "C413");
			ast->addNode ("4", "C42");
				ast->addNode ("42", "C421");
				ast->addNode ("42", "C422");
				ast->addNode ("42", "C423");
			ast->addNode ("4", "C43");
				ast->addNode ("43", "C431");
				ast->addNode ("43", "C432");
					ast->addNode ("432", "C4321");
					ast->addNode ("432", "C4322");
						ast->addNode ("4322", "C43221");
						ast->addNode ("4322", "C43222");
						ast->addNode ("4322", "C43223");
					ast->addNode ("432", "42323");
				ast->addNode ("43", "4233");


		fprintf (stdout, "<AST>\n");
		ast->print (NULL);
		fprintf (stdout, "</AST>\n");

		fprintf (stdout, "\n\n<Removing ID>4</Removing ID>\n\n");
		ast->removeNode ("4");
		fprintf (stdout, "<AST>\n");
		ast->print (NULL);
		fprintf (stdout, "</AST>\n");

		fprintf (stdout, "\n\n<Removing ID>22</Removing ID>\n");
		ast->removeNode ("22");
		fprintf (stdout, "<Removing ID>23222</Removing ID>\n");
		ast->removeNode ("23222");
		fprintf (stdout, "<Removing ID>23223</Removing ID>\n");
		ast->removeNode ("23223");
		fprintf (stdout, "<Removing ID>23</Removing ID>\n\n");
		ast->removeNode ("23");
		fprintf (stdout, "<AST>\n");
		ast->print (NULL);
		fprintf (stdout, "</AST>\n");


		fprintf (stdout, "\n\n<Removing ID>2</Removing ID>\n\n");
		ast->removeNode ("2");
		fprintf (stdout, "<AST>\n");
		ast->print (NULL);
		fprintf (stdout, "</AST>\n");

		fprintf (stdout, "\n\n<Removing ID>3</Removing ID>\n\n");
		ast->removeNode ("3");
		fprintf (stdout, "<AST>\n");
		ast->print (NULL);
		fprintf (stdout, "</AST>\n");

	return 0;
}
