/* ============================================================================
|
|   Filename:    oclstmt.cpp
|   Dated:       28 August, 2006
|   By:          Shahid Alam (shalam@ieee.org
|
|   Description: 
|
|   TO DO:       See below
|
 ----------------------------------------------------------------------------*/

#include "include/oclstmt.h"

/**
*
* Constructor
* @param lrP pointer to the LexicalAnalysis class to getNextToken from Lexer
* @param astP pointer to the AST class for Adding nodes to the current AST being built
*
*/
OclStmt :: OclStmt (Lexer *lrP, AST *astP, FILE *filePAP)
{
	lr = lrP;
	ast = astP;
	filePA = filePAP;
}

/**
*
* Destructor
*
 ----------------------------------------------------------------------------*/
OclStmt :: ~OclStmt ()
{
}

/**
*
* Check for the correctness of oclStatement
* OclStmt	::= LogicalStmt ( ( '^^' | '^' ) MessageStmt )? ;
*
* Write Syntax / Semantic Analysis output to file
* @param parentID node pointer of the parent node in the AST
* @param currSymP pointer to the current Symbol being processed
* Returns 0 if no oclStatement exists
*
 ----------------------------------------------------------------------------*/
int OclStmt :: oclStatement (TreeNode *parentID, SYMBOL *currSymP)
{
	int i, brackets = 0;
	int exist = 0;

	currSym = currSymP;

	//
	// Checking for opening brackets
	//
	getNextTokenWrapper ();
	while (currSym != NULL && currSym->token == lg.LBRKT1)
	{
		getNextTokenWrapper ();
		brackets++;
	}

	//
	// 1 if logicalStatement exists.
	// 2 if primaryStatement exists.
	// 0 if neither exists.
	//
	exist = logicalStatement (parentID);
	if (exist == 1 && currSym != NULL && currSym->token == lg.MSGOP)
	{
		prevID = ast->addNode (parentID, "MessageStmt");
		parentID = prevID;
		getNextTokenWrapper ();
		exist = messageStatement (parentID);

		if (exist <= 0)
			ast->removeNode (parentID);
	}

	//
	// Checking for closing brackets
	//
	for (i = 0; i < brackets; i++)
	{
		if (currSym == NULL || currSym->token != lg.RBRKT1)
			break;
		getNextTokenWrapper ();
	}
	if (i < brackets)
		err.warning ("Bracket Missing\n", SYNTAX_ERROR, lr->getCurrentLine (), lr->getCurrentCol ());

	return exist;
}

/**
*
* Checks for the actionStatement.
*
* ActionStmt                  ::= createStmt
*                                | deleteStmt
*                                | addAttrStmt
*                                | createLinkStmt
*                                | deleteLinkStmt ;
*
* createStmt                  ::= 'create' operation
*                                | 'create' operation 'to' pathName ;
*
* deleteStmt                  ::= 'delete' id
*                                | 'delete' id 'of' pathName ;
*
* addAttrStmt                 ::= 'addattr' id 'to' pathName ;
*
* createLinkStmt              ::= 'link' pathName id pathName ;
*
* deleteLinkStmt              ::= 'unlink' pathName pathName ;
*
* operation                   ::= visibility* id '(' parameters* ')' ;
*
* visibility                  ::= 'public' | 'private' | 'protected' ;
*
* Write Syntax / Semantic Analysis output to file.
* @param parentID node pointer of the parent node in the AST
* @param currSymP pointer to the current Symbol being processed
* @return 0 if actionStatement statement doesn't exist
* or bad actionStatement statement
*
 ----------------------------------------------------------------------------*/
int OclStmt :: actionStatement (TreeNode *parentID, SYMBOL *currSymP)
{
	int exist = 0;

	currSym = currSymP;
	getNextTokenWrapper ();

	if (!strcmp (currSym->lexeme, "create"))
	{
		prevID = ast->addNode (parentID, currSym->lexeme);
		parentID = prevID;

		getNextTokenWrapper ();
		if (exist == 0 && currSym != NULL && currSym->token == lg.VIS)
		{
			prevID = ast->addNode (parentID, currSym->lexeme);
			getNextTokenWrapper ();
		}

		if (currSym != NULL && currSym->token == lg.ID)
		{
			prevID = ast->addNode (parentID, currSym->lexeme);
	
			getNextTokenWrapper ();
			if (currSym != NULL && currSym->token == lg.LBRKT1)
			{
				getNextTokenWrapper ();
				prevID = ast->addNode (parentID, "args");
				exist = variableDeclarations (prevID, currSym);
				if (exist == 0)
					ast->removeNode (prevID);
				if (currSym != NULL && currSym->token == lg.RBRKT1)
				{
					getNextTokenWrapper ();
					if (currSym != NULL && currSym->token == lg.TO)
					{
						prevID = ast->addNode (parentID, currSym->lexeme);
						getNextTokenWrapper ();
						exist = pathName (prevID, currSym);
					}
					else
						exist = 1;
				}
				else
					exist = 0;
			}
		}
	}
	else if (!strcmp (currSym->lexeme, "delete"))
	{
		prevID = ast->addNode (parentID, currSym->lexeme);
		parentID = prevID;

		getNextTokenWrapper ();
		if (currSym != NULL && currSym->token == lg.ID)
		{
			prevID = ast->addNode (parentID, currSym->lexeme);
	
			getNextTokenWrapper ();
			if (currSym != NULL && currSym->token == lg.OF)
			{
				prevID = ast->addNode (parentID, currSym->lexeme);
				getNextTokenWrapper ();
				exist = pathName (prevID, currSym);
			}
			else
				exist = 1;
		}
	}
	else if (!strcmp (currSym->lexeme, "addattr"))
	{
		prevID = ast->addNode (parentID, currSym->lexeme);
		parentID = prevID;

		getNextTokenWrapper ();
		if (currSym != NULL && currSym->token == lg.ID)
		{
			prevID = ast->addNode (parentID, currSym->lexeme);

			getNextTokenWrapper ();
			if (currSym != NULL && currSym->token == lg.TO)
			{
				prevID = ast->addNode (parentID, currSym->lexeme);
				getNextTokenWrapper ();
				exist = pathName (prevID, currSym);
			}
			else
				exist = 1;
		}
	}
	else if (!strcmp (currSym->lexeme, "link"))
	{
		prevID = ast->addNode (parentID, currSym->lexeme);
		parentID = prevID;

		getNextTokenWrapper ();
		prevID = ast->addNode (parentID, "source");
		exist = pathName (prevID, currSym);
		if (exist > 0 && currSym != NULL && currSym->token == lg.ID)
		{
			prevID = ast->addNode (parentID, "association");
			prevID = ast->addNode (prevID, currSym->lexeme);

			getNextTokenWrapper ();
			prevID = ast->addNode (parentID, "destination");
			exist = pathName (prevID, currSym);
			if (exist <= 0)
				ast->removeNode (prevID);
		}
		else
			ast->removeNode (prevID);
	}
	else if (!strcmp (currSym->lexeme, "unlink"))
	{
		prevID = ast->addNode (parentID, currSym->lexeme);
		parentID = prevID;

		getNextTokenWrapper ();
		prevID = ast->addNode (parentID, "source");
		exist = pathName (prevID, currSym);
		if (exist > 0)
		{
			prevID = ast->addNode (parentID, "destination");
			exist = pathName (prevID, currSym);
			if (exist <= 0)
				ast->removeNode (prevID);
		}
		else
			ast->removeNode (prevID);
	}

//fprintf (stderr, "actionStatement: %s EXIST: %d\n", currSym->lexeme, exist);
	return exist;
}

/**
*
* Checks for the logicalStatement.
* Need to call getNextTOkenWtapper () before calling this function.
*
* @see LG::TOKENS
*
* Write Syntax / Semantic Analysis output to file
* @param parentID node pointer of the parent node in the AST
* @return
* 1 if logicalStatement statement exists.
* 2 if primaryStatement statement exists.
* 0 if neither exists.
*
 ----------------------------------------------------------------------------*/
int OclStmt :: logicalStatement (TreeNode *parentID)
{
	int exist = 0;

//fprintf (stderr, "--- logicalStatement: %s EXIST: %d\n", currSym->lexeme, exist);
	if (relationalStatement (parentID))
	{
		exist = 1;
		while (currSym != NULL)
		{
			if (currSym != NULL && currSym->token == lg.LOP)
			{
				prevID = ast->addNode (parentID, currSym->lexeme);
				getNextTokenWrapper ();
				exist = relationalStatement (parentID);
				if (exist == 0)
					break;
			}
			else
				break;
		}
	}

//fprintf (stderr, "logicalStatement: %s EXIST: %d\n", currSym->lexeme, exist);
	return exist;
}

/**
*
* Checks for the relationalStatement.
* Need to call getNextTOkenWtapper () before calling this function.
*
* Write Syntax / Semantic Analysis output to file
* @param parentID node pointer of the parent node in the AST
* @returns 1 if relationalStatement exists
*
 ----------------------------------------------------------------------------*/
int OclStmt :: relationalStatement (TreeNode *parentID)
{
	int exist = 0;

//fprintf (stderr, "--- relationalStatement: %s EXIST: %d\n", currSym->lexeme, exist);
	if (additiveStatement (parentID))
	{
		exist = 1;
		if (currSym != NULL && currSym->token == lg.ROP)
		{
			if (!strcmp (currSym->lexeme, "<"))
				prevID = ast->addNode (parentID, "LESS_THAN");
			else if (!strcmp (currSym->lexeme, "<="))
				prevID = ast->addNode (parentID, "LESS_THAN_EQUAL_TO");
			else if (!strcmp (currSym->lexeme, ">"))
				prevID = ast->addNode (parentID, "GREATER_THAN");
			else if (!strcmp (currSym->lexeme, ">="))
				prevID = ast->addNode (parentID, "GREATER_THAN_EQUAL_TO");
			else if (!strcmp (currSym->lexeme, "<>"))
				prevID = ast->addNode (parentID, "NOT_EQUAL_TO");
			else if (!strcmp (currSym->lexeme, "="))
				prevID = ast->addNode (parentID, "EQUAL_TO");

			getNextTokenWrapper ();
			exist = additiveStatement (parentID);
		}
	}

//fprintf (stderr, "relationalStatement: %s EXIST: %d\n", currSym->lexeme, exist);
	return exist;
}

/**
*
* Checks for the additiveStatement.
* Need to call getNextTOkenWtapper () before calling this function.
*
* Write Syntax / Semantic Analysis output to file
* @param parentID node pointer of the parent node in the AST
* @returns 1 if additiveStatement exists
*
 ----------------------------------------------------------------------------*/
int OclStmt :: additiveStatement (TreeNode *parentID)
{
	int exist = 0;

//fprintf (stderr, "--- additiveStatement: %s EXIST: %d\n", currSym->lexeme, exist);
	if (multiplicativeStatement (parentID))
	{
		exist = 1;
		while (currSym != NULL)
		{
			if (currSym != NULL && (currSym->token == lg.PLUS || currSym->token == lg.MINUS))
			{
				prevID = ast->addNode (parentID, currSym->lexeme);
				getNextTokenWrapper ();
				exist = multiplicativeStatement (parentID);
				if (exist == 0)
					break;
			}
			else
				break;
		}
	}

//fprintf (stderr, "additiveStatement: %s EXIST: %d\n", currSym->lexeme, exist);
	return exist;
}

/**
*
* Checks for the multiplicativeStatement.
* Need to call getNextTOkenWtapper () before calling this function.
*
* Write Syntax / Semantic Analysis output to file
* @param parentID node pointer of the parent node in the AST
* @returns 1 if multiplicativeStatement exists
*
 ----------------------------------------------------------------------------*/
int OclStmt :: multiplicativeStatement (TreeNode *parentID)
{
	int exist = 0;

//fprintf (stderr, "--- multiplicativeStatement: %s EXIST: %d\n", currSym->lexeme, exist);
	if (unaryStatement (parentID))
	{
		exist = 1;
		while (currSym != NULL)
		{
			if (currSym != NULL && (currSym->token == lg.MUL || currSym->token == lg.DIV))
			{
				prevID = ast->addNode (parentID, currSym->lexeme);
				getNextTokenWrapper ();
				exist = unaryStatement (parentID);
				if (exist == 0)
					break;
			}
			else
				break;
		}
	}

//fprintf (stderr, "multiplicativeStatement: %s EXIST: %d\n", currSym->lexeme, exist);
	return exist;
}

/**
*
* Checks for the unaryStatement.
* Need to call getNextTOkenWtapper () before calling this function.
*
* Write Syntax / Semantic Analysis output to file
* @param parentID node pointer of the parent node in the AST
* @returns 1 if unaryStatement exists
*
 ----------------------------------------------------------------------------*/
int OclStmt :: unaryStatement (TreeNode *parentID)
{
	int exist = 0;

//fprintf (stderr, "--- unaryStatement: %s EXIST: %d\n", currSym->lexeme, exist);
	if (currSym != NULL && (currSym->token == lg.MINUS || currSym->token == lg.NOT))
	{
		prevID = ast->addNode (parentID, currSym->lexeme);

		getNextTokenWrapper ();
		exist = primaryStatement (parentID);

		while (exist >= 1 && currSym != NULL)
		{
			if (currSym->token == lg.DAOP)
			{
				getNextTokenWrapper ();
				exist = propertyCallStatement (prevID);
			}
			else
				break;
		}
	}
	//
	// getNextTokenWrapper () already called by the calling function
	//
	else if (primaryStatement (parentID))
	{
		exist = 1;
		while (exist >= 1 && currSym != NULL)
		{
			if (currSym->token == lg.DAOP)
			{
				getNextTokenWrapper ();
				exist = propertyCallStatement (prevID);
			}
			else
				break;
		}
	}

//fprintf (stderr, "unaryStatement: %s EXIST: %d\n", currSym->lexeme, exist);
	return exist;
}

/**
*
* Check for the correctness of oclStatement
* OclStmt	::= LetStmt
*				| IfStmt
*				| LiteralStmt
*				| MessageStmt
*				| PropertyCallStmt ;
*
* Write Syntax / Semantic Analysis output to file
* @param parentID node pointer of the parent node in the AST
* @param currSymP pointer to the current Symbol being processed
* Returns 1 if primaryStatement exists
*
 ----------------------------------------------------------------------------*/
int OclStmt :: primaryStatement (TreeNode *parentID)
{
	int exist = 0;

	if (letStatement (parentID))
		exist = 1;
	else if (ifStatement (parentID))
		exist = 1;
	else if (propertyCallStatement (parentID))
		exist = 1;
	else if (literalStatement (parentID))
		exist = 1;

//fprintf (stderr, "primaryStatement: %s EXIST: %d\n", currSym->lexeme, exist);
	return exist;
}

/**
*
* Checks for the letStatement.
* Need to call getNextTOkenWtapper () before calling this function.
*
* LetStmt          ::= 'let' variableDeclaration LetStmtSub ;
*
* LetStmtSub       ::= ',' variableDeclaration LetStmtSub
*                      | 'in' OclStmt ;
*
* Write Syntax / Semantic Analysis output to file
* @param parentID node pointer of the parent node in the AST
* @return 0 if letStatement statement doesn't exist
* or bad letStatement statement
*
 ----------------------------------------------------------------------------*/
int OclStmt :: letStatement (TreeNode *parentID)
{
	int exist = 0;

	if (currSym != NULL && currSym->token == lg.LET)
	{
		prevID = ast->addNode (parentID, currSym->lexeme);
		parentID = prevID;

		prevID = ast->addNode (parentID, "args");
		TreeNode * pID = prevID;
		getNextTokenWrapper ();
		if (variableDeclaration (pID, currSym))
		{
			while (currSym != NULL)
			{
				if (currSym->token == lg.COMMA)
				{
					getNextTokenWrapper ();
					if (!variableDeclaration (pID, currSym))
					{
						exist = 0;
						break;
					}
					else
						exist = 1;
				}
				else if (!strcmp (currSym->lexeme, "in"))
				{
					prevID = ast->addNode (parentID, currSym->lexeme);
					if (oclStatement (prevID, currSym))
					{
						exist = 1;
						break;
					}
				}
				else
					break;
			}
		}
		else
			ast->removeNode (parentID);
	}

//fprintf (stderr, "letStatement: %s EXIST: %d\n", currSym->lexeme, exist);
	return exist;
}

/**
*
* Checks for the ifStatement.
* Need to call getNextTOkenWtapper () before calling this function.
*
* IfExp        ::= 'if' OclStmt
*                       'then' OclStmt
*                       'else' OclStmt
*                  'endif' ;
*
* Write Syntax / Semantic Analysis output to file
* @param parentID node pointer of the parent node in the AST
* @return 0 if ifStatement statement doesn't exist
* or bad ifStatement statement
*
 ----------------------------------------------------------------------------*/
int OclStmt :: ifStatement (TreeNode *parentID)
{
	int exist = 0;

	if (currSym != NULL && currSym->token == lg.IF)
	{
		prevID = ast->addNode (parentID, "If-Then-Else");
		parentID = prevID;

		prevID = ast->addNode (parentID, currSym->lexeme);
		if (oclStatement (prevID, currSym))
		{
			if (currSym != NULL & currSym->token == lg.THEN)
			{
				prevID = ast->addNode (parentID, "Then");
				if (oclStatement (prevID, currSym))
				{
					if (currSym != NULL & currSym->token == lg.ELSE)
					{
						prevID = ast->addNode (parentID, "Else");
						if (oclStatement (prevID, currSym))
						{
							if (currSym != NULL && currSym->token == lg.ENDIF)
							{
								getNextTokenWrapper ();
								exist = 1;
							}
						}
					}
				}
			}
		}

		if (exist <= 0)
			ast->removeNode (parentID);
	}

//fprintf (stderr, "ifStatement: %s EXIST: %d\n", currSym->lexeme, exist);
	return exist;
}

/**
*
* Checks for the literalStatement
* Write Syntax / Semantic Analysis output to file
* @param parentID node pointer of the parent node in the AST
* @return 0 if literalStatement statement doesn't exist
* or bad literalStatement statement
*
 ----------------------------------------------------------------------------*/
int OclStmt :: literalStatement (TreeNode *parentID)
{
	int exist = 0;

	if (collectionLiteralStatement (parentID))
		exist = 1;
	else if (tupleLiteralStatement (parentID))
		exist = 1;
	else if (primitiveLiteralStatement (parentID))
		exist = 1;

//fprintf (stderr, "literalStatement: %s EXIST: %d\n", currSym->lexeme, exist);
	return exist;
}

/**
*
* Checks for the collectionLiteralStatement.
* Need to call getNextTOkenWtapper () before calling this function.
*
* CollectionLiteralStmt       ::= CollectionTypeIdentifier
*                                 '{' CollectionLiteralParts? '}' ;
* 
* CollectionTypeIdentifier    ::= 'Set'
*                                 | 'Bag'
*                                 | 'Sequence'
*                                 | 'Collection'
*                                 | 'OrderedSet'
* 								  | 'Dag' ;
* 
* CollectionLiteralParts      ::= CollectionLiteralPart
*                                 ( ',' CollectionLiteralPart )* ;
* 
* CollectionLiteralPart       ::= CollectionRange | OclStmt ;
* 
* CollectionRange             :: = OclStmt '..' OclStmt ;
* 
* Write Syntax / Semantic Analysis output to file
* @param parentID node pointer of the parent node in the AST
* @return 0 if collectionLiteralStatement statement doesn't exist
* or bad collectionLiteralStatement statement
*
 ----------------------------------------------------------------------------*/
int OclStmt :: collectionLiteralStatement (TreeNode *parentID)
{
	int exist = 0;

	if (currSym != NULL && currSym->token == lg.CTID)
	{
		prevID = ast->addNode (parentID, currSym->lexeme);
		parentID = prevID;

		getNextTokenWrapper ();
		if (currSym != NULL && currSym->token == lg.LBRKT3)
		{
			while (oclStatement (parentID, currSym))
			{
				if (currSym != NULL && currSym->token == lg.RANGEOP)
				{
					prevID = ast->addNode (parentID, currSym->lexeme);
					if (!oclStatement (parentID, currSym))
						break;
				}

				if (currSym != NULL && currSym->token == lg.RBRKT3)
				{
					getNextTokenWrapper ();
					exist = 1;
					break;
				}
				else if (currSym != NULL && currSym->token != lg.COMMA)
					break;
			}
		}
		
		if (exist <= 0)
			ast->removeNode (parentID);
	}

//fprintf (stderr, "collectionLiteralStatement: %s EXIST: %d\n", currSym->lexeme, exist);
	return exist;
}

/**
*
* Checks for the tupleLiteralStatement statement.
* Need to call getNextTOkenWtapper () before calling this function.
*
* TupleLiteralStmt ::= 'Tuple' '{' variableDeclarations '}' ;
*
* Write Syntax / Semantic Analysis output to file
* @param parentID node pointer of the parent node in the AST
* @return 0 if tupleLiteralStatement statement doesn't exist
* or bad tupleLiteralStatement statement
*
 ----------------------------------------------------------------------------*/
int OclStmt :: tupleLiteralStatement (TreeNode *parentID)
{
	int exist = 0;

	if (currSym != NULL && currSym->token == lg.TUPLE)
	{
		prevID = ast->addNode (parentID, currSym->lexeme);
		parentID = prevID;

		getNextTokenWrapper ();
		if (currSym != NULL && currSym->token == lg.LBRKT3)
		{
			getNextTokenWrapper ();
			exist = variableDeclarations (parentID, currSym);
			if (currSym != NULL && currSym->token == lg.RBRKT3)
			{
				getNextTokenWrapper ();
				exist = 1;
			}
			else
				exist = 0;
		}
	}

//fprintf (stderr, "tupleLiteralStatement: %s EXIST: %d\n", currSym->lexeme, exist);
	return exist;
}

/**
*
* Checks for the primitiveLiteralStatement.
* Need to call getNextTOkenWtapper () before calling this function.
*
* PrimitiveLiteralStmt        ::= NumericLiteralStmt
*                                 | StringLiteralStmt
*                                 | BooleanLiteralStmt ;
*
* NumericLiteralStmt          ::= number ;   -- integer | real
*
* StringLiteralStmt           ::= '"' string '"' ;
*
* BooleanLiteralStmt          ::= 'true' | 'false' ;
*
* Write Syntax / Semantic Analysis output to file
* @param parentID node pointer of the parent node in the AST
* @return 0 if primitiveLiteralStatement statement doesn't exist
* or bad primitiveLiteralStatement statement
*
 ----------------------------------------------------------------------------*/
int OclStmt :: primitiveLiteralStatement (TreeNode *parentID)
{
	int exist = 0;

	if (currSym != NULL && currSym->token == lg.NUM)
	{
		char *num = new char[4+strlen(currSym->lexeme)];
		sprintf (num, "NUM_%s", currSym->lexeme);
		num[strlen(num)] = END_OF_STRING;
		prevID = ast->addNode (parentID, num);
		getNextTokenWrapper ();
		exist = 1;
	}
	else if (currSym != NULL && (currSym->token == lg.TRUE || currSym->token == lg.FALSE))
	{
		prevID = ast->addNode (parentID, currSym->lexeme);
		getNextTokenWrapper ();
		exist = 1;
	}
	else if (currSym != NULL && currSym->token == lg.STRING)
	{
		//
		// Insert a string node node and then insert
		// the value of the string as child
		//
		prevID = ast->addNode (parentID, "string");
		prevID = ast->addNode (prevID, currSym->lexeme);
		getNextTokenWrapper ();
		exist = 1;
	}

//fprintf (stderr, "primitiveLiteralStatement: %s EXIST: %d\n", currSym->lexeme, exist);
	return exist;
}

/**
*
* Checks for the messageStatement.
* Need to call getNextTOkenWtapper () before calling this function.
* OclStmt is checked by the calee function.
*
* MessageStmt              ::= id '(' MessageArguments? ')' ;
*
* MessageArguments         ::= MessageArg (',' MessageArg)* ;
*
* MessageArg               ::= OclStmt
*                              | ( '?' (':' type)? ) ;
*
* Write Syntax / Semantic Analysis output to file
* @param parentID node pointer of the parent node in the AST
* @return 0 if messageStatement statement doesn't exist
* or bad messageStatement statement
*
 ----------------------------------------------------------------------------*/
int OclStmt :: messageStatement (TreeNode *parentID)
{
	int exist = 0;

	if (currSym != NULL && currSym->token == lg.ID)
	{
		prevID = ast->addNode (parentID, currSym->lexeme);

		getNextTokenWrapper ();
		if (currSym != NULL && currSym->token == lg.LBRKT1)
		{
			//
			// Check for message arguments
			//
			prevID = ast->addNode (parentID, "args");
			parentID = prevID;
			while (currSym != NULL)
			{
				exist = oclStatement (parentID, currSym);
				if (exist == 0 && currSym != NULL && currSym->token == lg.QMARK)
				{
					prevID = ast->addNode (parentID, currSym->lexeme);
					exist = 1;
					getNextTokenWrapper ();
					if (currSym != NULL && currSym->token == lg.COLON)
					{
						getNextTokenWrapper ();
						exist = type (prevID, currSym);
					}
				}

				if (currSym != NULL && currSym->token == lg.RBRKT1)
				{
					if (exist == 0)
						ast->removeNode (parentID);
					exist = 1;
					getNextTokenWrapper ();
					break;
				}
				else if (currSym != NULL && currSym->token != lg.COMMA)
				{
					ast->removeNode (parentID);
					exist = 0;
					break;
				}
			}
		}
	}

//fprintf (stderr, "messageStatement: %s EXIST: %d\n", currSym->lexeme, exist);
	return exist;
}

/**
*
* Checks for the propertyCallStatement.
* Need to call getNextTOkenWtapper () before calling this function.
*
* PropertyCallStmt      ::= id isMarkedPre ( '(' arguments? ')' )?
*                           | id ( '[' arguments ']' )? isMarkedPre?
*                           | id '(' arguments? ')'
*                           | id 
*                             '(' (
*                                    VariableDeclaration
*                                    ( ',' VariableDeclaration )? '|'
*                                 )? OclStmt
*                             ')'
*                           | pathName ( '(' arguments? ')' )?
*                           | 'iterate'
*                             '(' ( VariableDeclaration ';' )?
*                                 VariableDeclaration '|' OclStmt
*                             ')' ;
*
*
* Write Syntax / Semantic Analysis output to file
* @param parentID node pointer of the parent node in the AST
* @return 0 if propertyCallStatement statement doesn't exist
* or bad propertyCallStatement statement
*
 ----------------------------------------------------------------------------*/
int OclStmt :: propertyCallStatement (TreeNode *parentID)
{
	int exist = 0;

	//
	// getNextTokenWrapper () already called by the calle function
	//
	exist = pathName (parentID, currSym);
	//
	// id isMarkedPre ( '(' arguments? ')' )?
	// | id '(' arguments? ')'
	// | id ('[' arguments ']')? isMarkedPre?
	// | id
	//
	// 2 means ID
	//
	if (exist == 2)
	{
		//
		// isMarkedPre ( '(' arguments? ')' )?
		//
		parentID = prevID;
		if (currSym != NULL && currSym->token == lg.ATPRE)
		{
			prevID = ast->addNode (parentID, currSym->lexeme);
			getNextTokenWrapper ();
			if (currSym != NULL && currSym->token == lg.LBRKT1)
			{
				prevID = ast->addNode (parentID, "args");
				exist = arguments (prevID);
				if (exist <= 0)
				{
					//ast->removeNode (prevID);
					prevID = parentID;
				}
				if (currSym != NULL && currSym->token == lg.RBRKT1)
				{
					exist = 1;
					getNextTokenWrapper ();
				}
			}
			else
				exist = 1;
		}
		//
		// ( '[' arguments ']' )? isMarkedPre?
		//
		else if (currSym != NULL && currSym->token == lg.LBRKT2)
		{
			prevID = ast->addNode (parentID, "args");
			exist = arguments (prevID);
			if (exist == 1 && currSym != NULL && currSym->token == lg.RBRKT2)
			{
				exist = 1;
				getNextTokenWrapper ();
			}
			else
			{
				//ast->removeNode (prevID);
				prevID = parentID;
			}

			if (currSym != NULL && currSym->token == lg.ATPRE)
			{
				prevID = ast->addNode (parentID, currSym->lexeme);
				exist = 1;
				getNextTokenWrapper ();
			}
		}
		//
		//   '('   (
		//            VariableDeclaration
		//            ( ',' VariableDeclaration )? '|'
		//         )? OclStmt
		//   ')'
		//
		//   |
		//
		//   '(' arguments? ')'
		//
		//
		else if (currSym != NULL && currSym->token == lg.LBRKT1)
		{
			prevID = ast->addNode (parentID, "args");
			TreeNode * pID = prevID;
			if (oclStatement (pID, currSym))
			{
				exist = 1;
				//
				// Check for variableDeclaration without calling the
				// variableDeclaration () function, since oclStatement ()
				// checks for ID.
				// So check for ID using previous symbol pointer.
				//
				if (prevSym1->token == lg.ID)
				{
					exist = 1;
					if (currSym != NULL && currSym->token == lg.COLON)
					{
						getNextTokenWrapper ();
						exist = type (prevID, currSym);
						if (exist == 0)
							err.normal ("Parser->type: Wrong type", SYNTAX_ERROR, lr->getCurrentLine(), lr->getCurrentCol());
					}

					if (currSym != NULL && !strcmp (currSym->lexeme, "="))
						exist = oclStatement (prevID, currSym);

					if (currSym != NULL && currSym->token == lg.COMMA)
					{
						getNextTokenWrapper ();
						exist = variableDeclaration (pID, currSym);
					}

					if (currSym != NULL && currSym->token == lg.PIPE)
						exist = oclStatement (parentID, currSym);
				}
				else if (currSym != NULL && currSym->token == lg.COMMA)
					exist = arguments (pID);
			}
			else
			{
				//ast->removeNode (prevID);
				prevID = parentID;
			}
			if (currSym != NULL && currSym->token == lg.RBRKT1)
				getNextTokenWrapper ();
			else
				exist = 0;
		}
		//
		// id
		//
		else
			exist = 1;
	}
	//
	// pathName ( '(' arguments? ')' )?
	//
	// 1 means pathName
	//
	else if (exist == 1)
	{
		//
		// ( '(' arguments? ')' )?
		//
		parentID = prevID;
		if (currSym != NULL && currSym->token == lg.LBRKT1)
		{
			prevID = ast->addNode (parentID, "args");
			exist = arguments (prevID);
			if (exist <= 0)
			{
				//ast->removeNode (prevID);
				prevID = parentID;
			}
			if (currSym != NULL && currSym->token == lg.RBRKT1)
			{
				exist = 1;
				getNextTokenWrapper ();
			}
		}
		else
			exist = 1;
	}
	//
	// 'iterate'
	//   '(' ( VariableDeclaration ';' )?
	//       VariableDeclaration '|' OclStmt
	//   ')' ;
	//
	else if (currSym != NULL && currSym->token == lg.ITERATE)
	{
		prevID = ast->addNode (parentID, currSym->lexeme);
		parentID = prevID;

		getNextTokenWrapper ();
		if (currSym != NULL && currSym->token == lg.LBRKT1)
		{
			prevID = ast->addNode (parentID, "args");
			TreeNode * pID = prevID;
			getNextTokenWrapper ();
			if (variableDeclaration (pID, currSym))
			{
				if (currSym != NULL && currSym->token == lg.SCOLON)
				{
					getNextTokenWrapper ();
					exist = variableDeclaration (pID, currSym);
				}

				if (currSym != NULL && currSym->token == lg.PIPE)
					exist = oclStatement (parentID, currSym);
			}

			if (currSym != NULL && currSym->token == lg.RBRKT1)
				getNextTokenWrapper ();
			else
				exist = 0;
		}
	}

//fprintf (stderr, "propertyCallStatement: %s EXIST: %d\n", currSym->lexeme, exist);
	return exist;
}

/**
*
* Check for the correctness of arguments.
* Need to call getNextTOkenWtapper () before calling this function.
*
* arguments              ::= OclStmt ( ',' OclStmt )* ;
*
* Write Syntax / Semantic Analysis output to file
* @param parentID node pointer of the parent node in the AST
* @return 0 if no arguments exists
*
 ----------------------------------------------------------------------------*/
int OclStmt :: arguments (TreeNode *parentID)
{
	int exist = 0;

	if (oclStatement (parentID, currSym))
	{
		exist = 1;
		while (currSym != NULL)
		{
			if (currSym->token == lg.COMMA)
			{
				if (!oclStatement (parentID, currSym))
				{
					exist = 0;
					break;
				}
				else
					exist = 1;
			}
			else
				break;
		}
	}

	return exist;
}

/**
*
* Check for the correctness of pathName.
* Need to call getNextTOkenWtapper () before calling this function.
*
* pathName ::= ID ( DCOLON ID )*
*
* 0 means no pathName
* 1 means fullPathName ::= pathName '::' id
* 2 means pathName ::= id
*
* Write Syntax / Semantic Analysis output to file
* @param parentID node pointer of the parent node in the AST
* @param currSymP pointer to the current Symbol being processed
* Returns 0 if no pathanme exists
*
 ----------------------------------------------------------------------------*/
int OclStmt :: pathName (TreeNode *parentID, SYMBOL *currSymP)
{
	int exist = 0;

	currSym = currSymP;
	if (currSym != NULL && currSym->token == lg.ID)
	{
		exist = 2;
		prevID = ast->addNode (parentID, currSym->lexeme);
		getNextTokenWrapper ();
		while (currSym != NULL)
		{
			if (currSym->token == lg.DCOLON)
			{
				getNextTokenWrapper ();
				if (currSym != NULL && currSym->token == lg.ID)
				{
					exist = 1;
					parentID = prevID;
					prevID = ast->addNode (parentID, currSym->lexeme);
					getNextTokenWrapper ();
				}
				else
				{
					exist = 0;
					break;
				}
			}
			else
				break;
		}
	}

	return exist;
}

/**
*
* Check for the correctness of type.
* Need to call getNextTOkenWtapper () before calling this function.
*
* type	::= pathName | collectionType | tupleType ;
*
* Write Syntax / Semantic Analysis output to file
* @param parentID node pointer of the parent node in the AST
* @param currSymP pointer to the current Symbol being processed
* Returns 0 if no type exists
*
 ----------------------------------------------------------------------------*/
int OclStmt :: type (TreeNode *parentID, SYMBOL *currSymP)
{
	int exist = 0;

	prevID = ast->addNode (parentID, "type");

	currSym = currSymP;
	if (pathName (prevID, currSym))
		exist = 1;
	else if (collectionType (prevID))
		exist = 1;
	else if (tupleType (prevID))
		exist = 1;

	if (exist == 0)
		ast->removeNode (prevID);

	return exist;
}

/**
*
* Check for the correctness of collectionType.
* Need to call getNextTOkenWtapper () before calling this function.
*
* Recursively call function type () to check the syntax
*
* collectionType	::= CTID '(' type ')' ;
*
* CTID = collectionTypeIdentifier
*
* CTID			::= 'Set'
*				| 'Bag'
*				| 'Sequence'
*				| 'Collection'
*				| 'OrderedSet'
* 				| 'Dag' ;
*
* Write Syntax / Semantic Analysis output to file
* @param parentID node pointer of the parent node in the AST
* @param currSymP pointer to the current Symbol being processed
* Returns 0 if no collectionType exists
*
 ----------------------------------------------------------------------------*/
int OclStmt :: collectionType (TreeNode *parentID)
{
	int exist = 0;

	if (currSym != NULL && currSym->token == lg.CTID)
	{
		prevID = ast->addNode (parentID, currSym->lexeme);
		parentID = prevID;
		getNextTokenWrapper ();
		if (currSym != NULL && currSym->token == lg.LBRKT1)
		{
			getNextTokenWrapper ();
			exist = type (parentID, currSym);
			if (exist == 0)
				err.normal ("Parser->type: Wrong type", SYNTAX_ERROR, lr->getCurrentLine(), lr->getCurrentCol());
			if (currSym != NULL && currSym->token == lg.RBRKT1)
			{
				getNextTokenWrapper ();
				exist = 1;
			}
		}
	}

	return exist;
}

/**
*
* Check for the correctness of tupleType.
* Need to call getNextTOkenWtapper () before calling this function.
*
* tupleType	::= 'TupleType' '(' variableDeclarations ')' ;
*
* Write Syntax / Semantic Analysis output to file
* @param parentID node pointer of the parent node in the AST
* @param currSymP pointer to the current Symbol being processed
* Returns 0 if no tupleType exists
*
 ----------------------------------------------------------------------------*/
int OclStmt :: tupleType (TreeNode *parentID)
{
	int exist = 0;

	if (currSym != NULL && currSym->token == lg.TUPLET)
	{
		prevID = ast->addNode (parentID, currSym->lexeme);
		getNextTokenWrapper ();
		if (currSym != NULL && currSym->token == lg.LBRKT1)
		{
			getNextTokenWrapper ();
			if (!variableDeclarations (prevID, currSym))
				exist = 0;
			else if (currSym != NULL && currSym->token == lg.RBRKT1)
			{
				getNextTokenWrapper ();
				exist = 1;
			}
		}
	}

	return exist;
}

/**
*
* Check for the correctness of variableDeclarations | variableDeclaration.
* Need to call getNextTOkenWtapper () before calling this function.
*
* variableDeclarations	::= VariableDeclaration	(',' VariableDeclaration )* ;
*
* Write Syntax / Semantic Analysis output to file
* @param runOnce To control the number of times while loop runs
*  see above for more explanation
* @param parentID node pointer of the parent node in the AST
* @param currSymP pointer to the current Symbol being processed
* Returns 0 if no variableDeclarations exists or 1 if exists
*
 ----------------------------------------------------------------------------*/
int OclStmt :: variableDeclarations (TreeNode *parentID, SYMBOL *currSymP)
{
	int exist = 0;

	//
	// Checking for VariableDeclarations
	//
	currSym = currSymP;
	while (currSym != NULL)
	{
		exist = 0;
		if (!variableDeclaration (parentID, currSym))
			break;
		else
			exist = 1;

		if (currSym != NULL && currSym->token != lg.COMMA)
			break;
		else
			getNextTokenWrapper ();
	}

	return exist;
}

/**
*
* Check for the correctness of variableDeclaration.
* Need to call getNextTOkenWtapper () before calling this function.
*
* variableDeclaration 	::= ID (':' type)? ( '=' OclStmt )? ;
*
* Write Syntax / Semantic Analysis output to file
* @param runOnce To control the number of times while loop runs
*  see above for more explanation
* @param parentID node pointer of the parent node in the AST
* @param currSymP pointer to the current Symbol being processed
* Returns 0 if no variableDeclarations exists or 1 if exists
*
 ----------------------------------------------------------------------------*/
int OclStmt :: variableDeclaration (TreeNode *parentID, SYMBOL *currSymP)
{
	int exist = 0;

	//
	// Checking for VariableDeclaration
	//
	currSym = currSymP;
	if (currSym != NULL && currSym->token == lg.ID)
	{
		prevID = ast->addNode (parentID, currSym->lexeme);
		parentID = prevID;
		exist = 1;

		getNextTokenWrapper ();
		if (currSym != NULL && currSym->token == lg.COLON)
		{
			getNextTokenWrapper ();
			exist = type (parentID, currSym);
			if (exist == 0)
				err.normal ("Parser->type: Wrong type", SYNTAX_ERROR, lr->getCurrentLine(), lr->getCurrentCol());
		}

		if (currSym != NULL && !strcmp (currSym->lexeme, "="))
			exist = oclStatement (parentID, currSym);
	}

	return exist;
}

/**
*
* Get pointer to the previous ID
* @return pointer to the previous ID of the node
*
 ----------------------------------------------------------------------------*/
TreeNode * OclStmt :: getPrevID (void)
{
	return (prevID);
}

/**
*
* Get pointer to the current symbol
* @return pointer to the current symbol table for tokens
*
 ----------------------------------------------------------------------------*/
SYMBOL * OclStmt :: getCurrSym (void)
{
	return (currSym);
}

/**
*
* Get pointer to the current symbol
* @return pointer to the previous symbol table for tokens
*
 ----------------------------------------------------------------------------*/
SYMBOL * OclStmt :: getPrevSym1 (void)
{
	return (prevSym1);
}

/**
*
* Get pointer to the current symbol
* @return pointer to the previous to previous symbol table for tokens
*
 ----------------------------------------------------------------------------*/
SYMBOL * OclStmt :: getPrevSym2 (void)
{
	return (prevSym2);
}

/**
*
* Wrapper to call getNextToken () function of Lexer class
* Store the prev column number and previous line number
* as column and line error numbers
* @return pointer to the symbol table for tokens
*
 ----------------------------------------------------------------------------*/
SYMBOL * OclStmt :: getNextTokenWrapper (void)
{
	prevSym2 = prevSym1;
	prevSym1 = currSym;
	currSym = lr->getNextToken ();
//fprintf (stderr, "------------------ %s\n", currSym->lexeme);
	return (currSym);
}
