/* ============================================================================
|
|   Filename:    intlist.cpp
|   Dated:       03 September, 2006
|   By:          Shahid Alam (shalam@ieee.org
|
|   Description: Implementation of Integer Linked List class
|
|   TO DO:       
|
 ----------------------------------------------------------------------------*/

#include "include/intlist.h"

/**
//
// Constructor function.
//
 ----------------------------------------------------------------------------*/
IntList :: IntList ()
{
	count = 0;
	firstptr = lastptr = iterator = 0;
}

/**
//
// Destructor function.
// It delete the pointer
//
 ----------------------------------------------------------------------------*/
IntList :: ~IntList ()
{
	if (!isempty ())
	{
		INT *tempptr = NULL;
		INT *currentptr = firstptr;

		while (currentptr != lastptr)
		{
			tempptr = currentptr;
			currentptr = currentptr-> nextptr;
			delete tempptr;
		}
		delete currentptr;
	}
}

/**
//
// Dynamically allocates memory for the new Node.
//
 ----------------------------------------------------------------------------*/
INT * IntList :: getNewNode (int val)
{
	INT *ptr = new INT (val);

	return ptr;
}

/**
//
// Returns first value in the integer list
// @return value of the first integer in the integer list
//
 ----------------------------------------------------------------------------*/
int IntList :: getFirst (void)
{
	int val = firstptr->value;
	
	return val;
}

/**
//
// Returns last value in the integer list
// @return value of the last integer in the integer list
//
 ----------------------------------------------------------------------------*/
int IntList :: getLast (void)
{
	int val = lastptr->value;
	
	return val;
}

/**
//
// Gets the number of integers in the list
// @return the number of integers in the list
//
 ----------------------------------------------------------------------------*/
int IntList :: getCount (void)
{
	return count;
}

/**
//
// Initializes the iterator
//
 ----------------------------------------------------------------------------*/
void IntList :: initIterator (void)
{
	iterator = firstptr;
}

/**
//
// Iterates the list and returns the value as it iterates
// Everytime it is called it increments the iterator.
// @return -1 at the end of the list
//
 ----------------------------------------------------------------------------*/
int IntList :: iterate (void)
{
	if (!isempty ())
	{
		if (iterator != lastptr)
			iterator = iterator->nextptr;
		else
			return -1;

		return (iterator->value);
	}

	return -1;
}

/**
//
// Appends a List. If it is empty new Node will be added
// and the pointer of the Node will be assigned to first else
// it will be assigned to last and the previous last pointer's
// next pointer will be set to the newptr.
//
 ----------------------------------------------------------------------------*/
void IntList :: append (int val)
{
	count++;
	INT *newptr = getNewNode (val);

   	if (isempty ())
		firstptr = newptr;
	else
		lastptr-> nextptr  = newptr;

	lastptr = newptr;
}

/**
//
// Appends a List at the end.
// It copies the integer list to be appended and append it at the end.
//
 ----------------------------------------------------------------------------*/
void IntList :: appendList (IntList *list)
{
	if (!list->isempty ())
	{
		INT *tempptr = NULL;
		INT *currentptr = list->firstptr;

		while (currentptr != list->lastptr)
		{
			tempptr = currentptr;
			currentptr = currentptr->nextptr;
			append (tempptr->value);
		}
		append (currentptr->value);
	}
}

/**
//
// Removes the last Node of the list. It starts with the
// firstptr then reach to the lastptr keeping track of the
// previous pointer because then that becomes the lastptr
// when lastptr is removed.
// It just delete the pointer for the data
// but not the data itself.
//
 ----------------------------------------------------------------------------*/
void IntList :: removeLast (void)
{
	if (!isempty ())
	{
		INT *tempptr = firstptr;
		if (firstptr == lastptr)
			firstptr = iterator = lastptr = 0;
		else if (iterator == lastptr)
		{
			tempptr = iterator;
			iterator = 0;
		}
		else
		{
			INT *nextptr = NULL;
			while (tempptr != lastptr)
			{
				nextptr = tempptr-> nextptr;
				if (nextptr == lastptr)
				{
					lastptr = tempptr;
					tempptr = nextptr;
					break;
				}
				tempptr = nextptr;
			}
		}
		delete tempptr;
	}
}

/**
//
// Prints the list for debugging.
//
 ----------------------------------------------------------------------------*/
void IntList :: print (FILE *stream) const
{
	if (!isempty ())
	{
		INT *nextPtr = firstptr;
		fprintf (stream, "%d:", nextPtr->value);
		while (nextPtr != lastptr)
		{
			nextPtr = nextPtr-> nextptr;
			fprintf (stream, "%d:", nextPtr->value);
		}
	}
}
