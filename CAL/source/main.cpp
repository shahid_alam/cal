/* ============================================================================
|
|   Filename:    main.cpp
|   Dated:       23 May, 2006
|   By:          Shahid Alam (shalam@ieee.org
|
|   Description: Main file for the CAL compiler.
|                 See figure below for more details.

 page 13 of DRAGON book (1986) on compilers pinciples, techniques and tools

|
|                result = x + y * 60
|
|                 -------------------------------
|                |       Lexical Analyser        |
|                 -------------------------------
|                                |
|                                v
|
|                         id1 = id2 + id3 * 60;
|
|                                |
|                                v
|                 -------------------------------
|                |       Syntax Analyser         |
|                 -------------------------------
|                                |
|                                v
|
|                             =
|                            / \
|                           /   \
|                         id1    +
|                               / \
|                              /   \
|                            id2    *
|                                  / \
|                                 /   \
|                               id3   60
|
|                                |
|                                v
|                 -------------------------------
|                |       Semantic Analyser       |
|                 -------------------------------
|                                |
|                                v
|
|                           =
|                          / \
|                         /   \
|                       id1    +                              SYMBOL TABLE
|                             / \                       ------------------------
|                            /   \                   1 |   result   |   . . .   |
|                          id2    *                     ------------------------
|                                / \                 2 |   x        |   . . .   |
|                               /   \                   ------------------------
|                             id3  inttoreal         3 |   y        |   . . .   |
|                                    |                  ------------------------
|                                   60               4 |   . . .    |   . . .   |
|
|                                |
|                                v
|                 -------------------------------
|                |  Intermediate Code Generator  |
|                 -------------------------------
|                                |
|                                v
|
|                temp1 = inttoreal (60)
|                temp2 = id3 * temp1
|                temp3 = id2 + temp2
|                id1 = temp3
|
|                                |
|                                v
|                 -------------------------------
|                |        Code Optimizer         |
|                 -------------------------------
|                                |
|                                v
|
|                temp1 = id3 * 60.0
|                id1 = id2 + temp1
|
|                                |
|                                v
|                 -------------------------------
|                |        Code Generator         |
|                 -------------------------------
|                                |
|                                v
|
|                MOVF id3, R2
|                MULF #60.0, R2
|                MOVF id2, R1
|                ADDF R2, R1
|                MOVF R1, id1
|
|   TO DO:       See below
|
 ----------------------------------------------------------------------------*/

#include "include/global.h"
#include "include/parser.h"
#include "test.h"

/* ============================================================================

External global data defined in global.h and are set here

-----------------------------------------------------------------------------*/
int _PRINT_LEXICAL_ANALYSIS_;
int _PRINT_SEMANTIC_SYNTAX_ANALYSIS_;
int _PRINT_INTERMEDIATE_REPRESENTATION_;

/* ============================================================================

Local global data for the compiler command line options

-----------------------------------------------------------------------------*/
LOCAL char* fileName   = "sample.cal";   // default input file
LOCAL char* lFilename  = "CAL_LA.out";   // default output file for lexical analysis
LOCAL char* pFilename  = "CAL_PA.out";   // default output file for parsing
LOCAL char* iFilename  = "CAL_IR.out";   // default output file for IR
LOCAL int optimization = 0;              // by default no optimization
LOCAL int assembly     = 1;              // by default 8086 assembly language

/* ============================================================================

	usage () function

PURPOSE:	Local helper method. Prints the help file.
SETUP:		None.
PARAMETERS: None.
CALL:		usage ();
RETURN:		None.
TO DO:      change with respect to compiler arguments

-----------------------------------------------------------------------------*/
LOCAL void usage (void)
{
	fprintf (stdout, "\n<Usage>\n"
		"    CAL [options] <CAL file>\n\n"
		"Compiler for CAL language\n\n"
		"Options:\n"
			"    -l=xxx     Ouput lexical analysis to file = xxx\n"
			"    -p=xxx     Ouput parsing output to file = xxx\n"
			"    -i=xxx     Ouput intermeidate representation to file = xxx\n"
			"    -o=x       Optimizations from 1 - 3, 1 being the minimum\n"
			"    -a=x       Generate assembly, 1 = 8086, 2 = 68HC12, 3 = 68000\n"
			"    -?	        Show this help\n");
	fprintf (stdout, "</Usage>\n\n");
}

/* ============================================================================

	readCommandLine () function

PURPOSE:	Local helper method. Prints the help file.
SETUP:		None.
PARAMETERS: argC: Number of arguments passed from the command line
            argV[]: Pointers to the arguments from the command line
CALL:		readCommandLine (int, char **)
RETURN:		0 if no error
            1 if no arguments
            2 if wrong argument
TO DO:      for reading more than one filename and check for extension of files

-----------------------------------------------------------------------------*/
LOCAL int readCommandLine (int argC, char *argV[])
{
#ifdef _DEBUG_
		fprintf (stdout, "<PRINT>\n", pFilename);
#endif

	//
	// Check command line and extract arguments.
	//
	if (argC < 2)
	{
		usage ();
#ifdef _DEBUG_
			fprintf (stdout, "</PRINT>\n", pFilename);
#endif
		return 1;
	}

	int parmN;
	char tempStr[256];
	for (parmN = 1; parmN < argC; parmN++)
	{
		//
		// Break out on first parameter not starting with a dash
		//
		if (argV[parmN][0] != '-')
		break;

		//
		// Special case help request
		//
		if (!strcmp(argV[parmN], "-?"))
		{
			usage ();
#ifdef _DEBUG_
				fprintf (stdout, "</PRINT>\n", pFilename);
#endif
			return 2;
		}
		//
		// Checking for -l=xxx option
		//
		else if (!strncmp(argV[parmN], "-l=", 3)
								||  !strncmp(argV[parmN], "-L=", 3))
		{
			lFilename = &argV[parmN][3];
			_PRINT_LEXICAL_ANALYSIS_ = 1;
#ifdef _DEBUG_
			fprintf (stdout, "   <PRINT_LEXICAL_ANALYSIS />\n", pFilename);
#endif
		}
		//
		// Checking for -p=xxx option
		//
		else if (!strncmp(argV[parmN], "-p=", 3)
								||  !strncmp(argV[parmN], "-P=", 3))
		{
			pFilename = &argV[parmN][3];
			_PRINT_SEMANTIC_SYNTAX_ANALYSIS_ = 1;
#ifdef _DEBUG_
			fprintf (stdout, "   <PRINT_SEMANTIC_SYNTAX_ANALYSIS />\n", pFilename);
#endif
		}
		//
		// Checking for -i=xxx option
		//
		else if (!strncmp(argV[parmN], "-i=", 3)
								||  !strncmp(argV[parmN], "-I=", 3))
		{
			iFilename = &argV[parmN][3];
			_PRINT_INTERMEDIATE_REPRESENTATION_ = 1;
#ifdef _DEBUG_
			fprintf (stdout, "   <PRINT_INTERMEDIATE_REPRESENTATION />\n", pFilename);
#endif
		}
		//
		// Checking for -o=x option
		//
		else if (!strncmp(argV[parmN], "-o=", 3)
								||  !strncmp(argV[parmN], "-O=", 3))
		{
			optimization = atoi (&argV[parmN][3]);
			if (optimization < 0 || optimization > 3)
			{
				fprintf (stdout, "<WARNING>Bad optimazation (o=%d) parameter, changing to '0'</WARNING>\n", optimization);
				optimization = 0;
			}
		}
		//
		// Checking for -a=x option
		//
		else if (!strncmp(argV[parmN], "-a=", 3)
								||  !strncmp(argV[parmN], "-A=", 3))
		{
			assembly = atoi (&argV[parmN][3]);
			if (assembly < 1 || assembly > 3)
			{
				fprintf (stdout, "<WARNING>Bad assembly (a=%d) parameter, changing to '1'</WARNING>\n", assembly);
				assembly = 1;
			}
		}
		//
		// Checking for unknown option
		//
		else
		{
			fprintf (stderr, "<Unknown_option>'%s', ignoring it<Unknown_option/>\n", argV[parmN]);
		}
	}

	//
	//  Filename parameter
	//
	if (parmN + 1 != argC)
	{
		usage ();
#ifdef _DEBUG_
			fprintf (stdout, "</PRINT>\n", pFilename);
#endif
		return 1;
	}
	fileName = argV[parmN];
	
#ifdef _DEBUG_
		fprintf (stdout, "</PRINT>\n", pFilename);
#endif
	return 0;
}

/* ============================================================================

	printCommandLine () function

PURPOSE:	Debug print the command line parameters
SETUP:		None
PARAMETERS: None
CALL:		printCommandLine ()
RETURN:		None
TO DO:      for reading more than one filename and check for extension of files

-----------------------------------------------------------------------------*/
LOCAL void printCommandLine (void)
{
	fprintf (stdout, "\n<CommandLineParameters>\n");
	fprintf (stdout, "   <Input_file>%s</Input_file>\n", fileName);
	fprintf (stdout, "   <Lexical_analysis_filename>%s</Lexical_analysis_filename>\n", lFilename);
	fprintf (stdout, "   <Parsing_analysis_filename>%s</Parsing_analysis_filename>\n", pFilename);
	fprintf (stdout, "   <IR_filename>%s</IR_filename>\n", iFilename);
	fprintf (stdout, "   <Optimization>%d (1 - 3)</Optimization>\n", optimization);
	fprintf (stdout, "   <Assembly>%d (1 = 8086, 2 = 68HC12, 3 = 68000)</Assembly>\n", assembly);
	fprintf (stdout, "</CommandLineParameters>\n\n");
}

/* ============================================================================

	main () function

PURPOSE:	Main function.
SETUP:		See function usage ().
PARAMETERS: argC: Number of arguments passed from the command line
            argV[]: Pointers to the arguments from the command line
CALL:		main (int, char **) See function usage ().
RETURN:		Zero when there is no error otherwise non zero.
TO DO:      change wrt compiler arguments

-----------------------------------------------------------------------------*/
int main (int argC, char *argV[])
{
	int error = 0;

	_PRINT_LEXICAL_ANALYSIS_ = 0;
	_PRINT_SEMANTIC_SYNTAX_ANALYSIS_ = 0;
	_PRINT_INTERMEDIATE_REPRESENTATION_ = 0;

#ifdef _DEBUG_
		fprintf (stdout, "<CAL_OUTPUT>\n");
		fflush (stdout);
#endif

//testSYMT();
//testIntList ();
//testAST ();
//return 0;

	error = readCommandLine (argC, argV);
	if (error < 1)
	{
#ifdef _DEBUG_
		printCommandLine ();
		fflush (stdout);
#endif
		Parser *parser = new Parser ();
		error = parser->readFileInBuffer (fileName, lFilename, pFilename);

		if (error == 0)
		{
#ifdef _PRINT_TOKENS_
			parser->printTokensWithLineAndColNumber ();
#else
			error = parser->buildAST ();
#endif
		}
		delete parser;
	}

#ifdef _DEBUG_
	fprintf (stdout, "</CAL_OUTPUT>\n");
	fflush (stdout);
#endif
	return error;
}
