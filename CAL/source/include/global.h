/* ----------------------------------------------------------------------------
|
|   Filename:    global.h
|   Dated:       23 May, 2006
|   By:          Shahid Alam (shalam@ieee.org
|
|   Description: Header file to be included in all the files
|
|   TO DO:       
|
 ----------------------------------------------------------------------------*/

#ifndef _GLOBAL_H_
#define _GLOBAL_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

//#define _DEBUG_
//#define _PRINT_TOKENS_
#define _HANDLE_ERROR_

#ifndef NULL
	#define NULL 0
#endif

#define SIZE_OF_SYMBOL_TABLE   211      // e.g 65599, a prime number nearest to (2 ^ 16)
#define MAX_BUFFER_SIZE        65536    // 64 KB
#define TAB                    0x09     // 9
#define CR                     0x0D     // 13
#define LF                     0x0A     // 10
#define SPACE                  0x20     // 32
#define SINGLE_QUOTE           0x60     // 96
#define END_OF_STRING          '\0'

#define LOCAL                  static

extern int _PRINT_LEXICAL_ANALYSIS_;
extern int _PRINT_SEMANTIC_SYNTAX_ANALYSIS_;
extern int _PRINT_INTERMEDIATE_REPRESENTATION_;

#endif /* _GLOBAL_H_ */
