/* ----------------------------------------------------------------------------
|
|   Filename:    oclstmt.h
|   Dated:       28 August, 2006
|   By:          Shahid Alam (salam3@connect.carleton.ca)
|
|   Description: Header file contains functions for checking the
|                correctness of OCL Statements.
|
|   TO DO:       documentation
|
 ----------------------------------------------------------------------------*/

#ifndef _OCLSTMT_H_
#define _OCLSTMT_H_

#include "global.h"
#include "lg.h"
#include "symtable.h"
#include "error.h"
#include "lexer.h"
#include "ast.h"

/**
*
* Class OclStmt.
* Check the correctness of OCL Statements.
*
*/
class OclStmt
{
	private:
		/**
		*
		* Keeps pointers to two previous symbols
		*
		*/
		SYMBOL *currSym, *prevSym1, *prevSym2;
		TreeNode *prevID;
		Error err;
		LG lg;
		Lexer *lr;
		AST *ast;
		FILE *filePA;

		/**
		*
		* Checks for the logicalStatement.
		* Need to call getNextTOkenWtapper () before calling this function.
		*
		* Write Syntax / Semantic Analysis output to file
		* @param parentID node pointer of the parent node in the AST
		* @return
		* 1 if logicalStatement statement exists.
		* 2 if primaryStatement statement exists.
		* 0 if neither exists.
		*
		*/
		int logicalStatement (TreeNode *parentID);

		/**
		*
		* Checks for the relationalStatement.
		* Need to call getNextTOkenWtapper () before calling this function.
		*
		* Write Syntax / Semantic Analysis output to file
		* @param parentID node pointer of the parent node in the AST
		* @returns 1 if relationalStatement exists
		*
		*/
		int relationalStatement (TreeNode *parentID);

		/**
		*
		* Checks for the additiveStatement.
		* Need to call getNextTOkenWtapper () before calling this function.
		*
		* Write Syntax / Semantic Analysis output to file
		* @param parentID node pointer of the parent node in the AST
		* @returns 1 if additiveStatement exists
		*
		*/
		int additiveStatement (TreeNode *parentID);

		/**
		*
		* Checks for the multiplicativeStatement.
		* Need to call getNextTOkenWtapper () before calling this function.
		*
		* Write Syntax / Semantic Analysis output to file
		* @param parentID node pointer of the parent node in the AST
		* @returns 1 if multiplicativeStatement exists
		*
		*/
		int multiplicativeStatement (TreeNode *parentID);

		/**
		*
		* Checks for the unaryStatement.
		* Need to call getNextTOkenWtapper () before calling this function.
		*
		* Write Syntax / Semantic Analysis output to file
		* @param parentID node pointer of the parent node in the AST
		* @returns 1 if unaryStatement exists
		*
		*/
		int unaryStatement (TreeNode *parentID);

		/**
		*
		* Check for the correctness of oclStatement
		* OclStmt	::= LetStmt
		*				| IfStmt
		*				| LiteralStmt
		*				| MessageStmt
		*				| PropertyCallStmt ;
		*
		* Write Syntax / Semantic Analysis output to file
		* @param parentID node pointer of the parent node in the AST
		* Returns 0 if no oclStatement exists
		*
		*/
		int primaryStatement (TreeNode *parentID);

		/**
		*
		* Checks for the propertyCallStatement
		* Write Syntax / Semantic Analysis output to file
		* @param parentID node pointer of the parent node in the AST
		* @return 0 if propertyCallStatement statement doesn't exist
		* or bad propertyCallStatement
		*
		*/
		int propertyCallStatement (TreeNode *parentID);

		/**
		*
		* Checks for the literalStatement
		* Write Syntax / Semantic Analysis output to file
		* @param parentID node pointer of the parent node in the AST
		* @return 0 if literalStatement statement doesn't exist
		* or bad literalStatement
		*
		*/
		int literalStatement (TreeNode *parentID);

		/**
		*
		* Checks for the collectionLiteralStatement
		* Write Syntax / Semantic Analysis output to file
		* @param parentID node pointer of the parent node in the AST
		* @return 0 if collectionLiteralStatement statement doesn't exist
		* or bad collectionLiteralStatement statement
		*
		*/
		int collectionLiteralStatement (TreeNode *parentID);
		
		/**
		*
		* Checks for the tupleLiteralStatement
		* Write Syntax / Semantic Analysis output to file
		* @param parentID node pointer of the parent node in the AST
		* @return 0 if tupleLiteralStatement statement doesn't exist
		* or bad tupleLiteralStatement statement
		*
		*/
		int tupleLiteralStatement (TreeNode *parentID);
		
		/**
		*
		* Checks for the primitiveLiteralStatement
		* Write Syntax / Semantic Analysis output to file
		* @param parentID node pointer of the parent node in the AST
		* @return 0 if primitiveLiteralStatement statement doesn't exist
		* or bad primitiveLiteralStatement statement
		*
		*/
		int primitiveLiteralStatement (TreeNode *parentID);

		/**
		*
		* Checks for the letStatement
		* Write Syntax / Semantic Analysis output to file
		* @param parentID node pointer of the parent node in the AST
		* @return 0 if letStatement statement doesn't exist
		* or bad letStatement
		*
		*/
		int letStatement (TreeNode *parentID);

		/**
		*
		* Checks for the messageStatement.
		* OclStmt is checked by the calee function.
		* Write Syntax / Semantic Analysis output to file
		* @param parentID node pointer of the parent node in the AST
		* @return 0 if messageStatement statement doesn't exist
		* or bad messageStatement
		*
		*/
		int messageStatement (TreeNode *parentID);

		/**
		*
		* Checks for the ifStatement
		* Write Syntax / Semantic Analysis output to file
		* @param parentID node pointer of the parent node in the AST
		* @return 0 if ifStatement statement doesn't exist
		* or bad ifStatement
		*
		*/
		int ifStatement (TreeNode *parentID);

		/**
		*
		* Check for the correctness of arguments.
		* Need to call getNextTOkenWtapper () before calling this function.
		*
		* arguments              ::= OclStmt ( ',' OclStmt )* ;
		*
		* Write Syntax / Semantic Analysis output to file
		* @param parentID node pointer of the parent node in the AST
		* @return 0 if no arguments exists
		*
		*/
		int arguments (TreeNode *parentID);

		/**
		*
		* Check for the correctness of collectionType
		* collectionType	::= collectionTypeIdentifier '(' type ')' ;
		*
		* Write Syntax / Semantic Analysis output to file
		* @param parentID node pointer of the parent node in the AST
		* @returns 0 if no collectionType exists and 1 if exists
		*
		*/
		int collectionType (TreeNode *parentID);

		/**
		*
		* Check for the correctness of tupleType
		*
		* tupleType	::= 'TupleType' '(' variableDeclarations ')' ;
		*
		* Write Syntax / Semantic Analysis output to file
		* @param parentID node pointer of the parent node in the AST
		* @return 0 if no tupleType exists and 1 if exists
		*
		*/
		int tupleType (TreeNode *parentID);

		/**
		*
		* Wrapper to call getNextToken () function of Lexer class
		* Store the prev column number and previous line number
		* as column and line error numbers
		* @return pointer to the symbol table for tokens
		*
		*/
		SYMBOL * getNextTokenWrapper (void);

	public:
		/**
		*
		* Constructor
		* @param lrP pointer to the LexicalAnalysis class to getNextToken from Lexer
		* @param astP pointer to the AST class for Adding nodes to the current AST being built
		* @param filePAP File for writing Syntax / Semantic Analysis output
		*
		*/
		OclStmt (Lexer *lrP, AST *astP, FILE *filePAP);

		/**
		*
		* Destructor
		*
		*/
		~OclStmt ();

		/**
		*
		* Check for the correctness of oclStatement
		* Write Syntax / Semantic Analysis output to file
		* @param parentID node pointer of the parent node in the AST
		* @param currSymP pointer to the current Symbol being processed
		* @return 0 if no oclStatement exists and 1 if exists
		*
		*/
		int oclStatement (TreeNode *parentID, SYMBOL *currSymP);

		/**
		*
		* Checks for the actionStatement
		* Write Syntax / Semantic Analysis output to file
		* @param parentID node pointer of the parent node in the AST
		* @param currSymP pointer to the current Symbol being processed
		* @return 0 if actionStatement statement doesn't exist
		* or bad actionStatement
		*
		*/
		int actionStatement (TreeNode *parentID, SYMBOL *currSymP);

		/**
		*
		* Checks for the pathName
		*
		* Return Values:
		* 0 means no pathName
		* 1 means fullPathName ::= pathName '::' id
		* 2 means pathName ::= id
		*
		* Write Syntax / Semantic Analysis output to file
		* @param parentID node pointer of the parent node in the AST
		* @param currSymP pointer to the current Symbol being processed
		* @return
		* 0 i.e no pathName.
		* 1 i.e fullPathName ::= pathName '::' id.
		* 2 i.e pathName ::= id.
		*
		*/
		int pathName (TreeNode *parentID, SYMBOL *currSym);

		/**
		*
		* Check for the correctness of type
		* type	::= pathName | collectionType | tupleType ;
		*
		* Write Syntax / Semantic Analysis output to file
		* @param parentID node pointer of the parent node in the AST
		* @param currSymP pointer to the current Symbol being processed
		* @return 0 if no type exists and 1 if exists
		*
		*/
		int type (TreeNode *parentID, SYMBOL *currSymP);

		/**
		*
		* Check for the correctness of variableDeclarations
		*
		* variableDeclarations	::= VariableDeclaration	(',' variableDeclarations )? ;
		*
		* Write Syntax / Semantic Analysis output to file.
		*
		* @param parentID node pointer of the parent node in the AST
		* @param currSymP pointer to the current Symbol being processed
		* @return 0 if no variableDeclarations exists or 1 if exists
		*
		*/
		int variableDeclarations (TreeNode *parentID, SYMBOL *currSymP);

		/**
		*
		* Check for the correctness of variableDeclaration
		*
		* VariableDeclaration 	::= ID (':' type)? ( '=' OclStmt )? ;
		*
		* Write Syntax / Semantic Analysis output to file
		* @param parentID node pointer of the parent node in the AST
		* @param currSymP pointer to the current Symbol being processed
		* @return 0 if no variableDeclarations exists or 1 if exists
		*
		*/
		int variableDeclaration (TreeNode *parentID, SYMBOL *currSymP);

		/**
		*
		* Get pointer to the previous ID
		* @return pointer to the previous ID of the node
		*
		*/
		TreeNode * OclStmt :: getPrevID (void);

		/**
		*
		* Get pointer to the current symbol
		* @return pointer to the current symbol table for tokens
		*
		*/
		SYMBOL * OclStmt :: getCurrSym (void);

		/**
		*
		* Get pointer to the current symbol
		* @return pointer to the previous symbol table for tokens
		*
		*/
		SYMBOL * OclStmt :: getPrevSym1 (void);
		
		/**
		*
		* Get pointer to the current symbol
		* @return pointer to the previous to previous symbol table for tokens
		*
		*/
		SYMBOL * OclStmt :: getPrevSym2 (void);
};

#endif // _OCLSTMT_H_
