#ifndef _ERROR_H_
#define _ERROR_H_

#include "global.h"

/**
*
* Error numbers
*
*/
#define FILE_NOT_OPEN          101
#define FILE_NOT_READ          102
#define LEXICAL_ERROR          103
#define SYNTAX_ERROR           104
#define SEMANTIC_ERROR         105

class Error
{
	public:
		/**
		*
		* Constructor
		*
		*/
		Error ();

		/**
		*
		* Destructor
		*
		*/
		~Error ();

		/**
		*
		* Prints the error
		* Compiler Should Exit using the main
		* After deleting all the allocated memory
		* @param errorStr to be printed
		* @param errorNum number of error to get more detailed information
		*
		*/
		void fatal (char *errorStr, int errorNum);

		/**
		*
		* Prints the error and current line and current column
		* @param errorStr to be printed
		* @param errorNum number of error to get more detailed information
		* @param currentLine line number where error occured
		* @param currentCol column number where error occured
		*
		*/
		void normal (char *errorStr, int errorNum, int currentLine, int currentCol);

		/**
		*
		* Prints the warning if DEBUGGING ON and current line and current column
		* @param errorStr to be printed
		* @param errorNum number of error to get more detailed information
		* @param currentLine line number where error occured
		* @param currentCol column number where error occured
		*
		*/
		void warning (char *errorStr, int errorNum, int currentLine, int currentCol);

		/**
		*
		* Get the error string according to the error number
		* @param errorNum number of error to get more detailed information
		* @return errorStr according to the erro number
		*
		*/
		char * getErrorType (int errorNum);
};

#endif /* _ERROR_H_ */
