/* ----------------------------------------------------------------------------
|
|   Filename:    lexer.h
|   Dated:       31 May, 2006
|   By:          Shahid Alam (salam3@connect.carleton.ca)
|
|   Description: Header file for lexer.cpp
|
|   TO DO:       documentation
|
 ----------------------------------------------------------------------------*/

#ifndef _LEXER_H_
#define _LEXER_H_

#include "global.h"
#include "lg.h"
#include "symtable.h"
#include "error.h"

/**
*
* Lexical Analysis (first stage) part of FRONT END.
* Used by class Parser for Syntax Analysis (second stage).
* TAB considered as one character.
*
* For string CHARACTERS = ASCII character set from 32 - 126.
*
*/
class Lexer
{
	private:
		int currentCol, currentLine;
		char lastToken[128];
		char *current, *lookahead, *laoutNum;
		char *buffer;
		int count, size;
		int buffN;

		Error err;
		LG lg;
		Symtable *st;
		FILE *in;
		FILE *fileLA, *fileLANum;

		/**
		*
		* Fills the buffer to write to fileLANum
		* @param count Count to be written to the buffer
		*
		*/
		void fillBuffLaNum (int count);

		/**
		*
		* Returns the file size
		* @param stream whose size is to be computed
		* @return size of the file stream
		*
		*/
		long fileSize (FILE *stream);

	public:
		/**
		*
		* Constructor
		* Open and process input file.
		* @param filename Name of the source file to be scanned
		*
		*/
		Lexer (int sizeOfSymbolTable);

		/**
		*
		* Destructor
		* Delete Parser and symbol table and close the input file
		*
		*/
		~Lexer ();

		/**
		*
		* Read the input file (CAL file) in buffer.
		* @param filename Name of the file to be processed
		* @param lFilename Name of the file for Lexical Analysis output
		* @return 0 on success and error number on ERROR and
		* prints the line and column number of error.
		* @see ERROR.H
		*
		*/
		int readFile (char *filename, char *lFilename);

		/**
		*
		* Process the stored buffer and returns the next token
		* It look ahead several characters beyond the lexeme
		* for a pattern before a match can be found.
		* @return pointer to the symbol table for next token
		*
		*/
		SYMBOL * getNextToken (void);

		/**
		*
		* Returns current column
		* @return Column number currently processed by the Lexical Analyser
		*
		*/
		int getCurrentCol (void);

		/**
		*
		* Returns current line
		* @Return Line number currently processed by the Lexical Analyser
		*
		*/
		int getCurrentLine (void);

		/**
		*
		* Returns pointer to the Symbol Table
		* @Return pointer to the Symbol Table
		*
		*/
		Symtable * getST (void);
};

#endif /* _LEXER_H_ */
