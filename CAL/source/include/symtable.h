/* ============================================================================
|
|   Filename:    symtable.h
|   Dated:       03 June, 2006
|   By:          Shahid Alam (shalam@ieee.org
|
|   Description: Header file for symtable.cpp
|
|   TO DO:       
|
 ----------------------------------------------------------------------------*/

#ifndef _SYMTABLE_H_
#define _SYMTABLE_H_

#include "global.h"
#include "list.h"

/**
*
* Local structure for storing symbol table entry
* for printing the list
*
*/
typedef struct
{
	char *lexeme;
	int token;
	// char *attribute; Not yet implemented
} SYMBOL;

/**
*
*   Implementation of SYMBOL TABLE for storing lexemes and tokens.
*   When a lexeme is saved, we also save the token associated with
*   the lexeme. See the figure below for the data strucrue for
*   symbol table of size M. A linear list is used to store data
*   from 0 - M and buckets (linked list) are used to store the data
*   in horizontal direction, to avoid collission. This is called
*   CHAINING.
*
*         ---
*      0 |   |
*         ---          -----------          -----------
*      1 |  -|------->|   |   |  -|------->|   |   |   |
*         ---          -----------          -----------
*      2 |   |
*         ---
*      3 |   |
*         ---          -----------          -----------          -----------
*      4 |  -|------->|   |   |  -|------->|   |   |  -|------->|   |   |   |
*         ---          -----------          -----------          -----------
*      . |   |
*      . |   |
*      . |   |
*      . |   |
*      . |   |
*      . |   |
*      . |   |
*         ---
*      M |   |
*         ---
*
*
*/
class Symtable
{
	private:
		int HASH_KEY;
		unsigned int sizeOfSymbolTable;
		
		/**
		*
		* Linear array for storing key (index / hash)
		* and buckets (linked list) for hash table.
		* Key is the index of the array
		*
		*/
		typedef struct
		{
			List bucket;   // Linked list for storing buckets in the symbol table
		} HASH_TABLE;
		HASH_TABLE *ht;

		/**
		*
		* Hash function taken from P J Weinberger C compiler
		* @param lexeme Lexeme (string) whose hash / key is to be calculated
		* @return PJW hash / key (unsigned int) of lexeme (string)
		*
		*/
		unsigned int pjwHash (const char *lexeme);

	public:
		/**
		*
		* Constructor
		* @param size Size of the symbol table
		*
		*/
		Symtable (int size);

		/**
		*
		* Destructor
		*
		*/
		~Symtable ();

		/**
		*
		* @param lexeme Lexeme to be searched
		* @return index of the entry for lexeme or -1 if lexeme is not found
		*
		*/
		SYMBOL * lookup (const char *lexeme);

		/**
		*
		* Inserts the lexeme and the token associated with lexeme
		* in the symbol table at the appropiate key / hash value.
		* Client is responsible for allocating memory for lexeme.
		* NOTE: call lookup for computing HASH_KEY before calling this function.
		* @param lexeme Lexeme to be inserted
		* @param token Token to be inserted
		* @return pointer to SYMBOL on success or 0 if lexeme already exist
		* or there is an ERROR for HASH KEY
		*
		*/
		SYMBOL * insert (const char *lexeme, const int token);

		/**
		*
		* Deletes lexeme from the table
		* @param lexeme Lexeme to be deleted
		* @return 1 on success or 0 if lexeme doesn't exist
		*
		*/
		int remove (const char *lexeme);

		/**
		*
		* Prints the complete symbol table for debugging
		* @param all If all = 1 then prints complete symbol table
		* including empty entries else prints only the filled entries.
		*
		*/
		const void print (int all);
};

#endif /* _SYMTABLE_H_ */
