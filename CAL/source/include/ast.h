/* ----------------------------------------------------------------------------
|
|   Filename:    oclstmt.h
|   Dated:       28 August, 2006
|   By:          Shahid Alam (salam3@connect.carleton.ca)
|
|   Description: Header file for Abstract Syntax Tree (AST). Contains functions
|                for manipulating an AST as described below:
|
|   TO DO:       
|
 ----------------------------------------------------------------------------*/

#ifndef _AST_H_
#define _AST_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "intlist.h"
#include "list.h"
#include "lg.h"
#include "symtable.h"

/**
*
* Class TreeNode. Data structure for a Node in the Tree.
* It defines and implements a TreeNode class. Tree Node class
* keeps an ID for each node which makes it easy to search the node
* in the Tree. The ID is kept as a list of integers to store number
* bigger than 9. To let parent have more than 9 children.
* See figure below:
*
*
*                   0
*                  /|\\\...\
*                 / |       \
*                /  |        \
*               /   |         \
*              /    |          \
*             1     2 ........ 30
*            /|\               /|\\...\
*           / | \             / |      \
*          /  |  \           /  |       \
*        1,1 1,2 1,3      30,1 30,2 .. 30,12
*        / \                           /   \
*       /   \                         /     \
*      /     \                       /       \
*     /       \                     /         \
*  1,1,1     1,1,2              30,12,1    30,12,2
*   .   .      .   .                      .       .
*  .     .    .     .                    .         .
* .       .  .       .                  .           .
*
*/
class TreeNode : private List
{
	private:
		/**
		*
		* ID ::= parentID childCount (path in the tree).
		* Using the ID it's easy to search for a node in the tree.
		* ID is implemented as a list of integers, to represent
		* nodes count greater than 9.
		*
		*/
		IntList *ID;

		/**
		*
		* Count of children
		*
		*/
		int childCount;

		/**
		*
		* Pointer to the value stored in the list as string
		*
		*/
		char *value;

		/**
		*
		* Pointer to parent of the tree node
		*
		*/
		TreeNode *parent;

		/**
		*
		* List of child of the tree node
		*
		*/
		List *child;

	public:
		/**
		*
		* Constructor
		* @param lbl pointer points to the value of the Tree Node
		* @see value
		*
		*/
		TreeNode (const char *val)
		{
			childCount = 0;
			value = (char *)val;
			parent = 0;
			ID = new IntList ();
			child = new List ();
		};

		/**
		*
		* Destructor function
		*
		*/
		~TreeNode ()
		{
			childCount = 0;
			value = NULL;
			if (parent != NULL)
				parent->childCount--;
			delete ID;
			delete child;
		};

		/**
		*
		* Sets the ID of the Tree Node.
		* @param ptr pointer to be set as parent Node in the Tree
		* @see TreeNode
		*
		*/
		void setID (int val)
		{
			ID->append (val);
			return;
		};

		/**
		*
		* Gets the ID of the Tree Node.
		* @return ID of the Tree Node
		* @see TreeNode
		*
		*/
		IntList * getID (void)
		{
			return ID;
		};

		/**
		*
		* Sets the parent of the Tree Node.
		* @param ptr pointer to be set as parent Node in the Tree
		* @see TreeNode
		*
		*/
		void setParent (TreeNode *ptr)
		{
			parent = ptr;
			return;
		};

		/**
		*
		* Gets the parent of the Tree Node
		* @return pointer to TreeNode
		* @see TreeNode
		*
		*/
		TreeNode * getParent () const
		{
			return parent;
		};

		/**
		*
		* Sets the value to the new value
		* @param pvalue pointer points to the new value
		* @see value
		*
		*/
		void setValuePtr (const char *pvalue)
		{
			value = (char *)pvalue;
			return;
		};

		/**
		*
		* Gets the value of the data.
		* @return pointer to the value in the Tree Node
		* @see value
		*
		*/
		const char * getValuePtr () const
		{
			return value;
		};

		/**
		*
		* Adds child to the Tree Node.
		* @param chd pointer to be added as child Node to the Tree Node
		* @see TreeNode
		*
		*/
		void addChild (TreeNode *chd)
		{
			childCount++;
			if (chd->parent)
			{
				IntList *id = chd->parent->ID;
				if (id->getFirst () > 0)
					chd->ID->appendList (chd->parent->ID);
			}
			chd->ID->append (childCount);
			child->append ((const char *)chd);

			return;
		};

		/**
		*
		* Initilizes the iterator for the list of children
		*
		*/
		void initChildIterator (void)
		{
			child->initIterator ();
		};

		/**
		*
		* Gets the next child.
		* Make sure to call initChildIterator () before calling this function.
		* @see initChildIterator
		* @return pointer to the node of next child
		*
		*/
		TreeNode * getNextChild (void)
		{
			TreeNode *node = NULL;
			node = (TreeNode *)child->iterate ();

			return node;
		};

		/**
		*
		* Gets the child with the given id
		* @param id node id for the child to search for
		* @return pointer to the node of child with the given id
		*
		*/
		TreeNode * getChild (IntList *id)
		{
			TreeNode *node = NULL;
			int intID = id->getLast ();
			int count = id->getCount ();

			child->initIterator ();
			for (int i = 0; i < childCount; i++)
			{
				node = (TreeNode *)child->iterate ();
				IntList *nID = node->getID ();

				if (count == nID->getCount ())
				{
					int nIntID = nID->getLast ();
					if (intID == nIntID)
						break;
					else if (intID < nIntID)
					{
						node = NULL;
						break;
					}
				}
				node = NULL;
			}
			return node;
		};

		/**
		*
		* Gets number of children
		* @return number of children of this node
		*
		*/
		int getNumberOfChildren (void)
		{
			return childCount;
		};

		/**
		*
		* Removes child from the list. It iterates and find the
		* child to be removed and then pass it to the List
		* function removeIterator () to be removed.
		*
		* Iterator is adjusted after calling this function.
		* So need to init the iterator after calling this function.
		*
		* @param id node id to be removed from the list of children
		* @return 1 if removed else 0
		*
		*/
		int removeChildFromList (IntList *id)
		{
			int removed = 0;
			TreeNode *node = NULL;

			child->initIterator ();
			for (int i = 0; i < childCount; i++)
			{
				node = (TreeNode *)child->iterate ();
//				if (node->getChild (id) != NULL)
				if (node->getID ()->getCount() == id->getCount())
				{
					child->removeIterator ();
					removed = 1;
					break;
				}
			}
			
			return removed;
		};

};


/**
*
* Class AST. Implements an AST (Abstract Syntax Tree)
*
*/
class AST
{
	private:
		/**
		*
		* pointers for the root node in the Tree.
		* @see TreeNode
		*
		*/
		TreeNode *root;

		/**
		*
		* Get Node from the Tree using the ID
		* @param id ID to be searched for the Node
		* @return TreeNode with the given ID
		*
		*/
		TreeNode * getNode (IntList *id);

		/**
		*
		* Removes Child Node including all the children
		* @param node child node to be removed from the AST
		*
		*/
		void removeChild (TreeNode *node);

		/**
		*
		* Prints all the children of the AST node for debugging using ID's.
		* Recursively call this function to print all the childs.
		* @param depth depth of the child node for printing spaces
		* @param node child node to be printed including all it's children
		* @param file File pointer to write to, if NULL then
		*  write to stdout
		* @param sym Pointer to the Symbol Table for printing tokens
		* @param lg Class LG (Language grammar class)
		*
		*/
		void printChildrenWithToken (int depth, TreeNode *node, FILE *file, Symtable *sym, LG lg) const;

		/**
		*
		* Prints all the children of the AST node for debugging using ID's.
		* Recursively call this function to print all the childs.
		* @param depth depth of the child node for printing spaces
		* @param node child node to be printed including all it's children
		* @param file File pointer to write to, if NULL then
		*  write to stdout
		*
		*/
		void printChildren (int depth, TreeNode *node, FILE *file) const;

		/**
		*
		* Remove Root Node including all the children.
		*
		*/
		void removeAll (void);

		/**
		*
		* Check if AST is empty or not
		* @return 0 if AST is empty else non zero
		*
		*/
		int isempty (void) const
		{
			return root == 0;
		};

	public:
		/**
		*
		* Constructor
		*
		*/
		AST ();

		/**
		*
		* Destructor
		*
		*/
		~AST ();

		/**
		*
		* Insert root of the AST
		* @param value pointer points to the value of the root node of the AST
		* @see value
		*
		*/
		TreeNode * insertRoot (const char *cvalue);

		/**
		*
		* Add child node to the parent node. Make sure you add the nodes
		* with correct ID's.
		* @param pnode pointer of the parent node where child is to be added
		* @param cvalue pointer points to the value of child node to be added
		* @return pointer of the node which has been added
		*
		*/
		TreeNode * addNode (TreeNode *pnode, const char *cvalue);

		/**
		*
		* Removes the node from the AST and it's children recursively
		* @param id node pointer to be removed
		*
		*/
		void removeNode (TreeNode *node);

		/**
		*
		* Change the lable of the node to new value
		* @param id pointer to the node
		* @param value pointer points to the new value
		*/
		void changeValue (TreeNode *node, const char *cvalue);

		/**
		*
		* Returns value of the node of the AST
		* @param id pointer to the node id
		* @see TreeNode:ID
		* @return pointer to the value (as string) for the Node
		*/
		const char * getValue (IntList *id);

		/**
		*
		* Prints the AST  using ID's starting from the root node and
		* calling printChildren() recursively to print all the children
		* @param file File pointer to write to, if NULL then
		*  write to stdout
		* @param sym Pointer to the Symbol Table for printing tokens
		*
		*/
		void printWithToken (FILE *file, Symtable *sym) const;

		/**
		*
		* Prints the AST  using ID's starting from the root node and
		* calling printChildren() recursively to print all the children
		* @param file File pointer to write to, if NULL then
		*  write to stdout
		*
		*/
		void print (FILE *file) const;
};

#endif // _AST_H_
