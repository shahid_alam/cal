/* ----------------------------------------------------------------------------
|
|   Filename:    parser.h
|   Dated:       09 June, 2006
|   By:          Shahid Alam (salam3@connect.carleton.ca)
|
|   Description: Header file for parser.cpp
|
|   TO DO:       documentation
|
 ----------------------------------------------------------------------------*/

#ifndef _PARSER_H_
#define _PARSER_H_

#include "global.h"
#include "lg.h"
#include "symtable.h"
#include "error.h"
#include "lexer.h"
#include "ast.h"
#include "oclstmt.h"

/**
*
* Parser for Syntax Analysis (second stage).
* Checking that the tokens form an allowable expression.
* Receives token from Lexer class and process them to
* create AST (Abstract Syntacx Tree)
*
*/
class Parser
{
	private:
		/**
		*
		* Keeps pointers to two previous symbols
		*
		*/
		SYMBOL *currSym, *prevSym1, *prevSym2;
		TreeNode *prevID;
		Error err;
		LG lg;
		Lexer *lr;
		AST *ast;
		OclStmt *oclStmt;
		FILE *filePA;

		/**
		*
		* Get next token and check it for
		*
		* ATTRORASSOCCONTEXT | CLASSIFIERCONTEXT | OPERATIONCONTEXT
		*
		* Then appropiately calling the required function to process
		* the particular context. It also changes the context's label
		* in the AST according to particular context detected.
		*
		* Write Syntax / Semantic Analysis output to file
		*
		* @param parentID node pointer of the parent node in the AST
		* @return 1 on error and 0 on no error
		*
		*/
		int processContext (TreeNode *parentID);

		/**
		*
		* Checks for the attrOrAssocContext statement
		* Write Syntax / Semantic Analysis output to file
		* @param parentID node pointer of the parent node in the AST
		* @return 0 if attrOrAssocContext statement doesn't exist
		* or bad attrOrAssocContext statement
		*
		*/
		int attrOrAssocContext (TreeNode *parentID);

		/**
		*
		* Checks for the classifierContext statement
		* Write Syntax / Semantic Analysis output to file
		* @param parentID node pointer of the parent node in the AST
		* @return 0 if classifierContext statement doesn't exist
		* or bad classifierContext statement
		*
		*/
		int classifierContext (TreeNode *parentID);

		/**
		*
		* Checks for the operationContext statement
		* Write Syntax / Semantic Analysis output to file
		* @param parentID node pointer of the parent node in the AST
		* @return 0 if operationContext statement doesn't exist
		* or bad operationContext statement
		*
		*/
		int operationContext (TreeNode *parentID);

		/**
		*
		* Check for the correctness of oclStatement.
		* DefStmt	::= variableDeclaration '=' OclStmt
		*				| operationName '=' OclStmt ;
		*
		* Write Syntax / Semantic Analysis output to file.
		* @param parentID node pointer of the parent node in the AST
		* @return 0 if no defStatement exists
		*
		*/
		int Parser :: defStatement (TreeNode *parentID);

		/**
		*
		* Check for the correctness of operationName
		* Write Syntax / Semantic Analysis output to file
		* @param parentID node pointer of the parent node in the AST
		* Returns 0 if no context exists
		*
		*/
		int Parser :: operationName (TreeNode *parentID);

		/**
		*
		* Sets the ID (prevID) and symbols (currSym, prevSym1 and prevSym2)
		* after calling any of the functions from OclStmt class.
		*
		*/
		void Parser :: setSymID (void);

		/**
		*
		* Wrapper to call getNextToken () function of Lexer class
		* Store the prev column number and previous line number
		* as column and line error numbers
		* @return pointer to the symbol table for tokens
		*
		*/
		SYMBOL * getNextTokenWrapper (void);

	public:
		/**
		*
		* Constructor
		*
		*/
		Parser ();

		/**
		*
		* Destructor
		*
		*/
		~Parser ();

		/**
		*
		* Read the input file into buffer, if error return 1
		* else open other files and continue Parsing
		* @param filename Name of the file to be processed
		* @param lFilename Name of the file for Lexical Analysis output
		* @param pFilename Name of the file for Parser (Semantic / Syntax Analysis) output
		* @return 1 on error and 0 on success
		*
		*/
		int readFileInBuffer (char *fileName, char *lFilename, char *pFilename);

		/**
		*
		* Build AST and write Lexical Analysis output to file
		* Write Syntax / Semantic Analysis output to file
		* @return 1 on error and 0 on success
		*
		*/
		int buildAST (void);

		/**
		*
		* Printd all the tokens with line and column number for debugging
		*
		*/
		void printTokensWithLineAndColNumber (void);
};

#endif /* _PARSER_H_ */
