/* ----------------------------------------------------------------------------
|
|   Filename:    intlist.h
|   Dated:       03 September, 2006
|   By:          Shahid Alam (salam3@connect.carleton.ca)
|
|   Description: Header file for Integer Linked List class in intlist.cpp
|
|   TO DO:       
|
 ----------------------------------------------------------------------------*/

#ifndef _INTLIST_H_
#define _INTLIST_H_

#include <stdio.h>

/**
*
* Class ListNode. Data structure for a Node in the List.
*
*/
class INT
{
	public:						
		/**
		*
		* Value of integer
		*
		*/
		int value;

		/**
		*
		* Next and Previous pointers
		*
		*/
		INT *nextptr;

		/**
		*
		* Constructor
		* @param dPtr data pointer points to the data stored in the Node
		* @see dataPtr
		*
		*/
		INT (int val)
		{
			value = val;
			nextptr = 0;
		};
};


/**
*
* Class IntList. Implements an Integer linked list
*
*/
class IntList
{
	private:
		/**
		*
		* Count of the integers in ht elist.
		*
		*/
		int count;

		/**
		*
		* pointers for first and last Node in the Integer List,
		* and pointer for iteration.
		* @see INT
		*
		*/
		INT *firstptr, *lastptr, *iterator;

		/**
		*
		* Dynamically allocates memory for the new Node.
		* @param val integer value to be stored in the node
		* @see dataPtr
		*
		*/
		INT *getNewNode (int val);

	public:
		/**
		*
		* Constructor
		*
		*/
		IntList ();

		/**
		*
		* Destructor
		* Remove the complete integer list from memory.
		*
		*/
		~IntList ();

		/**
		*
		* Appends an Integer List. If it is empty new Node will be added 
		* and the pointer of the Node will be assigned to first else 
		* it will be assigned to last and the previous last pointer's 
		* next pointer will be set to the newptr.
		* @param val integer value to be stored in the node
		* @see INT::val
		*
		*/
		void append (int val);

		/**
		*
		* Appends an Integer List at the end.
		* It copies the integer list to be appended and append it at the end.
		* @param list integer list to be appended at the end
		*
		*/
		void appendList (IntList *list);

		/**
		*
		* Returns first value in the integer list
		* @return value of the first integer in the integer list
		*
		*/
		int getFirst (void);

		/**
		*
		* Returns last value in the integer list
		* @return value of the last integer in the integer list
		*
		*/
		int getLast (void);

		/**
		*
		* Gets the number of integers in the list
		* @return the number of integers in the list
		*
		*/
		int IntList :: getCount (void);

		/**
		*
		* Removes the last integer in the list. It starts with the 
		* firstptr then reach to the lastptr keeping track of the 
		* previous pointer because then that becomes the lastptr 
		* when lastptr is removed. It just delete the pointer.
		*
		*/
		void removeLast (void);

		/**
		*
		* Initializes the iterator
		*
		*/
		void initIterator (void);

		/**
		*
		* Iterates the list and returns the value as it iterates
		* Everytime it is called it increments the iterator.
		* @return NULL at the end of the list
		*
		*/
		int iterate (void);

		/**
		*
		* Check if Integer List is empty or not
		* @return 0 if Integer List is empty
		*
		*/
		int isempty (void) const
		{
			return firstptr == 0;
		};

		/**
		*
		* Prints the list for debugging.
		* @param file File pointer to write to
		*
		*/
		void print (FILE *stream) const;
};

#endif // _INTLIST_H_
