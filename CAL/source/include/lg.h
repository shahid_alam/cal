/* ----------------------------------------------------------------------------
|
|   Filename:    lg.h
|   Dated:       23 May, 2006
|   By:          Shahid Alam (shalam@ieee.org
|
|   Description: Header file to be included to acces langugae grammar defined
|                in file CALGrammar.txt
|
|   TO DO:       
|
 ----------------------------------------------------------------------------*/

#ifndef _LG_H_
#define _LG_H_

/**
*
* Language Grammar class / structure to store tokens defined in the language grammar
*
*/
class LG
{
	public:
		/**
		*
		* Constructor
		* 
		*/
		LG () { };

		/**
		*
		* Destructor
		*
		*/
		~LG () { };

		/**
		*
		* Tokens (ID defined)
		*
		*/
		enum TOKENS
		{
			/**
			*
			* Ignored tokens
			*
			* WS				// ::= blank | tab | newline
			* COMMENT 			// ::= '--' blank | tab | character | comment* newline
			*
			*/
			/**
			*
			* Tokens used
			*
			*
			*/
			ID = 0,				// ::= letter+ allowedChar*
			NUM,				// ::= digit+ ( '.' digit* )?
								// ( ('e' | 'E') ('+' | '-')? digit+ )?
			COMMA,				// ::= ','
			COLON,				// ::= ':'
			SCOLON,				// ::= ';'
			DCOLON,				// ::= '::'
			QMARK,              // ::= '?'
			TRUE,               // ::= 'true'
			FALSE,              // ::= 'false'
			LOP,				// ::= 'and' | 'or' | 'xor' | 'implies'
			ROP,				// ::= '=' | '<' | '>' | '<=' | '>=' | '<>'
			NOT,				// ::= 'not'
			MUL,				// ::= '*'
			DIV,				// ::= '/'
			PLUS,				// ::= '+'
			MINUS,				// ::= '-'
			DAOP,				// ::= '->' | '.'
			MSGOP,				// ::= '^' | '^^'
			RANGEOP,			// ::= '..'
			CTID,				// ::= 'Set' | 'Bag' | 'Sequence' | 'Collection' | 'OrderedSet' | 'Dag'
			STRING,				// ::= '"' character* '"'
			LBRKT1,				// ::= '('
			RBRKT1,				// ::= ')'
			LBRKT2,				// ::= '['
			RBRKT2,				// ::= ']'
			LBRKT3,				// ::= '{'
			RBRKT3,				// ::= '}'
			PIPE,				// ::= '|'
			ATPRE,				// ::= '@pre'
			TUPLE,				// ::= 'Tuple'
			TUPLET,				// ::= 'TupleType'
			PACKAGE,			// ::= 'package'
			ENDPACKAGE,			// ::= 'endpackage'
			CONTEXT,			// ::= 'context'
			INIT,				// ::= 'init'
			DRIVE,				// ::= 'drive'
			INV,				// ::= 'inv'
			DEF,				// ::= 'def'
			LET,				// ::= 'let'
			PRE,                // ::= 'pre'
			POST,               // ::= 'post'
			BODY,               // ::= 'body'
			ACTION,				// ::= 'action'
			ITERATE,            // ::= 'iterate'
			IF,					// ::= 'if'
			THEN,				// ::= 'then'
			ELSE,				// ::= 'else'
			ENDIF,				// ::= 'endif'
			TO,                 // ::= 'to'
			OF,                 // ::= 'of'
			VIS                 // ::= 'public' | 'private' | 'protected'
		};
		
		/**
		*
		* Return token as string
		* @param id ID of the token as defined above
		* @return String containing the token ID as string.
		* @see TOKENS
		*
		*/
		char * getToken (int id)
		{
			switch (id)
			{
				case 0:	 return "ID";
				case 1:	 return "NUM";
				case 2:	 return "COMMA";
				case 3:	 return "COLON";
				case 4:	 return "SCOLON";
				case 5:	 return "DCOLON";
				case 6:  return "QMARK";
				case 7:  return "TRUE";
				case 8:  return "FALSE";
				case 9:	 return "LOP";
				case 10: return "ROP";
				case 11: return "NOT";
				case 12: return "MUL";
				case 13: return "DIV";
				case 14: return "PLUS";
				case 15: return "MINUS";
				case 16: return "DAOP";
				case 17: return "MSGOP";
				case 18: return "RANGEOP";
				case 19: return "CTID";
				case 20: return "STRING";
				case 21: return "LBRKT1";
				case 22: return "RBRKT1";
				case 23: return "LBRKT2";
				case 24: return "RBRKT2";
				case 25: return "LBRKT3";
				case 26: return "RBRKT3";
				case 27: return "PIPE";
				case 28: return "ATPRE";
				case 29: return "TUPLE";
				case 30: return "TUPLET";
				case 31: return "PACKAGE";
				case 32: return "ENDPACKAGE";
				case 33: return "CONTEXT";
				case 34: return "INIT";
				case 35: return "DRIVE";
				case 36: return "INV";
				case 37: return "DEF";
				case 38: return "LET";
				case 39: return "PRE";
				case 40: return "POST";
				case 41: return "BODY";
				case 42: return "ACTION";
				case 43: return "ITERATE";
				case 44: return "IF";
				case 45: return "THEN";
				case 46: return "ELSE";
				case 47: return "ENDIF";
				case 48: return "TO";
				case 49: return "OF";
				case 50: return "VIS";
				default:
					return NULL;
			}
			return NULL;
		};
};

#endif /* _LG_H_ */
