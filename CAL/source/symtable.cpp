/* ============================================================================
|
|   Filename:    symtable.cpp
|   Dated:       03 June, 2006
|   By:          Shahid Alam (shalam@ieee.org
|
|   Description: Implementation of symbol table for storing lexemes and tokens.
|                When a lexeme is saved, we also save the token associated with
|                the lexeme. See the figure below for the data strucrue for
|                symbol table of size M. A linear list is used to store data
|                from 0 - M and buckets (linked list) are used to store the data
|                in horizontal direction, to avoid collission. This is called
|                CHAINING.
|
|         ---
|      0 |   |
|         ---          -----------          -----------
|      1 |  -|------->|   |   |  -|------->|   |   |   |
|         ---          -----------          -----------
|      2 |   |
|         ---
|      3 |   |
|         ---          -----------          -----------          -----------
|      4 |  -|------->|   |   |  -|------->|   |   |  -|------->|   |   |   |
|         ---          -----------          -----------          -----------
|      . |   |
|      . |   |
|      . |   |
|      . |   |
|      . |   |
|      . |   |
|      . |   |
|         ---
|      M |   |
|         ---
|
|   TO DO:       Use self-balancing binary search tree instead of linked list
|                for buckets
|
 ----------------------------------------------------------------------------*/

#include "include/symtable.h"

/**
*
* CALLBACK function prototype
*
*/
void printList (const char *);

/**
*
* Function pointer, to printList function, to be passed for CALLBACK
*
*/
void (*printListFunc) (const char *) = &printList;

/**
|
| Constructor
|
 ----------------------------------------------------------------------------*/
Symtable :: Symtable (int size)
{
	HASH_KEY = -1;
	sizeOfSymbolTable = size;
	ht = new HASH_TABLE [sizeOfSymbolTable];
}

/**
|
| Destructor
| Delete the hash table and buckets (linked list)
|
 ----------------------------------------------------------------------------*/
Symtable :: ~Symtable ()
{
	SYMBOL *sym = 0;

	for (int i = 0; i < sizeOfSymbolTable; i++)
	{
		if (!ht[i].bucket.isempty())
		{
			ht[i].bucket.initIterator ();
			sym = (SYMBOL *)ht[i].bucket.iterate ();
			while (sym != 0)
			{
				delete (sym);
				sym = (SYMBOL *)ht[i].bucket.iterate ();
			}
		}
	}
	delete[] ht;
}

/**
|
| Returns hash (int) of lexeme (string) -- From P J Weinberger C compiler
| mentioned in compiler's dragon book
|
 ----------------------------------------------------------------------------*/
unsigned int Symtable :: pjwHash (const char *lexeme)
{
	unsigned int i, g, hash = 0;
	unsigned int len = strlen (lexeme);

	for (i = 0; i < len; i++)
	{
		//
		// shift hash 3 bits left and add lexeme[i]
		//
		hash = (hash << 3) ^ lexeme[i];
		//
		// get the top 4 bits of hash
		// and if the top 4 bits are not zero
		//
		if (g = hash & 0xf0000000)        
		{
			//
			// move them to the low end of hash
			//
			hash = hash ^ (g << 24);
			hash = hash ^ g;
		}
	}

	//
	// keep the hash within maximum table size
	//
	return (hash % SIZE_OF_SYMBOL_TABLE);
}

/**
|
| Returns pointer to SYMBOL on success and 0 if lexeme already exists
| or there is an ERROR for HASH KEY
| Client is responsible for allocating memory for string lexeme
| NOTE: It calls lookup before inserting the token and lexeme
|
 ----------------------------------------------------------------------------*/
SYMBOL * Symtable :: insert (const char *lexeme, const int token)
{
	//
	// Check if lexeme exists or not
	// if not then insert the lexeme with token
	//
	SYMBOL *symL;
	symL = lookup (lexeme);
	if (symL <= 0)
	{
		if (HASH_KEY >= 0)
		{
			symL = new SYMBOL ();
			symL->lexeme = (char *)lexeme;
			symL->token = token;
			ht[HASH_KEY].bucket.append ((const char *)symL);
			HASH_KEY = -1;
			return symL;
		}
		else
		{
			fprintf (stderr, "ERROR :: Symtable :: insert: ");
			fprintf (stderr, "Not able to insert lexeme %s and token %d\n", lexeme, token);
			return 0;
		}
	}

	return symL;
}

/**
|
| Returns index of the lexeme, and returns key if lexeme doesn't exist
| and returns 0 if lexeme does exist. Also compute HASH KEY and returns
| in global parameter HASH_KEY.
|
 ----------------------------------------------------------------------------*/
SYMBOL * Symtable :: lookup (const char *lexeme)
{
	SYMBOL *symL;
	//
	// Compute the hash of the lexeme
	//
	HASH_KEY = pjwHash (lexeme);

	//
	// Search for the lexeme in the Linked List
	// using the iterator function of the List.
	// First call to iterate returns the first pointer
	// Successive calls return next pointer.
	// 0 marks the end of the Linked List.
	//
	// if lexeme not present then return the key
	// else return -1
	//
	ht[HASH_KEY].bucket.initIterator ();
	symL = (SYMBOL *)ht[HASH_KEY].bucket.iterate ();
	while (symL != 0)
	{
		if (!strcmp (symL->lexeme, lexeme))
			return symL;
		symL = (SYMBOL *)ht[HASH_KEY].bucket.iterate ();
	}

	return 0;
}

/**
|
| Deletes lexeme from the symbol table
| Returns 1 on success and 0 if lexeme doesn't exist
|
 ----------------------------------------------------------------------------*/
int Symtable :: remove (const char *lexeme)
{
	SYMBOL *symL;
	//
	// Compute the hash of the lexeme
	//
	unsigned int key = pjwHash (lexeme);

	ht[key].bucket.initIterator ();
	symL = (SYMBOL *)ht[key].bucket.iterate ();
	while (symL != 0)
	{
		if (!strcmp (symL->lexeme, lexeme))
		{
			ht[key].bucket.removeIterator ();
			return 1;
		}
		symL = (SYMBOL *)ht[key].bucket.iterate ();
	}

	return 0;
}

/**
|
| Prints complete symbol table for debugging
|
 ----------------------------------------------------------------------------*/
const void Symtable :: print (int all)
{
	for (int key = 0; key < sizeOfSymbolTable; key++)
	{
		if (!ht[key].bucket.isempty ())
		{
			fprintf (stdout, "KEY / HASH: %5d\n", key);
			fprintf (stdout, "       --- Printing Nodes ---\n");
			ht[key].bucket.print (printListFunc);
		}
		else if (all == 1)
			fprintf (stdout, "KEY / HASH: %5d   EMPTY\n", key);
	}
}

/**
|
| CALLBACK function, to be passed to the print function of Class List for
| CALLBACK. It prints the data stored in the list for debugging
|
 ----------------------------------------------------------------------------*/
void printList (const char *dPtr)
{
	fprintf (stdout, "       TOKEN: %3d LEXEME: %s\n", ((SYMBOL *)dPtr)->token, ((SYMBOL *)dPtr)->lexeme);
}
