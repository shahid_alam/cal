/* ============================================================================
|
|   Filename:    list.cpp
|   Dated:       26 Feburary, 2006
|   By:          Shahid Alam (shalam@ieee.org
|
|   Description: Implementation of Linked List class
|
|   TO DO:       
|
 ----------------------------------------------------------------------------*/

#include "include/list.h"

/**
//
// Constructor function.
//
 ----------------------------------------------------------------------------*/
List :: List ()
{
	firstptr = iterator = previterator = lastptr = 0;
}

/**
//
// Destructor function.
// It just delete the pointer for the data
// but not the data itself.
//
 ----------------------------------------------------------------------------*/
List :: ~List ()
{
	if (!isempty ())
	{
		ListNode *tempptr = NULL;
		ListNode *currentptr = firstptr;
		while (currentptr != lastptr)
		{
			tempptr = currentptr;
			currentptr = currentptr-> getNextPtr ();
			//
			// --- NOTE ---
			// If want to delete the data just uncomment this line
			//
			//delete (tempptr->getDataPtr());
			delete tempptr;
		}
		delete currentptr;
	}
}

/**
//
// Dynamically allocates memory for the new Node.
//
 ----------------------------------------------------------------------------*/
ListNode * List :: getNewNode (const char *dPtr)
{
	ListNode *ptr = new ListNode (dPtr);
	return ptr;
}

/**
//
// Returns first pointer
//
 ----------------------------------------------------------------------------*/
void * List :: getFirst (void)
{
	return firstptr;
}

/**
//
// Returns next pointer from a current pointer
//
 ----------------------------------------------------------------------------*/
void * List :: getNext (void * cPtr)
{
	ListNode *tempPtr = (ListNode *)cPtr;
	return tempPtr-> getNextPtr ();
}

/**
//
// Returns data pointer
//
 ----------------------------------------------------------------------------*/
const char * List :: getData (void *nPtr)
{
	ListNode *tempPtr = (ListNode *)nPtr;
	return tempPtr->getDataPtr ();
}

/**
//
// Prints the list for debugging. CALLBACK the function passed
// to it to let the callee print the data. Because data can have
// different strucure.
//
 ----------------------------------------------------------------------------*/
void List :: print (void(*fPtr)(const char *)) const
{
	if (!isempty ())
	{
		ListNode *nextptr = firstptr;
		fPtr (nextptr->getDataPtr ());
		while (nextptr != lastptr)
		{
			nextptr = nextptr-> getNextPtr ();
			fPtr (nextptr->getDataPtr ());
		}
		fprintf (stdout, "\n");
	}
}

/**
//
// Initialize iterator
//
 ----------------------------------------------------------------------------*/
void List :: initIterator (void)
{
	iterator = 0;
	previterator = 0;
}

/**
//
// Iterates the list and returns the data pointer as it iterates
// Everytime it is called it increments the iterator and returns
// 0 at the end of the list
//
 ----------------------------------------------------------------------------*/
const char * List :: iterate (void)
{
	if (!isempty ())
	{
		if (iterator == 0)
		{
			iterator = firstptr;
			previterator = 0;
		}
		else if (iterator != lastptr)
		{
			previterator = iterator;
			iterator = iterator->getNextPtr ();
		}
		else
			return 0;

		return (iterator->getDataPtr ());
	}

	return 0;
}

/**
//
// Appends a List. If it is empty new Node will be added
// and the pointer of the Node will be assigned to first else
// it will be assigned to last and the previous last pointer's
// next pointer will be set to the newptr.
//
 ----------------------------------------------------------------------------*/
void List :: append (const char *dPtr)
{
	ListNode *newptr = getNewNode (dPtr);

   	if (isempty ())
		firstptr = newptr;
	else
		lastptr-> setNextPtr (newptr);
	lastptr = newptr;
}

/**
//
// Removes the last Node of the list. It starts with the
// firstptr then reach to the lastptr keeping track of the
// previous pointer because then that becomes the lastptr
// when lastptr is removed.
// It just delete the pointer for the data
// but not the data itself.
//
 ----------------------------------------------------------------------------*/
void List :: removeLast (void)
{
	if (!isempty ())
	{
		ListNode *tempptr = firstptr;
		if (firstptr == lastptr)
			firstptr = iterator = previterator = lastptr = 0;
		else if (iterator == lastptr)
		{
			tempptr = iterator;
			lastptr = previterator;
			iterator = previterator = 0;
		}
		else
		{
			ListNode *nextptr = NULL;
			while (tempptr != lastptr)
			{
				nextptr = tempptr-> getNextPtr ();
				if (nextptr == lastptr)
				{
					lastptr = tempptr;
					tempptr = nextptr;
					break;
				}
				tempptr = nextptr;
			}
		}
		//
		// --- NOTE ---
		// If want to delete the data just uncomment this line
		//
//		delete (tempptr->getDataPtr());
		delete tempptr;
	}
}

/**
//
// Removes the current node (iterator) of the List.
// It just delete the pointer for the data
// but not the data itself.
//
 ----------------------------------------------------------------------------*/
void List :: removeIterator (void)
{
	if (iterator != 0)
	{
		if (iterator == lastptr)
			removeLast ();
		else
		{
			ListNode *nextptr = NULL;
			ListNode *tempptr = iterator;
			if (iterator == firstptr)
			{
				nextptr = iterator-> getNextPtr ();
				firstptr = nextptr;
				iterator = firstptr;
			}
			else
			{
				//
				// move iterator one node and set nextptr for previterator
				//
				nextptr = iterator-> getNextPtr ();
				iterator = nextptr;
				previterator-> setNextPtr (nextptr);
			}
			//
			// --- NOTE ---
			// If want to delete the data just uncomment this line
			//
//			delete (tempptr->getDataPtr());
			delete tempptr;
		}
	}
}

/**
//
// It counts the number of Nodes in the List by counting
// the linking pointers of the List using getNextPtr ()
// function.
//
 ----------------------------------------------------------------------------*/
long int List :: count (void) const
{
	long int n = 0;

	if (!isempty ())
	{
		ListNode *tempptr = firstptr;
		while (tempptr != lastptr)
		{
			n = n + 1;
			tempptr = tempptr-> getNextPtr ();
		}
		n = n + 1;
	}

	return n;
}

/**
//
// It adds a Node with a data pointer(dPtr) at the given position(posn).
// If the user gives the position as -1 it will however add the
// Node but with a warning. Position '0' is for first Node.
//
 ----------------------------------------------------------------------------*/
void List :: addN (const char *dPtr, long int posn)
{
	if (posn < 0)
	{
		fprintf (stderr, "Warning -- Attempt to add before the beginning");
		fprintf (stderr, " of the list.\nAdding as the first node\n");
	}
	if (isempty() || posn >= count())
 		append (dPtr);
	else if (posn <= 0)
	{
		ListNode *tempptr = NULL;
		ListNode *newptr = getNewNode (dPtr);
		newptr-> setNextPtr (firstptr);
		firstptr = newptr;
	}
	else
	{
		ListNode *tempptr = NULL;
		ListNode *newptr = getNewNode (dPtr);
		ListNode *currentptr = firstptr;
		for (long int n = 1;n < posn;n++)
			currentptr = currentptr-> getNextPtr ();
		tempptr = currentptr-> getNextPtr (); 
		currentptr-> setNextPtr (newptr);
		newptr-> setNextPtr (tempptr);
	}
}
