/* ============================================================================
|
|   Filename:    parser.cpp
|   Dated:       31 May, 2006
|   By:          Shahid Alam (shalam@ieee.org
|
|   Description: 
|
|   TO DO:       See below
|
 ----------------------------------------------------------------------------*/

#include "include/parser.h"

/**
|
| Constructor
|
 ----------------------------------------------------------------------------*/
Parser :: Parser ()
{
	currSym = prevSym1 = prevSym2;
	lr = new Lexer (SIZE_OF_SYMBOL_TABLE);
	ast = new AST ();
	filePA = NULL;
}

/**
|
| Destructor
|
 ----------------------------------------------------------------------------*/
Parser :: ~Parser ()
{
#ifdef _DEBUG_
	fprintf (stdout, "<PARSER_OUTPUT>\n");
	ast->print (NULL);
	fprintf (stdout, "</PARSER_OUTPUT>\n");
	fflush (stdout);
#endif

	if (_PRINT_SEMANTIC_SYNTAX_ANALYSIS_ == 1 && filePA != NULL)
	{
		fprintf (filePA, "<SyntaxTree>");
		fflush (filePA);
		ast->printWithToken (filePA, lr->getST ());
		fprintf (filePA, "</SyntaxTree>");
		fflush (filePA);
		fclose (filePA);
	}

	delete lr;
	delete ast;
}

/**
|
| Print all the tokens with line and column number for debugging
|
 ----------------------------------------------------------------------------*/
void Parser :: printTokensWithLineAndColNumber (void)
{
	fprintf (stdout, "CL = Current Line, CC = Current Column\n");
	fprintf (stdout, "%12s:%10s :: %2s:%2s\n", "TOKEN", "LEXEME", "CL", "CC");
	currSym = lr->getNextToken ();
	while (currSym != NULL)
	{
		fprintf (stdout, "%12s:%10s :: %2d:%2d\n", lg.getToken (currSym->token), currSym->lexeme, lr->getCurrentLine (), lr->getCurrentCol ());
		currSym = lr->getNextToken ();
	}
}

/**
|
| Read the input file into buffer
| if error return 1
| else open other files and continue Parsing
|
 ----------------------------------------------------------------------------*/
int Parser :: readFileInBuffer (char *fileName, char *lFilename, char *pFilename)
{
	int error = 0;

	//
	// Read the input file
	// if error return 1
	// else open other files and continue Parsing
	//
	error = lr->readFile (fileName, lFilename);
	if (error == 0)
	{
		if (_PRINT_SEMANTIC_SYNTAX_ANALYSIS_ == 1 && (filePA = fopen (pFilename, "wat")) == NULL)
		{
			char errStr[128];
			sprintf (errStr, "Parser: Cannot open Parser output file %s.\n", pFilename);
			err.warning (errStr, FILE_NOT_OPEN, 0, 0);
		}
		oclStmt = new OclStmt (lr, ast, filePA);
	}

	return error;
}

/**
|
| Build AST
|
| Write Syntax / Semantic Analysis output to file
|
| Check for the first two tokens which are part of package statement as:
| PACKAGE ID . . .
|
| Returns 1 on error or 0 on no error
|
 ----------------------------------------------------------------------------*/
int Parser :: buildAST (void)
{
	int error = 0, SUCCESS = 0;

	//
	// Parsing Starts
	// Getting the first token
	// Checking it for PACKAGE
	// if error returns 1
	//
	// First time call, so set the
	// pointers for two previous tokens
	//
	getNextTokenWrapper ();

	//
	// Insert the root node
	//
	TreeNode *rootID;
	if (currSym != NULL)
		rootID = ast->insertRoot ("CAL_AST");
	else
	{
		fprintf (stderr, "<SyntaxError line='1', col='1' />\n");
		error = 1;
	}

	while (currSym != NULL)
	{
		if (currSym->token == lg.PACKAGE)
		{
			//
			// Insert package node
			//
			TreeNode *packageID = ast->addNode (rootID, currSym->lexeme);

			//
			// Getting next token
			// Checking it for PATHNAME (i.e name of the package)
			// if error returns 1
			//
			getNextTokenWrapper ();
			if (!oclStmt->pathName (packageID, currSym))
			{
				setSymID ();
				err.normal ("Parser->buildAST: Name of the package", SYNTAX_ERROR, lr->getCurrentLine(), lr->getCurrentCol());
				error = 1;
				return error;
			}
			setSymID ();
	
			while (currSym != NULL)
			{
				//
				// Getting next token
				// Checking it for CONTEXT
				// if error returns 1
				//
				if (currSym->token != lg.CONTEXT)
				{
					err.normal ("Parser->buildAST: Context statement missing OR Comments Syntax Error", 
																SYNTAX_ERROR, lr->getCurrentLine(), lr->getCurrentCol());
					error = 1;
					return error;
				}
	
				if (processContext (packageID))
				{
					err.normal ("Parser->buildAST: Context statement error", SYNTAX_ERROR, lr->getCurrentLine(), lr->getCurrentCol());
					error = 1;
					return error;
				}
	
				if (currSym != NULL && currSym->token == lg.ENDPACKAGE)
				{
					SUCCESS = 1;
					break;
				}
			}
		}
		else
		{
			err.normal ("Parser->buildAST: Package statement missing", SYNTAX_ERROR, lr->getCurrentLine(), lr->getCurrentCol());
			error = 1;
			return error;
		}
	
		if (SUCCESS != 1)
		{
			err.normal ("Parser->buildAST: Package end statement missing", SYNTAX_ERROR, lr->getCurrentLine(), lr->getCurrentCol());
			error = 1;
			return error;
		}

		getNextTokenWrapper ();
	}

	return error;
}

/**
|
| Get next token and check it for
|
| CLASSIFIERCONTEXT | OPERATIONCONTEXT | ATTRORASSOCCONTEXT
|
| Then appropiately calling the required function to process
| the particular context. It also changes the context's label
| in the AST according to particular context detected.
|
| Write Syntax / Semantic Analysis output to file
|
| @param parentID node pointer of the parent node in the AST
| @return 1 on error and 0 on no error
|
 ----------------------------------------------------------------------------*/
int Parser :: processContext (TreeNode *parentID)
{
	int error = 1;

	prevID = ast->addNode (parentID, "context");
	parentID = prevID;

	getNextTokenWrapper ();
	int exist  = oclStmt->pathName (parentID, currSym);
	setSymID ();
	//
	// 2 means pathName ::= ID
	//
	if (exist == 2)
	{
		//
		// id ( 'inv' | 'def' )
		//
		if (currSym != NULL && (currSym->token == lg.INV || currSym->token == lg.DEF))
		{
			ast->changeValue (parentID, "classifierContext");
			error = !classifierContext (parentID);
		}
		//
		// id '('
		//
		else if (currSym != NULL && currSym->token == lg.LBRKT1)
		{
			ast->changeValue (parentID, "operationContext");
			error = !operationContext (parentID);
		}
	}
	//
	// 1 means fullPathName ::= pathName '::' ID
	//
	else if (exist == 1)
	{
		//
		// pathName ( 'inv' | 'def' )
		//
		if (currSym != NULL && (currSym->token == lg.INV || currSym->token == lg.DEF))
		{
			ast->changeValue (parentID, "classifierContext");
			error = !classifierContext (parentID);
		}
		//
		// pathName '::' id '('
		//
		else if (currSym != NULL && currSym->token == lg.LBRKT1)
		{
			ast->changeValue (parentID, "operationContext");
			error = !operationContext (parentID);
		}
		//
		// pathName '::' id ':'
		//
		else if (currSym != NULL && currSym->token == lg.COLON)
		{
			ast->changeValue (parentID, "attrOrAssocContext");
			error = !attrOrAssocContext (parentID);
		}
	}
	else
		err.fatal ("Parser->buildAST: Name of the package", SYNTAX_ERROR);

	return error;
}

/**
|
| Check for the correctness of attrOrAssocContext
| attrOrAssocContext ::= pathName DCOLON ID COLON type
|						(
|							( 'init' ':' OclStmt )+
|							| ( 'drive' ':' OclStmt )+
|						) ;
|
| processContext () checks for pathName DCOLON ID COLON
| @see processContext
|
| Write Syntax / Semantic Analysis output to file
| @param parentID node pointer of the parent node in the AST
| Returns 0 if no context exists
|
 ----------------------------------------------------------------------------*/
int Parser :: attrOrAssocContext (TreeNode *parentID)
{
	int exist = 0;

	getNextTokenWrapper ();
	if (oclStmt->type (parentID, currSym))
	{
		//
		// init | drive inserted as oclStatement node to the AST
		//
		setSymID ();
		while (currSym != NULL)
		{
			if (currSym->token == lg.INIT || currSym->token == lg.DRIVE)
			{
				prevID = ast->addNode (parentID, currSym->lexeme);

				getNextTokenWrapper ();
				if (currSym != NULL && currSym->token == lg.COLON)
				{
					exist = oclStmt->oclStatement (prevID, currSym);
					setSymID ();
				}
			}
			else
				break;
		}
	}
	else
		setSymID ();

//fprintf (stderr, "-------   attrOrAssocContext: %s EXIST: %d\n", currSym->lexeme, exist);
	return exist;
}

/**
|
| Check for the correctness of classifierContext
| classifierContext	::= pathName
|						(
|							( 'inv' id?':' OclStmt )+
|							| ( 'def' id? ':' DefStmt )+
|						) ;
|
| processContext () checks for pathName INV
| @see processContext
|
| Write Syntax / Semantic Analysis output to file
| @param parentID node pointer of the parent node in the AST
| Returns 0 if no context exists
|
 ----------------------------------------------------------------------------*/
int Parser :: classifierContext (TreeNode *parentID)
{
	TreeNode *pID;
	int exist = 0;

	//
	// inv | def inserted as oclStatement node to the AST
	//
	while (currSym != NULL)
	{
		if (currSym->token == lg.INV)
		{
			prevID = ast->addNode (parentID, currSym->lexeme);
			pID = prevID;

			getNextTokenWrapper ();
			if (currSym != NULL && currSym->token == lg.ID)
			{
				pID = ast->addNode (pID, currSym->lexeme);
				prevID = pID;
				getNextTokenWrapper ();
			}

			if (currSym != NULL && currSym->token == lg.COLON)
			{
				exist = oclStmt->oclStatement (pID, currSym);
				setSymID ();
			}
		}
		else if (currSym->token == lg.DEF)
		{
			prevID = ast->addNode (parentID, currSym->lexeme);
			pID = prevID;

			getNextTokenWrapper ();
			if (currSym != NULL && currSym->token == lg.ID)
			{
				pID = ast->addNode (pID, currSym->lexeme);
				prevID = pID;
				getNextTokenWrapper ();
			}

			if (currSym != NULL && currSym->token == lg.COLON)
				exist = defStatement (pID);
		}
		else
			break;
	}

//fprintf (stderr, "-------   classifierContext: %s EXIST: %d\n", currSym->lexeme, exist);
	return exist;
}

/**
|
| Check for the correctness of operationContext
| operationContext	::= operationName
|						(
|							( 'pre' id? ':' OclStmt )+
|							| ( 'post' id? ':' OclStmt )+
|							| ( 'body' id? ':' OclStmt )+
|							| ( 'action' id? ':' OclStmt )+
|						) ;
|
| Write Syntax / Semantic Analysis output to file
| @param parentID node pointer of the parent node in the AST
| Returns 0 if no context exists
|
 ----------------------------------------------------------------------------*/
int Parser :: operationContext (TreeNode *parentID)
{
	int exist = 0;

	if (!operationName (parentID))
		return exist;

	//
	// pre | post | body | action inserted as oclStatement node to the AST
	//
	while (currSym != NULL)
	{
		if (currSym->token == lg.PRE || currSym->token == lg.POST
					 || currSym->token == lg.BODY)
		{
			prevID = ast->addNode (parentID, currSym->lexeme);
			TreeNode *pID = prevID;

			getNextTokenWrapper ();
			if (currSym != NULL && currSym->token == lg.ID)
			{
				prevID = ast->addNode (pID, currSym->lexeme);
				getNextTokenWrapper ();
			}
			else
				prevID = ast->addNode (pID, "null");

			if (currSym != NULL && currSym->token == lg.COLON)
			{
				exist = oclStmt->oclStatement (pID, currSym);
				setSymID ();
			}
		}
		else if (currSym->token == lg.ACTION)
		{
			prevID = ast->addNode (parentID, currSym->lexeme);
			TreeNode *pID = prevID;

			getNextTokenWrapper ();
			if (currSym != NULL && currSym->token == lg.ID)
			{
				prevID = ast->addNode (pID, currSym->lexeme);
				getNextTokenWrapper ();
			}
			else
				prevID = ast->addNode (pID, "null");

			if (currSym != NULL && currSym->token == lg.COLON)
			{
				exist = oclStmt->actionStatement (pID, currSym);
				setSymID ();
			}
		}
		else
			break;
	}

//fprintf (stderr, "-------   operationContext: %s EXIST: %d\n", currSym->lexeme, exist);
	return exist;
}

/**
|
| Check for the correctness of operationName
| operationName ::= pathName '::' id '(' parameters? ')' ':' type?
|                   | id '(' parameters? ')' ':' type? ;
|
| processContext () checks for
|                      pathName DCOLON ID LBRKT1  |  ID LBRKT1
| @see processContext
|
| Write Syntax / Semantic Analysis output to file
| @param parentID node pointer of the parent node in the AST
| Returns 0 if no context exists
|
 ----------------------------------------------------------------------------*/
int Parser :: operationName (TreeNode *parentID)
{
	int exist = 0;

	//
	// Check for parameters and parameters = variableDeclarations
	// see grammar for more details
	//
	getNextTokenWrapper ();
	if (currSym != NULL)
	{
		prevID = ast->addNode (parentID, "args");
		exist = oclStmt->variableDeclarations (prevID, currSym);
		setSymID ();
		if (exist == 0)
		{
			ast->removeNode (prevID);
			prevID = parentID;
		}
		exist = 0;
	}

	if (currSym != NULL && currSym->token == lg.RBRKT1)
	{
		getNextTokenWrapper ();
		if (currSym != NULL && currSym->token == lg.COLON)
		{
			getNextTokenWrapper ();
			if (currSym != NULL)
			{
				exist = oclStmt->type (parentID, currSym);
				setSymID ();
			}
			exist = 1;
		}
	}

	return exist;
}

/**
|
| Check for the correctness of defStatement
| DefStmt	::= variableDeclaration '=' OclStmt
|				| operationName '=' OclStmt ;
|
| Write Syntax / Semantic Analysis output to file
| @param parentID node pointer of the parent node in the AST
| Returns 0 if no defStatement exists
|
 ----------------------------------------------------------------------------*/
int Parser :: defStatement (TreeNode *parentID)
{
	int exist = 0;

	getNextTokenWrapper ();
	if (oclStmt->variableDeclaration (parentID, currSym))
		exist = 1;
	else if (operationName (parentID))
		exist = 1;

	setSymID ();

	if (currSym != NULL && exist == 1)
	{
		if (!strcmp (currSym->lexeme, "="))
		{
			exist = oclStmt->oclStatement (parentID, currSym);
			setSymID ();
		}
	}

//fprintf (stderr, "---   END   --- defStatement: %s EXIST: %d\n", currSym->lexeme, exist);
	return exist;
}

/**
|
| Sets the ID (prevID) and symbols (currSym, prevSym1 and prevSym2)
| after calling any of the functions from OclStmt class.
|
 ----------------------------------------------------------------------------*/
void Parser :: setSymID (void)
{
	prevID = oclStmt->getPrevID ();
	currSym = oclStmt->getCurrSym ();
	prevSym1 = oclStmt->getPrevSym1 ();
	prevSym2 = oclStmt->getPrevSym2 ();
}

/**
|
| Wrapper to call getNextToken () function of Lexer class
| Store the prev column number and previous line number
| as column and line error numbers
|
 ----------------------------------------------------------------------------*/
SYMBOL * Parser :: getNextTokenWrapper (void)
{
	prevSym2 = prevSym1;
	prevSym1 = currSym;
	currSym = lr->getNextToken ();
	return (currSym);
}
