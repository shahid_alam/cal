/* ============================================================================
|
|   Filename:    VCAL.java
|   Dated:       15 Aug, 2006
|   By:          Shahid Alam (salam@sce.carleton.ca)
|
|   Description: See below
|
|   TO DO: Support for running CAL and sample file from different directories
|          Check for DAG INSTRUCTION in the syntax tree and set DAG_INSTRUCTION
|          and then find rootElement to be passed to ProcessXMI for visualizing DAG
|
 ----------------------------------------------------------------------------*/

package org.vcal;

import java.lang.Runtime;
import java.io.File;
import java.io.InputStream;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.printing.PrintDialog;
import org.eclipse.swt.printing.PrinterData;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.Tree;

import org.vcal.vdag.VisualDAG;
import org.vcal.window.StatusWindow;
import org.vcal.window.TextEditorWindow;
import org.vcal.config.Config;
import org.vcal.dialog.AboutDialog;
import org.vcal.dialog.SpeedScaleDialog;
import org.vcal.util.ReadStream;
import org.vcal.util.XMLTree;
import org.vcal.interpreter.CALInterpreter;
import org.vcal.help.CALHelp;

/**
 * 
 * <p>
 * The top most class of the package.
 * <p>
 * Implementation of the Visual CAL Parser. It calls the CAL Parser porgram implemented in C++
 * for parsing CAL files. It provides GUI to visualy display the output from the CAL parser.
 * 
 * @author Shahid Alam
 * @version 3.0
 * 
 */
public class VCAL
{
	public static final String Version = "VCAL (version 3.0)";
	private static boolean CAL_SUCCESS = false;
	private static boolean AST = false;

	private final int toolItemCount = 5;
    private static Image images[];

	private static Image cal_logo = null;
	private static Image logo = null;
	private static Menu menu = null;

	private static Display display = null;
	private static Shell shell = null;
	private static Composite compositeStatus = null;
	private static CALInterpreter CALIntr = null;
	private static TextEditorWindow textEditorWindow = null;
	private static StatusWindow SW = null;
	private static Config config = null;
	private static Label labelLexicalAnalysis = null, labelSyntaxAnalysis = null;

	private static boolean UML_MODEL_LOADED = false;

	private static String FILE_LA_OUTPUT = "", FILE_SA_OUTPUT = Config.astFileName, FILE_GC_OUTPUT = "";

	private static MenuItem subMenuLA, subMenuSA, subMenuCG, subMenuRun;
	private static Table lexicalAnalysisTable = null;
	private static Tree syntaxTree = null;

	private final static int MIN_SLIDE = 10;
	private final static int MAX_SLIDE = 30;

	/**
	 *
	 * Constructor Initializing all the objects
	 *
	 */
    public VCAL ()
    {
        images = new Image[toolItemCount];
    }

	public void handleRun()
	{
		File f = new File (Config.CURRENT_CAL_FILE);
		if (f.exists())
		{
			enableChildren (getMenuItem("Compiler"), "Generate Lexical Analysis Output", false);
			enableChildren (getMenuItem("Compiler"), "Run", false);
   	        config.enableToolItem(config.getToolBar(), "Run", false);

   	        config.setStatusText("Running CAL");
   	        CAL_SUCCESS = false;
			runCAL (f.getAbsoluteFile().toString());
			if (CAL_SUCCESS)
			{
				config.setStatusText("CAL Success");
				f = new File (Config.astFileName);
				if (f.exists())
				{
					CALIntr = new CALInterpreter (Config.astFileName, config);
					Config.VIEW_DAG_ROOT_ELEMENT = CALIntr.viewDAG();
					if (Config.VIEW_DAG_ROOT_ELEMENT == null)
						Config.VIEW_DAG_ROOT_ELEMENT = "_ROOT_";
					VisualDAG vdag = config.getVDAG();
					if (vdag != null)
					{
						if (vdag.makeSubGraphVisible (Config.VIEW_DAG_ROOT_ELEMENT))
							textEditorWindow.viewDependencyAnalysisWindow("Dependency Analysis");
					}
				}
				else
				{
					config.setStatusText("CAL AST File Error");
					config.setStatusWindowText("CAL AST File Error:\n   No DAG Generation");
				}
				if (Config.GENERATE_LEXICAL_ANALYSIS_OUTPUT)
		        {
	        		try
	        		{
	        			LexicalAnalysis la = new LexicalAnalysis (FILE_LA_OUTPUT, textEditorWindow, config);
	    	        	lexicalAnalysisTable = la.createTable ();
	        		}
	        		catch (Exception e)
	        		{
	        			enableChildren (getMenuItem("Compiler"), "Generate Lexical Analysis Output", true);
	        			enableChildren (getMenuItem("Compiler"), "Run", true);
	           	        config.enableToolItem(config.getToolBar(), "Run", true);
	        			String errorMessage = "VCAL::handleRun:Error: " + e;
	        			config.setStatusWindowError(errorMessage);
	        		}
		        }

		        if (Config.GENERATE_SYNTAX_ANALYSIS_OUTPUT)
		        {
		        	Composite comp = (Composite)textEditorWindow.getOutputWindowFolder();
		        	if (comp != null)
		        	{
						syntaxTree = null;
						if (Config.CURRENT_CAL_FILE != null)
						{
							if (AST)
							{
								File file = new File (Config.astFileName);
								XMLTree xml = new XMLTree (file.getAbsoluteFile().toString(), comp, SWT.BORDER, config);
								file = null;
								syntaxTree = xml.buildTree ("SyntaxTree", "name");
								if (syntaxTree != null)
								{
									syntaxTree.setBackground(Config.WHITE_COLOR);
									syntaxTree.setForeground(Config.DARK_BLUE_COLOR);
									xml.makeChildrenColored("PACKAGE", syntaxTree.getBackground(), Config.DARK_BLUE_COLOR, true);
								}
							}
				        	textEditorWindow.openSyntaxAnalysisWindow ("Syntax Analysis", syntaxTree);
		        		}
		        	}
		        }
			}
			else
				config.setStatusText("CAL Error");

			enableChildren (getMenuItem("Compiler"), "Generate Lexical Analysis Output", true);
			enableChildren (getMenuItem("Compiler"), "Run", true);
   	        config.enableToolItem(config.getToolBar(), "Run", true);
		}
		else
			config.setStatusText("CAL No File Error");
		f = null;
	}

	private boolean runCAL (String calFileFullPath)
	{
		try
		{
			File file = new File (Config.CAL_PARSER_EXE_FILE);
//			if (UML_MODEL_LOADED && file.exists())
			{
				file = new File (calFileFullPath);
				if (file.exists())
				{
					/**
			  		 *
			  		 * You can change the parser options here.
			  		 * On compilation error it prints the error and exit.
			  		 *
			  		 */
					String LA = "", SA = "", GC = "";
					if (Config.GENERATE_LEXICAL_ANALYSIS_OUTPUT)
					{
						FILE_LA_OUTPUT = "CAL_LA.out";
						LA = "-l=" + FILE_LA_OUTPUT;
					}
					if (Config.GENERATE_SYNTAX_ANALYSIS_OUTPUT)
					{
						if (FILE_SA_OUTPUT.length() <= 0)
							FILE_SA_OUTPUT = "CAL_AST.xml";
						SA = "-p=" + FILE_SA_OUTPUT;
					}
					if (Config.GENERATE_CODE)
					{
						FILE_GC_OUTPUT = "CAL_IR.out";
						GC = "-i=" + FILE_GC_OUTPUT;
					}
					String commandArray[] = {
												Config.CAL_PARSER_EXE_FILE,
												LA,
												SA,
												GC,
												"-o=1",
												"-a=1",
												calFileFullPath
											};
			  		if (Config.DEBUG)
		  				config.setStatusWindowText("Running " + Config.CAL_PARSER_EXE_FILE);
			  		System.gc ();
			  		Process process = Runtime.getRuntime ().exec (commandArray);
			        InputStream stderr = process.getErrorStream ();
			        ReadStream re = new ReadStream (stderr, config);
			        String sbError = re.start();
					
					int exitValue = process.waitFor ();
					if (exitValue <= 0)
			  		{
						CAL_SUCCESS = true;
						AST = true;
				   		if (Config.DEBUG)
				   		{
				   			config.setStatusWindowText("Successfully finished running " + Config.CAL_PARSER_EXE_FILE + "\n");
				   			config.setStatusWindowError("CLEAR");
				   		}
						file = null;
						process.destroy();
						return true;
			  		}
					else
					{
						config.setStatusWindowText("Finished Running CAL with Error / Errors");
						config.setStatusWindowError("<CALError>\n" + sbError + "</CALError>");
						String lines = null, cols = null;
						int i = sbError.indexOf("line='");
						if (i > 0)
						{
							String temp = sbError.substring(i+6, sbError.length());
							i = temp.indexOf("'");
							if (i > 0)
								lines = temp.substring(0, i);
							i = sbError.indexOf("col='");
							if (i > 0)
							{
								temp = sbError.substring(i+5, sbError.length());
								i = temp.indexOf("'");
								if (i > 0)
									cols = temp.substring(0, i);
							}
						}
						int line = 0, col = 0;
						if (lines != null && cols != null)
						{
							line = new Integer(lines).intValue();
							col = new Integer(cols).intValue();
							textEditorWindow.selectText (line-1, col);
						}
						file = null;
						process.destroy();
						return false;
					}
				}
				else
				{
					file = null;
					config.setStatusText("CAL Parser Error");
					config.setStatusWindowError("Parser [" + calFileFullPath + "] not present");
					return false;
				}
			}
/*			else
			{
				file = null;
				config.setStatusText("CAL Parser Error");
				if (!UML_MODEL_LOADED)
					config.setStatusWindowError("UML Model Not Loaded\nFirst load the model and then Press Run");
				else
					config.setStatusWindowError("Parser [" + Config.CAL_PARSER_EXE_FILE + "] not present");
				return false;
			}*/
		}
		catch (Exception e)
		{
			config.setStatusWindowError("VCAL::runCAL: " + e);
			return false;
		}
	}

	/**
	 * Get the main menu
	 * @return The main menu
	 */
	public Menu getMenu ()
	{
		return menu;
	}

	/**
	 * Get the menu item with menuName from the children of the Main menu
	 * @param menuName Name of the menu to be searched for
	 * @return The menu item
	 */
	public MenuItem getMenuItem (String menuName)
	{
		if (menuName != null)
		{
			menuName = "&" + menuName;
			MenuItem items[] = menu.getItems();
			for (int i = 0; i < items.length; i++)
			{
				if (items[i].getText().equals(menuName))
					return items[i];
			}
		}
		return null;
	}

    /**
     * Enable or Disable all the children of the Menu Item
     * @param onlyOne Name of the children menu item to be enabled
     * @param enable If true enable the children else vice versa
     */
	public void enableChildren (MenuItem menu, String onlyOne, boolean enable)
	{
		MenuItem items[] = menu.getMenu().getItems();
		if (onlyOne != null)
		{
			for (int i = 0; i < items.length; i++)
			{
				if (items[i].getText().equals(onlyOne))
				{
					items[i].setEnabled (enable);
					break;
				}
			}
		}
		else
		{
			for (int i = 0; i < items.length; i++)
			{
				items[i].setEnabled (enable);
			}
		}
	}

	/**
	 *
     * Create the menu.
     *
     */
    private Menu createMenu ()
    {
        menu = new Menu (shell, SWT.BAR);

        /**
         * 
         * Create File menu
         * 
         */
        MenuItem file = new MenuItem (menu, SWT.CASCADE);
        file.setText ("&File");
        Menu filemenu = new Menu (shell, SWT.DROP_DOWN);
        file.setMenu(filemenu);

        MenuItem newFile = new MenuItem (filemenu, SWT.PUSH);
        newFile.setText ("New\tCtrl+N");
        newFile.setAccelerator (SWT.MOD1 + 'N');
        newFile.addListener(SWT.Selection, new Listener() {
        	public void handleEvent(Event e) {
        		textEditorWindow.editFile (config, null);
        	}
        });

        final MenuItem openFile = new MenuItem (filemenu, SWT.PUSH);
        openFile.setText ("Open...\tCtrl+O");
        openFile.setAccelerator (SWT.MOD1 + 'O');
        openFile.addListener(SWT.Selection, new Listener() {
        	public void handleEvent(Event e) {
        		String[] filterExtensions = { "*." + Config.CAL };
        		FileDialog fileDialog = new FileDialog(shell, SWT.OPEN);
        		fileDialog.setText("Select File");
        		fileDialog.setFilterPath(config.getStartDir());
        		fileDialog.setFilterExtensions(filterExtensions);
        		String selectedFile = fileDialog.open();
        		if (selectedFile != null)
        		{
        			/**
        			 * checking file extension (.cal)
        			 */
        			int i = selectedFile.lastIndexOf('.');
        			if (i > 0)
        			{
	        			String fileX = selectedFile.substring(i);
	        			if (fileX.equals(".cal"))
	        			{
			        		config.setStartDir (selectedFile);
			        		textEditorWindow.editFile (config, selectedFile);
	        			}
	        			else
	        				config.setStatusText(" CAL File Error");
        			}
        			else
        				config.setStatusText(" CAL File Error");
        		}
        	}
        });

        final MenuItem loadModel = new MenuItem (filemenu, SWT.PUSH);
        loadModel.setText ("Load Model...");
        loadModel.addListener(SWT.Selection, new Listener() {
        	public void handleEvent(Event e) {
        		String[] filterExtensions = { "*.xml" };
        		FileDialog fileDialog = new FileDialog(shell, SWT.OPEN);
        		fileDialog.setText("Select Model (XML) File");
        		fileDialog.setFilterPath(config.getStartDir());
        		fileDialog.setFilterExtensions(filterExtensions);
        		String selectedFile = fileDialog.open();
        		if (selectedFile != null)
        		{
        			/**
        			 * checking file extension (.xml)
        			 */
        			int i = selectedFile.lastIndexOf('.');
        			if (i > 0)
        			{
	        			String fileX = selectedFile.substring(i);
	        			if (fileX.equals(".xml"))
	        			{
			        		config.setStartDir (selectedFile);
			        		config.setXMIFileName (selectedFile);
			        		if (textEditorWindow.openModelFile (selectedFile, config))
			        		{
			        			loadModel.setEnabled(false);
			        			UML_MODEL_LOADED = true;
			        		}
	        			}
	        			else
	        				config.setStatusText(" XML File Error");
        			}
        			else
        				config.setStatusText(" XML File Error");
        		}
        	}
        });

        final MenuItem saveFile = new MenuItem (filemenu, SWT.PUSH);
        saveFile.setText ("Save\tCtrl+S");
        saveFile.setAccelerator(SWT.MOD1 + 'S');
        saveFile.setEnabled(false);
        saveFile.addListener(SWT.Selection, new Listener() {
        	public void handleEvent(Event e)
        	{
           		textEditorWindow.saveFile(Config.CURRENT_CAL_FILE);
        	}
        });

        MenuItem saveAsFile = new MenuItem (filemenu, SWT.PUSH);
        saveAsFile.setText ("Save As...");
        saveAsFile.addListener(SWT.Selection, new Listener() {
        	public void handleEvent(Event e)
        	{
        		String[] filterExtensions = { "*."+Config.CAL };
        		FileDialog fileDialog = new FileDialog(shell, SWT.SAVE);
        		fileDialog.setText("Save As");
        		fileDialog.setFilterPath(config.getStartDir());
        		fileDialog.setFilterExtensions(filterExtensions);
        		String selectedFile = fileDialog.open();
        		if (selectedFile != null)
        		{
        			/**
        			 * checking file extension (.cal)
        			 */
        			int i = selectedFile.lastIndexOf('.');
        			if (i > 0)
        			{
	        			String fileX = selectedFile.substring(i);
	        			if (fileX.equals("."+Config.CAL))
	        			{
			        		config.setStartDir (selectedFile);
			        		textEditorWindow.saveFile(selectedFile);
	        			}
	        			else
	        				config.setStatusText(" File Save Error");
        			}
        			else
        				config.setStatusText(" File Save Error");
        		}
        	}
        });

        final MenuItem closeFile = new MenuItem (filemenu, SWT.PUSH);
        closeFile.setText ("Close");
        closeFile.setEnabled (false);
        closeFile.addListener(SWT.Selection, new Listener() {
        	public void handleEvent(Event e)
        	{
        		textEditorWindow.closeSelectedFile ();
                closeFile.setEnabled (false);
        	}
        });

        MenuItem print = new MenuItem (filemenu, SWT.PUSH);
        print.setText ("Print...\tCtrl+P");
        print.setAccelerator (SWT.MOD1 + 'P');
        print.setEnabled(false);
        print.addListener(SWT.Selection, new Listener() {
        	public void handleEvent(Event e)
        	{
                PrintDialog printDialog = new PrintDialog(shell, SWT.NONE);
                printDialog.setText("Print");
                printDialog.setScope(PrinterData.ALL_PAGES);
                printDialog.setPrintToFile(true);
                PrinterData printerData = printDialog.open();
                if (printerData != null){
                	switch(printerData.scope){
                	case PrinterData.ALL_PAGES:
                		config.setStatusWindowText("Printing all pages.");
                		break;
                	case PrinterData.SELECTION:
                		config.setStatusWindowText("Printing selected page.");
                		break;
                	case PrinterData.PAGE_RANGE:
                		config.setStatusWindowText("Printing page range. ");
                		config.setStatusWindowText("From:"+printerData.startPage);
                		config.setStatusWindowText(" to:"+printerData.endPage);
                		break;
                	}
                	if (printerData.printToFile)
                		config.setStatusWindowText("Printing to file.");
                	else
                		config.setStatusWindowText("Not printing to file.");
                	if (printerData.collate)
                		config.setStatusWindowText("Collating.");
                	else
                		config.setStatusWindowText("Not collating.");
                	config.setStatusWindowText("Number of copies:"+printerData.copyCount);
                	config.setStatusWindowText("Printer Name:"+printerData.name);
               	}
            }
        });

        MenuItem separatorF = new MenuItem (filemenu, SWT.SEPARATOR);

        MenuItem exit = new MenuItem (filemenu, SWT.PUSH);
        exit.setText ("Exit\tCtrl+E");
        exit.setAccelerator (SWT.MOD1 + 'E');
        exit.addListener(SWT.Selection, new Listener() {
        	public void handleEvent(Event e) {
        		shell.close();
        		System.exit(0);
        	}
        });

        /**
         * 
         * Create Compiler menu
         * 
         */
        MenuItem compiler = new MenuItem (menu, SWT.CASCADE);
        compiler.setText ("&Compiler");
        Menu compilermenu = new Menu (shell, SWT.DROP_DOWN);
        compiler.setMenu(compilermenu);

        subMenuLA = new MenuItem (compilermenu, SWT.CHECK);
        subMenuLA.setText ("Generate Lexical Analysis Output");
        subMenuSA = new MenuItem (compilermenu, SWT.CHECK);
        subMenuSA.setText ("Generate Syntax Analysis Output");
        subMenuSA.setSelection(true);
		if (subMenuSA.getSelection())
    		labelSyntaxAnalysis.setText("Syntax Analysis");
        subMenuSA.setEnabled(false);
        subMenuCG = new MenuItem (compilermenu, SWT.PUSH);
        subMenuCG.setText ("Generate Code...");
        subMenuCG.setEnabled(false);
        final MenuItem setParam = new MenuItem (compilermenu, SWT.PUSH);
        setParam.setText ("Scanning Speed...");
        setParam.setEnabled(Config.GENERATE_LEXICAL_ANALYSIS_OUTPUT);
        MenuItem separatorC = new MenuItem (compilermenu, SWT.SEPARATOR);
        subMenuRun = new MenuItem (compilermenu, SWT.PUSH);
        subMenuRun.setText ("Run");
        subMenuRun.setEnabled(false);
        final MenuItem subMenuLoadCAL = new MenuItem (compilermenu, SWT.PUSH);
        subMenuLoadCAL.setText ("Select CAL Parser...");
        if (Config.CAL_PARSER_EXE_FILE != null)
        {
        	File f = new File (Config.CAL_PARSER_EXE_FILE);
        	boolean present = f.exists();
        	if (present)
        	{
        		subMenuLoadCAL.setEnabled(false);
        		config.setStatusText(" CAL Parser Loaded");
        	}
        	else
				config.setStatusText(" CAL Parser Error");
        	f = null;
    	}
    	else
			config.setStatusText(" CAL Parser Error");

        subMenuLA.addListener (SWT.Selection, new Listener() {
        	public void handleEvent(Event e)
        	{
        		labelLexicalAnalysis.setText ("");
        		if (subMenuLA.getSelection())
            		labelLexicalAnalysis.setText ("Lexial Analysis:" + Config.SCANNING_SPEED);
        		Config.GENERATE_LEXICAL_ANALYSIS_OUTPUT = subMenuLA.getSelection();
        		/**
        		 * Not using the option of setting the scanning speed in this version
        		 */
                // setParam.setEnabled(Config.GENERATE_LEXICAL_ANALYSIS_OUTPUT);
        	}
        });

        subMenuSA.addListener (SWT.Selection, new Listener() {
        	public void handleEvent(Event e)
        	{
        		labelSyntaxAnalysis.setText("");
        		if (subMenuSA.getSelection())
            		labelSyntaxAnalysis.setText("Syntax Analysis");
        		Config.GENERATE_SYNTAX_ANALYSIS_OUTPUT = subMenuSA.getSelection();
        	}
        });

        subMenuCG.addListener (SWT.Selection, new Listener() {
        	public void handleEvent(Event e)
        	{
        	}
        });

        subMenuRun.addListener (SWT.Selection, new Listener() {
        	public void handleEvent(Event e)
        	{
        		handleRun ();
        	}
        });

        subMenuLoadCAL.addListener (SWT.Selection, new Listener() {
        	public void handleEvent(Event e)
        	{
        		String[] filterExtensions = { "*.exe" };
        		FileDialog fileDialog = new FileDialog(shell, SWT.OPEN);
        		fileDialog.setText("Select CAL Parser to Run");
        		fileDialog.setFilterPath(config.getStartDir());
        		fileDialog.setFilterExtensions(filterExtensions);
        		String selectedFile = fileDialog.open();
        		if (selectedFile != null)
        		{
        			/**
        			 * checking CAL file
        			 */
        			int i = selectedFile.lastIndexOf('.');
        			String file = null;
        			if (i <= 0)
        				i = selectedFile.length();
        			file = selectedFile.substring(i-Config.CAL.length(), i);
        			if (file.equals(Config.CAL))
        			{
        				File f = new File (selectedFile);
		                boolean present = f.exists();
		                if (present)
		                {
			        		Config.CAL_PARSER_EXE_FILE = selectedFile;
	        				config.setStatusText(" CAL Parser Loaded");
			        		config.setStartDir (selectedFile);
		        			subMenuLoadCAL.setEnabled(false);
		                }
		                else
	        				config.setStatusText(" CAL Parser Error");
		                f = null;
        			}
        			else
        				config.setStatusText(" CAL Parser Error");
        		}
        	}
        });

        setParam.addListener (SWT.Selection, new Listener() {
        	public void handleEvent(Event e)
        	{
        		SpeedScaleDialog dlg = new SpeedScaleDialog (shell);
        		dlg.setSlideParameters (MIN_SLIDE, Config.SCANNING_SPEED, MAX_SLIDE);
        		SpeedScaleDialog.InputDialogData data = dlg.open();
        		Config.SCANNING_SPEED = data.getSpeedResponse();
        		if (Config.DEBUG)
        			labelLexicalAnalysis.setText("Lexical Analysis:" + Config.SCANNING_SPEED);
        	}
        });

        /**
         * 
         * Create View menu
         * 
         */
        MenuItem view = new MenuItem (menu, SWT.CASCADE);
        view.setText ("&View");
        Menu viewmenu = new Menu (shell, SWT.DROP_DOWN);
        view.setMenu(viewmenu);

        MenuItem viewLA = new MenuItem (viewmenu, SWT.PUSH);
        viewLA.setText ("Lexical Analysis");
        viewLA.setEnabled (false);
        viewLA.addListener (SWT.Selection, new Listener() {
        	public void handleEvent(Event e)
        	{
        		if (lexicalAnalysisTable != null)
        			textEditorWindow.openLexicalAnalysisWindow("Lexical Analysis", lexicalAnalysisTable);
        		else
        			config.setStatusText("Lexical Analysis Not Done");
        	}
        });

        MenuItem viewSA = new MenuItem (viewmenu, SWT.PUSH);
        viewSA.setText ("Syntax Analysis");
        viewSA.setEnabled (false);
        viewSA.addListener (SWT.Selection, new Listener() {
        	public void handleEvent(Event e)
        	{
        		if (syntaxTree != null)
        			textEditorWindow.openSyntaxAnalysisWindow("Syntax Analysis", syntaxTree);
        		else
        			config.setStatusText("Syntax Analysis Not Done");
        		
        	}
        });

        MenuItem viewCG = new MenuItem (viewmenu, SWT.PUSH);
        viewCG.setText ("Code Generated");
        viewCG.setEnabled (false);
        viewCG.addListener (SWT.Selection, new Listener() {
        	public void handleEvent(Event e)
        	{
        	}
        });

        MenuItem viewDA = new MenuItem (viewmenu, SWT.PUSH);
        viewDA.setText ("Dependency Analysis");
        viewDA.setEnabled (false);
        viewDA.addListener (SWT.Selection, new Listener() {
        	public void handleEvent(Event e)
        	{
        		textEditorWindow.viewDependencyAnalysisWindow("Dependency Analysis");
        	}
        });

        MenuItem model = new MenuItem (viewmenu, SWT.PUSH);
        model.setText ("UML Model");
        model.setEnabled (false);
        model.addListener (SWT.Selection, new Listener() {
        	public void handleEvent(Event e)
        	{
        		textEditorWindow.viewUMLModelWindow();
        	}
        });

        MenuItem viewStatus = new MenuItem (viewmenu, SWT.PUSH);
        viewStatus.setText ("Status");
        viewStatus.setEnabled (false);
        viewStatus.addListener (SWT.Selection, new Listener() {
        	public void handleEvent(Event e)
        	{
        		textEditorWindow.viewStatusWindow("Status");
        	}
        });

        MenuItem viewError = new MenuItem (viewmenu, SWT.PUSH);
        viewError.setText ("Error");
        viewError.setEnabled (false);
        viewError.addListener (SWT.Selection, new Listener() {
        	public void handleEvent(Event e)
        	{
        		textEditorWindow.viewStatusWindow("Error");
        	}
        });

        /**
         * 
         * Create VDAG Menu
         * 
         */
        MenuItem vdag = new MenuItem (menu, SWT.CASCADE);
        vdag.setText ("&Vdag");
        Menu vdagmenu = new Menu (shell, SWT.DROP_DOWN);
        vdag.setMenu(vdagmenu);

        MenuItem saveDAG = new MenuItem (vdagmenu, SWT.PUSH);
        saveDAG.setText ("Save DAG...");
        saveDAG.addListener (SWT.Selection, new Listener() {
        	public void handleEvent(Event e)
        	{
        		VisualDAG vDAG = config.getVDAG ();
        		if (vDAG != null)
        			vDAG.saveDAG();
        	}
        });

        final MenuItem zoomin = new MenuItem (vdagmenu, SWT.PUSH);
        zoomin.setText ("Zoom In");
        final MenuItem zoomout = new MenuItem (vdagmenu, SWT.PUSH);
        zoomout.setText ("Zoom Out");
        zoomout.setEnabled(false);
        zoomin.addListener (SWT.Selection, new Listener() {
        	public void handleEvent(Event e)
        	{
        		VisualDAG vDAG = config.getVDAG ();
        		if (vDAG != null)
        			vDAG.handleZoomIn (config);
        	}
        });

        zoomout.addListener (SWT.Selection, new Listener() {
        	public void handleEvent(Event e)
        	{
        		VisualDAG vDAG = config.getVDAG ();
        		if (vDAG != null)
        			vDAG.handleZoomOut (config);
        	}
        });

        MenuItem add = new MenuItem (vdagmenu, SWT.PUSH);
        add.setText ("Add\tCtrl+A");
        add.setAccelerator(SWT.MOD1 + 'A');
        add.addListener (SWT.Selection, new Listener() {
        	public void handleEvent(Event e)
        	{
        		VisualDAG vdag = config.getVDAG();
    			if (vdag != null)
    			{
    				vdag.showAddVertexDialog();
    			}
        	}
        });

        MenuItem del = new MenuItem (vdagmenu, SWT.PUSH);
        del.setText ("Delete\tCtrl+D");
        del.setAccelerator(SWT.MOD1 + 'D');
        del.addListener (SWT.Selection, new Listener() {
        	public void handleEvent(Event e)
        	{
        		VisualDAG vdag = config.getVDAG();
    			if (vdag != null)
    			{
    				vdag.showDelVertexDialog();
    			}
        	}
        });

        MenuItem separatorV = new MenuItem (vdagmenu, SWT.SEPARATOR);

        MenuItem viewD = new MenuItem (vdagmenu, SWT.PUSH);
        viewD.setText ("View Descendants");
        viewD.addListener (SWT.Selection, new Listener() {
        	public void handleEvent(Event e)
        	{
        		VisualDAG vdag = config.getVDAG();
    			if (vdag != null)
    			{
    				vdag.DESCENDANTS_VISIBLE = true;
    				vdag.makeVisible();
    			}
        	}
        });

        MenuItem viewA = new MenuItem (vdagmenu, SWT.PUSH);
        viewA.setText ("View Ancestors");
        viewA.addListener (SWT.Selection, new Listener() {
        	public void handleEvent(Event e)
        	{
        		VisualDAG vdag = config.getVDAG();
    			if (vdag != null)
    			{
    				vdag.ANCESTORS_VISIBLE = true;
    				vdag.makeVisible();
    			}
        	}
        });

        MenuItem viewBoth = new MenuItem (vdagmenu, SWT.PUSH);
        viewBoth.setText ("View Both");
        viewBoth.addListener (SWT.Selection, new Listener() {
        	public void handleEvent(Event e)
        	{
        		VisualDAG vdag = config.getVDAG();
    			if (vdag != null)
    			{
    				vdag.BOTH_VISIBLE = true;
    				vdag.makeVisible();
    			}
        	}
        });

        MenuItem viewAll = new MenuItem (vdagmenu, SWT.PUSH);
        viewAll.setText ("View All");
        viewAll.addListener (SWT.Selection, new Listener() {
        	public void handleEvent(Event e)
        	{
        		VisualDAG vdag = config.getVDAG();
        			if (vdag != null)
        				vdag.makeAllVisible ();
        	}
        });

        /**
         * 
         * Create Help menu
         * 
         */
        MenuItem help = new MenuItem (menu, SWT.CASCADE);
        help.setText ("&Help");
        Menu helpmenu = new Menu (shell, SWT.DROP_DOWN);
        help.setMenu(helpmenu);

        MenuItem contentIndex = new MenuItem (helpmenu, SWT.PUSH);
        contentIndex.setText ("Contents and Index");
        contentIndex.addListener (SWT.Selection, new Listener() {
        	public void handleEvent(Event e)
        	{
        		CALHelp ch = new CALHelp (shell.getDisplay());
        		if (!ch.open ())
        		{
        			config.setStatusText("Help Not Available");
        			config.setStatusWindowError("Error loading help file");
        		}
        	}
        });

        MenuItem separatorH = new MenuItem (helpmenu, SWT.SEPARATOR);

        MenuItem about = new MenuItem (helpmenu, SWT.PUSH);
        about.setText ("About VCAL");
        about.addListener (SWT.Selection, new Listener() {
        	public void handleEvent(Event e)
        	{
        		AboutDialog ad = new AboutDialog (shell, cal_logo);
        		ad.open ();
        	}
        });

        enableChildren (vdag, null, false);
        return menu;
    }

    /**
     *
     * Create Toolbar
     *
     */
    private ToolBar createToolBar ()
    {
    	//
        // Create the toolbar
    	//
       final ToolBar toolBar = new ToolBar (shell, SWT.FLAT);
       RowLayout rl = new RowLayout ();
       rl.pack = false;
       toolBar.setLayout(rl);
       FormData data = new FormData();
       data.top = new FormAttachment(0, 0);
       data.left = new FormAttachment(0, 0);
       data.right = new FormAttachment(100, 0);
       toolBar.setLayoutData (data);

       File file = null;
       final ToolItem newt = new ToolItem (toolBar, SWT.PUSH);
       final ToolItem open = new ToolItem (toolBar, SWT.PUSH);
       final ToolItem save = new ToolItem (toolBar, SWT.PUSH);
       final ToolItem print = new ToolItem (toolBar, SWT.PUSH);
       final ToolItem run = new ToolItem (toolBar, SWT.PUSH);
       final ToolItem sep1 = new ToolItem (toolBar, SWT.SEPARATOR);

       newt.setToolTipText("New");
       file = new File (Config.NEW_IMAGE_FILE);
       if (file.exists())
       {
	       images[0] = new Image (shell.getDisplay(), file.getAbsoluteFile().toString());
	       newt.setImage (images[0]);
       }
//       else
    	   newt.setText("New");
       newt.addListener(SWT.Selection, new Listener() {
       	public void handleEvent(Event e)
       	{
    		textEditorWindow.editFile (config, null);
       	}
       });

       open.setToolTipText("Open");
       file = new File (Config.OPEN_IMAGE_FILE);
       if (file.exists())
       {
    	   images[1] = new Image (shell.getDisplay(), file.getAbsoluteFile().toString());
	       open.setImage (images[1]);
       }
//       else
    	   open.setText("Open");
       open.addListener(SWT.Selection, new Listener() {
       	public void handleEvent(Event e)
       	{
    		String[] filterExtensions = { "*." + Config.CAL };
    		FileDialog fileDialog = new FileDialog(shell, SWT.OPEN);
    		fileDialog.setText("Select File");
    		fileDialog.setFilterPath(config.getStartDir());
    		fileDialog.setFilterExtensions(filterExtensions);
    		String selectedFile = fileDialog.open();
    		if (selectedFile != null)
    		{
    			/**
    			 * checking file extension (.cal)
    			 */
    			int i = selectedFile.lastIndexOf('.');
    			if (i > 0)
    			{
        			String fileX = selectedFile.substring(i);
        			if (fileX.equals(".cal"))
        			{
		        		Config.CURRENT_CAL_FILE = selectedFile;
		        		config.setStartDir (selectedFile);
		        		textEditorWindow.editFile (config, selectedFile);
        			}
        			else
        				config.setStatusText(" CAL File Error");
    			}
    			else
    				config.setStatusText(" CAL File Error");
    		}
       	}
       });

       save.setToolTipText("Save");
       save.setEnabled(false);
       file = new File (Config.SAVE_IMAGE_FILE);
       if (file.exists())
       {
    	   images[2] = new Image (shell.getDisplay(), file.getAbsoluteFile().toString());
	       save.setImage (images[2]);
       }
//       else
    	   save.setText("Save");
       save.addListener(SWT.Selection, new Listener() {
       	public void handleEvent(Event e)
       	{
       		if (textEditorWindow.saveFile(Config.CURRENT_CAL_FILE))
       		{
	            save.setEnabled(false);
	            enableChildren (getMenuItem("File"), "Save\tCtrl+S", false);
	            run.setEnabled(true);
	            enableChildren (getMenuItem("Compiler"), "Run", true);
	            enableChildren (getMenuItem("File"), "Print...\tCtrl+P", true);
       		}
       	}
       });

       print.setToolTipText("Print");
       file = new File (Config.PRINT_IMAGE_FILE);
       if (file.exists())
       {
    	   images[3] = new Image (shell.getDisplay(), file.getAbsoluteFile().toString());
	       print.setImage (images[3]);
       }
//       else
    	   print.setText("Print");
       print.setEnabled(false);
       print.addListener(SWT.Selection, new Listener() {
       	public void handleEvent(Event e)
       	{
            PrintDialog printDialog = new PrintDialog(shell, SWT.NONE);
            printDialog.setText("Print");
            printDialog.setScope(PrinterData.ALL_PAGES);
            printDialog.setPrintToFile(true);
            PrinterData printerData = printDialog.open();
            if (printerData != null){
            	switch(printerData.scope){
            	case PrinterData.ALL_PAGES:
            		System.out.println("Printing all pages.");
            		break;
            	case PrinterData.SELECTION:
            		System.out.println("Printing selected page.");
            		break;
            	case PrinterData.PAGE_RANGE:
            		System.out.print("Printing page range. ");
            		System.out.print("From:"+printerData.startPage);
            		System.out.println(" to:"+printerData.endPage);
            		break;
            	}
            	if (printerData.printToFile)
            		System.out.println("Printing to file.");
            	else
            		System.out.println("Not printing to file.");
            	if (printerData.collate)
            		System.out.println("Collating.");
            	else
            		System.out.println("Not collating.");
            	System.out.println("Number of copies:"+printerData.copyCount);
            	System.out.println("Printer Name:"+printerData.name);
           	}
       	}
       });

       run.setToolTipText("Run");
       file = new File (Config.RUN_IMAGE_FILE);
       if (file.exists())
       {
    	   images[4] = new Image (shell.getDisplay(), file.getAbsoluteFile().toString());
	       run.setImage (images[4]);
       }
//       else
    	   run.setText("Run");
       run.setEnabled(false);
       run.addListener (SWT.Selection, new Listener () {
        	public void handleEvent (Event e)
        	{
        		handleRun ();
        	}
       });

       toolBar.pack();
       return toolBar;
    }

    /**
     * 
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     * 
     */
    private static void createAndShowGUI ()
    {
    	display = new Display ();
		File file = new File (Config.CAL_LOGO_FILE);
		cal_logo = new Image (display, file.getAbsoluteFile().toString());
		file = new File (Config.APPLICATION_LOGO_FILE);
		logo = new Image (display, file.getAbsoluteFile().toString());
		file = null;
    	shell = new Shell (display, SWT.SHELL_TRIM | SWT.DOUBLE_BUFFERED);
    	shell.setText ("VCAL - Visual CAL");
    	shell.setImage(logo);
    	shell.setLayout(new FormLayout());
        final VCAL vcal = new VCAL ();
    	/**
    	 * 
    	 * Create and setup toolbar
    	 * 
    	 */
        ToolBar tb = vcal.createToolBar();

    	final SashForm form = new SashForm (shell ,SWT.BORDER | SWT.HORIZONTAL | SWT.SMOOTH);
    	form.SASH_WIDTH = 1;
    	form.setLayout (new FillLayout());
        FormData formRow = new FormData();
        formRow.top = new FormAttachment(tb, 0);
        formRow.left = new FormAttachment(0, 0);
        formRow.bottom = new FormAttachment(100, -25);
        formRow.right = new FormAttachment(100, 0);
        form.setLayoutData(formRow);

    	compositeStatus = new Composite (shell, SWT.NONE);
        FormData statusLocation = new FormData();
        statusLocation.top = new FormAttachment(form, 3);
        statusLocation.left = new FormAttachment(0, 0);
        statusLocation.bottom = new FormAttachment(100, 0);
        statusLocation.right = new FormAttachment(100, 0);
        compositeStatus.setLayoutData(statusLocation);

        Label labelStatus = new Label (compositeStatus, SWT.CENTER);
        int x = 5, y = 3;
        labelStatus.setBounds (x, y, 150, 20);
        labelStatus.setText ("Loading CAL Parser");
        Label sep1 = new Label (compositeStatus, SWT.SEPARATOR | SWT.VERTICAL);
        x += 150;
        sep1.setBounds (x, 0, 1, 19);
        labelLexicalAnalysis = new Label (compositeStatus, SWT.CENTER);
        x += 1;
        labelLexicalAnalysis.setBounds(x, y, 120, 20);
        Label sep2 = new Label (compositeStatus, SWT.SEPARATOR | SWT.VERTICAL);
        x += 120;
        sep2.setBounds (x, 0, 1, 19);
        labelSyntaxAnalysis = new Label (compositeStatus, SWT.CENTER);
        x += 1;
        labelSyntaxAnalysis.setBounds(x, y, 120, 20);
        Label sep3 = new Label (compositeStatus, SWT.SEPARATOR | SWT.VERTICAL);
        x += 120;
        sep3.setBounds (x, 0, 1, 19);
        Label labelEditor = new Label (compositeStatus, SWT.CENTER);
        x += 1;
        labelEditor.setBounds(x, y, 120, 20);
        Label sep4 = new Label (compositeStatus, SWT.SEPARATOR | SWT.VERTICAL);
        x += 120;
        sep4.setBounds (x, 0, 1, 19);

        shell.addListener(SWT.Dispose, new Listener() {
        	public void handleEvent(Event e)
        	{
        		config.writeToFile (shell);
        		textEditorWindow.closeAll ();
        	}
        });

		/**
		 * Reading dynamic configuration from file "vcal.ini" and setting local parameters
		 */
		config = new Config (display, labelStatus, labelEditor);
		config.setToolBar(tb);
		config.setVCAL (vcal);
		
        /**
        *
        * Create and Set up the menu
        *
        */
        Menu m = vcal.createMenu ();
        shell.setMenuBar(m);

        /**
         * Create and Control text window, status window, output window and UML window
         */
    	textEditorWindow = new TextEditorWindow (form, config);

		shell.pack();
    	shell.setSize(config.getWindowWidth(), config.getWindowHeight());
        shell.open();
        while (!shell.isDisposed())
        {
        	if (!display.readAndDispatch())
        		display.sleep();
        }
        textEditorWindow.closeAll();
        for (int i = 0; i < images.length; i++)
        	images[i].dispose();
		display.dispose();
    }

    /**
     *
     * The standard main method.
     *
     */
    public static void main (String[] args)
    {
   	    createAndShowGUI ();
    }
}
