/* ============================================================================
|
|   Filename:    CALInterpreter.java
|   Dated:       12 Dec, 2006
|   By:          Shahid Alam (salam@sce.carleton.ca)
|
|   Description: See below
|
|   TO DO:
|
 ----------------------------------------------------------------------------*/

package org.vcal.interpreter;

import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import java.io.File;
import java.io.FileInputStream;

import org.vcal.config.Config;
import org.vcal.interpreter.ac.AC;
import org.vcal.interpreter.cc.CC;
import org.vcal.interpreter.oc.OC;

/**
 * 
 * <p>
 * This class reads instructions from CAL Parser
 * <p>
 * It stores these instructions in a List, to be passed to VCAL
 * <p>
 * For now it just reads and stores DAG instructions. It also checks
 * for semantic correctness.
 * 
 * @author Shahid Alam
 * @version 3.0
 * 
 */
public class CALInterpreter
{
	private static Config config = null;
	AC ac = null; CC cc = null; OC oc = null;

	/**
	 *
	 * Constructor Initializing all the objects.
	 * This version only translates / interprets operation context of CAL XML Syntax Tree.
	 * @param xmlFileName Name of the parser output xml file
	 * @param con Config class for global access to status and error windows
	 *
	 */
    public CALInterpreter (String xmlFileName, Config con)
    {
    	config = con;
    	try
    	{
			DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance ();
			DocumentBuilder builder = builderFactory.newDocumentBuilder ();
			Document document = builder.parse(new FileInputStream (new File(xmlFileName)));
			document.getDocumentElement().normalize();

			NodeList packageChildren = document.getElementsByTagName ("PACKAGE");
			for (int p = 0; p < packageChildren.getLength(); p++)
			{
				NodeList nodesOp = packageChildren.item(p).getChildNodes();
				for (int n = 0; n < nodesOp.getLength(); n++)
				{
					Node node = nodesOp.item(n);
					if (!(node instanceof Text || node instanceof Comment))
					{
						String op = node.getNodeName();
						if (op.equals("attrOrAssocContext"))
						{
							checkAttrOrAssocContextOrder (node);
						}
						else if (op.equals("classifierContext"))
						{
							checkClassifierContextOrder (node);
						}
						else if (op.equals("operationContext"))
						{
							if (checkOperationContextOrder (node))
							{
								oc = new OC (config);
								interpretOperationContext (node);
							}
						}
					}
				}
			}
    	}
		catch (Exception e)
		{
			config.setStatusWindowError("CALInterpreter: " + e);
		}
    }

    private boolean checkAttrOrAssocContextOrder (Node node)
    {
    	return false;
    }

    private boolean checkClassifierContextOrder (Node node)
    {
    	return false;
    }

    /**
	 * 
	 * Checking for correct order of operation types which is
	 * PRE+, ACTION*, BODY*, POST+
	 * OR
	 * PRE+, BODY*, ACTION*, POST+
	 * 
	 */
    private boolean checkOperationContextOrder (Node node)
    {
		String contextError = "";
		String opName = "";
		boolean ORDER_OK = true;
		boolean PRE = false, BODY = false, ACTION = false, POST = false;
		NodeList nodes = node.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++)
		{
			Node childElement = nodes.item(i);
			if (!(childElement instanceof Text || childElement instanceof Comment))
			{
				String opType = childElement.getNodeName();
				if (opType.equals("ID"))
				{
					try
					{
						NamedNodeMap attributes = childElement.getAttributes();
						Node item = null;
						if ((item = attributes.getNamedItem ("name")) != null)
							opName += "." + item.getNodeValue();
					}
					catch (NullPointerException e) { }
					NodeList ns = childElement.getChildNodes();
					for (int n1 = 0; n1 < ns.getLength(); n1++)
					{
						childElement = ns.item(n1);
						if (childElement.getNodeName().equals("ID"))
						{
							try
							{
								NamedNodeMap attributes = childElement.getAttributes();
								Node item = null;
								if ((item = attributes.getNamedItem ("name")) != null)
									opName += "." + item.getNodeValue();
							}
							catch (NullPointerException e) { }
						}
					}
				}
				else if (opType.equals("PRE"))
				{
					if (BODY == true || ACTION == true || POST == true)
					{
						ORDER_OK = false;
						opName += ", PRE";
					}
					PRE = true;
				}
				else if (opType.equals("BODY"))
				{
					if (POST == true)
					{
						ORDER_OK = false;
						opName += ", BODY";
					}
					BODY = true;
				}
				else if (opType.equals("ACTION"))
				{
					if (POST == true)
					{
						ORDER_OK = false;
						opName += ", ACTION";
					}
					ACTION = true;
				}
				else if (opType.equals("POST"))
					POST = true;
			}
		}
		if (ORDER_OK == false)
		{
			contextError += "[Context" + opName + "] ";
			/**
			 * Don't break on first error, keep on checking other
			 * errors and report at the end.
			 */
			//break;
		}
		if (contextError.length() > 7)
		{
			config.setStatusText ("Semantic Error");
			config.setStatusWindowError ("Semantic Error:\n" + contextError + "order is not correct");
		}

		return ORDER_OK;
	}

    private void interpretOperationContext (Node node)
    {
    	oc.interpret (node);
    }

    /**
     * 
     * Returns the name of the element in the DAG instruction to be viewed as DAG.
     * @return If there is View DAG Instruction then returns
     * the name of the node to be viewed as DAG else
     * returns null
     * 
     */
    public String viewDAG ()
    {
    	return oc.viewDAG ();
    }
}
