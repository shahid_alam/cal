/* ============================================================================
|
|   Filename:    StmtReturn.java
|   Dated:       14 Jan, 2007
|   By:          Shahid Alam (salam@sce.carleton.ca)
|
|   Description: See below
|
|   TO DO:
|
 ----------------------------------------------------------------------------*/

package org.vcal.interpreter;

/**
 * 
 * <p>
 * This class contains the return type and value of the CAL statement.
 * <b>Writing:</b> The type and values can be set by setting the TYPE and the value
 * variable accroding to the type. For example if the type is BOOLEAN,
 * then set the BOOLEAN_VALUE variable.
 * <p>
 * <b>Reading:</b> First check the type of the return value and then accordingly read that value
 * from the correct type variable. For example if the type is BOOLEAN, then read
 * BOOLEAN_VALUE. If the type is set to UNKNOWN, then there is no value to read.
 * 
 * @author Shahid Alam
 * @version 3.0
 * 
 */
public class StmtReturn
{
	/**
	 * Type of returns
	 */
	public final static int UNKNOWN = 0;
	public final static int BOOLEAN = 1;
	public final static int INTEGER = 2;

   	public static int TYPE = UNKNOWN;
   	public static int INTEGER_VALUE;
   	public static boolean BOOLEAN_VALUE;
}
