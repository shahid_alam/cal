/* ============================================================================
||   Filename:    OC.java
|   Dated:       12 Jan, 2007
|   By:          Shahid Alam (salam@sce.carleton.ca)
|
|   Description: See below
|
|   TO DO:
|
 ----------------------------------------------------------------------------*/

package org.vcal.interpreter.oc;

import org.w3c.dom.*;

import org.vcal.config.Config;
import org.vcal.vdag.ADAG;
import org.vcal.vdag.VisualDAG;
import org.vcal.interpreter.StmtReturn;

/**
 * 
 * <p>
 * This class processes operation context of CAL Syntax Tree in XML
 * <p>
 * 
 * @author Shahid Alam
 * @version 3.0
 * 
 */
public class OC
{
	private static Config config = null;
	private static String VIEW_DAG_ROOT = null;
	
	/**
	 * Constructor
	 * @param con Config class to be used for globally accessing status and error windows
	 */
	public OC (Config con)
	{
		config = con;
	}

	public void interpret (Node node)
	{
		NodeList nodes = node.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++)
		{
			Node childElement = nodes.item(i);
			if (!(childElement instanceof Text || childElement instanceof Comment))
			{
				String opType = childElement.getNodeName();
				if (opType.equals("PRE") || opType.equals("BODY")
						|| opType.equals("ACTION") || opType.equals("POST"))
				{
					StmtReturn.TYPE = StmtReturn.UNKNOWN;
					String condn = "";

					NodeList ns = childElement.getChildNodes();
					for (int n = 0; n < ns.getLength(); n++)
					{
						childElement = ns.item(n);
						if (!(childElement instanceof Text || childElement instanceof Comment))
						{
							String opTypeC = childElement.getNodeName();
							if (opTypeC.equals("CTID"))
								interpretCTID (childElement);
							else if (opTypeC.equals("ID"))
							{
								if (opType.equals("ACTION"))
									interpretActionStmt (childElement);
								else
									interpretID (childElement);
							}

							if (opTypeC.equals("GREATER_THAN"))
								condn = ">";
							else if (opTypeC.equals("LESS_THAN"))
								condn = "<";
							else if (opTypeC.startsWith("NUM_"))
							{
								int value = new Integer (opTypeC.substring(4)).intValue();
								if (StmtReturn.TYPE == StmtReturn.INTEGER)
								{
									StmtReturn.TYPE = StmtReturn.BOOLEAN;
									if (condn.equals(">"))
										StmtReturn.BOOLEAN_VALUE = StmtReturn.INTEGER_VALUE > value;
									else if (condn.equals("<"))
											StmtReturn.BOOLEAN_VALUE = StmtReturn.INTEGER_VALUE < value;
									String str = "\t\t\t" + StmtReturn.INTEGER_VALUE + " " + condn + " ";
									str += value + " = " + StmtReturn.BOOLEAN_VALUE;
									config.setStatusWindowText(str);
								}
							}
							else if (StmtReturn.TYPE == StmtReturn.BOOLEAN)
							{
								String str = "\t\t\t" + StmtReturn.BOOLEAN_VALUE;
								config.setStatusWindowText(str);
							}
						}
					}
				}
			}
		}
	}

	private void interpretCTID (Node node)
	{
		NamedNodeMap attributes = node.getAttributes();
		if (attributes != null && attributes.getLength () > 0)
		{
			try
			{
				String action = null, element = null;
				Node rootElement = null, args = null;
				String obj = attributes.getNamedItem ("name").getNodeValue();
				/**
				 * 
				 * Interpret Dag
				 * 
				 */
				if (obj.equals("Dag"))
				{
					NodeList nodes = node.getChildNodes();
					int e = nodes.getLength();
					for (int i = 0; i < nodes.getLength(); i++)
					{
						node = nodes.item(i);
						if (!(node instanceof Text || node instanceof Comment))
						{
							if (i > e )
							{
								config.setStatusText("Semantic Error");
								String msg = "Semantic Error\n"
												+ obj + " must have one element only\n";
								config.setStatusWindowError(msg);
								break;
							}
							else
							{
								e = i;
								rootElement = node;
							}
						}
					}
					if (rootElement != null)
					{
						boolean done = false;
						nodes = rootElement.getChildNodes();
						for (int i = 0; i < nodes.getLength(); i++)
						{
							node = nodes.item(i);
							if (!(node instanceof Text || node instanceof Comment))
							{
								action = node.getNodeName();
								if (action.equals("ID"))
								{
									attributes = node.getAttributes();
									if (attributes != null && attributes.getLength () > 0)
									{
										action = attributes.getNamedItem ("name").getNodeValue();
										nodes = node.getChildNodes();
										if (nodes.getLength() > 0)
										{
											for (int n = 0; n < nodes.getLength(); n++)
											{
												node = nodes.item(n);
												if (!(node instanceof Text || node instanceof Comment))
												{
													String argStr = node.getNodeName();
													if (argStr.equals("args"))
													{
														done = true;
														args = node;
														break;
													}
													else
													{
														done = true;
														config.setStatusText("Semantic Error");
														String msg = "Semantic Error\n"
																		+ argStr + " is not valid\n";
														config.setStatusWindowError(msg);
														break;
													}
												}
											}
										}
										else
										{
											config.setStatusText("Semantic Error");
											String msg = "Semantic Error\n No arguments";
											config.setStatusWindowError(msg);
											break;
										}
									}
								}
							}
							if (done == true)
								break;
						}
						attributes = rootElement.getAttributes();
						if (attributes != null && attributes.getLength () > 0)
						{
							element = attributes.getNamedItem ("name").getNodeValue();
							String argsArray[] = null;
							if (args != null)
							{
								String argString = "";
								nodes = args.getChildNodes();
								for (int n = 0; n < nodes.getLength(); n++)
								{
									node = nodes.item(n);
									if (!(node instanceof Text || node instanceof Comment))
									{
										if (node.getNodeName().equals("ID"))
										{
											attributes = node.getAttributes();
											if (attributes != null && attributes.getLength () > 0)
												argString += attributes.getNamedItem ("name").getNodeValue() + ":";
										}
									}
								}
								if (argString.length() > 1)
									argsArray = argString.split(":");
							}
							if (element != null && action != null && argsArray != null)
								handleDAGActionWithArgs (element, action, argsArray);
							else if (element != null && action != null && args != null)
								handleDAGAction (element, action, args);
							else if (element != null)
								handleDAG (element);
						}
					}
				}
			}
			catch (NullPointerException e) { e.printStackTrace(); }
		}
	}

	private void handleDAGActionWithArgs (String rootElement, String action, String args[])
	{
		StmtReturn.TYPE = StmtReturn.BOOLEAN;
		StmtReturn.BOOLEAN_VALUE = false;
		if (Config.DEBUG)
		{
			String str = "handleDAGActionWithArgs: " + rootElement + "-->" + action + " (" + args[0];
			for (int l = 1; l < args.length; l++)
			str += ", " + args[l];
			str += ")";
			config.setStatusWindowText(str);
		}
		String errors = "";
		if (rootElement != null && action != null && args != null)
		{
			for (int i = 0; i < args.length; i++)
			{
				try
				{
					ADAG adag = config.getADAG ();
					if (adag != null)
					{
						String nodeName = args[i];
						/**
						 * add to the dag
						 */
						if (action.equals("add"))
						{
							String isDescendant = adag.isDescendant (rootElement, nodeName);
							if (isDescendant.equals("TRUE"))
							{
								int weight = 1;
								adag.addNode (nodeName, weight);
								StmtReturn.BOOLEAN_VALUE = true;
							}
							else
							{
								config.setStatusText("Add Error");
								errors += ("Add Error\n" + isDescendant);
								StmtReturn.BOOLEAN_VALUE = true;
							}
						}
						/**
						 * delete from the dag
						 */
						else if (action.equals("delete"))
						{
							String descendants[] = adag.getChildren(nodeName);
							if (descendants == null)
							{
								adag.delNode (nodeName);
								StmtReturn.BOOLEAN_VALUE = true;
							}
							else
							{
								config.setStatusText("Delete Error");
								String ds = "";
								for (int d = 0; d < descendants.length; d++)
									ds += descendants[d];
								errors += "Delete Error\nDirect Descendants of " + nodeName + ":\n" + ds;
							}
						}
					}
				}
				catch (NullPointerException e) { }
			}
		}
		config.setStatusWindowError(errors);
	}

	private void handleDAGAction (String rootElement, String action, Node args)
	{
		StmtReturn.TYPE = StmtReturn.BOOLEAN;
		StmtReturn.BOOLEAN_VALUE = false;
		String str = "";
		if (Config.DEBUG)
		{
			str = "handleDAGAction: " + rootElement + "-->" + action + "()";
		}
		String errors = "";
		if (rootElement != null && action != null)
		{
			try
			{
				ADAG adag = config.getADAG ();
				if (adag != null)
				{
					/**
					 * Children of the Element
					 */
					if (action.equals("children"))
					{
						String childrens = "";
						String children[] = adag.getChildren(rootElement);
						StmtReturn.BOOLEAN_VALUE = true;
						if (args != null)
						{
							NodeList nodes = args.getParentNode().getChildNodes();
							for (int i = 0; i < nodes.getLength(); i++)
							{
								Node node = nodes.item(i);
								if (!(node instanceof Text || node instanceof Comment))
								{
									if (node.getNodeName().equals("ID"))
									{
										NamedNodeMap attributes = node.getAttributes();
										if (attributes != null && attributes.getLength () > 0)
										{
											action = attributes.getNamedItem ("name").getNodeValue();
											if (Config.DEBUG)
											{
												str += "-->" + action + "()";
												for (int c = 0; c < children.length; c++)
													childrens += "\t" + children[c] + "\n";
												str += "\n\t" + rootElement + " Children:\n" + childrens;
											}
											if (action.equals("size"))
											{
												StmtReturn.TYPE = StmtReturn.INTEGER;
												StmtReturn.INTEGER_VALUE = children.length;
											}
										}
									}
								}
							}
						}
					}
				}
			}
			catch (NullPointerException e) { }
		}
		if (Config.DEBUG)
		{
			config.setStatusWindowText(str);
		}
		config.setStatusWindowError(errors);
	}

	private void handleDAG (String rootElement)
	{
		StmtReturn.TYPE = StmtReturn.BOOLEAN;
		StmtReturn.BOOLEAN_VALUE = false;
		if (Config.DEBUG)
		{
			String str = "handleDAG: " + rootElement;
			config.setStatusWindowText(str);
		}
		String errors = "";
		if (rootElement != null)
		{
			try
			{
				VisualDAG vdag = config.getVDAG ();
				if (vdag != null)
				{
					/**
					 * Display the DAG with element as the root
					 */
					vdag.makeSubGraphVisible(rootElement);
					StmtReturn.BOOLEAN_VALUE = true;
				}
			}
			catch (NullPointerException e) { }
		}
		config.setStatusWindowError(errors);
	}

	private void interpretID (Node node)
	{
		NamedNodeMap attributes = node.getAttributes();
		if (attributes != null && attributes.getLength () > 0)
		{
			String rootElement = null, action = null;
			Node args = null;
			try
			{
				rootElement = attributes.getNamedItem ("name").getNodeValue();
				NodeList nodes = node.getChildNodes();
				for (int i = 0; i < nodes.getLength(); i++)
				{
					node = nodes.item(i);
					if (!(node instanceof Text || node instanceof Comment))
					{
						action = node.getNodeName();
						if (action.equals("ID"))
						{
							attributes = node.getAttributes();
							if (attributes != null && attributes.getLength () > 0)
							{
								action = attributes.getNamedItem ("name").getNodeValue();
								nodes = node.getChildNodes();
								for (int n = 0; n < nodes.getLength(); n++)
								{
									node = nodes.item(n);
									if (!(node instanceof Text || node instanceof Comment))
									{
										String argStr = node.getNodeName();
										if (argStr.equals("args"))
										{
											args = node;
											break;
										}
									}
								}
							}
							break;
						}
					}
				}
			}
			catch (NullPointerException e) { }

			String argsArray[] = null;
			if (args != null)
			{
				String argString = "";
				NodeList nodes = args.getChildNodes();
				for (int n = 0; n < nodes.getLength(); n++)
				{
					node = nodes.item(n);
					if (!(node instanceof Text || node instanceof Comment))
					{
						if (node.getNodeName().equals("ID"))
						{
							attributes = node.getAttributes();
							if (attributes != null && attributes.getLength () > 0)
								argString += attributes.getNamedItem ("name").getNodeValue() + ":";
						}
					}
				}
				if (argString.length() > 1)
					argsArray = argString.split(":");
			}
			if (rootElement != null && action != null && argsArray != null)
				handleDAGActionWithArgs (rootElement, action, argsArray);
			else if (rootElement != null && action != null && args != null)
				handleDAGAction (rootElement, action, args);
		}
	}

	private void interpretActionStmt (Node node)
	{
//System.out.println("ACTION: " + node.getNodeName());
	}

	/**
     * 
     * Returns the name of the element in the DAG instruction to be viewed as DAG.
     * @return If there is View DAG Instruction then returns
     * the name of the node to be viewed as DAG else
     * returns null
     * 
     */
	public String viewDAG ()
	{
    	if (VIEW_DAG_ROOT != null)
    	{
    		if (VIEW_DAG_ROOT.equals ("Model"))
    			return "_ROOT_";
   			else
    			return VIEW_DAG_ROOT;
    	}
    	else
    		return null;
	}
}
