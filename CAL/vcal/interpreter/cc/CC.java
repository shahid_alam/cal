/* ============================================================================
|
|   Filename:    CC.java
|   Dated:       12 Jan, 2007
|   By:          Shahid Alam (salam@sce.carleton.ca)
|
|   Description: See below
|
|   TO DO:
|
 ----------------------------------------------------------------------------*/

package org.vcal.interpreter.cc;

import org.vcal.config.Config;

/**
 * 
 * <p>
 * This version of API's doesn't support classifier context
 * <p>
 * 
 * @author Shahid Alam
 * @version 3.0
 * 
 */
public class CC
{
	private static Config config = null;

	/**
	 * Constructor
	 * @param con Config class to be used for globally accessing status and error windows
	 */
	public CC (Config con)
	{
		config = con;
	}
}
