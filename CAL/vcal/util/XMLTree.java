/* ============================================================================
|
|   Filename:    XMLTree.java
|   Dated:       15 Aug, 2006
|   By:          Shahid Alam (salam@sce.carleton.ca)
|
|   Description: See below
|
|   TO DO:       
|
 ----------------------------------------------------------------------------*/

package org.vcal.util;

import java.io.*;

import org.w3c.dom.*;

import javax.xml.parsers.*;

import java.util.Hashtable;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.swt.widgets.Tree;

import org.vcal.config.Config;

/**
*
* <p>
* Given a filename, Composite parent and style,
* this class generates a Tree representing the
* XML structure contained in the XML file.
* Parses with DOM then copies the tree structure
* (minus text and comment nodes).
*
* @author Shahid Alam
* @version 3.0
* 
*/
public class XMLTree
{
	private static Config config = null;
	private static Element rootElement = null;
	private static Tree tree = null;
	private static String TREE_TOP_ITEM = null;
	private static String SINGLE_ELEMENT = null;
	private static String ONE_ATTRIBUTE = null;
	private static Color BG_COLOR = Config.BLACK_COLOR;
	private static Color FG_COLOR = Config.WHITE_COLOR;
	private static boolean COLOR_ALL_CHILDREN = false;

	private static Hashtable htImages = null;
	
	/**
	*
	* @param xmlFile Name of the XML file  (full pathname) to be processed
	* @param parent Composite parent of the Tree
	* @param style Style of the Tree
	* @throws IOException
	*
	*/
	public XMLTree (String xmlFile, Composite parent, int style, Config con)
	{
		config = con;
		try
		{
			if (xmlFile != null)
			{
				tree = new Tree (parent, style);
				InputStream in = new FileInputStream (new File(xmlFile));
				DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance ();
				DocumentBuilder builder = builderFactory.newDocumentBuilder ();

				Document document = builder.parse(in);
				document.getDocumentElement().normalize();
				rootElement = document.getDocumentElement();

				/**
				 * IMPORTANT:
				 * Name of images should match the XMI tag names
				 */
				File file = null;
				htImages = new Hashtable ();
				String images[] = { "Model", "Class", "Operation", "Attribute" };
				file = new File (Config.MODEL_IMAGE_FILE);
				if (file.exists())
				{
					Image image = new Image (parent.getDisplay(), file.getAbsoluteFile().toString());
					htImages.put (images[0], image);
				}
				file = new File (Config.CLASS_IMAGE_FILE);
				if (file.exists())
				{
					Image image = new Image (parent.getDisplay(), file.getAbsoluteFile().toString());
					htImages.put (images[1], image);
				}
				file = new File (Config.OPERATION_IMAGE_FILE);
				if (file.exists())
				{
					Image image = new Image (parent.getDisplay(), file.getAbsoluteFile().toString());
					htImages.put (images[2], image);
				}
				file = new File (Config.ATTRIBUTE_IMAGE_FILE);
				if (file.exists())
				{
					Image image = new Image (parent.getDisplay(), file.getAbsoluteFile().toString());
					htImages.put (images[3], image);
				}
				file = null;
			}
			else
			{
				config.setStatusWindowError("XMLTree::createTree:Error: XML File " + xmlFile + " Not Present");
				return;
			}
		}
		catch (Exception e)
		{
			config.setStatusWindowError("XMLTree::createTree:Error: " + e);
			return;
		}
	}

	/**
	 * 
	 * This method builds the Tree from XML file
	 * 
	 * @param topElement Top element of the XML tree
	 * @param attribute Name of the attribute to include in the tree, if null
	 * then include all the attributes.
	 *
	 */
	public Tree buildTree (String topElement, String attribute)
	{
		try
		{
			TREE_TOP_ITEM = topElement;
			ONE_ATTRIBUTE = attribute;
			startTopElement (rootElement);
			
			return tree;
		}
		catch (Exception e)
		{
			config.setStatusWindowError("XMLTree::buildTree:Error: " + e);
			return null;
		}
	}

	/**
	 * Color the children
	 * @param element The starting element / elements name
	 * @param bgcolor Bacground color
	 * @param fgcolor Foreground color
	 * @param color If children are to be colored
	 */
	public void makeChildrenColored (String element, Color bgcolor, Color fgcolor, boolean color)
	{
		BG_COLOR = bgcolor;
		FG_COLOR = fgcolor;
		COLOR_ALL_CHILDREN = color;
		SINGLE_ELEMENT = element;
		TreeItem item = tree.getTopItem();
		String text = item.getText();
		int i = text.indexOf('(') - 1;
		String str = null;
		if (i > 0)
			str = text.substring(0, i).trim();
		if (str != null && SINGLE_ELEMENT != null && str.equals(SINGLE_ELEMENT))
			makeChildColored (item, true);
		else
			makeChildColored (item, false);
	}

	/**
	 * Color each child depending on the value of element and COLOR_ALL_CHILDREN
	 * @param item Item of the tree to be colored
	 * @param color If color or not
	 */
	private void makeChildColored (TreeItem item, boolean color)
	{
		if (color)
		{
			item.setBackground(BG_COLOR);
			item.setForeground(FG_COLOR);
		}

		TreeItem items[] = item.getItems();
		for (int n = 0; n < items.length; n++)
		{
			String text = items[n].getText();
			int i = text.indexOf('[') - 1;
			String str = null;
			if (i > 0)
				str = text.substring(0, i).trim();

			if (str != null && SINGLE_ELEMENT != null && str.equals(SINGLE_ELEMENT))
			{
				makeParentsExpand(items[n].getParentItem());
				makeChildColored (items[n], true);
			}
			else if (COLOR_ALL_CHILDREN && color)
				makeChildColored (items[n], true);
			else
				makeChildColored (items[n], false);
		}
	}

	/**
	 * Make parent items expand
	 * @param parent Parent item to expand
	 */
	public void makeParentsExpand (TreeItem parent)
	{
		parent.setExpanded(true);
		TreeItem p = null;
		if ((p = parent.getParentItem()) != null)
			makeParentsExpand (p);
	}

	/**
	 * Find top element of the tree
	 * @param parentElement Element to be searched for the top element
	 */
	public static void startTopElement (Node parentElement)
	{
		if (parentElement.getNodeName().equals(TREE_TOP_ITEM))
		{
			TreeItem item = new TreeItem (tree, SWT.NONE);
			item.setText (treeNodeLabel(item, parentElement));
			tree.setTopItem (item);
			addChildren (item, parentElement);
		}
		else
		{
			NodeList childElements = parentElement.getChildNodes ();
			for(int i = 0; i < childElements.getLength(); i++)
			{
				Node childElement = childElements.item(i);
				short type = childElement.getNodeType();
				if (type != Node.TEXT_NODE && type != Node.COMMENT_NODE)
				{
					startTopElement(childElement);
				}
			}
		}
	}

	/**
	*
	* Recursive method that finds all the child elements
	* and adds them to the parent node. We have two types
	* of nodes here: the ones corresponding to the actual
	* XML structure and the entries of the graphical Tree.
	*
	* @param parent Parent TreeItem for adding child nodes to
	* @param parentXMLElement XML element of the parent node
	*
	*/
	private static void addChildren (TreeItem parent, Node parentXMLElement)
	{
		NodeList childElements = parentXMLElement.getChildNodes ();
		for(int i = 0; i < childElements.getLength(); i++)
		{
			Node childElement = childElements.item(i);
			short type = childElement.getNodeType();
			if (type != Node.TEXT_NODE && type != Node.COMMENT_NODE)
			{
				TreeItem item = new TreeItem (parent, SWT.NONE);
				if (ONE_ATTRIBUTE != null)
					treeNodeLabelWithOneAttribute(item, childElement);
				else
					treeNodeLabel(item, childElement);
				addChildren(item, childElement);
			}
		}
	}

	/**
	*
	* If the XML element has no attributes, the Tree node
	* will just have the name of the XML element. If the
	* XML element has attributes, the names and value of the
	* one attribute will be listed in parens after the XML
	* element name. For example:
	* XML Element: <blah>
	* Tree Node:  blah
	* XML Element: <blah foo="bar" baz="quux">
	* Tree Node:  blah (foo=bar, baz=quux)
	*
	* @param childElement child node XML element
	*
	*/
	private static String treeNodeLabelWithOneAttribute (TreeItem item, Node childElement)
	{
		String treeNodeLabel = childElement.getNodeName ();
		int i = treeNodeLabel.lastIndexOf(':') + 1;
		if (i > 1)
			treeNodeLabel = treeNodeLabel.substring(i).trim();
		try
		{
			Image image = (Image)htImages.get(treeNodeLabel);
			if (image != null)
				item.setImage (image);
		}
		catch (NullPointerException e) { }
		NamedNodeMap attributes = childElement.getAttributes();
		if (attributes != null && attributes.getLength () > 0)
		{
			try
			{
				String obj = attributes.getNamedItem (ONE_ATTRIBUTE).getNodeValue().trim();
				treeNodeLabel += " [" + obj + "]";
//				treeNodeLabel += " [" + ONE_ATTRIBUTE + "=" + obj + "]";
			}
			catch (NullPointerException e) { }
		}

		item.setText(treeNodeLabel);
		return(treeNodeLabel);
	}

	/**
	*
	* If the XML element has no attributes, the Tree node
	* will just have the name of the XML element. If the
	* XML element has attributes, the names and values of the
	* attributes will be listed in parens after the XML
	* element name. For example:
	* XML Element: <blah>
	* Tree Node:  blah
	* XML Element: <blah foo="bar" baz="quux">
	* Tree Node:  blah (foo=bar, baz=quux)
	*
	* @param childElement child node XML element
	*
	*/
	private static String treeNodeLabel (TreeItem item, Node childElement)
	{
		NamedNodeMap elementAttributes = childElement.getAttributes ();
		String treeNodeLabel = childElement.getNodeName ();
		int i = treeNodeLabel.lastIndexOf(':') + 1;
		if (i > 1)
			treeNodeLabel = treeNodeLabel.substring(i).trim();
		try
		{
			Image image = (Image)htImages.get(treeNodeLabel);
			if (image != null)
				item.setImage (image);
		}
		catch (NullPointerException e) { }
		if (elementAttributes != null && elementAttributes.getLength () > 0)
		{
			treeNodeLabel = treeNodeLabel + " [";
			int numAttributes = elementAttributes.getLength();
			for (i = 0; i < numAttributes; i++)
			{
				Node attribute = elementAttributes.item(i);
				if (i > 0)
				{
					treeNodeLabel = treeNodeLabel + ", ";
				}
				treeNodeLabel = treeNodeLabel + attribute.getNodeName() + "=" + attribute.getNodeValue();
			}
			treeNodeLabel = treeNodeLabel + "]";
		}

		item.setText(treeNodeLabel);
		return(treeNodeLabel);
	}
}
