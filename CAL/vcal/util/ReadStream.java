/* ============================================================================
|
|   Filename:    ReadStream.java
|   Dated:       12 Dec, 2006
|   By:          Shahid Alam (salam@sce.carleton.ca)
|
|   Description: See below
|
|   TO DO:
|
 ----------------------------------------------------------------------------*/

package org.vcal.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.vcal.config.Config;

/**
 * 
 * 
 * 
 * @author Shahid Alam
 * @version 3.0
 * 
 */
public class ReadStream
{
	private static Config config = null;
	private static InputStream input = null;

    public ReadStream (InputStream in, Config con)
    {
    	config = con;
    	input = in;
    }

    public String start ()
    {
    	try
    	{
    		InputStreamReader stream = new InputStreamReader (input);
	        BufferedReader br = new BufferedReader(stream);
			StringBuffer sb = new StringBuffer();
			String line = null;
			while ((line = br.readLine()) != null)
				sb.append(line + "\n");

			return sb.toString();
    	}
		catch (IOException e)
		{
			config.setStatusWindowError(e.toString());
			return null;
		}
    }
}
