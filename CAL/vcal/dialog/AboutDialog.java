/* ============================================================================
|
|   Filename:    AboutDialog.java
|   Dated:       22 Dec, 2006
|   By:          Shahid Alam (salam@sce.carleton.ca)
|
|   Description: See below
|
|   TO DO:       
|
 ----------------------------------------------------------------------------*/

package org.vcal.dialog;

import java.io.File;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;

import org.vcal.config.Config;
import org.vcal.window.BrowserWindow;

/**
*
* <p>
* This class implements an About Dialog for VCAL.
* </p>
*
* @author Shahid Alam
* @version 3.0
* 
*/
public class AboutDialog
{
	private static Shell shellParent;
	private static Display displayParent;
	private static Image logo;
	private final static String calLink = "http://chat.carleton.ca/~salam3/thesis/cal/cal.html";
	private final static String calLinkLocal = "http://chat.carleton.ca/~salam3/thesis/cal/cal.html";
	private final static String licenseLink = "html/License.html";
	
	public AboutDialog (Shell shell, Image logoPassed)
	{
		logo = logoPassed;
		displayParent = shell.getDisplay();
		shellParent = shell;
	}

	public void open ()
	{
		Color textColor = Config.FOREST_GREEN_COLOR;
		int fontSize = 8;
		Font textFont = new Font(displayParent, "Verdana", fontSize, SWT.NORMAL);

		final Shell shell = new Shell (shellParent, SWT.SYSTEM_MODAL);
		shell.setBackground (Config.WHITE_COLOR);
		shell.setText("About VCAL");
		shell.setSize(450, 250);
		Point p = shellParent.getLocation();
		int cx = p.x;
		int cy = p.y;
		p = shellParent.getSize();
		cx = cx + p.x/2 - 450/2;
		cy = cy + p.y/2 - 200/2;
		shell.setLocation (cx, cy);

		Label labelLogo = new Label (shell, SWT.NONE);
		labelLogo.setBackground (Config.WHITE_COLOR);
		labelLogo.setAlignment(SWT.CENTER);
		int w = 0; int h = 0;
		if (logo != null)
		{
			/**
			 * Scale the logo image to half
			 */
			w = logo.getImageData().width;
			h = logo.getImageData().height;
			Image i = new Image (displayParent, logo.getImageData().scaledTo(w/2, h/2));
			labelLogo.setImage (i);
		}
		labelLogo.setBounds (0, 0, (w/2)+30, 110);
		p = labelLogo.getSize();

		Label labelSV = new Label (shell, SWT.SEPARATOR | SWT.VERTICAL);
		labelSV.setBounds (p.x, 0, 3, 110);

		Label label = new Label (shell, SWT.NONE);
		label.setBackground (Config.WHITE_COLOR);
		label.setForeground(textColor);
		label.setFont(textFont);
		label.setAlignment(SWT.CENTER);
		String aboutStr = "\n";
		aboutStr += "Visual CAL and Dependency Analysis Tool\n\n";
		aboutStr += "Version 3.0\n";
		aboutStr += "Copyright � 2006\n";
		aboutStr += "By\n";
		aboutStr += "Shahid Alam (salam@sce.carleton.ca)\n";
		label.setText (aboutStr);
		label.setBounds (p.x, 0, 450-p.x, 110);

		Label labelSH = new Label (shell, SWT.SEPARATOR | SWT.HORIZONTAL);
		labelSH.setBounds (0, 110, 450, 3);

		Link link = new Link (shell, SWT.NONE);
		link.setBackground (Config.WHITE_COLOR);
		link.setFont(textFont);
		char SC = '"';
		aboutStr = "<a href=" + SC + calLinkLocal + SC + ">" + calLink + "</a>\n";
		aboutStr += "<a href=" + SC + licenseLink + SC + ">Academic Free License</a>";
		link.setText (aboutStr);
		int sizeX = 450 - 150;
		int sizeY = 4 * fontSize;
		cy = 110 + 30;
		link.setBounds (20, cy, sizeX, sizeY);
		link.setEnabled(false);
		link.addListener(SWT.Selection, new Listener() {
			public void handleEvent (Event e)
			{
				BrowserWindow bw = null;
				if (e.text.equals(licenseLink))
				{
					File file = new File (e.text);
					String fileStr = file.getAbsoluteFile().toString();
					bw = new BrowserWindow (displayParent, e.text, fileStr);
					file = null;
				}
				else
					bw = new BrowserWindow (displayParent, e.text, e.text);
				if (bw != null && bw.open())
					shell.close();
			}
		});

		Button bOK = new Button (shell, SWT.PUSH);
		bOK.setText ("Ok");
		bOK.setSize(75, 25);
		p = link.getSize();
		cx = 450 - 75 - 25;
		cy = cy - (25 - p.y)/4;
		bOK.setLocation(cx, cy);
		bOK.addListener(SWT.Selection, new Listener() {
			public void handleEvent (Event e)
			{
				shell.close ();
			}
		});

		shell.open();
		link.setEnabled(true);
		while (!shell.isDisposed())
		{
			if (!displayParent.readAndDispatch()) displayParent.sleep();
		}
	}
}
