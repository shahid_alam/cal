/* ============================================================================
|
|   Filename:    FileCloseDialog.java
|   Dated:       22 Dec, 2006
|   By:          Shahid Alam (salam@sce.carleton.ca)
|
|   Description: See below
|
|   TO DO:       
|
 ----------------------------------------------------------------------------*/

package org.vcal.dialog;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.graphics.Point;

/**
*
* <p>
* This class implements File Close Dialog for VCAL.
* </p>
*
* @author Shahid Alam
* @version 3.0
* 
*/
public class FileCloseDialog
{
	private static Widget widget = null;
	private static Shell shellParent;
	
	public FileCloseDialog (Shell shell, Widget w)
	{
		shellParent = shell;
		widget = w;
	}

	public void open ()
	{
		final Shell shell = new Shell(shellParent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		shell.setText("File Not Saved ");
		shell.setSize(420, 150);
		/**
		 * Display at the centre of the parent shell
		 */
		Point p = shellParent.getLocation();
		int cx = p.x;
		int cy = p.y;
		p = shellParent.getSize();
		cx = cx + p.x/2 - 420/2;
		cy = cy + p.y/2 - 150/2;
		shell.setLocation (cx, cy);

		final Label label = new Label (shell, SWT.CENTER);
		label.setFont(new Font(shell.getDisplay(), "Courier new", 11, SWT.NORMAL));
		label.setText ("Are you sure you want to close");
		label.setBounds (0, 25, 420, 30);

        /**
         * Create OK button
         */
		final Button btnOkay = new Button (shell, SWT.PUSH);
		btnOkay.setText ("Ok");
		p = shell.getSize();
		cx = (p.x/2 - 75) - 10;
		cy = 65;
		btnOkay.setBounds (cx, cy, 75, 25);
		btnOkay.addListener(SWT.Selection, new Listener() {
			public void handleEvent (Event event)
			{
				shell.setText("Closing File ");
				shell.close ();
				if (widget != null)
					widget.dispose ();
			}
		});

        /**
         * Create Cancel button
         */
		Button bCancel = new Button (shell, SWT.PUSH);
		bCancel.setText ("Cancel");
		cx = (p.x/2) + 10;
		bCancel.setBounds (cx, cy, 75, 25);
		bCancel.addListener(SWT.Selection, new Listener() {
			public void handleEvent (Event e)
			{
				shell.close ();
			}
		});

		shell.open();
		Display display = shellParent.getDisplay();
		while (!shell.isDisposed())
		{
			if (!display.readAndDispatch()) display.sleep();
		}
	}
}
