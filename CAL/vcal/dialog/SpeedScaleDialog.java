/* ============================================================================
|
|   Filename:    SpeedScaleDialog.java
|   Dated:       22 Dec, 2006
|   By:          Shahid Alam (salam@sce.carleton.ca)
|
|   Description: See below
|
|   TO DO:       
|
 ----------------------------------------------------------------------------*/

package org.vcal.dialog;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.graphics.Point;

/**
*
* <p>
* This class implements Speed Scale Dialog to adjust the scanning speed of VCAL.
* </p>
*
* @author Shahid Alam
* @version 3.0
* 
*/
public class SpeedScaleDialog
{
	private static Shell shellParent;
	private static Scale speedScale;
	private static Button btnOkay;
	private static InputDialogData data;
	
	private static int MIN_SLIDE = 0;
	private static int CURR_SPEED = 0;
	private static int MAX_SLIDE = 0;

	public SpeedScaleDialog (Shell shell)
	{
		shellParent = shell;
	}

	public void setSlideParameters (int min, int speed, int max)
	{
		MIN_SLIDE = min;
		CURR_SPEED = speed;
		MAX_SLIDE = max;
	}

	public InputDialogData open ()
	{
		data = new InputDialogData();
		final Shell shell = new Shell(shellParent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		shell.setText("Scanning Speed " + CURR_SPEED);
		shell.setSize(420, 150);
		/**
		 * Display at the centre of the parent shell
		 */
		Point p = shellParent.getLocation();
		int cx = p.x;
		int cy = p.y;
		p = shellParent.getSize();
		cx = cx + p.x/2 - 420/2;
		cy = cy + p.y/2 - 150/2;
		shell.setLocation (cx, cy);

		/**
		 * Create scale for setting the scanning speed
		 */
		speedScale = new Scale (shell, SWT.NONE);
		speedScale.setSelection (CURR_SPEED);
		speedScale.setMinimum (MIN_SLIDE);
		speedScale.setMaximum (MAX_SLIDE);
		speedScale.setPageIncrement (1);
		speedScale.setSize(350, 50);
		speedScale.setLocation(40, 20);
		speedScale.addListener(SWT.Selection, new Listener() {
			public void handleEvent (Event event)
			{
				shell.setText("Scanning Speed " + speedScale.getSelection());
			}
		});

        /**
         * Create OK button
         */
		btnOkay = new Button (shell, SWT.PUSH);
		btnOkay.setText ("Ok");
		btnOkay.setSize(65, 25);
		btnOkay.setLocation(40+(350/2)-(65/2), 80);
		btnOkay.addListener(SWT.Selection, new Listener() {
			public void handleEvent (Event event)
			{
				data.setButtonResponse(event.widget == btnOkay);
				data.setSpeedResponse(speedScale.getSelection());
				shell.setText("Scanning Speed " + speedScale.getSelection());
				shell.close ();
			}
		});
		
		shell.open();
		Display display = shellParent.getDisplay();
		while (!shell.isDisposed())
		{
			if (!display.readAndDispatch()) display.sleep();
		}

		return data;
	}

	public class InputDialogData
	{
		int speed;
		boolean buttonResponse;
		InputDialogData()
		{
			setSpeedResponse(0);
			setButtonResponse(false);
		}
		public boolean isButtonResponse()
		{
			return buttonResponse;
		}
		public void setButtonResponse(boolean b)
		{
			buttonResponse = b;
		}
		public int getSpeedResponse()
		{
			return speed;
		}
		public void setSpeedResponse(int s)
		{
			speed = s;
		}
	}
}
