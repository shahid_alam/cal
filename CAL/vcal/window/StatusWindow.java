/* ============================================================================
|
|   Filename:    StatusWindow.java
|   Dated:       23 Dec, 2006
|   By:          Shahid Alam (salam@sce.carleton.ca)
|
|   Description: See below
|
|   TO DO:       
|
 ----------------------------------------------------------------------------*/

package org.vcal.window;

import java.io.File;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.custom.*;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Text;

import org.vcal.config.Config;

public class StatusWindow
{
	private static Config config = null;
	private static CTabFolder folder = null;
	private static SashForm form = null;
	private static Image imageStatus = null, imageError = null;
	private static Text textNormal = null, textError = null;
	private static int folderNo = 0;

	public StatusWindow (SashForm f, CTabFolder mf[], Config con)
	{
		config = con;
		config.setStatusWindow (this);
		form = f;

		folder = new CTabFolder (form, SWT.BORDER | SWT.FLAT);
    	folder.setMRUVisible(true);
    	folder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
    	folder.setSimple(false);
    	folder.setUnselectedImageVisible(false);
    	folder.setUnselectedCloseVisible(true);
    	folder.setSelectionBackground(Config.LIGHT_GRAY_COLOR);
    	folder.setSelectionForeground(Config.BLACK_COLOR);

        File file = new File (Config.STATUS_WINDOW_IMAGE_FILE);
        if (file.exists())
        	imageStatus = new Image (form.getDisplay(), file.getAbsoluteFile().toString());
        file = new File (Config.ERROR_WINDOW_IMAGE_FILE);
        if (file.exists())
        	imageError = new Image (form.getDisplay(), file.getAbsoluteFile().toString());
        file = null;

    	folder.addSelectionListener (new SelectionListener () {
    		public void widgetSelected (SelectionEvent e)
    		{
    			CTabItem item = (CTabItem)e.item;
				FontData fData[] = item.getFont().getFontData();
				if (fData[0] != null && fData[0].getStyle() != SWT.NORMAL)
				{
					int h = fData[0].getHeight();
					String fontName = fData[0].getName();
					item.setFont(new Font(folder.getDisplay(), fontName, h, SWT.NORMAL));
				}
    		}
    		public void widgetDefaultSelected (SelectionEvent e) { }
    	});

    	folder.setVisible(false);
	}

	public CTabFolder getFolder()
	{
		return folder;
	}

	/**
	 * Open status window according to the number.
	 * There are two windows one for normal text information
	 * and other for error messages. If number is '1' then
	 * normal window is opened and if number is '2' then
	 * open error window.
	 * @param number Window number to open
	 */
	public void open (int number)
	{
		try
		{
			if (folder != null)
			{
				final CTabItem item = new CTabItem (folder, SWT.CLOSE);
				/**
				 * Status window
				 */
				if (number == 1)
				{
					if (imageStatus != null)
						item.setImage(imageStatus);
					textNormal = new Text (folder, SWT.BORDER | SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
					textNormal.setEditable(false);
					item.setControl(textNormal);
					item.setText("Status");
					textNormal.addModifyListener(new ModifyListener() {
						public void modifyText (ModifyEvent e)
						{
							if (!item.equals(folder.getSelection()))
							{
								FontData fData[] = item.getFont().getFontData();
								if (textNormal.getText().length() > 0)
								{
									if (fData[0] != null)
									{
										int h = fData[0].getHeight();
										String fontName = fData[0].getName();
										item.setFont(new Font(folder.getDisplay(), fontName, h, SWT.BOLD));
									}
								}
								else
								{
									if (fData[0] != null)
									{
										int h = fData[0].getHeight();
										String fontName = fData[0].getName();
										item.setFont(new Font(folder.getDisplay(), fontName, h, SWT.NORMAL));
									}
								}
							}
						}
					});
				}
				/**
				 * Error window
				 */
				else if (number == 2)
				{
					if (imageError != null)
						item.setImage(imageError);
					textError = new Text (folder, SWT.BORDER | SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
					textError.setEditable(false);
					item.setControl(textError);
					item.setText("Error");
					textError.addModifyListener(new ModifyListener() {
						public void modifyText (ModifyEvent e)
						{
							if (!item.equals(folder.getSelection()))
							{
								FontData fData[] = item.getFont().getFontData();
								if (textError.getText().length() > 0)
								{
									if (fData[0] != null)
									{
										int h = fData[0].getHeight();
										String fontName = fData[0].getName();
										item.setFont(new Font(folder.getDisplay(), fontName, h, SWT.BOLD));
									}
								}
								else
								{
									if (fData[0] != null)
									{
										int h = fData[0].getHeight();
										String fontName = fData[0].getName();
										item.setFont(new Font(folder.getDisplay(), fontName, h, SWT.NORMAL));
									}
								}
							}
						}
					});
				}
				folderNo++;
    			MenuItem mitem = config.getVCAL().getMenuItem("View");
    			if (mitem != null)
    				config.getVCAL().enableChildren(mitem, item.getText(), false);

		        item.addDisposeListener(new DisposeListener() {
		        	public void widgetDisposed(DisposeEvent e)
		        	{
		        		folderNo--;
		        		if (folderNo < 1)
		        		{
		        			folder.setVisible (false);
		        			form.layout(true);
		        		}
	        			MenuItem mitem = config.getVCAL().getMenuItem("View");
	        			if (mitem != null)
	        				config.getVCAL().enableChildren(mitem, item.getText(), true);
		        	}
		        });

		    	folder.setVisible(true);
		    	if (folderNo < 2)
		    		form.layout(true);
			}
		}
		catch (Exception e)
		{
			config.setStatusWindowError("TextEditorWindow::editFile:Error: " + e);
		}
	}

	public void setText (String str)
	{
		if (str != null && str.length() > 1)
		{
			if (textNormal != null)
			{
				if (textNormal.getText().length() <= Config.STATUS_WINDOW_TEXT_LIMIT)
					textNormal.append (str + "\n");
				else
					textNormal.setText (str + "\n");
			}
		}
	}

	public void setError (String str)
	{
		if (str != null)
		{
			if (str.equals("CLEAR"))
				textError.setText ("");
			else if (str.length() > 1)
				textError.setText (str);
		}
	}

	public void closeAll ()
	{
		if (folder != null)
		{
			CTabItem items[] = folder.getItems();
			for (int n = 0; n < items.length; n++)
				items[n].dispose();
		}
	}
}
