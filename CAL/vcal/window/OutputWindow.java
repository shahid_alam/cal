/* ============================================================================
|
|   Filename:    OutputWindow.java
|   Dated:       23 Dec, 2006
|   By:          Shahid Alam (salam@sce.carleton.ca)
|
|   Description: See below
|
|   TO DO:       
|
 ----------------------------------------------------------------------------*/

package org.vcal.window;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Component;
import java.io.File;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.widgets.ToolBar;

import org.vcal.VCAL;
import org.vcal.config.Config;
import org.vcal.vdag.VisualDAG;

public class OutputWindow
{
	private static Config config = null;
	private static CTabFolder folder = null, folders[];
	private static SashForm form = null;
	private static int folderNo = 0;
	private static Composite COMP_AWT_WIDGET = null;

	public OutputWindow (SashForm f, CTabFolder mf[], Config c)
	{
		config = c;
		folders = mf;
		form = f;

		folder = new CTabFolder (form, SWT.BORDER | SWT.FLAT);
    	folder.setMRUVisible(true);
    	folder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
    	folder.setSimple(false);
    	folder.setUnselectedImageVisible(false);
    	folder.setUnselectedCloseVisible(true);
    	folder.setSelectionBackground(Config.STEEL_COLOR);
    	folder.setSelectionForeground(Config.WHITE_COLOR);

    	folder.addSelectionListener (new SelectionListener () {
    		public void widgetSelected (SelectionEvent e)
    		{
    			CTabItem item = (CTabItem)e.item;
				FontData fData[] = item.getFont().getFontData();
				if (fData[0] != null && fData[0].getStyle() != SWT.NORMAL)
				{
					int h = fData[0].getHeight();
					String fontName = fData[0].getName();
					item.setFont(new Font(folder.getDisplay(), fontName, h, SWT.NORMAL));
				}

				if (item.getText() != null)
    			{
					if (config.getVDAG() != null)
					{
						String data = item.getText();
		    			if (data.equals("Dependency Analysis"))
		    				config.getVDAG().toolVisible(true);
		    			else
		    				config.getVDAG().toolVisible(false);
					}
    			}
    		}
    		public void widgetDefaultSelected (SelectionEvent e) { }
    	});

		folder.setVisible(false);
    	form.setVisible(false);
	}

	public CTabFolder getFolder()
	{
		return folder;
	}

	private Control getAsWidget (Composite parent, CTabItem item, Object obj)
	{
		COMP_AWT_WIDGET = new Composite (parent, SWT.EMBEDDED);
		COMP_AWT_WIDGET.setLayout(new FillLayout());
		final Frame frame = SWT_AWT.new_Frame (COMP_AWT_WIDGET);

        if (obj != null)
        {
        	Rectangle r = folder.getBounds();
        	final JScrollPane vdagPane = new JScrollPane ((Component)obj);
        	final int x = vdagPane.getVerticalScrollBar().getPreferredSize().width / 2;
        	final int y = vdagPane.getHorizontalScrollBar().getPreferredSize().width / 2;
        	vdagPane.setPreferredSize (new Dimension(r.width-x, r.height-y));
        	JPanel panel = new JPanel ();
        	panel.add (vdagPane);
        	frame.add (panel);
        	frame.pack();

        	folder.addControlListener(new ControlListener () {
        		public void controlMoved (ControlEvent e) { }
        		public void controlResized (ControlEvent e)
        		{
                	Rectangle r = folder.getBounds();
       	        	vdagPane.setPreferredSize (new Dimension(r.width-x, r.height-y));
        		}
        	});
        }

		return COMP_AWT_WIDGET;
	}

	public boolean setControl (String title, Object object, boolean AWT_GRAPHIC_WIDGET)
	{
		CTabItem item = null;
		CTabItem items[] = folder.getItems();
		for (int i = 0; i < items.length; i++)
		{
			if (items[i].getText().equals(title))
			{
				item = items[i];
				break;
			}
		}
		if (item != null)
		{
			CTabItem itemS = folder.getSelection();
			boolean thisItemNotSelected = true;
			if (itemS != null && itemS.equals(item))
				thisItemNotSelected = false;
			if (AWT_GRAPHIC_WIDGET)
			{
				if (COMP_AWT_WIDGET == null)
					object = getAsWidget (folder, item, object);
				else
					object = COMP_AWT_WIDGET;
				if (Config.GRAPH_VISIBLE && thisItemNotSelected)
				{
					FontData fData[] = item.getFont().getFontData();
					if (fData[0] != null)
					{
						int h = fData[0].getHeight();
						String fontName = fData[0].getName();
						item.setFont(new Font(folder.getDisplay(), fontName, h, SWT.BOLD));
					}
				}
			}
			else if (thisItemNotSelected)
			{
				FontData fData[] = item.getFont().getFontData();
				if (fData[0] != null)
				{
					int h = fData[0].getHeight();
					String fontName = fData[0].getName();
					item.setFont(new Font(folder.getDisplay(), fontName, h, SWT.BOLD));
				}
			}
			item.setControl((Control)object);
			return true;
		}
		return false;
	}

	public void open (String title, Object object, Image image, boolean AWT_GRAPHIC_WIDGET)
	{
		try
		{
			if (folder != null)
			{
		    	if (folderNo < 2)
		    	{
		    		folder.pack();
			    	folder.setVisible(true);
			    	form.pack();
		        	form.setVisible(true);
		    		form.layout(true);
		    		if (form.getParent() != null)
		    			form.getParent().layout(true);
		    	}
				final CTabItem item = new CTabItem (folder, SWT.CLOSE);
				CTabItem itemS = folder.getSelection();
				boolean thisItemNotSelected = true;
				if (itemS != null && itemS.equals(item))
					thisItemNotSelected = false;
				if (AWT_GRAPHIC_WIDGET)
				{
					object = getAsWidget (folder, item, object);
					if (Config.GRAPH_VISIBLE && thisItemNotSelected)
					{
						FontData fData[] = item.getFont().getFontData();
						if (fData[0] != null)
						{
							int h = fData[0].getHeight();
							String fontName = fData[0].getName();
							item.setFont(new Font(folder.getDisplay(), fontName, h, SWT.BOLD));
						}
					}
				}
				else if (thisItemNotSelected)
				{
					FontData fData[] = item.getFont().getFontData();
					if (fData[0] != null)
					{
						int h = fData[0].getHeight();
						String fontName = fData[0].getName();
						item.setFont(new Font(folder.getDisplay(), fontName, h, SWT.BOLD));
					}
				}
				item.setControl((Control)object);
				item.setText(title);
				if (image != null)
					item.setImage(image);
				folderNo++;

		        item.addDisposeListener(new DisposeListener() {
		        	public void widgetDisposed(DisposeEvent e)
		        	{
		        		if (item.getText().equals("Dependency Analysis"))
		        		{
		        			if (config.getVDAG() != null)
		        				config.getVDAG().toolVisible(false);
		        			Config.DA_WIN_ALREADY_OPEN = false;
		        			COMP_AWT_WIDGET = null;
		        		}
		        		else if (item.getText().equals("Lexical Analysis"))
		        			Config.LA_WIN_ALREADY_OPEN = false;
		        		else if (item.getText().equals("Syntax Analysis"))
		        			Config.SA_WIN_ALREADY_OPEN = false;
		        		folderNo--;
		        		if (folderNo < 1)
		        		{
		        			folder.setVisible (false);
		        			form.layout(true);
		        			Control controls[] = form.getChildren();
		        			boolean isVisible = false;
		        			for (int n = 0; n < controls.length; n++)
		        			{
		        				if (controls[n].isVisible())
		        				{
		        					isVisible = true;
		        					break;
		        				}
		        			}
		        			if (!isVisible)
		        			{
			        			form.setVisible(false);
			        			form.getParent().layout(true);
		        			}
		        			controls = null;
		        		}
	        			MenuItem mitem = config.getVCAL().getMenuItem("View");
	        			if (mitem != null)
	        				config.getVCAL().enableChildren(mitem, item.getText(), true);
		        	}
		        });
			}
			MenuItem mitem = config.getVCAL().getMenuItem("View");
			if (mitem != null)
				config.getVCAL().enableChildren(mitem, title, false);
		}
		catch (Exception e)
		{
			config.setStatusWindowError("OutputWindow::open:Error: " + e);
		}
	}

	public void closeAll ()
	{
		if (folder != null)
		{
			CTabItem items[] = folder.getItems();
			for (int n = 0; n < items.length; n++)
				items[n].dispose();
		}
	}
}
