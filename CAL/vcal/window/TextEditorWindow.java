/* ============================================================================
|
|   Filename:    TextEditorWindow.java
|   Dated:       23 Dec, 2006
|   By:          Shahid Alam (salam@sce.carleton.ca)
|
|   Description: See below
|
|   TO DO:       
|
 ----------------------------------------------------------------------------*/

package org.vcal.window;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Control;

import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.MouseEvent;

import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.layout.GridData;

import org.vcal.vdag.VisualDAG;
import org.vcal.config.Config;
import org.vcal.dialog.FileCloseDialog;
import org.vcal.vdag.ProcessXMI;
import org.vcal.window.OutputWindow;
import org.vcal.window.StatusWindow;
import org.vcal.window.UMLWindow;

public class TextEditorWindow
{
	private static Config config = null;
	private static boolean threadRetVal = false;
    private static Image images[];
	private static CTabFolder folders[];
	private static SashForm form, formLeft, formRight;
	private static Image image = null;
	private static Image imageLexical = null, imageSyntax = null, imageDependency = null;
	private static CTabFolder folder = null;
	private static int folderNo = 0;

	private static ProcessXMI xmi = null;
	private static StatusWindow statusWindow = null;
	private static OutputWindow outputWindow = null;
	private static UMLWindow umlWindow = null;

	public TextEditorWindow (SashForm f, Config con)
	{
		config = con;
    	folders = new CTabFolder[4];
		form = f;

		formLeft = new SashForm (form, SWT.NONE | SWT.VERTICAL | SWT.SMOOTH);
		formLeft.SASH_WIDTH = 1;
    	folder = new CTabFolder (formLeft, SWT.BORDER | SWT.FLAT);
    	folders[0] = folder;
    	folder.setMRUVisible(true);
    	folder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
    	folder.setSimple(false);
    	folder.setUnselectedImageVisible(false);
    	folder.setUnselectedCloseVisible(true);
    	folder.setSelectionBackground(Config.STEEL_COLOR);
    	folder.setSelectionForeground(Config.WHITE_COLOR);

    	statusWindow = new StatusWindow (formLeft, folders, config);
    	folders[1] = statusWindow.getFolder();
    	formLeft.setWeights(new int[] { 80, 20 });

    	formRight = new SashForm (form, SWT.NONE | SWT.HORIZONTAL | SWT.SMOOTH);
    	formRight.SASH_WIDTH = 1;
		outputWindow = new OutputWindow (formRight, folders, config);
		folders[2] = outputWindow.getFolder();
		umlWindow = new UMLWindow (formRight, folders, config);
		folders[3] = umlWindow.getFolder();
		formRight.setWeights(new int[] { 70, 30 });

    	form.setWeights(new int[] { 30, 70 });

        File file = new File (Config.TEXT_EDITOR_WINDOW_IMAGE_FILE);
        if (file.exists())
        	image = new Image (form.getDisplay(), file.getAbsoluteFile().toString());

        file = new File (Config.LEXICAL_ANALYSIS_WINDOW_IMAGE_FILE);
        if (file.exists())
        	imageLexical = new Image (form.getDisplay(), file.getAbsoluteFile().toString());
        file = new File (Config.SYNTAX_ANALYSIS_IMAGE_FILE);
        if (file.exists())
        	imageSyntax = new Image (form.getDisplay(), file.getAbsoluteFile().toString());
        file = new File (Config.DEPENDENCY_ANALYSIS_WINDOW_IMAGE_FILE);
        if (file.exists())
        	imageDependency = new Image (form.getDisplay(), file.getAbsoluteFile().toString());
        file = null;

    	folder.setVisible(false);
		formLeft.setVisible(false);
		formRight.setVisible(false);
	}

	public void editFile (Config con, String fileFullPathName)
	{
		try
		{
			if (folder != null)
			{
				final Config config = con;
				boolean FILE_ALREADY_OPENED = false;
				/**
				 * Check if file already opened
				 */
				String files = fileFullPathName;
				if (files == null)
					files = Config.NO_NAME;
				CTabItem items[] = folder.getItems();
				for (int i = 0; i < items.length; i++)
				{
					if (files.equals(items[i].getToolTipText()))
					{
						config.setStatusText("File Already Opened");
						FILE_ALREADY_OPENED = true;
						break;
					}
				}

				if (!FILE_ALREADY_OPENED)
				{
					final StyledText text = new StyledText (folder, SWT.BORDER | SWT.MULTI
															| SWT.H_SCROLL | SWT.V_SCROLL);
					text.setSelectionForeground(Config.YELLOW_COLOR);
					text.setSelectionBackground(Config.FOREST_GREEN_COLOR);
			    	final CTabItem citem = new CTabItem (folder, SWT.CLOSE);
			    	if (image != null)
			    		citem.setImage(image);
					citem.setControl(text);
			        /**
			         * Old file opened
			         */
					if (fileFullPathName != null)
					{
						File file = new File (fileFullPathName);
						citem.setText(file.getName());
						BufferedReader fileB = new BufferedReader (new FileReader (fileFullPathName));
						String line = null, str = "";
						while ((line = fileB.readLine()) != null)
							str += line + "\n";
						text.setText(str);
						file = null;
						fileB.close();
						citem.setData("OldFile");
						MenuItem item = config.getVCAL ().getMenuItem("File");
						config.getVCAL ().enableChildren(item, "Save", false);
						item = config.getVCAL ().getMenuItem("Compiler");
						if (item != null)
						{
							config.getVCAL ().enableChildren(item, "Generate Lexical Analysis Output", true);
							config.getVCAL ().enableChildren(item, "Run", true);
						}
						config.enableToolItem(config.getToolBar(), "Save", false);
						config.enableToolItem(config.getToolBar(), "Run", true);
						Config.CURRENT_CAL_FILE = fileFullPathName;
					}
					/**
					 * New file started
					 */
					else
					{
						citem.setText(Config.NO_NAME);
						citem.setData("NewFile");
						MenuItem item = config.getVCAL ().getMenuItem("File");
						config.getVCAL ().enableChildren(item, "Save", false);
						item = config.getVCAL ().getMenuItem("Compiler");
						if (item != null)
						{
							config.getVCAL ().enableChildren(item, "Generate Lexical Analysis Output", false);
							config.getVCAL ().enableChildren(item, "Run", false);
						}
						config.enableToolItem(config.getToolBar(), "Save", false);
						config.enableToolItem(config.getToolBar(), "Run", false);
						Config.CURRENT_CAL_FILE = Config.NO_NAME;
					}
					citem.setToolTipText(Config.CURRENT_CAL_FILE);
					folder.setSelection(citem);
					folderNo++;

					text.addFocusListener(new FocusListener () {
						public void focusGained (FocusEvent e)
						{
							int charOffset = text.getCaretOffset();
							int line = text.getLineAtOffset (charOffset);
							int column = charOffset - text.getOffsetAtLine(line);
							config.setEditorText ((line + 1) + ":" + (column + 1));
						}
						public void focusLost (FocusEvent e)
						{
							config.setEditorText ("");
						}
					});

					text.addMouseListener(new MouseListener () {
						public void mouseDown (MouseEvent e)
						{
							int charOffset = text.getCaretOffset();
							int line = text.getLineAtOffset (charOffset);
							int column = charOffset - text.getOffsetAtLine(line);
							config.setEditorText ((line + 1) + ":" + (column + 1));
						}
						public void mouseUp (MouseEvent e) { }
						public void mouseDoubleClick (MouseEvent e) { }
					});

					text.addKeyListener(new KeyListener () {
						public void keyPressed (KeyEvent e)
						{
							int charOffset = text.getCaretOffset();
							int line = text.getLineAtOffset (charOffset);
							int column = charOffset - text.getOffsetAtLine(line);
							config.setEditorText ((line + 1) + ":" + (column + 1));
						}
						public void keyReleased (KeyEvent e) { }
					});

					text.addModifyListener(new ModifyListener () {
						public void modifyText (ModifyEvent e)
						{
							String title =citem.getText().trim();
							char star = title.charAt(0);
							if (star != Config.FILE_MODIFIED_CHAR)
								citem.setText(Config.FILE_MODIFIED_CHAR + " " + citem.getText());
							if (text.getText().length() > 0)
							{
								MenuItem item = config.getVCAL ().getMenuItem("File");
								if (item != null)
								{
									config.getVCAL ().enableChildren(item, "Save\tCtrl+S", true);
									config.getVCAL ().enableChildren(item, "Close", true);
									config.getVCAL ().enableChildren(item, "Print...\tCtrl+P", false);
								}
								item = config.getVCAL ().getMenuItem("Compiler");
								if (item != null)
								{
									config.getVCAL ().enableChildren(item, "Generate Lexical Analysis Output", false);
									config.getVCAL ().enableChildren(item, "Run", false);
								}
								config.enableToolItem(config.getToolBar(), "Save", true);
								config.enableToolItem(config.getToolBar(), "Run", false);
							}
							else
							{
								MenuItem item = config.getVCAL ().getMenuItem("Compiler");
								if (item != null)
									config.getVCAL ().enableChildren(item, "Run", false);
								config.enableToolItem(config.getToolBar(), "Run", false);
							}
						}
					});

					folder.addSelectionListener(new SelectionListener () {
						public void widgetSelected (SelectionEvent e)
						{
							CTabItem i = (CTabItem)e.item;
							String txt = ((StyledText)i.getControl()).getText();
							String title = i.getText().trim();
							char star = title.charAt(0);
							if (star == Config.FILE_MODIFIED_CHAR)
							{
								MenuItem item = config.getVCAL ().getMenuItem("File");
								if (item != null)
								{
									config.getVCAL ().enableChildren(item, "Save\tCtrl+S", true);
									config.getVCAL ().enableChildren(item, "Print...\tCtrl+P", false);
								}
								item = config.getVCAL ().getMenuItem("Compiler");
								if (item != null)
				    			{
				    				config.getVCAL ().enableChildren(item, "Generate Lexical Analysis Output", false);
				    				config.getVCAL ().enableChildren(item, "Run", false);
				    			}
								config.enableToolItem(config.getToolBar(), "Save", true);
								config.enableToolItem(config.getToolBar(), "Run", false);
							}
							else
							{
								MenuItem item = config.getVCAL ().getMenuItem("File");
								if (item != null)
								{
									config.getVCAL ().enableChildren(item, "Save\tCtrl+S", false);
									config.getVCAL ().enableChildren(item, "Print...\tCtrl+P", true);
								}
								config.enableToolItem(config.getToolBar(), "Save", false);
								item = config.getVCAL ().getMenuItem("Compiler");
								if (txt.length() > 0)
								{
									if (item != null)
					    			{
					    				config.getVCAL ().enableChildren(item, "Generate Lexical Analysis Output", true);
					    				config.getVCAL ().enableChildren(item, "Run", true);
					    			}
									config.enableToolItem(config.getToolBar(), "Run", true);
								}
								else
								{
									if (item != null)
					    			{
					    				config.getVCAL ().enableChildren(item, "Generate Lexical Analysis Output", false);
					    				config.getVCAL ().enableChildren(item, "Run", false);
					    			}
									config.enableToolItem(config.getToolBar(), "Run", false);
								}
							}
							Config.CURRENT_CAL_FILE = i.getToolTipText();
						}
						public void widgetDefaultSelected (SelectionEvent e) { }
					});
	
			        citem.addDisposeListener(new DisposeListener() {
			        	public void widgetDisposed(DisposeEvent e)
			        	{
			        		folderNo--;
			        		if (folderNo < 1)
			        		{
			        			MenuItem item = config.getVCAL ().getMenuItem("File");
			        			if (item != null)
			        			{
			        				config.getVCAL ().enableChildren(item, "Save\tCtrl+S", false);
			        				config.getVCAL ().enableChildren(item, "Save As...", false);
			        				config.getVCAL ().enableChildren(item, "Close", false);
			        				config.getVCAL ().enableChildren(item, "Print...\tCtrl+P", false);
			        			}
			        			item = config.getVCAL ().getMenuItem("Compiler");
			        			if (item != null)
			        			{
			        				config.getVCAL ().enableChildren(item, "Generate Lexical Analysis Output", false);
			        				config.getVCAL ().enableChildren(item, "Run", false);
			        			}
			        			item = config.getVCAL ().getMenuItem("View");
			        			if (item != null)
			        			{
			        				config.getVCAL ().enableChildren(item, "Lexical Analaysis", false);
			        				config.getVCAL ().enableChildren(item, "Syntax Analaysis", false);
			        				config.getVCAL ().enableChildren(item, "Code Generated", false);
			        				config.getVCAL ().enableChildren(item, "Dependency Analaysis", false);
			        				config.getVCAL ().enableChildren(item, "UML Model", false);
			        			}
			        			config.enableToolItem(config.getToolBar(), "Save", false);
			        			config.enableToolItem(config.getToolBar(), "Print", false);
			        			config.enableToolItem(config.getToolBar(), "Run", false);
	
			        			statusWindow.closeAll ();
				        		outputWindow.closeAll ();
			        			folder.setVisible (false);
			        			formLeft.setVisible(false);
			        			formLeft.layout(true);
			        			formRight.setVisible(false);
			        			formRight.layout(true);
			        			form.layout(true);
			        		}
			        	}
			        });
	
			    	if (folderNo < 2)
			    	{
						MenuItem item = config.getVCAL ().getMenuItem("File");
						if (item != null)
						{
							config.getVCAL ().enableChildren(item, "Save As...", true);
							config.getVCAL ().enableChildren(item, "Close", true);
							config.getVCAL ().enableChildren(item, "Print\tCtrl+P", true);
						}
	        			config.enableToolItem(config.getToolBar(), "Print", true);
			    		/**
			    		 * Open status windows for info (1) and error (2)
			    		 */
			    		statusWindow.open (1);
			    		statusWindow.open (2);
			        	folder.setVisible(true);
	        			formLeft.setVisible(true);
	        			formLeft.layout(true);
	        			formRight.setVisible(true);
	        			formRight.layout(true);
	        			form.layout(true);
			    	}
				}
			}
		}
		catch (Exception e)
		{
			config.setStatusWindowError("TextEditorWindow::editFile:Error: " + e);
		}
	}

	private void enableMenusTools (boolean enable)
	{
		config.getVCAL().getMenu().setEnabled (enable);
		config.getToolBar().setEnabled (enable);
	}

	/**
	 * 
	 * Open the XMI model file and VDAG in two different windows.
	 * This function runs in a thread. It first disable all the menus and tools, then
	 * load the XMI file and create a Visaul DAG, to be displayed latter using other
	 * menu functions and by running CAL files.
	 * @param file Name of the XMI file containing the Model
	 * @param con Configure class for global acceess to different status and error windows
	 * @return true if succes and vice versa
	 * 
	 */
	public boolean openModelFile (String file, Config con)
	{
		final Config configL = con;
		final String filename = file;
		try
		{
			enableMenusTools (false);
        	configL.setStatusText ("Loading Model...");
	   		final int SLEEP_AMOUNT = 100;
	   		folder.getDisplay().syncExec(new Runnable ()
	        {
    	        public void run ()
        	    {
	    			try { Thread.sleep (500); }
	    			catch (InterruptedException e) { enableMenusTools (true); threadRetVal = false; return; }

	    			xmi = new ProcessXMI (config);
	    			if (xmi.parse (filename, config))
	    			{
	    				configL.setStatusText ("Generating DAG...");
	    				VisualDAG vdag = xmi.createGraphControl (form.getShell());
	    				config.setVDAG (vdag);
	    				if (vdag != null)
	    				{
	    					vdag.makeAllInvisible ();
	    					openDependencyAnalysisWindow ("Dependency Analysis", vdag.getGraph());
	    					images = vdag.createToolBar (config);
	    					config.setStatusText ("DAG Loaded");
	    					if (umlWindow.open (filename))
	    					{
	    						form.layout(true);
	    						configL.setStatusWindowText("Model Loaded");
	    						configL.setStatusText ("Model Loaded");
	    						threadRetVal = true;
	    					}
	    					else
	    					{
	    						configL.setStatusWindowError("Model Not Loaded");
	    						configL.setStatusText ("Model Not Loaded");
	    						threadRetVal = false;
	    					}
	    				}
	    				else
	    				{
	    					configL.setStatusText ("DAG Error");
	    					threadRetVal = false;
	    				}
	    			}
	    			else
	    			{
	    				configL.setStatusText ("XMI Model Error");
	    				threadRetVal = false;
	    			}

	    			try { Thread.sleep (SLEEP_AMOUNT); }
   	    			catch (InterruptedException e) { enableMenusTools (true); threadRetVal = false; return; }
                }
	        });

			enableMenusTools (true);
	   		return threadRetVal;
		}
		catch (Exception e)
		{
			enableMenusTools (true);
			configL.setStatusText ("Model Not Loaded");
			configL.setStatusWindowError("TextEditorWindow::openModelFile:Error: " + e);
			return threadRetVal;
		}
	}

	public Control getOutputWindowFolder ()
	{
		return outputWindow.getFolder();
	}

	public void openLexicalAnalysisWindow (String title, Object obj)
	{

		if (Config.LA_WIN_ALREADY_OPEN)
			Config.LA_WIN_ALREADY_OPEN = outputWindow.setControl (title, obj, false);
		else
		{
			outputWindow.open (title, obj, imageLexical, false);
			Config.LA_WIN_ALREADY_OPEN = true;
		}
	}

	public void openSyntaxAnalysisWindow (String title, Object obj)
	{
		if (Config.SA_WIN_ALREADY_OPEN)
			Config.SA_WIN_ALREADY_OPEN = outputWindow.setControl (title, obj, false);
		else
		{
			outputWindow.open (title, obj, imageSyntax, false);
			Config.SA_WIN_ALREADY_OPEN = true;
		}
	}

	/**
	 * 
	 * Since get loaded the first time this function is called.
	 * So Use this only once for the first time. For subsequent calls use
	 * viewDependencyAnalysisWindow(). 
	 * @param title Title of the window to open
	 * @param obj Object to be displayed in the window
	 * 
	 */
	public void openDependencyAnalysisWindow (String title, Object obj)
	{
		if (Config.DA_WIN_ALREADY_OPEN)
			Config.DA_WIN_ALREADY_OPEN = outputWindow.setControl (title, obj, true);
		else
		{
			outputWindow.open (title, obj, imageDependency, true);
			Config.DA_WIN_ALREADY_OPEN = true;
		}
	}

	/**
	 * 
	 * If the window is disposed off then this function opens the window
	 * and displays the already loaded graph. Call openDependencyAnalysisWindow()
	 * before calling this window.
	 * @param title Title of the window to open
	 * 
	 */
	public void viewDependencyAnalysisWindow (String title)
	{
		VisualDAG vdag = config.getVDAG();
		if (vdag != null)
		{
			if (!Config.DA_WIN_ALREADY_OPEN)
					outputWindow.open (title, vdag.getGraph(), imageDependency, true);
			else
				Config.DA_WIN_ALREADY_OPEN = outputWindow.setControl (title, vdag.getGraph(), true);
		}
	}

	public void viewUMLModelWindow ()
	{
		umlWindow.open (null);
	}

	public void viewStatusWindow (String title)
	{
		if (title.equals("Status"))
			statusWindow.open (1);
		else if (title.equals("Error"))
			statusWindow.open (2);
	}

	public boolean saveFile (String fileFullPathName)
	{
		try
		{
			CTabItem item = folder.getSelection();
			String selectedFile = fileFullPathName;
       		File file = new File (selectedFile);
       		if (!file.exists() && item.getData().equals("NewFile"))
       		{
        		String[] filterExtensions = { "*."+Config.CAL };
        		FileDialog fileDialog = new FileDialog(folder.getShell(), SWT.SAVE);
        		fileDialog.setText("Save As");
        		fileDialog.setFilterPath(config.getStartDir());
        		fileDialog.setFilterExtensions(filterExtensions);
        		selectedFile = fileDialog.open();
        		if (selectedFile != null)
        		{
        			/**
        			 * checking file extension (.cal)
        			 */
        			int i = selectedFile.lastIndexOf('.');
        			if (i > 0)
        			{
	        			String fileX = selectedFile.substring(i);
	        			if (fileX.equals("."+Config.CAL))
			        		config.setStartDir (selectedFile);
	        			else
	        			{
	        				config.setStatusText(" File Save Error");
	        				return false;
	        			}
        			}
        			else
        			{
        				config.setStatusText(" File Save Error");
    					return false;
        			}
        		}
       		}
			StyledText text = (StyledText)(folder.getSelection().getControl());
			if (text != null && selectedFile != null)
			{
	       		file = new File (selectedFile);
				Config.CURRENT_CAL_FILE = file.getAbsoluteFile().toString();
				item.setText(file.getName());
				item.setData("OldFile");
				item.setToolTipText(Config.CURRENT_CAL_FILE);
				BufferedWriter bw = new BufferedWriter (new FileWriter (Config.CURRENT_CAL_FILE));
				bw.write(text.getText());
				bw.close();

    			MenuItem mitem = config.getVCAL ().getMenuItem("File");
    			if (mitem != null)
    			{
    				config.getVCAL ().enableChildren(mitem, "Save\tCtrl+S", false);
    				config.getVCAL ().enableChildren(mitem, "Print...\tCtrl+P", true);
    			}
    			mitem = config.getVCAL ().getMenuItem("Compiler");
    			if (item != null)
    			{
    				config.getVCAL ().enableChildren(mitem, "Generate Lexical Analysis Output", true);
    				config.getVCAL ().enableChildren(mitem, "Run", true);
    			}
    			config.enableToolItem(config.getToolBar(), "Save", false);
    			config.enableToolItem(config.getToolBar(), "Run", true);

                return true;
			}
			return false;
		}
		catch (Exception e)
		{
			config.setStatusWindowError("TextEditorWindow::saveFile:Error: " + e);
			e.printStackTrace();
			return false;
		}
	}

	public void selectText (int line, int col)
	{
		if (folder != null)
		{
			CTabItem item = folder.getSelection();
			if (item != null)
			{
				StyledText text = (StyledText)item.getControl();
				int start = text.getOffsetAtLine(line);
				int end = start + col;
				text.setSelection (start, end);
			}
		}
	}

	public String getSelectedFile ()
	{
		String sf = folder.getSelection().getToolTipText();
		if (sf != null && sf.length() > 0)
			return sf;
		else
			return null;
	}

	public void closeSelectedFile ()
	{
		CTabItem item = folder.getSelection();
		if (item != null)
		{
			if (!config.isToolItemEnabled("Save"))
					item.dispose();
			else
			{
				FileCloseDialog fc = new FileCloseDialog (folder.getShell(), item);
				fc.open ();
			}
		}
	}

	public StyledText getTextWidget ()
	{
		StyledText text = null;
		if (folder != null)
		{
			CTabItem items[] = folder.getItems();
			for (int i = 0; i < items.length; i++)
			{
				if (items[i].getToolTipText().equals(Config.CURRENT_CAL_FILE))
					text = (StyledText)items[i].getControl();
			}
		}
		return text;
	}

	public void closeAll ()
	{
		statusWindow.closeAll ();
		outputWindow.closeAll ();
		umlWindow.closeAll ();

		if (folder != null)
		{
			CTabItem items[] = folder.getItems();
			for (int n = 0; n < items.length; n++)
				items[n].dispose();
		}

		if (images != null)
		{
			for (int i = 0; i < images.length; i++)
				images[i].dispose();
		}
	}
}
