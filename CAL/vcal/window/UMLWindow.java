/* ============================================================================
|
|   Filename:    UMLWindow.java
|   Dated:       23 Dec, 2006
|   By:          Shahid Alam (salam@sce.carleton.ca)
|
|   Description: See below
|
|   TO DO:       
|
 ----------------------------------------------------------------------------*/

package org.vcal.window;

import java.io.File;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.*;

import org.vcal.config.Config;
import org.vcal.util.XMLTree;

public class UMLWindow
{
	private static Config config = null;
	private static Tree tree = null;
	private static CTabFolder folder = null, folders[];
	private static SashForm form = null;
	private static Image image = null;
	private static int folderNo = 0;

	public UMLWindow (SashForm f, CTabFolder mf[], Config c)
	{
		config = c;
		folders = mf;
		form = f;

		folder = new CTabFolder (form, SWT.BORDER | SWT.FLAT);
    	folder.setMRUVisible(true);
    	folder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
    	folder.setSimple(false);
    	folder.setUnselectedImageVisible(false);
    	folder.setUnselectedCloseVisible(true);
    	folder.setSelectionBackground(Config.LIGHT_GRAY_COLOR);
    	folder.setSelectionForeground(Config.BLACK_COLOR);

        File file = new File (Config.UML_MODEL_WINDOW_IMAGE_FILE);
        if (file.exists())
        	image = new Image (form.getDisplay(), file.getAbsoluteFile().toString());
        file = null;

        folder.setVisible(false);
	}

	public CTabFolder getFolder()
	{
		return folder;
	}

	public boolean open (String xmlFile)
	{
		try
		{
			if (folder != null)
			{
				final CTabItem item = new CTabItem (folder, SWT.CLOSE);
				if (tree != null)
				{
					item.setControl (tree);
        			MenuItem mitem = config.getVCAL().getMenuItem("View");
        			if (mitem != null)
        				config.getVCAL().enableChildren(mitem, "UML Model", false);
				}
				else if (xmlFile != null)
				{
					XMLTree xml = new XMLTree (xmlFile, item.getParent(), SWT.BORDER, config);
					/**
					 * Build tree with only attribute "name" visible
					 */
					tree = xml.buildTree ("UML:Model", "name");
					tree.setBackground (Config.WHITE_COLOR);
					tree.setForeground (Config.LIGHT_GRAY_COLOR);
					/**
					 * Make UML:Class Element Color changes for visual effects of selection,
					 * which this tools currently process for dependency analysis.
					 * Also make all the parents of UML:Class expand
					 */
					xml.makeChildrenColored ("Class", tree.getBackground(), Config.BLACK_COLOR, true);
					item.setControl (tree);
				}
				item.setText ("UML Model");
				item.setImage(image);
				folder.setSelection(item);
				folderNo++;

		        item.addDisposeListener(new DisposeListener() {
		        	public void widgetDisposed(DisposeEvent e)
		        	{
		        		folderNo--;
		        		if (folderNo < 1)
		        		{
		        			closeAll ();
		        			folder.setVisible (false);
		        			form.layout(true);
		        			Control controls[] = form.getChildren();
		        			boolean isVisible = false;
		        			for (int n = 0; n < controls.length; n++)
		        			{
		        				if (controls[n].isVisible())
		        				{
		        					isVisible = true;
		        					break;
		        				}
		        			}
		        			if (!isVisible)
		        			{
			        			form.setVisible(false);
			        			form.getParent().layout(true);
		        			}
		        			controls = null;
		        		}
	        			MenuItem mitem = config.getVCAL().getMenuItem("View");
	        			if (mitem != null)
	        				config.getVCAL().enableChildren(mitem, item.getText(), true);
		        	}
		        });

		    	if (folderNo < 2)
		    	{
		    		folder.setVisible(true);
		        	form.setVisible(true);
		    		form.layout(true);
		    	}
		    	return true;
			}
			return false;
		}
		catch (Exception e)
		{
			config.setStatusWindowError("UMLWindow::open:Error: " + e);
			return false;
		}
	}
	
	public void closeAll ()
	{
		if (folder != null)
		{
			CTabItem items[] = folder.getItems();
			for (int n = 0; n < items.length; n++)
				items[n].dispose();
		}
	}
}
