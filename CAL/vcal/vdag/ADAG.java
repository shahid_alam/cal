/* ============================================================================
|
|   Filename:    ADAG.java
|   Dated:       01 Dec, 2006
|   By:          Shahid Alam (salam@sce.carleton.ca)
|
|   Description: See below
|
|   TO DO:       See below
|
 ----------------------------------------------------------------------------*/

package org.vcal.vdag;

import java.lang.reflect.Field;
import java.math.BigInteger;
import java.util.Enumeration;
import java.util.LinkedList;

import org.vcal.config.Config;
import org.vcal.vdag.DAG;

/**
*
* <p>
* This class implements data strucutre for storing
* ADAG (Annotated Directed Acyclic Graph).
*
* @author Shahid Alam
* @version 3.0
* 
*/
public class ADAG
{
	private static Config config = null;
	private static DAG dag;

	/**
	*
	* Default Constructor
	*
	*/
	public ADAG (Config con)
	{
		config = con;
		dag = new DAG (config);
	}

	/**
	 * Size of the ADAG is the count of all the vertices in the ADAG
	 * @return All the vertex count in the ADAG
	 */
	public int getSize ()
	{
		return dag.size ();
	}
	
	/**
	 * 
	 * This method gets the node label.
	 * @param node Node whose lable is to be found
	 * @return The label of the node else null
	 * 
	 */
	public BigInteger getLabel (Object node)
	{
		try
		{
			BigInteger label = null;
			if (node != null)
			{
				label = dag.getNodeLabel (node);
			}
			return label;
		}
		catch (Exception e)
		{
			String errorMessage = "ADAG:getLabel: Error getting node label: " + e;
			config.setStatusWindowError(errorMessage);
			return null;
		}
	}

	/**
	 * 
	 * This method adds a node to the DAG with label 1 and weight w.
	 * If w is minus or not defined then it assignes a default weight of 1.
	 * Object has to be different from the one in the DAG.
	 * If not then it doesn't add the object.
	 * It also relabel all the parents.
	 * IMPORTANT NOTE: Call setparents to sets the parent and child
	 * relation.
	 * @param obj Object ot be added
	 * @param w Weight to be assigned to the node
	 * @return the key of the object added
	 * 
	 */
	public Object addNode (Object obj, int w)
	{
		return dag.addNode (obj, w);
	}

	/**
	 * 
	 * This method sets the parent of a node.
	 * It only adds if parent is present.
	 * It then checks all the node's parents for relabeling.
	 * It also relabel all the parents.
	 * @param obj Node whose parent is to be set
	 * @param parent Node to be set as parent 
	 * @return true if success
	 * 
	 */
	public boolean setParent (Object obj, Object parent)
	{
		try
		{
			if (obj != null && parent != null)
				return dag.setParent (obj, parent);
		}
		catch (Exception e)
		{
			String errorMessage = "ADAG:setParent: Error setting parents: " + e;
			config.setStatusWindowError(errorMessage);
			return false;
		}
		return false;
	}

	/**
	*
	* This method reutrns true if there is path from source to target.
	* It computes the mod of the label of source and target and
	* if it's zero then return true else false.
	* @param source Source object
	* @param target Target object
	* @return true if there is path from source to target
	*
	*/
	public boolean isPath (Object source, Object target)
	{
		try
		{
			BigInteger s = dag.getNodeLabel (source);
			BigInteger t = dag.getNodeLabel (target);
			if (s != null && t != null)
			{
				BigInteger path = new BigInteger ("1");
				if (s.compareTo (t) <= 0)
					path = s.mod (t);
				else
					path = s.mod (t);
				if (path.equals("0"))
					return true;
			}
			return false;
		}
		catch (Exception e)
		{
			String errorMessage = "ADAG:isPath: Error: " + e;
			config.setStatusWindowError(errorMessage);
			return false;
		}
	}

	/**
	*
	* This method reutrns all the descendants of the node.
	* 
	* TO DO
	* 
	* STILL NOT FULLY IMPLEMENTED
	* 
	* @param node Whose descendants are to be returned
	* @return all the descendants of the node
	*
	*/
	public String descendants (Object node)
	{
		return null;
	}

	/**
	 * 
	 * Find if two elements in the DAG depends on each other.
	 * Order of the parameters is important for return value.
	 * @param pe Parent Element
	 * @param ce Child Element
	 * @return String value
	 * <p>TRUE if ce is descendant of pe
	 * <p>FALSE if ce is not descendant of pe
	 * <p>It returns the name of the element with an error,
	 *  if any one of the element is not in the DAG.
	 * 
	 */
	public String isDescendant (String pe, String ce)
	{
		String result = null;

		BigInteger labelPE = getLabel(pe);
		BigInteger labelCE = getLabel(ce);
		if (labelPE != null)
		{
			if (labelCE != null)
			{
				int r = labelPE.mod(labelCE).intValue();
				if (r == 0)
					result = "TRUE";
				else
					result = "FALSE";
			}
			else
				result = ce + " Not in the DAG";
		}
		else
		{
			result = pe + " Not in the DAG";
			if (labelCE == null)
				result += "\n" + ce + " Not in the DAG";
		}

		return result;
	}

	/**
	*
	* This method deletes the node from ADAG.
	 * If the node delted has a special prime then 
	 * it also updates the prime list of deleted primes.
	* @param obj Object to be deleted
	*
	*/
	public boolean delNode (Object obj)
	{
		try
		{
			return dag.delNode (obj);
		}
		catch (Exception e)
		{
			String errorMessage = "ADAG:delNode: Error deleting node: " + e;
			config.setStatusWindowError(errorMessage);
			return false;
		}
	}

	/**
	 * 
	 * Gets labels of all the parents of the node, not all the ancestors.
	 * @return An array of label in BigInteger containg parents of the node
	 * 
	 */
	public BigInteger[] getParentsLabels (Object node)
	{
		try
		{
			Object parents[] = dag.getParentsLabels (node);
			if (parents != null)
			{
				BigInteger labelArray[] = new BigInteger[parents.length];
				for (int n = 0; n < parents.length; n++)
				{
					labelArray[n] = (BigInteger)parents[n];
				}
				return labelArray;
			}
		}
		catch (Exception e)
		{
			String errorMessage = "ADAG:getParentsLabels: Error: " + e;
			config.setStatusWindowError(errorMessage);
			return null;
		}
		return null;
	}

	/**
	 * 
	 * Gets all the children of the node, not all the desendants.
	 * @return An array of value string containg children of the node
	 * 
	 */
	public String[] getChildren (Object node)
	{
		try
		{
			Object children[] = dag.getChildren (node);
			String strArray[] = new String[children.length];
			for (int n = 0; n < children.length; n++)
			{
				strArray[n] = (String)children[n];
			}
			return strArray;
		}
		catch (Exception e)
		{
			String errorMessage = "ADAG:getChildren: Error: " + e;
			config.setStatusWindowError(errorMessage);
			return null;
		}
	}

	/**
	 * 
	 * Translates ADAG into a flatten array of string.
	 * @return An array of string containg values and weight of each vertex in ADAG
	 * 
	 */
	public String[] flattenArray ()
	{
		try
		{
			int n = 0;
			String strArray[] = null;
			int SIZE = 2 * dag.size ();
			strArray = new String[SIZE];
			Enumeration e = dag.elements ();
			while (e.hasMoreElements())
			{
				LinkedList list = (LinkedList)e.nextElement ();
				Object obj = list.getFirst();
				Field fValue = obj.getClass().getDeclaredField("value");
				Field fWeight = obj.getClass().getDeclaredField("weight");
				String value = fValue.get(obj).toString();
				String weight = fWeight.get(obj).toString();
				strArray[n] = value;
				n++;
				strArray[n] = weight;
				n++;
				if (n > SIZE)
					break;
			}
			return strArray;
		}
		catch (Exception e)
		{
			String errorMessage = "ADAG:flattenArray: Error: " + e;
			config.setStatusWindowError(errorMessage);
			return null;
		}
	}

	/**
	 * 
	 * Translates ADAG into XML without parents and children information.
	 * @return StringBuffer containg ADAG in XML tags
	 * 
	 */
	public String flatten ()
	{
		try
		{
			String str = null;
			str = dag.toXMLString ().toString();
			return str;
		}
		catch (Exception e)
		{
			String errorMessage = "ADAG:flatten: Error: " + e;
			config.setStatusWindowError(errorMessage);
			return null;
		}
	}

	/**
	 * 
	 * Translates ADAG into XML with parents and children information.
	 * Good for validating the ADAG. It prints 0 if the mod of this.label to
	 * parent's or child's label is 0 else prints 1.
	 * @param withModValue If true then mod is to be printed for reachability
	 * @return StringBuffer containg ADAG in XML tags
	 * 
	 */
	public String flattenFull (boolean withModValue)
	{
		try
		{
			String str = null;
			if (withModValue)
				str = dag.toXMLStringFull (true).toString();
			else
				str = dag.toXMLStringFull (false).toString();
			return str;
		}
		catch (Exception e)
		{
			String errorMessage = "ADAG:flattenFull: Error: " + e;
			config.setStatusWindowError(errorMessage);
			return null;
		}
	}

	/**
	 * 
	 * Translates ADAG into XML with parents and children information.
	 * Good for validating the ADAG. It prints 0 if the mod of this.label to
	 * parent's or child's label is 0 else prints 1.
	 * @param withModValue If true then mod is to be printed for reachability
	 * @return StringBuffer containg ADAG in XML tags
	 * 
	 */
	public String flattenFullWithParentLabel (boolean withModValue)
	{
		try
		{
			String str = null;
			if (withModValue)
				str = dag.toXMLStringParentWithLabel (true).toString();
			else
				str = dag.toXMLStringParentWithLabel (false).toString();
			return str;
		}
		catch (Exception e)
		{
			String errorMessage = "ADAG:print: flattenFullWithParentLabel: " + e;
			config.setStatusWindowError(errorMessage);
			return null;
		}
	}
}
