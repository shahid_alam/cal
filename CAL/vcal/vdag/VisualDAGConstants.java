/* ============================================================================
|
|   Filename:    VisualDAGConstants.java
|   Dated:       01 Dec, 2006
|   By:          Shahid Alam (salam@sce.carleton.ca)
|
|   Description: See below
|
|   TO DO:       
|
 ----------------------------------------------------------------------------*/

package org.vcal.vdag;

import java.util.Map;

import org.jgraph.graph.GraphConstants;

/**
*
* <p>
* This class adds a weight (annotation) to the vertex in a view graph.
*
* @author Shahid Alam
* @version 3.0
* 
*/
public class VisualDAGConstants extends GraphConstants
{
	/**
	 * Represents weight of the vertex.
	 * 
	 * @see #setWeight (Map map, String weight)
	 * @see #getWeight (Map map)
	 */
	private static String WEIGHT = "";

	/**
	*
	* Constructor
	* 
	*/
	public VisualDAGConstants ()
	{
		super ();
	}
	
	/**
	 * Sets the Weight of the vertex.
	 * 
	 * @param map Map of the attributes where weight will be added
	 * @param weight value of the weight
	 * @see #WEIGHT
	 */
	public static final void setWeight (Map map, String weight)
	{
		map.put (WEIGHT, weight);
	}

	/**
	 * Returns the Weight of the vertex as String.
	 * 
	 * @param map Map of the attributes where weight will be added
	 * @return Weight of the vertex
	 * @see #WEIGHT
	 */
	public static final String getWeight (Map map)
	{
		return ((String)map.get (WEIGHT));
	}
}
