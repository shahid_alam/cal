/* ============================================================================
|
|   Filename:    DAG.java
|   Dated:       01 Dec, 2006
|   By:          Shahid Alam (salam@sce.carleton.ca)
|
|   Description: See below
|
*   Data structure for storing DAG:
* 
*         ---          -----------          -----------
*      0 |   |------->|   |   |  -|------->|   |   |   |
*         ---          -----------          -----------
*      1 |  -|         |         |          |         |
*         ---        p[0]      c[0]       p[0]      c[0]
*      2 |   |       p[1]      c[1]       p[1]      c[1]
*         ---          .         .          .         .
*      3 |   |         N         N          N         N
*         ---
*      4 |  -|
*         ---          -----------          -----------          -----------
*      5 |   |------->|   |   |  -|------->|   |   |  -|------->|   |   |   |
*         ---          -----------          -----------          -----------
*      . |   |         |         |          |         |          |         |
*      . |   |       p[0]      c[0]       p[0]      c[0]       p[0]      c[0]
*      . |   |       p[1]      c[1]       p[1]      c[1]       p[1]      c[1]
*      . |   |       p[2]      c[2]       p[2]      c[2]       p[2]      c[2]
*      . |   |         .         .          .         .          .         .
*      . |   |         N         N          N         N          N         N
*      . |   |
*         ---
*      M |   |
*         ---
*
*
|   TO DO:       See below
|
 ----------------------------------------------------------------------------*/

package org.vcal.vdag;

import java.math.BigInteger;
import java.util.Vector;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.Enumeration;
import java.lang.reflect.Field;

import org.vcal.config.Config;

/**
*
* <p>
* This class implements DAG (Directed Acyclic Graph) with annotation.
* </p>
*
* <p>
* For deletion it maintains a list of deleted primes. So that it can be used to return
* the next prime. A variable CURRENT_PRIME is maintained so that if this list is empty
* then Java BigInteger funstion nextProbablePrime() is used to return the next prime
* and stored in CURRENT_PRIME variable.
* </p>
* @see Node
*
* @author Shahid Alam
* @version 3.0
* 
*/
public class DAG extends Hashtable
{
	private static Config config = null;
	private static boolean _DEBUG_ = true;
	private static int vertexCount = 0;
	private static BigInteger CURRENT_PRIME = new BigInteger("1");
	private static Vector primeListDel = null;

	public DAG (Config con)
	{
		super ();
		config = con;
		vertexCount = 0;
		primeListDel = new Vector ();
	}

	/**
	 * Size of the DAG is the count of all the vertices in the DAG
	 * @return All the vertex count in the DAG
	 */
	public int size ()
	{
		return vertexCount;
	}
	
	/**
	 * 
	 * This method adds a node to the DAG with label 1 and weight w.
	 * If w is minus or not defined then it assignes a default weight of 1.
	 * Object has to be different from the one in the DAG.
	 * If not then it doesn't add the object.
	 * Object has to be different from the one in the DAG. If not
	 * then it doesn't add the object.
	 * IMPORTANT NOTE: Call setparents to sets the parent and child
	 * relation and it also relabel all the parents.
	 * @param obj Object ot be added
	 * @param w Weight to be assigned to the node
	 * @return the key of the object added
	 * 
	 */
	public Object addNode (Object obj, int w)
	{
		/**
		 * If key is not there then create a Linked List add it
		 * to the hash table, and then add the object to the Linked List.
		 * Otherwise Linked List already created just add.
		 */
		try
		{
			Node node = new Node ();
			if (w > 1)
				node.weight = w;
			node.value = obj;
			node.label = new BigInteger ("1");
			node.parents = new LinkedList ();
			node.children = new LinkedList ();

			Integer key = new Integer (obj.hashCode ());

			LinkedList list = null;
			if ((list = (LinkedList)this.get(key)) != null)
			{
				Node tNode = null;
				Iterator i = list.iterator ();
				while ((tNode = (Node)i.next()) != null)
				{
					if (tNode.value == obj)
						return null;
				}
				if (list.add (node))
				{
					vertexCount++;
					return key;
				}
			}
			else
			{
				list = new LinkedList ();
				this.put (key, list);
				if (list.add (node))
				{
					vertexCount++;
					return key;
				}
			}
		}
		catch (Exception e)
		{
			String errorMessage = "DAG:addNode: Error adding node: " + e;
			config.setStatusWindowError(errorMessage);
			return null;
		}
		return null;
	}

	/**
	 * 
	 * This method sets the parent of a node.
	 * It only adds if parent is present.
	 * It then checks all the node's parents for relabeling.
	 * It also relabel all the parents.
	 * @param obj Node whose parent is to be set
	 * @param parent Node to be set as parent 
	 * @return true if success
	 * 
	 */
	public boolean setParent (Object obj, Object parent)
	{
		Integer key = new Integer (obj.hashCode ());
		/**
		 * If key is not there return false.
		 */
		LinkedList list;
		if ((list = (LinkedList)this.get(key)) != null)
		{
			Node node = null;
			Iterator i = list.iterator ();
			while ((node = (Node)i.next()) != null)
			{
				if (node.value == obj)
				{
					Node pnode = null;
					if ((pnode = containsObject(parent)) != null)
					{
						node.parents.add (pnode);
						pnode.children.add (node);
						relabel (pnode, false);
						return true;
					}
					else
					{
						if (_DEBUG_)
							config.setStatusWindowText ("DAG::setParent: Parent Node " + parent + " Not Present");
					}
					return false;
				}
			}
		}
		return false;
	}

	/**
	 * 
	 * This method relabels all the ancestor of the node.
	 * It's a recursive function. First it label the current node
	 * and then recursively lable all the parent nodes.
	 * @param node Node whose ancestors are to be relabeled.
	 * 
	 */
	private void relabel (Node node, boolean deleting)
	{
		label (node, deleting);
//System.out.println("\nRELABLE: " + node.value + ", " + node.label);
		LinkedList p = null;
		if ((p = node.parents) != null)
		{
			Iterator i = p.iterator ();
			while (i.hasNext())
			{
				Node pnode = (Node)i.next();
//System.out.println("\nRELABLE NEXT: " + pnode.value + ", " + pnode.label);
				relabel (pnode, deleting);
			}
		}
	}

	/**
	 * 
	 * This method label the node.
	 * It lables a node by using all the labels of it's children.
	 * First it computes the
	 * label = LCM of 1st child label and 2nd child label
	 * Then it computes
	 * lable = LCM of label and 3rd child label
	 * and so on untill it checks all the children of the node.
	 * It stores the biggest label of the children in largest
	 * and if the final LCM value is equal to this value then
	 * it multiplies the final value with the next prime in the list.
	 * It then sets the label of the node to this final computed value.
	 * @param node Node to be labeled
	 * @return the lable of the node
	 * 
	 */
	private BigInteger label (Node node, boolean deleting)
	{
		BigInteger ONE = new BigInteger("1");
		BigInteger largest = ONE;
		BigInteger label = node.label;
		if (deleting)
		{
			if (node.prime != 1)
			{
				String val = node.prime + "";
				BigInteger dp = new BigInteger (val);
				primeListDel.addElement (dp);
			}
			label = ONE;
		}
		LinkedList c = null;

		if ((c = node.children) != null)
		{
			Iterator i = c.iterator ();
//System.out.print ("------------------------------------- " + node.value + ", " + node.label + " -- ");
			while (i.hasNext())
			{
				Node n = (Node)i.next();
				BigInteger clabel = n.label;
				if (clabel.compareTo(largest) == 1)
					largest = clabel;
//System.out.print (" LCM[" + label + ", " + clabel + "] = ");
				label = lcm (label, clabel);
//System.out.print (label);
			}
			if (label.equals (largest))
			{
				BigInteger np = nextPrime();
				if (np != null)
				{
//System.out.print (" " + label + " x " + np + "=");
					label = label.multiply (np);
					node.prime = np.intValue();
				}
				else
					config.setStatusWindowError("DAG::nextPrime:ERROR: No Prime number available");
			}
			node.label = label;
//System.out.print (" --- " + label);
		}
		else
			node.label = ONE;
//System.out.println (" ---- " + node.label);
		ONE = null;
		return label;
	}

	/**
	 * 
	 * This method computes the LCM of two BigIntegers using
	 * the following formuls:
	 * i = GCD (i1, i2)
	 * LCM (i1, i2) = [(i1 / i) * (i2 / i)] * i
	 * @param i1 The first BigInteger
	 * @param i2 The second BigInteger
	 * @return the LCM (i1, i2) computed as shown above
	 * 
	 */
	private BigInteger lcm (BigInteger i1, BigInteger i2)
	{
		BigInteger ZERO = new BigInteger ("0");
		BigInteger i = i1.gcd (i2);
		if (!i.equals(ZERO))
			i = i1.divide(i).multiply(i2.divide(i)).multiply(i);
		else
			i = i.multiply(ZERO);
		return i;
	}

	/**
	 * 
	 * Returns the next prime starting from 2.
	 * A prime list is maintained for deleted primes.
	 * If there is prime in that list then return that 
	 * prime otherwise use Java BigInteger function 
	 * nextProbablePrime() to return the next prime.
	 * 
	 * @return the next prime in the list of primes
	 * 
	 */
	private BigInteger nextPrime ()
	{
		BigInteger prime = null;
		if (!primeListDel.isEmpty())
		{
			prime = (BigInteger)primeListDel.firstElement();
			primeListDel.removeElementAt(0);
		}
		else
		{
			CURRENT_PRIME = CURRENT_PRIME.nextProbablePrime();
			prime = CURRENT_PRIME;
		}
//System.out.println("---------------------------------- " + prime.toString());
		return prime;
	}

	/**
	 * 
	 * Gets node's label.
	 * @param obj Object whosw node label is returned
	 * @return Label of the node
	 * 
	 */
	public BigInteger getNodeLabel (Object obj)
	{
		LinkedList list;
		Integer key = new Integer (obj.hashCode ());
		/**
		 * If key is there then get the object from the list.
		 */
		if ((list = (LinkedList)this.get(key)) != null)
		{
			Node node = null;
			Iterator i = list.iterator ();
			while ((node = (Node)i.next()) != null)
			{
				if (node.value.equals(obj.toString()))
					return node.label;
			}
			return null;
		}
		return null;
	}

	/**
	 * 
	 * Checks if the object is present in the DAG.
	 * @param obj To be searched
	 * @return The node that contains that object
	 * 
	 */
	public Node containsObject (Object obj)
	{
		if (obj != null)
		{
			LinkedList list;
			Integer key = new Integer (obj.hashCode ());
			/**
			 * If key is there then delete the object from the Linked List.
			 * If the Linked List is empty then delete list from Hash Table.
			 */
			if ((list = (LinkedList)this.get(key)) != null)
			{
				if (!list.isEmpty ())
				{
					Node node = null;
					Iterator i = list.iterator ();
					while ((node = (Node)i.next()) != null)
					{
						if (node.value == obj)
						{
							return node;
						}
					}
				}
			}
		}
		return null;
	}

	/**
	 * 
	 * Gets labels of all the parents of the node, not all the ancestors.
	 * @return An array of objects containg parents labels of the node
	 * 
	 */
	public Object[] getParentsLabels (Object obj)
	{
		Object parents[] = null;
		Node node = null;
		if ((node = containsObject (obj)) != null)
		{
			if (node.parents != null)
			{
				Object list[] = node.parents.toArray();
				parents = new Object[list.length];
				for (int i = 0; i < parents.length; i++)
				{
					Node n = (Node)list[i];
					parents[i] = n.label;
				}
			}
		}
		return parents;
	}
	
	/**
	 * 
	 * Gets all the children of the node, not all the desendants.
	 * @return An array of objects containg children values of the node
	 * 
	 */
	public Object[] getChildren (Object obj)
	{
		Object children[] = null;
		Node node = null;
		if ((node = containsObject (obj)) != null)
		{
			Object list[] = node.children.toArray();
			children = new Object[list.length];
			for (int i = 0; i < children.length; i++)
			{
				Node n = (Node)list[i];
				children[i] = n.value;
			}
		}
		return children;
	}
	
	/**
	 * 
	 * Deletes object from the DAG.
	 * If the node delted has a special prime then 
	 * it also updates the prime list of deleted primes.
	 * 
	 * @param obj Object to be deleted
	 * @return true if object deleted from the DAG
	 * 
	 */
	public boolean delNode (Object obj)
	{
		LinkedList list;
		Integer key = new Integer (obj.hashCode ());
		/**
		 * If key is there then delete the object from the Linked List.
		 * If the Linked List is empty then delete list from Hash Table.
		 */
		if ((list = (LinkedList)this.get(key)) != null)
		{
			if (!list.isEmpty ())
			{
				Node node = null;
				Iterator i = list.iterator ();
				while ((node = (Node)i.next()) != null)
				{
					if (node.value == obj)
					{
						/**
						* Store the prime in as deleted so that it can be used next time
						*/
						if (node.prime != 1)
						{
							String val = node.prime + "";
							BigInteger dp = new BigInteger (val);
							primeListDel.addElement (dp);
						}
						/**
						 * Delete this node from the list of children of its parents.
						 * Relabel all the parents of this node.
						 */
						LinkedList p = null;
						if ((p = node.parents) != null)
						{
							i = p.iterator ();
							Node n = null;
							while (i.hasNext())
							{
								n = (Node)i.next();
								n.children.remove(node);
								relabel (n, true);
							}
						}
						/**
						 * Delete this node from the list of parents of its children.
						 */
						LinkedList c = null;
						if ((c = node.children) != null)
						{
							i = c.iterator ();
							Node n = null;
							while (i.hasNext())
							{
								n = (Node)i.next();
								n.parents.remove(node);
							}
						}
						/**
						 * Now remove the node from the list (Hash Table).
						 */
						if (list.size() == 1)
							this.remove (key);
						list.remove (node);
						node.parents = null;
						node.children = null;
						node.value = null;
						node.label = null;
						node = null;
						if (list.isEmpty())
							list = null;
						vertexCount--;
						return true;
					}
				}
			}
			list = null;
		}
		return false;
	}

	/**
	 * 
	 * Translates DAG into XML with parents and children information.
	 * Good for validating the DAG. It prints 0 if the mod of this.label to
	 * parent's or child's label is 0 else prints 1.
	 * @param withModValue If mod is to be printed for reachability
	 * @return StringBuffer containg DAG in XML tags
	 * 
	 */
	public StringBuffer toXMLStringFull (boolean withModValue)
	{
		try
		{
			StringBuffer strB = new StringBuffer ();
			Enumeration e = this.elements ();
			strB.append("<ADAG>");
			while (e.hasMoreElements())
			{
				LinkedList list = (LinkedList)e.nextElement ();
				if (list != null & list.size() > 0)
				{
					Object obj = list.getFirst();
					Field fValue = obj.getClass().getDeclaredField("value");
					Field fWeight = obj.getClass().getDeclaredField("weight");
					Field fLabel = obj.getClass().getDeclaredField("label");
					Field fParents = obj.getClass().getDeclaredField("parents");
					Field fChildren = obj.getClass().getDeclaredField("children");
					String value = fValue.get(obj).toString();
					String weight = fWeight.get(obj).toString();
					String label = fLabel.get(obj).toString();
					LinkedList parents = (LinkedList)fParents.get(obj);
					LinkedList children = (LinkedList)fChildren.get(obj);
					strB.append("<node");
					strB.append(" value='" + value + "'");
					strB.append(" weight='" + weight + "'");
					strB.append(" label='" + label + "'");
					BigInteger ZERO = new BigInteger ("0");
					int isReachable = 1;
					if (parents != null)
					{
						Iterator i = parents.iterator ();
						if (i.hasNext())
						{
							strB.append(" parents='");
							while (i.hasNext())
							{
								isReachable = 1;
								Node node = (Node)i.next();
								if (node.label.equals(new BigInteger("1")))
								{
									LinkedList p = node.parents;
									Iterator n = p.iterator ();
									while (n.hasNext())
									{
										Node pnode = (Node)n.next ();
										if (node.label.mod(pnode.label).equals(ZERO))
										{
											isReachable = 0;
											break;
										}
									}
								}
								else
								{
									if (node.label.mod ((BigInteger)fLabel.get(obj)).equals(ZERO))
										isReachable = 0;
								}
								if (withModValue)
									strB.append(node.value + ", " + isReachable + ", ");
								else
									strB.append(node.value + ", ");
							}
							strB.setLength(strB.length() - 2);
							strB.append("'");
						}
					}
					if (children != null)
					{
						Iterator i = children.iterator ();
						if (i.hasNext())
						{
							strB.append(" children='");
							while (i.hasNext())
							{
								isReachable = 1;
								Node node = (Node)i.next();
								if (node.label.equals(new BigInteger("1")))
								{
									LinkedList p = node.parents;
									Iterator n = p.iterator ();
									while (n.hasNext())
									{
										Node pnode = (Node)n.next ();
										if (pnode.label.mod(node.label).equals(ZERO))
										{
											isReachable = 0;
											break;
										}
									}
								}
								else
								{
									if (((BigInteger)fLabel.get(obj)).mod(node.label).equals(ZERO))
										isReachable = 0;
								}
								if (withModValue)
									strB.append(node.value + ", " + isReachable + ", ");
								else
									strB.append(node.value + ", ");
							}
							strB.setLength(strB.length() - 2);
							strB.append("'");
						}
					}
					strB.append("/>");
				}
			}
			strB.append("</ADAG>");
			return strB;
		}
		catch (Exception e)
		{
			String errorMessage = "DAG::toXMLString: Error: " + e;
			config.setStatusWindowError(errorMessage);
			return null;
		}
	}

	/**
	 * 
	 * Translates DAG into XML with parents and children information.
	 * Good for validating the DAG. It prints 0 if the mod of 
	 * parent's or child's label is 0 else prints 1.
	 * @param withModValue If mod is to be printed for reachability
	 * @return StringBuffer containg DAG in XML tags
	 * 
	 */
	public StringBuffer toXMLStringParentWithLabel (boolean withModValue)
	{
		try
		{
			StringBuffer strB = new StringBuffer ();
			Enumeration e = this.elements ();
			strB.append("<ADAG>");
			while (e.hasMoreElements())
			{
				LinkedList list = (LinkedList)e.nextElement ();
				if (list != null && list.size() > 0)
				{
					Object obj = list.getFirst();
					Field fValue = obj.getClass().getDeclaredField("value");
					Field fWeight = obj.getClass().getDeclaredField("weight");
					Field fLabel = obj.getClass().getDeclaredField("label");
					Field fParents = obj.getClass().getDeclaredField("parents");
					Field fChildren = obj.getClass().getDeclaredField("children");
					String value = fValue.get(obj).toString();
					String weight = fWeight.get(obj).toString();
					String label = fLabel.get(obj).toString();
					LinkedList parents = (LinkedList)fParents.get(obj);
					LinkedList children = (LinkedList)fChildren.get(obj);
					strB.append("<node");
					strB.append(" value='" + value + "'");
					strB.append(" weight='" + weight + "'");
					strB.append(" label='" + label + "'");
					BigInteger ZERO = new BigInteger ("0");
					int isReachable = 1;
					if (parents != null)
					{
						Iterator i = parents.iterator ();
						if (i.hasNext())
						{
							strB.append(" parents='");
							while (i.hasNext())
							{
								isReachable = 1;
								Node node = (Node)i.next();
								if (node.label.equals(new BigInteger("1")))
								{
									LinkedList p = node.parents;
									Iterator n = p.iterator ();
									while (n.hasNext())
									{
										Node pnode = (Node)n.next ();
										if (node.label.mod(pnode.label).equals(ZERO))
										{
											isReachable = 0;
											break;
										}
									}
								}
								else
								{
									if (node.label.mod ((BigInteger)fLabel.get(obj)).equals(ZERO))
										isReachable = 0;
								}
								if (withModValue)
									strB.append(node.label + ", " + isReachable + ", ");
								else
									strB.append(node.label + ", ");
							}
							strB.setLength(strB.length() - 2);
							strB.append("'");
						}
					}
					if (children != null)
					{
						Iterator i = children.iterator ();
						if (i.hasNext())
						{
							strB.append(" children='");
							while (i.hasNext())
							{
								isReachable = 1;
								Node node = (Node)i.next();
								if (node.label.equals(new BigInteger("1")))
								{
									LinkedList p = node.parents;
									Iterator n = p.iterator ();
									while (n.hasNext())
									{
										Node pnode = (Node)n.next ();
										if (pnode.label.mod(node.label).equals(ZERO))
										{
											isReachable = 0;
											break;
										}
									}
								}
								else
								{
									if (((BigInteger)fLabel.get(obj)).mod(node.label).equals(ZERO))
										isReachable = 0;
								}
								if (withModValue)
									strB.append(node.value + ", " + isReachable + ", ");
								else
									strB.append(node.value + ", ");
							}
							strB.setLength(strB.length() - 2);
							strB.append("'");
						}
					}
					strB.append("/>");
				}
			}
			strB.append("</ADAG>");
			return strB;
		}
		catch (Exception e)
		{
			String errorMessage = "DAG::toXMLString: Error: " + e;
			config.setStatusWindowError(errorMessage);
			return null;
		}
	}

	/**
	 * 
	 * Translates DAG into XML without parents and children information.
	 * @return StringBuffer containg DAG in XML tags
	 * 
	 */
	public StringBuffer toXMLString ()
	{
		try
		{
			StringBuffer strB = new StringBuffer ();
			Enumeration e = this.elements ();
			strB.append("<ADAG>");
			while (e.hasMoreElements())
			{
				LinkedList list = (LinkedList)e.nextElement ();
				Object obj = list.getFirst();
				Field fValue = obj.getClass().getDeclaredField("value");
				Field fLabel = obj.getClass().getDeclaredField("label");
				String value = fValue.get(obj).toString();
				String label = fLabel.get(obj).toString();
				strB.append("<node");
				strB.append(" value='" + value + "'");
				strB.append(" label='" + label + "'" + "/>");
			}
			strB.append("</ADAG>");
			return strB;
		}
		catch (Exception e)
		{
			String errorMessage = "DAG:toXMLString: Error: " + e;
			config.setStatusWindowError(errorMessage);
			return null;
		}
	}

	/**
	 * 
	 * <p>
	 * Data strucutre for storing annotated Node.
	 * 
	 * @author Shahid Alam
	 * @version 3.0
	 * 
	 */
	private static class Node
	{
		public Object value = null;
		public int prime = 1;
		public int weight = 1;
		public BigInteger label = null;
		public LinkedList parents = null;
		public LinkedList children = null;
	}
}
