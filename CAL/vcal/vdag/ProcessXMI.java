/* ============================================================================
|
|   Filename:    ProcessXMI.java
|   Dated:       01 Dec, 2006
|   By:          Shahid Alam (salam@sce.carleton.ca)
|
|   Description: See below
|
|   TO DO:       See below
|
 ----------------------------------------------------------------------------*/

package org.vcal.vdag;

import org.w3c.dom.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import java.awt.Font;
import java.awt.Color;
import java.awt.geom.Rectangle2D;
import javax.swing.BorderFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.util.Hashtable;

import org.jgraph.JGraph;
import org.jgraph.graph.DefaultCellViewFactory;
import org.jgraph.graph.GraphLayoutCache;
import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.DefaultPort;
import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.DefaultGraphModel;
import org.jgraph.graph.GraphModel;

import org.eclipse.swt.widgets.Shell;

import org.vcal.config.Config;
import org.vcal.vdag.VisualDAG;
import org.vcal.vdag.VisualDAGConstants;

/**
*
* <p>
* This class processes XMI (UML model in XML).
* <p>
* It extracts class dependency information from XMI and uses
* this information and class ADAG to create an annotated DAG.
*
* @author Shahid Alam
* @version 3.0
* 
*/
public class ProcessXMI
{
	private static Config config = null;
	private static int START_X = 10, START_Y = 10, NODES_IN_ONE_ROW = 5, FONT_SIZE = 10;
	private static Document document = null;
	private static ADAG adag = null;
	private static JGraph graph = null;
	private static String AdagXMLFileName = "vdag.xml";
	private static DefaultGraphCell cells[] = null;

	/**
	 * Process the XMI file which contains information about the model and diagram view. 
	 * @param con Config class for acc3essing global parameters
	 */
	public ProcessXMI (Config con)
	{
		config = con;
		START_X = 10; START_Y = 10; NODES_IN_ONE_ROW = 5; FONT_SIZE = 10;
		AdagXMLFileName = "vdag.xml";
		adag = new ADAG (config);
	}

	/**
	 * Parse the XMI file which contains information about the model and diagram view. 
	 * @param xmlFileName Name fo the XMI file to be processed
	 */
	public boolean parse (String xmlFileName, Config config)
	{
		try {
			String rootElement = "_ROOT_";
			//
			// Use JAXP's DocumentBuilderFactory so that there
			// is no code here that is dependent on a particular
			// DOM parser.
			//
			DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance ();
			DocumentBuilder builder = builderFactory.newDocumentBuilder ();
			//
			// Standard DOM code from hereon. The "parse"
			// method invokes the parser and returns a fully parsed
			// Document object. We'll then recursively descend the
			// tree and copy non-text nodes into JTree nodes.
			//
			document = builder.parse(new FileInputStream (new File(xmlFileName)));
			document.getDocumentElement().normalize();

			Element re = document.getDocumentElement();
			if (re != null && re.getNodeName().equals("XMI"))
			{
				/**
				 * Construct Model and Graph
				 */
				GraphModel model = new DefaultGraphModel ();
				DefaultCellViewFactory cellViewFactory = new DefaultCellViewFactory();
				GraphLayoutCache graphLayout = new GraphLayoutCache (model, cellViewFactory, true);
				graph = new JGraph (graphLayout);
				Hashtable htD = new Hashtable ();
				Hashtable htE = new Hashtable ();
				NodeList nodes = null;
				NamedNodeMap attributes = null;
	
				/**
				 * Sometimes the tool also saves the deleted relationships.
				 * So Create a hash table of UMLGeneralizationView and UMLDependencyView
				 * Diagram elements. These are the actual Generalization and Dependency
				 * used in the diagram. To be used latter for comparing with the
				 * Generalization and Dependency saved for the model by the tool.
				 */
				nodes = document.getElementsByTagName ("UML:DiagramElement");
				int count = 0;
				for (int n = 0; n < nodes.getLength(); n++)
				{
					attributes = nodes.item(n).getAttributes();
					if (attributes != null && attributes.getLength () > 0)
					{
						try
						{
							String obj = attributes.getNamedItem ("xmi.id").getNodeValue();
							if (obj.startsWith ("UMLGeneralizationView"))
							{
								obj = attributes.getNamedItem ("subject").getNodeValue();
								if (!htD.contains(obj))
									htD.put (new Integer(count), obj);
								count++;
							}
							else if (obj.startsWith ("UMLRealizationView"))
							{
								obj = attributes.getNamedItem ("subject").getNodeValue();
								if (!htD.contains(obj))
									htD.put (new Integer(count), obj);
								count++;
							}
							else if (obj.startsWith ("UMLDependencyView"))
							{
								obj = attributes.getNamedItem ("subject").getNodeValue();
								if (!htD.contains(obj))
									htD.put (new Integer(count), obj);
								count++;
							}
						}
						catch (NullPointerException e) { }
					}
				}
	
				/**
				 * Create a hash table of the following Model Elements:<br>
				 * Class<br>
				 * To be used latter.
				 */
				nodes = document.getElementsByTagName ("UML:Class");
				for (int n = 0; n < nodes.getLength(); n++)
				{
					attributes = nodes.item(n).getAttributes();
					if (attributes != null && attributes.getLength () > 0)
					{
						try
						{
							String id = attributes.getNamedItem ("xmi.id").getNodeValue();
							String name = attributes.getNamedItem ("name").getNodeValue();
							if (!htE.contains(name))
								htE.put (id, name);
						}
						catch (NullPointerException e) { }
					}
				}
	
				/**
				 * Now compare Generalization and Dependency relationships with the 
				 * relationships in the hash table. Also get the name of the class
				 * with which the relationship is used.
				 * @see rootElement
				 */
				if (rootElement.equals(Config.ROOT_ELEMENT))
				{
					if (Config.DEBUG)
						config.setStatusWindowText ("\nGENERALIZATION:");
					nodes = document.getElementsByTagName ("UML:Generalization");
					for (int n = 0; n < nodes.getLength(); n++)
					{
						attributes = nodes.item(n).getAttributes();
						if (attributes != null && attributes.getLength () > 0)
						{
							try
							{
								String gen = attributes.getNamedItem ("xmi.id").getNodeValue();
								if (htD.containsValue(gen))
								{
									String child = attributes.getNamedItem ("child").getNodeValue();
									child = (String)htE.get(child);
									if (child != null)
									{
										/**
										 * Generalization gets weight of 2
										 */
										adag.addNode (child, 2);
										if (Config.DEBUG)
											config.setStatusWindowText ("   Child: " + child);
									}
									String parent = attributes.getNamedItem ("parent").getNodeValue();
									parent = (String)htE.get(parent);
									if (parent != null)
									{
										/**
										 * Generalization gets weight of 2
										 */
										adag.addNode (parent, 2);
										if (child != null)
										{
											adag.setParent (child, parent);
										}
										if (Config.DEBUG)
											config.setStatusWindowText ("   Parent: " + parent);
									}
								}
							}
							catch (NullPointerException e) { }
						}
					}
					if (Config.DEBUG)
						config.setStatusWindowText ("\nREALIZATION:");
					nodes = document.getElementsByTagName ("UML:Abstraction");
					for (int n = 0; n < nodes.getLength(); n++)
					{
						attributes = nodes.item(n).getAttributes();
						if (attributes != null && attributes.getLength () > 0)
						{
							try
							{
								String rel = attributes.getNamedItem ("xmi.id").getNodeValue();
								if (htD.containsValue(rel))
								{
									String client = attributes.getNamedItem ("client").getNodeValue();
									client = (String)htE.get(client);
									if (client != null)
									{
										/**
										 * Dependency gets weight of 1
										 */
										adag.addNode (client, 1);
										if (Config.DEBUG)
											config.setStatusWindowText ("   Client: " + client);
									}
									String supplier = attributes.getNamedItem ("supplier").getNodeValue();
									supplier = (String)htE.get(supplier);
									if (supplier != null)
									{
										/**
										 * Dependency gets weight of 1
										 */
										adag.addNode (supplier, 1);
										if (client != null)
										{
											adag.setParent (client, supplier);
										}
										if (Config.DEBUG)
											config.setStatusWindowText ("   Supplier: " + supplier);
									}
								}
							}
							catch (NullPointerException e) { }
						}
					}
					if (Config.DEBUG)
						config.setStatusWindowText ("\nDEPENDENCY:");
					nodes = document.getElementsByTagName ("UML:Dependency");
					for (int n = 0; n < nodes.getLength(); n++)
					{
						attributes = nodes.item(n).getAttributes();
						if (attributes != null && attributes.getLength () > 0)
						{
							try
							{
								String dep = attributes.getNamedItem ("xmi.id").getNodeValue();
								if (htD.containsValue(dep))
								{
									String client = attributes.getNamedItem ("client").getNodeValue();
									client = (String)htE.get(client);
									if (client != null)
									{
										/**
										 * Dependency gets weight of 1
										 */
										adag.addNode (client, 1);
										if (Config.DEBUG)
											config.setStatusWindowText ("   Client: " + client);
									}
									String supplier = attributes.getNamedItem ("supplier").getNodeValue();
									supplier = (String)htE.get(supplier);
									if (supplier != null)
									{
										/**
										 * Dependency gets weight of 1
										 */
										adag.addNode (supplier, 1);
										if (client != null)
										{
											adag.setParent (client, supplier);
										}
										if (Config.DEBUG)
											config.setStatusWindowText ("   Supplier: " + supplier);
									}
								}
							}
							catch (NullPointerException e) { }
						}
					}
				}
				/**
				 * TO DO
				 * 
				 * TO BE COMPLETED IN FUTURE VERSIONS
				 * 
				 * see rootElement
				 * 
				 */
				else
				{
				}
				return true;
			}
			else
			{
				config.setStatusText(" XMI File Error");
				return false;
			}
		}
		catch (Exception e)
		{
			String errorMessage = "ProcessXMI: Error: " + e;
			config.setStatusWindowError(errorMessage);
			return false;
		}
	}

	/**
	 * 
	 * Create graph
	 *
	 */
	private void createGraph ()
	{
		if (graph != null)
		{
			// Uneditable vertices and edges
			graph.setEditable (false);
			// Edges are not able to disconnect from vertices
			graph.setDisconnectable (false);
			// Control-drag should clone selection
			graph.setCloneable(true);
			// Enable edit without final RETURN keystroke
			graph.setInvokesStopCellEditing(true);
			// When over a cell, jump to its default port (we only have one, anyway)
			graph.setJumpToDefaultPort(true);

			addVerticesEdges ();
		}
	}

	/**
	 * Add vertices and edges to the graph
	 * Create vertices.
	 * Setting parents for vertices with label '1'.
	 * Ports of these vertices are set to the parent labels.
	 *
	 */
	private void addVerticesEdges ()
	{
		try
		{
			int x = START_X;
			int y = START_Y;
			String strArray[] = adag.flattenArray ();
			int SIZE = strArray.length / 2;
			cells = new DefaultGraphCell[SIZE];

			//
			// Create vertices.
			// Setting parents for vertices with label '1'.
			// Ports of these vertices are set to the parent labels.
			//
			int n1 = 0;
			for (int n = 0; n < SIZE; n++)
			{
				String value = strArray[n1];
				n1++;
				String weight = strArray[n1];
				n1++;
				cells[n] = createVertex (value, weight, x, y, Color.YELLOW, Color.BLUE);
				y += (-20 * (n%2)) + 10;
				if ((n % NODES_IN_ONE_ROW) == 0)
				{
					x = START_X;
					y += 100;
				}
				else
					x += 200;
				if (n1 > 2*SIZE)
					break;
			}

			//
			// Create edges
			//
			n1 = 0;
			for (int n = 0; n < SIZE; n++)
			{
				String value = strArray[n1];
				n1 += 2;
				DefaultGraphCell cellChild = null;
				String children[] = adag.getChildren (value);
				for (int i = 0; i < children.length; i++)
				{
					cellChild = getCell (cells, children[i].trim());
					if (cellChild != null)
					{
						createEdge (cells[n], cellChild);
					}
				}
			}
		}
		catch (Exception e)
		{
			String errorMessage = "ProcessXMI::addVerticesEdges: Error: " + e;
			config.setStatusWindowError(errorMessage);
		}
	}

	/**
	 * Create an edge
	 * @param source Source node of the edge
	 * @param target Target node of the edge
	 * @return the edge created
	 */
	public static DefaultEdge createEdge (DefaultGraphCell source, DefaultGraphCell target)
	{
		DefaultEdge edge = new DefaultEdge();
		// Set Arrow Style for edge
		VisualDAGConstants.setLineEnd (edge.getAttributes(), VisualDAGConstants.ARROW_CLASSIC);
		VisualDAGConstants.setEndFill (edge.getAttributes(), true);
		VisualDAGConstants.setSizeable (edge.getAttributes(), true);
		VisualDAGConstants.setSelectable (edge.getAttributes(), false);
		graph.getGraphLayoutCache().insertEdge (edge, source.getChildAt(0), target.getChildAt(0));

		return edge;
	}
	
	/**
	 * Search for the node in the list of nodes
	 * 
	 * TO DO
	 * 
	 * TO CHANGE TO IMPROVE SEARCHING USING OTHER SEARCHING TECHNIQUE
	 * 
	 * @param cells List of nodes to be searched
	 * @param value Value to be searched
	 * @return The node found or null if not found
	 */
	public static DefaultGraphCell getCell (Object cells[], String value)
	{
		DefaultGraphCell cell = null;
		for (int n = 0; n < cells.length; n++)
		{
			if (cells[n].toString() != null && cells[n].toString().compareTo (value) == 0)
			{
				cell = (DefaultGraphCell)cells[n];
				break;
			}
		}
		return cell;
	}

	/**
	 * Create Vertex
	 * 
	 * @param name Name of the vertex
	 * @param weight Weight of the vertex
	 * @param x Horizontal position of the view vertex
	 * @param y Vertical position of the view vertex
	 * @param bg Background color of the vertex
	 * @return Vertex created or null if not successful
	 */
	public static DefaultGraphCell createVertex (String name, String weight, double x, double y,
																	Color bg, Color fg)
	{
		// Create vertex with the given name
		DefaultGraphCell cell = new DefaultGraphCell (name);

		VisualDAGConstants.setWeight (cell.getAttributes(), weight);
//		Font font = new Font ("Arial", Font.BOLD, FONT_SIZE);
//		VisualDAGConstants.setFont (cell.getAttributes(), font);
		VisualDAGConstants.setInset (cell.getAttributes(), 3);
		VisualDAGConstants.setAutoSize (cell.getAttributes(), true);
		VisualDAGConstants.setSelectable (cell.getAttributes(), false);
		VisualDAGConstants.setEditable (cell.getAttributes(), false);
		VisualDAGConstants.setSizeable (cell.getAttributes(), false);
		VisualDAGConstants.setBounds (cell.getAttributes(), new Rectangle2D.Double(x, y, 0, 0));

		// Set fill color
		if (bg != null)
		{
			VisualDAGConstants.setGradientColor (cell.getAttributes(), bg);
			VisualDAGConstants.setForeground (cell.getAttributes(), fg);
			VisualDAGConstants.setOpaque (cell.getAttributes(), true);
		}

		// Set blue border
		VisualDAGConstants.setBorderColor (cell.getAttributes(), fg);

		// Add a Floating Port
		DefaultPort port = new DefaultPort ();
		cell.add (port);
		port.setParent(cell);
		graph.getGraphLayoutCache().insert (cell);
		VisualDAGConstants.setSelectable (cell.getAttributes(), true);

		return cell;
	}

	/**
	 * Create graph control for displaying
	 * 
	 * @return Graph control for displaying
	 */
	public VisualDAG createGraphControl (Shell shell)
	{
		try
		{
			if (adag != null)
			{
				/**
				 * 
				 * Write XML string to file
				 */
				String adagXMLStr = adag.flattenFull (false);
				BufferedWriter bw = new BufferedWriter (new FileWriter (AdagXMLFileName));
				bw.write(adagXMLStr);
				bw.close();
				// Create graph using the XML file
				createGraph ();
				if (Config.DEBUG)
					config.setStatusWindowText (adagXMLStr);
				// Show graph in frame
				VisualDAG vDAG = null;
				graph.setCloneable (false);
				vDAG = new VisualDAG (shell, adag, graph, config);
				config.setADAG(adag);
				if (vDAG != null)
				{
					return vDAG;
				}
			}
			return null;
		}
		catch (Exception e)
		{
			String errorMessage = "ProcessXMI::display: Error: " + e;
			config.setStatusWindowError(errorMessage);
			return null;
		}
	}
}
