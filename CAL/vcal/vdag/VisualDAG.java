/* ============================================================================
|
|   Filename:    VisualDAG.java
|   Dated:       01 Dec, 2006
|   By:          Shahid Alam (salam@sce.carleton.ca)
|
|   Description: See below
|
|   TO DO:       
|
 ----------------------------------------------------------------------------*/

package org.vcal.vdag;

import java.awt.Color;
import java.util.HashSet;
import java.util.Iterator;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.io.BufferedWriter;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.ToolBar;

import org.jgraph.JGraph;
import org.jgraph.graph.DefaultGraphCell;

import org.vcal.VCAL;
import org.vcal.config.Config;

/**
 *
 * <p>
 * This class displays the graph and implements basic GUI.
 * <p>
 * It displays the graph (DAG) in a frame, and creates the basic GUI.
 * <p>
 * It zoom in and zoom out the graph.
 * <p>
 * It checks for reachability between two or more than two selected elements.
 * <p>
 * It checks for descendants and ancestors of a selected element.
 *
 * @author Shahid Alam
 * @version 3.0
 * 
 */
public class VisualDAG
{
	private static Config config = null;
	private static Shell shellParent = null;
	private final static int ADD_X = 10, ADD_Y = 10;
	private static ADAG adag = null;
	private static JGraph graph = null;
	private static String dependencyInfoStr = null;

	public boolean DESCENDANTS_VISIBLE = false;
	public boolean ANCESTORS_VISIBLE = false;
	public boolean BOTH_VISIBLE = false;


	private static int SCALE_DEFAULT = 1;
	private static int SCALE = SCALE_DEFAULT;
	private static int SCALE_LIMIT_UPPER = 5;
	private static int SCALE_LIMIT_LOWER = SCALE_DEFAULT;
	private static ToolItem zoomInB = null, zoomOutB = null;
	private static HashSet ASET = null;         			// Set of ancestors
	private static HashSet DSET = null;         			// Set of descendants

	/**
	 * Constructor
	 * 
	 * @param shell Shell to be used for displaying VDAG
	 * @param a ADAG class (Annotated DAG)
	 * @param g Graph (Directed Acyclic Graph) to be displayed.
	 * @param con Config class for accessing global parameters
	 */
	public VisualDAG (Shell shell, ADAG a, JGraph g, Config con)
	{
		config = con;
		shellParent = shell;
		adag = a;
		graph = g;
		if (adag != null && graph != null)
		{
			Object allCells[] = graph.getRoots();
	   		if (Config.DEBUG)
	   		{
				for (int n = 0; n < allCells.length; n++)
				{
		           	DefaultGraphCell cell = (DefaultGraphCell)allCells[n];
		           	String obj = cell.toString();
		           	if (obj != null)
		           	{
		            	String labelS = adag.getLabel (obj).toString();
		            	if (labelS != null)
		            		config.setStatusWindowText(cell + ", " + labelS);
		           	}
				}
	   		}

			/**
			*
			* Make the graph scaled
			*/
  			graph.setScale (SCALE_DEFAULT);
		}
	}

	/**
	 * Destructor
	 *
	 */
	public void closeAll ()
	{
		graph = null;
		zoomInB = null; zoomOutB = null;
		ASET = null;
		DSET = null;
		adag = null;
	}

	/**
	 * 
	 * make the complete graph visible
	 *
	 */
	public void makeAllInvisible ()
	{
		try
		{
			if (graph != null)
			{
				Object allCells[] = graph.getRoots();
				graph.getGraphLayoutCache().setVisible (allCells, false);
				Config.GRAPH_VISIBLE = false;
			}
		}
		catch (Exception e)
		{
			config.setStatusWindowError("VisualDAG::makeAllVisible: " + e);
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * make the complete graph visible
	 *
	 */
	public boolean makeAllVisible ()
	{
		try
		{
			if (graph != null)
			{
				Object allCells[] = graph.getRoots();
				graph.getGraphLayoutCache().setVisible (allCells, true);
				if (allCells.length > 0 && allCells[0].toString() != null)
				{
					graph.getGraphLayoutCache().setVisible (allCells[0], true);
					Config.GRAPH_VISIBLE = true;
					return true;
				}
			}
			return false;
		}
		catch (Exception e)
		{
			config.setStatusWindowError("VisualDAG::makeAllVisible: " + e);
			e.printStackTrace();
			return false;
		}
	}

	/**
	 *
	 * make all the descendants of the cell visible
	 * and hence it becomes a subgraph of the full graph.
	 * @param cellToView Graph cell to be viewed with all the descendants.
	 * It can be passed as value of the cell which in this case is type String.
	 *
	 */
	public boolean makeSubGraphVisible (Object cellToView)
	{
		boolean value = false;
		if (Config.DEBUG)
			config.setStatusWindowText("DAG Node to View: " + cellToView);
		if (graph != null && cellToView != null)
		{
			if (cellToView.equals("_ROOT_"))
			{
				value = makeAllVisible ();
			}
			else
			{
				try
				{
					makeAllInvisible ();
					BigInteger One = new BigInteger ("1");
		        	BigInteger label = adag.getLabel (cellToView.toString());
		        	if (label != null && !label.equals(One))
		        	{
						Object allCells[] = graph.getRoots();
		        		for (int i = 0; i < allCells.length; i++)
		        		{
			            	if (allCells[i].toString() != null)
			            	{
				            	DefaultGraphCell cell = (DefaultGraphCell)(allCells[i]);
			            		BigInteger labelS = adag.getLabel (cell.toString());
				            	BigInteger labelC[] = null;
				            	if (labelS.equals(One))
				            	{
				            		labelC = adag.getParentsLabels (cell.toString());
				            	}
				            	else
				            	{
				            		labelC = new BigInteger[1];
				            		labelC[0] = labelS;
				            	}
				            	for (int n = 0; n < labelC.length; n++)
				            	{
				            		int r = 0;
				            		if (label.compareTo(labelC[n]) == 1)
						    	    {
			    	            		r = label.mod(labelC[n]).intValue();
			    	            		if (r == 0)
			    	            		{
			    	            			graph.getGraphLayoutCache().setVisible (cell, true);
			    	            			value = true;
			    	            			break;
			    	            		}
						    	    }
				            		else if (labelC[n].compareTo(label) == 0)
				            		{
		    	            			graph.getGraphLayoutCache().setVisible (cell, true);
		    	            			value = true;
		    	            			break;
				            		}
				            	}
			            	}
		        		}
		        	}
		        	else
		        		config.setStatusWindowError("DAG Node " + cellToView + " Not Present");
				}
				catch (Exception e)
				{
					config.setStatusWindowError("VisualDAG::makeSubGraphVisible: " + e);
					return value;
				}
			}
		}

		Config.GRAPH_VISIBLE = value;
		return value;
	}

	/**
	 * 
	 * make visible according to the variable set for visibility as follows:
	 *    DESCENDANTS_VISIBLE
	 *    ANCESTORS_VISIBLE
	 *    BOTH_VISIBLE
	 * 
	 */
	public void makeVisible ()
	{
		if (!Config.GRAPH_VISIBLE)
			return;
   		try
		{
   			ASET = new HashSet ();
   			DSET = new HashSet ();

			dependencyInfoStr = "Please select one element to check for descendants and ancestors " +
								"and more than one element to check for reachability";

	        /**
	         * 
	         * If the elements selected are two or more than two
	         * only then check for dependency otherwise display an info message.
	         * It computes the mod of one label with other label. If the mod is 0
	         * only then the two elements are reachable.
	         * Since the nodes with label '1' have there parents stored in ports.
	         * So it checks for that and store them in an array to be checked recursively
	         * with other labels.
	         *
	         * If more than two elements are selected then they are checked recursively
	         * with other elements for reachability.
	         */
	        Object cells[] = graph.getSelectionCells();
            int len = cells.length;
            /**
             * 
             * Check for REACHABILITY
             * 
             */
            if (cells != null && len > 1)
            {
       			if (!DESCENDANTS_VISIBLE && !ANCESTORS_VISIBLE && !BOTH_VISIBLE)
       			{
	            	dependencyInfoStr = "DEPENDENCY LIST where ---> is REACHABLE and ---! is NOT REACHABLE\n\n";
	            	for (int n = 0; n < (len-1); n++)
	            	{
		            	DefaultGraphCell cell = (DefaultGraphCell)cells[n];
		            	String labelS = adag.getLabel(cell.toString()).toString();
		            	BigInteger label[] = null;
		            	if (labelS.equals("1"))
		            	{
		            		label = adag.getParentsLabels (cell.toString());
		            	}
		            	else
		            	{
		            		label = new BigInteger[1];
		            		label[0] = new BigInteger (labelS);
		            	}
		            	if (label != null)
		            	{
		            		int c = n + 1;
		            		for ( ; c < len; c++)
		            		{
		            			int r = 1;
		            			DefaultGraphCell cellC = (DefaultGraphCell)cells[c];
		    	            	labelS = adag.getLabel(cellC.toString()).toString();
		    	            	BigInteger labelC[] = null;
		    	            	if (labelS.equals("1"))
		    	            	{
		    	            		labelC = adag.getParentsLabels (cellC.toString());
		    	            	}
		    	            	else
		    	            	{
		    	            		labelC = new BigInteger[1];
		    	            		labelC[0] = new BigInteger (labelS);
		    	            	}
		    	            	if (labelC != null)
		    	            	{
			    	            	for (int n1 = 0; n1 < label.length; n1++)
			    	            	{
			    	            		for (int c1 = 0 ; c1 < labelC.length; c1++)
			    	            		{
					    	            	if (label[n1].compareTo(labelC[c1]) == 1)
					    	            		r = label[n1].mod(labelC[c1]).intValue();
					    	            	else
					    	            		r = labelC[c1].mod(label[n1]).intValue();
					    	            	if (r == 0)
					    	            		break;
			    	            		}
			    	            		if (r == 0)
			    	            			break;
			    	            	}
				            		if (r == 0)
				            			dependencyInfoStr += cell + " ---> " + cellC + "\n";
			    	            	else
			    	            		dependencyInfoStr += cell + " ---! " + cellC + "\n";
			    	            	if (Config.DEBUG)
			    	            		config.setStatusWindowText(dependencyInfoStr);
		    	            	}
		            		}
		            	}
	            	}
       			}
            }
            /**
             * 
             * Check for DESCENDANTS and ANCESTORS
             * 
             */
            else if (cells != null && len == 1)
            {
				Object allCells[] = graph.getRoots();
            	boolean isLabelOne_1 = false, isLabelOne_2 = false;
            	String d = "";
            	String a = "";
            	DefaultGraphCell cell = (DefaultGraphCell)(cells[0]);
            	String labelS = adag.getLabel(cell.toString()).toString();
            	BigInteger label[] = null;
            	if (labelS.equals("1"))
            	{
            		isLabelOne_1 = true;
            		label = adag.getParentsLabels (cell.toString());
            	}
            	else
            	{
            		label = new BigInteger[1];
            		label[0] = new BigInteger (labelS);
            	}
            	/**
            	 * 
            	 * Compute the mod recursively for all the cell label (label[])
            	 *  with labels (labelC[]) of all other cells in the graph.
            	 * 
            	 * label[] either contains the label of the current cell.
            	 *                          OR
            	 * label[] contains all the label of parents of current
            	 * cell in case current cell' label is '1'.
            	 * 
            	 * labelC contains the label of all the graph cells
            	 * to be checked.
            	 * 
            	 * labelC[] either contains the label of the cell to
            	 * be checked.
            	 *                          OR
            	 * labelC[] contains all the label of parents of the
            	 * cell to be checked in case the cell' label is '1'.
            	 * 
            	 */
            	if (label != null)
            	{
	            	for (int n = 0; n < label.length; n++)
	            	{
	            		for (int c = 0; c < allCells.length; c++)
		            	{
		            		isLabelOne_2 = false;
	            			int r = 1;
			            	DefaultGraphCell cellC = (DefaultGraphCell)(allCells[c]);
			            	String obj = cellC.toString();
				            if (obj != null)
				            {
				            	labelS = adag.getLabel (obj).toString();
			            		BigInteger labelC[] = null;
		    	            	if (labelS.equals("1"))
		    	            	{
		    	            		isLabelOne_2 = true;
		    	            		labelC = adag.getParentsLabels (cellC.toString());
		    	            	}
		    	            	else
		    	            	{
		    	            		labelC = new BigInteger[1];
		    	            		labelC[0] = new BigInteger (labelS);
		    	            	}
		    	            	/**
		    	            	 * 
		    	            	 * If label is not one then look for ancestors and descendants
		    	            	 * 
		    	            	 */
		    	            	if (labelC != null)
		    	            	{
									if (!isLabelOne_1)
									{
										for (int n1 = 0 ; n1 < labelC.length; n1++)
										{
											/**
											 * If label is greater than labelC
											 */
			    	            			if (label[n].compareTo(labelC[n1]) == 1)
					    	            	{
					    	            		r = label[n].mod(labelC[n1]).intValue();
					    	            		if (r == 0)
					    	            		{
					    	            			d += "   " + cellC + "\n";
					    	            			if (!DSET.contains(cellC))
				    	            					DSET.add(cellC);
					    	            			break;
					    	            		}
					    	            	}
											/**
											 * If label is less than or equal to labelC.
											 * It can be only equal if cellC has label '1'
											 */
					    	            	else if (!cellC.toString().equals(cell.toString()))
					    	            	{
					    	            		if (isLabelOne_2)
					    	            		{
					    	            			/**
					    	            			 * If label one then check if both labels are equal
					    	            			 * only then cellC is descendant of cell
					    	            			 */
													if (labelC[n1].equals(label[n]))
													{
														d += "   " + cellC + "\n";
						    	            			if (!DSET.contains(cellC))
					    	            					DSET.add(cellC);
					    	            				break;
													}
					    	            		}
					    	            		/**
					    	            		 * Now label cannot be equal, so check
					    	            		 * for mod and add as ancestor
					    	            		 */
					    	            		else
					    	            		{
						    	            		r = labelC[n1].mod(label[n]).intValue();
						    	            		if (r == 0)
						    	            		{
				    	            					a += "   " + cellC + "\n";
						    	            			if (!ASET.contains(cellC))
					    	            					ASET.add(cellC);
						    	            			break;
						    	            		}
					    	            		}
			    	            			}
										}
				            		}
									/**
									 * 
									 * If label is '1' then only look for ancestors.
									 * But if both labels are one then no need to check
									 * i.e. there are no possible ancestors.
									 * 
									 */
									else if (!isLabelOne_2)
									{
										for (int n1 = 0 ; n1 < labelC.length; n1++)
										{
											/**
											 * labelC is greater than or equal to the label.
											 * and cellC is already not inserted in the ancestor's list
											 */
			    	            			if (!ASET.contains(cellC) && labelC[n1].compareTo(label[n]) >= 0)
					    	            	{
			    	            				r = labelC[n1].mod(label[n]).intValue();
					    	            		if (r == 0)
					    	            		{
					    	            			a += "   " + cellC + "\n";
				    	            				ASET.add(cellC);
					    	            			break;
					    	            		}
					    	            	}
										}
									}
		    	            	}
			            	}
		            	}
	            	}
            	}
       			if (!DESCENDANTS_VISIBLE && !ANCESTORS_VISIBLE && !BOTH_VISIBLE)
       			{
       				dependencyInfoStr = "DESCENDANTS OF " + cell + "\n";
	        		dependencyInfoStr += d + "\n";
	        		dependencyInfoStr += "ANCESTORS OF " + cell + "\n";
	        		dependencyInfoStr += a;
       			}
       			else if (DESCENDANTS_VISIBLE)
       			{
       				dependencyInfoStr = null;
   					graph.getGraphLayoutCache().setVisible (allCells, false);
        			graph.getGraphLayoutCache().setVisible (cell, true);
        			Config.GRAPH_VISIBLE = true;
        			Iterator i = DSET.iterator ();
       				while (i.hasNext())
       				{
       					DefaultGraphCell c = (DefaultGraphCell)i.next();
            			graph.getGraphLayoutCache().setVisible (c, true);
       				}
       			}
       			else if (ANCESTORS_VISIBLE)
       			{
       				dependencyInfoStr = null;
        			graph.getGraphLayoutCache().setVisible (allCells, false);
        			graph.getGraphLayoutCache().setVisible (cell, true);
        			Config.GRAPH_VISIBLE = true;
       				Iterator i = ASET.iterator ();
       				while (i.hasNext())
       				{
       					DefaultGraphCell c = (DefaultGraphCell)i.next();
            			graph.getGraphLayoutCache().setVisible (c, true);
       				}
       			}
       			else if (BOTH_VISIBLE)
       			{
       				dependencyInfoStr = null;
        			graph.getGraphLayoutCache().setVisible (allCells, false);
        			graph.getGraphLayoutCache().setVisible (cell, true);
        			Config.GRAPH_VISIBLE = true;
       				Iterator i = DSET.iterator ();
       				while (i.hasNext())
       				{
       					DefaultGraphCell c = (DefaultGraphCell)i.next();
            			graph.getGraphLayoutCache().setVisible (c, true);
       				}
       				i = ASET.iterator ();
       				while (i.hasNext())
       				{
       					DefaultGraphCell c = (DefaultGraphCell)i.next();
            			graph.getGraphLayoutCache().setVisible (c, true);
       				}
       			}
            }
   			if (dependencyInfoStr != null)
   			{
   				config.setStatusWindowText(dependencyInfoStr);
   			}
			DESCENDANTS_VISIBLE = false;
			ANCESTORS_VISIBLE = false;
			BOTH_VISIBLE = false;
		}
   		catch (Exception e)
   		{
   			config.setStatusWindowError("VisualDAG::makeVisible: Error: " + e);
   		}
	}

	/**
	 * Add vertex to the graph
	 * @param vertexName Name of the vertex to be added
	 * @param vertexWeight Weight of the vertex to be added
	 * @param vertexParents Parents of the vertex. Name of all the parents separated by comma.
	 * @param vertexChildren Children of the vertex. Name of all the children separated by comma.
	 * @return true on success and false on failure
	 */
	private static boolean addVertex (String vertexName, String vertexWeight, String vertexParents, String vertexChildren)
	{
		boolean added = false;
		int weight = 1;
		try
		{
			weight = new Integer(vertexWeight).intValue();
		}
		catch (Exception e)
		{
			weight = 1;
		}

		/**
		 * Adding vertex to the graph
		 * Also add it to ADAG
		 */
		if (vertexName != null && adag.addNode (vertexName, weight) != null)
		{
			Object allCells[] = graph.getRoots();
			String w = weight + "";
			DefaultGraphCell cell = ProcessXMI.createVertex (vertexName, w, ADD_X, ADD_Y, Color.GREEN, Color.BLUE);
	
			/**
			 * Adding edges to all the parents
			 * Also set parents of vertex in ADAG
			 */
			DefaultGraphCell cellE = null;
			if (vertexParents != null && vertexParents.length() > 0)
			{
				String parents[] = vertexParents.split(",");
				for (int i = 0; i < parents.length; i++)
				{
					cellE = ProcessXMI.getCell (allCells, parents[i].trim());
					if (cellE != null)
					{
						if (!adag.setParent (cell.toString(), cellE.toString()))
							return false;
						ProcessXMI.createEdge (cellE, cell);
					}
				}
			}

			/**
			 * Adding edges to all the children
			 * Also set parent of each child to vertex in ADAG
			 */
			cellE = null;
			if (vertexChildren != null && vertexChildren.length() > 0)
			{
				String children[] = vertexChildren.split(",");
				for (int i = 0; i < children.length; i++)
				{
					cellE = ProcessXMI.getCell (allCells, children[i].trim());
					if (cellE != null)
					{
						if (!adag.setParent (cellE.toString(), cell.toString()))
							return false;
						ProcessXMI.createEdge (cell, cellE);
					}
				}
			}
	
			added = true;
		}

		return added;
	}

	/**
	 * Delete the selected vertex from the graph.
	 * @return true on success and false on failure
	 */
	private static boolean delVertex ()
	{
		/**
		 * Delete from ADAG
		 */
        Object cells[] = graph.getSelectionCells();
       	String info = "Select one element";
        if (cells.length == 1)
        {
        	String vertex = cells[0].toString();
	       	info = "Error deleting " + vertex;
	   		if (adag.delNode(vertex))
	   		{
				graph.getGraphLayoutCache().remove (cells, false, true);
	    		info = vertex + " deleted successfully";
	    		config.setStatusWindowText(info);
				return true;
	   		}
        }
        config.setStatusWindowText(info);

		return false;
	}

	/**
	 * Show add vertex window
	 */
	public void showAddVertexDialog ()
	{
		if (Config.GRAPH_VISIBLE)
		{
			final Shell shell = new Shell (shellParent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
			shell.setText("Add Vertex");
			shell.setSize (350, 230);
			Point p = shellParent.getLocation();
			int cx = p.x;
			int cy = p.y;
			Point p1 = shellParent.getSize();
			cx = cx + p1.x/2 - 450/2;
			cy = cy + p1.y/2 - 200/2;
			shell.setLocation (cx, cy);
	
			Label labelN = new Label (shell, SWT.NONE);
			labelN.setText ("Name");
			cx = 15; cy = 10;
			labelN.setBounds (cx, cy+3, 60, 20);
			final Text name = new Text (shell, SWT.BORDER);
			name.setBounds (cx+60, cy, 200, 20);
			Label labelW = new Label (shell, SWT.NONE);
			labelW.setText ("Weight");
			cy += 30;
			labelW.setBounds (cx, cy+3, 60, 20);
			final Text weight = new Text (shell, SWT.BORDER);
			weight.setBounds (cx+60, cy, 200, 20);
			Label labelP = new Label (shell, SWT.NONE);
			labelP.setText ("Parents");
			cy += 30;
			labelP.setBounds (cx, cy+3, 60, 20);
			final Text parents = new Text (shell, SWT.BORDER);
			parents.setBounds (cx+60, cy, 200, 20);
			Label labelC = new Label (shell, SWT.NONE);
			labelC.setText ("Children");
			cy += 30;
			labelC.setBounds (cx, cy+3, 60, 20);
			final Text children = new Text (shell, SWT.BORDER);
			children.setBounds (cx+60, cy, 200, 20);
	
			Button bOK = new Button (shell, SWT.PUSH);
			bOK.setText ("Ok");
			p1 = shell.getSize();
			cx = (p1.x/2 - 75) - 10;
			cy += 40;
			bOK.setBounds (cx, cy, 75, 25);
			bOK.addListener(SWT.Selection, new Listener() {
				public void handleEvent (Event e)
				{
					try
					{
				    	String nameStr = name.getText();
				    	if (nameStr.length() > 0)
				    	{
				    		if (addVertex (nameStr, weight.getText(), parents.getText(), children.getText()))
				    			config.setStatusWindowText (nameStr + " successfully added");
					    	else
					    		config.setStatusWindowText ("Error adding vertex " + nameStr);
				    	}
				    	else
				    		config.setStatusWindowText ("Error adding vertex\nNo name entered");
						shell.close ();
					}
					catch (Exception ex)
					{
						String errorMessage = "VisualDAG::showAddVertexDialog: " + ex;
						config.setStatusWindowError (errorMessage);
					}
				}
			});
	
			Button bCancel = new Button (shell, SWT.PUSH);
			bCancel.setText ("Cancel");
			cx = (p1.x/2) + 10;
			bCancel.setBounds (cx, cy, 75, 25);
			bCancel.addListener(SWT.Selection, new Listener() {
				public void handleEvent (Event e)
				{
					shell.close ();
				}
			});
	
			shell.open();
			while (!shell.isDisposed())
			{
				if (!shellParent.getDisplay().readAndDispatch()) shellParent.getDisplay().sleep();
			}
		}
	}

	/**
	 * Show delete vertex window
	 */
	public void showDelVertexDialog ()
	{
		if (Config.GRAPH_VISIBLE)
		{
	        Object cells[] = graph.getSelectionCells();
	        if (cells.length == 1)
	    		delVertex ();
	        else
	        	config.setStatusWindowText("Select one element");
		}
	}
	
	public JGraph getGraph ()
	{
		return graph;
	}

	public void handleZoomIn (Config config)
	{
		if (Config.GRAPH_VISIBLE)
		{
	   		SCALE++;
			graph.setScale (SCALE);
			VCAL vcal = config.getVCAL();
			if (vcal != null)
			{
				MenuItem item = vcal.getMenuItem("Vdag");
				if (item != null)
					vcal.enableChildren (item, "Zoom Out", true);
			}
			if (zoomOutB != null)
				zoomOutB.setEnabled (true);
	   		if (SCALE >= SCALE_LIMIT_UPPER)
	   		{
	   			if (vcal != null)
	   			{
	   				MenuItem item = vcal.getMenuItem("Vdag");
	   				if (item != null)
	   					vcal.enableChildren (item, "Zoom In", false);
	   			}
	   			if (zoomInB != null)
	   				zoomInB.setEnabled (false);
	   		}
		}
	}

	public void handleZoomOut (Config config)
	{
		if (Config.GRAPH_VISIBLE)
		{
	   		SCALE--;
			graph.setScale (SCALE);
			VCAL vcal = config.getVCAL();
			if (vcal != null)
			{
				MenuItem item = vcal.getMenuItem("Vdag");
				if (item != null)
					vcal.enableChildren (item, "Zoom In", true);
			}
			if (zoomInB != null)
				zoomInB.setEnabled (true);
	   		if (SCALE <= SCALE_LIMIT_LOWER)
	   		{
	   			if (vcal != null)
	   			{
	   				MenuItem item = vcal.getMenuItem("Vdag");
	   				if (item != null)
	   					vcal.enableChildren (item, "Zoom Out", false);
	   			}
	   			if (zoomOutB != null)
	   				zoomOutB.setEnabled (false);
	   		}
		}
	}

	public void saveDAG ()
	{
		String[] filterExtensions = { "*.xml" };
		FileDialog fileDialog = new FileDialog (shellParent, SWT.SAVE);
		fileDialog.setText("Save DAG");
		fileDialog.setFileName(Config.vdagFileName);
		fileDialog.setFilterPath(config.getStartDir());
		fileDialog.setFilterExtensions(filterExtensions);
		String selectedFile = fileDialog.open();
		if (selectedFile != null)
		{
			/**
			 * checking file extension (.xml)
			 */
			int i = selectedFile.lastIndexOf('.');
			if (i > 0)
			{
    			String fileX = selectedFile.substring(i);
    			if (fileX.equals(".xml"))
    			{
	        		config.setStartDir (selectedFile);
	        		try
	        		{
	        			File file = new File (selectedFile);
	        			BufferedWriter bw = new BufferedWriter (new FileWriter (file.getAbsoluteFile()));
	        			String DAGXMLStr = adag.flattenFull(false);
	        			Config.vdagFileName = selectedFile;
	        			if (DAGXMLStr != null)
	        				bw.write(DAGXMLStr);
	        			else
	        				config.setStatusWindowError("VisualDAG::saveDAG:DAG Conversion Error");
	        			bw.close();
	        		}
	        		catch (IOException ioe)
	        		{
	        			String errorMessage = "VisualDAG::saveDAG:IO Error: " + ioe;
	        			config.setStatusWindowError(errorMessage);
	        		}
    			}
    			else
    				config.setStatusText(" File Save Error");
			}
			else
				config.setStatusText(" File Save Error");
		}
	}

	public void toolVisible (boolean visible)
	{
		if (Config.GRAPH_VISIBLE)
		{
		   ToolBar tb = config.getToolBar();
		   ToolItem items[] = tb.getItems();
		   for (int i = 0; i < items.length; i++)
		   {
			   String text = items[i].getToolTipText ();
	
			   if (text != null)
			   {
				   if (text.equals("Save DAG"))
					   items[i].setEnabled(visible);
				   else if (text.equals("Zoom In"))
				   {
					   if (SCALE == SCALE_DEFAULT)
						   items[i].setEnabled(visible);
					   else
						   items[i].setEnabled(false);
				   }
				   else if (text.equals("Zoom Out"))
				   {
					   if (SCALE > SCALE_DEFAULT)
						   items[i].setEnabled(visible);
					   else
						   items[i].setEnabled(false);
				   }
				   else if (text.equals("Add"))
					   items[i].setEnabled(visible);
				   else if (text.equals("Delete"))
					   items[i].setEnabled(visible);
				   else if (text.equals("View Descendants"))
					   items[i].setEnabled(visible);
				   else if (text.equals("View Ancestors"))
					   items[i].setEnabled(visible);
				   else if (text.equals("View Both"))
					   items[i].setEnabled(visible);
				   else if (text.equals("View All"))
					   items[i].setEnabled(visible);
			   }
		   }
	
		   MenuItem item = config.getVCAL().getMenuItem("Vdag");
		   if (item != null)
		   {
			   VCAL vcal = config.getVCAL();
			   vcal.enableChildren(item, "Save DAG...", visible);
			   if (SCALE == SCALE_DEFAULT)
				   vcal.enableChildren(item, "Zoom In", visible);
			   else
				   vcal.enableChildren(item, "Zoom In", false);
			   if (SCALE > SCALE_DEFAULT)
				   vcal.enableChildren(item, "Zoom Out", visible);
			   else
				   vcal.enableChildren(item, "Zoom Out", false);
			   vcal.enableChildren(item, "Add\tCtrl+A", visible);
			   vcal.enableChildren(item, "Delete\tCtrl+D", visible);
			   vcal.enableChildren(item, "View Descendants", visible);
			   vcal.enableChildren(item, "View Ancestors", visible);
			   vcal.enableChildren(item, "View Both", visible);
			   vcal.enableChildren(item, "View All", visible);
		   }
		}
	}

	/**
    *
    * Create Toolbar.
    * It creates toolbar and implements the basic toolbar GUI to manage.
    *
    */
   public Image[] createToolBar (Config c)
   {
	   final Config config = c;
	   Image images[] = new Image[9];
	   ToolBar tb = config.getToolBar();
	   File file = null;

	   final ToolItem saveDAG = new ToolItem (tb, SWT.NONE | SWT.FLAT);
	   saveDAG.setToolTipText("Save DAG");
	   saveDAG.setEnabled (false);
       file = new File (Config.SAVE_DAG_IMAGE_FILE);
       if (file.exists())
       {
	       images[0] = new Image (shellParent.getDisplay(), file.getAbsoluteFile().toString());
	       saveDAG.setImage (images[0]);
       }
//       else
       saveDAG.setText("Save DAG");

	   zoomInB = new ToolItem (tb, SWT.NONE | SWT.FLAT);
	   zoomInB.setToolTipText("Zoom In");
	   zoomInB.setEnabled (false);
       file = new File (Config.ZOOM_IN_IMAGE_FILE);
       if (file.exists())
       {
	       images[1] = new Image (shellParent.getDisplay(), file.getAbsoluteFile().toString());
	       zoomInB.setImage (images[1]);
       }
//       else
    	   zoomInB.setText("Zoom In");

       zoomOutB = new ToolItem (tb, SWT.NONE | SWT.FLAT);
	   zoomOutB.setToolTipText("Zoom Out");
	   zoomOutB.setEnabled (false);
       file = new File (Config.ZOOM_OUT_IMAGE_FILE);
       if (file.exists())
       {
    	   images[2] = new Image (shellParent.getDisplay(), file.getAbsoluteFile().toString());
	       zoomOutB.setImage (images[2]);
       }
//       else
    	   zoomOutB.setText("Zoom Out");

       final ToolItem addB = new ToolItem (tb, SWT.NONE | SWT.FLAT);
	   addB.setToolTipText("Add");
	   addB.setEnabled(false);
       file = new File (Config.ADD_IMAGE_FILE);
       if (file.exists())
       {
    	   images[3] = new Image (shellParent.getDisplay(), file.getAbsoluteFile().toString());
	       addB.setImage (images[3]);
       }
//       else
    	   addB.setText("Add");

       final ToolItem delB = new ToolItem (tb, SWT.NONE | SWT.FLAT);
	   delB.setToolTipText("Delete");
	   delB.setEnabled(false);
       file = new File (Config.DELETE_IMAGE_FILE);
       if (file.exists())
       {
    	   images[4] = new Image (shellParent.getDisplay(), file.getAbsoluteFile().toString());
	       delB.setImage (images[4]);
       }
//       else
    	   delB.setText("Delete");

       final ToolItem sep2 = new ToolItem (tb, SWT.SEPARATOR);

       final ToolItem viewDB = new ToolItem (tb, SWT.NONE | SWT.FLAT);
	   viewDB.setToolTipText("View Descendants");
	   viewDB.setEnabled(false);
       file = new File (Config.VIEW_DESCENDANTS_IMAGE_FILE);
       if (file.exists())
       {
    	   images[5] = new Image (shellParent.getDisplay(), file.getAbsoluteFile().toString());
	       viewDB.setImage (images[5]);
       }
//       else
    	   viewDB.setText("View Descendants");

       final ToolItem viewAB = new ToolItem (tb, SWT.NONE | SWT.FLAT);
	   viewAB.setToolTipText("View Ancestors");
	   viewAB.setEnabled(false);
       file = new File (Config.VIEW_ANCESTORS_IMAGE_FILE);
       if (file.exists())
       {
    	   images[6] = new Image (shellParent.getDisplay(), file.getAbsoluteFile().toString());
	       viewAB.setImage (images[6]);
       }
//       else
    	   viewAB.setText("View Ancestors");

       final ToolItem viewBoth = new ToolItem (tb, SWT.NONE | SWT.FLAT);
	   viewBoth.setToolTipText("View Both");
	   viewBoth.setEnabled(false);
       file = new File (Config.VIEW_BOTH_IMAGE_FILE);
       if (file.exists())
       {
    	   images[7] = new Image (shellParent.getDisplay(), file.getAbsoluteFile().toString());
	       viewBoth.setImage (images[7]);
       }
//       else
    	   viewBoth.setText("View Both");

       final ToolItem viewAllB = new ToolItem (tb, SWT.NONE | SWT.FLAT);
	   viewAllB.setToolTipText("View All");
	   viewAllB.setEnabled(false);
       file = new File (Config.VIEW_ALL_IMAGE_FILE);
       if (file.exists())
       {
    	   images[8] = new Image (shellParent.getDisplay(), file.getAbsoluteFile().toString());
	       viewAllB.setImage (images[8]);
       }
//       else
    	   viewAllB.setText("View All");

   	   saveDAG.addListener(SWT.Selection, new Listener() {
       	public void handleEvent(Event e)
       	{
       		saveDAG ();
       	}
       });

	   zoomInB.addListener(SWT.Selection, new Listener() {
       	public void handleEvent(Event e)
       	{
       		handleZoomIn (config);
       	}
       });

		zoomOutB.addListener(SWT.Selection, new Listener() {
		public void handleEvent(Event e)
       	{
			handleZoomOut (config);
       	}
       });

		addB.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e)
       	{
       		showAddVertexDialog ();
       	}
       });

		delB.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e)
       	{
       		showDelVertexDialog ();
       	}
       });

	   viewDB.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e)
	    {
   			DESCENDANTS_VISIBLE = true;
   			makeVisible ();
       	}
       });

	   viewAB.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e)
	    {
  			ANCESTORS_VISIBLE = true;
   			makeVisible ();
       	}
       });

       viewBoth.addListener(SWT.Selection, new Listener() {
   		public void handleEvent(Event e)
	    {
   			BOTH_VISIBLE = true;
   			makeVisible ();
       	}
       });

	   viewAllB.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e)
	    {
  			makeAllVisible ();
      	}
      });

       tb.pack();
       return images;
   }
}
