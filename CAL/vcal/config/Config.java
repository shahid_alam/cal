/* ============================================================================
|
|   Filename:    Config.java
|   Dated:       22 Dec, 2006
|   By:          Shahid Alam (salam@sce.carleton.ca)
|
|   Description: See below
|
|   TO DO: 		 Add other configurations
|
 ----------------------------------------------------------------------------*/

package org.vcal.config;

import org.w3c.dom.*;
import javax.xml.parsers.*;

import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;

import org.vcal.VCAL;
import org.vcal.vdag.ADAG;
import org.vcal.vdag.VisualDAG;
import org.vcal.window.StatusWindow;

/**
 * <p>
 * This class implements the Configuration settings for VCAL.
 * It reads them from a file and pass it to the main VCAL program.
 * </p>
 * 
 * @author Shahid Alam
 * @version 3.0
 *
 */
public class Config
{
	private final static String configFileName = "vcal.ini";
	public static boolean DEBUG = true;

	private static VCAL vcal = null;
	private static ADAG adag = null;
	private static VisualDAG vdag = null;
	private static ToolBar toolBar = null;
	private static int WIN_WIDTH = 1024;
	private static int WIN_HEIGHT = 640;
	private static int MAX_FILE_SIZE = 65535;
	private static String START_DIR = "C:/";

	private static Document document;
	private static StatusWindow statusWindow = null;
	private static Label statusLabel = null, editorLabel = null;

	public static int STATUS_WINDOW_TEXT_LIMIT = 4028;

	public static boolean GRAPH_VISIBLE = false;

	public static Color WHITE_COLOR;
	public static Color BLACK_COLOR;
	public static Color KHAKI_COLOR;
	public static Color YELLOW_COLOR;
	public static Color DARK_GRAY_COLOR;
	public static Color DARK_BLUE_COLOR;
	public static Color FOREST_GREEN_COLOR;
	public static Color STEEL_COLOR;
	public static Color LIGHT_STEEL_COLOR;
	public static Color LIGHT_GRAY_COLOR;

	public static int MAX_SPEED = 30;
	public static int SCANNING_SPEED = 30;
	public static int GRAPH_SCALE = 1;

	public static boolean GENERATE_LEXICAL_ANALYSIS_OUTPUT = false;
	public static boolean GENERATE_SYNTAX_ANALYSIS_OUTPUT = true;
	public static boolean GENERATE_CODE = false;
	public static boolean VIEW_LEXICAL_ANALYSIS = false;
	public static boolean VIEW_SYNTAX_ANALYSIS = false;

	/**
	 * Images for toolbar
	 */
	public final static String NEW_IMAGE_FILE = "images/new.gif";
	public final static String OPEN_IMAGE_FILE = "images/open.gif";
	public final static String SAVE_IMAGE_FILE = "images/save.gif";
	public final static String PRINT_IMAGE_FILE = "images/print.gif";
	public final static String RUN_IMAGE_FILE = "images/run.gif";
	public final static String SAVE_DAG_IMAGE_FILE = "images/save_dag.gif";
	public final static String ZOOM_IN_IMAGE_FILE = "images/zoom_in.gif";
	public final static String ZOOM_OUT_IMAGE_FILE = "images/zoom_out.gif";
	public final static String ADD_IMAGE_FILE = "images/add.gif";
	public final static String DELETE_IMAGE_FILE = "images/delete.gif";
	public final static String VIEW_DESCENDANTS_IMAGE_FILE = "images/view_descendants.gif";
	public final static String VIEW_ANCESTORS_IMAGE_FILE = "images/view_ancestors.gif";
	public final static String VIEW_BOTH_IMAGE_FILE = "images/view_both.gif";
	public final static String VIEW_ALL_IMAGE_FILE = "images/view_all.gif";

	/**
	 * Different windows images
	 */
	public final static String TEXT_EDITOR_WINDOW_IMAGE_FILE = "images/cal_editor.gif";
	public final static String UML_MODEL_WINDOW_IMAGE_FILE = "images/uml_model.gif";
	public final static String LEXICAL_ANALYSIS_WINDOW_IMAGE_FILE = "images/lexical_analysis.gif";
	public final static String SYNTAX_ANALYSIS_IMAGE_FILE = "images/syntax_analysis.gif";
	public final static String DEPENDENCY_ANALYSIS_WINDOW_IMAGE_FILE = "images/dependency_analysis.gif";
	public final static String STATUS_WINDOW_IMAGE_FILE = "images/status.gif";
	public final static String ERROR_WINDOW_IMAGE_FILE = "images/error.gif";

	/**
	 * Images for UML Model
	 */
	public final static String MODEL_IMAGE_FILE = "images/model.gif";
	public final static String CLASS_IMAGE_FILE = "images/class.gif";
	public final static String OPERATION_IMAGE_FILE = "images/operation.gif";
	public final static String ATTRIBUTE_IMAGE_FILE = "images/attribute.gif";

	/**
	 * CAL logo and application image
	 */
	public final static String CAL_LOGO_FILE = "images/cal.gif";
	public final static String APPLICATION_LOGO_FILE = "images/logo.gif";

	public final static String CAL = "cal";
	public final static String NO_NAME = "noname.cal";
	public final static char FILE_MODIFIED_CHAR = '+';
	public static String CAL_PARSER_EXE_FILE = null;
	public static String CURRENT_CAL_FILE = "";
	public static String calFileName = null;
	public static String xmiFileName = null;
	public static String astFileName = "CAL_AST.xml";
	public static String vdagFileName = "vdag.xml";
	public static String ROOT_ELEMENT = "_ROOT_";

	public static boolean LA_WIN_ALREADY_OPEN = false;
	public static boolean SA_WIN_ALREADY_OPEN = false;
	public static boolean DA_WIN_ALREADY_OPEN = false;

	public static String VIEW_DAG_ROOT_ELEMENT = null;

	/**
	 * Constructor
	 * @param display Display to be used to configure colors
	 * @param sl Label Configure the status label to print to for gloabal use
	 * @param el Label Configure the editor label to print to for gloabal use 
	 */
	public Config (Display display, Label sl, Label el)
	{
		WHITE_COLOR = new Color (display, 255, 255, 255);
		BLACK_COLOR = new Color (display, 0, 0, 0);
		KHAKI_COLOR = new Color (display, 240, 230, 140);
		YELLOW_COLOR = new Color (display, 255, 255, 0);
		DARK_GRAY_COLOR = new Color (display, 105, 105, 105);
		DARK_BLUE_COLOR = new Color (display, 0, 0, 139);
		FOREST_GREEN_COLOR = new Color (display, 34, 139, 34);
		STEEL_COLOR = new Color (display, 70, 130, 180);
		LIGHT_STEEL_COLOR = new Color (display, 176, 196, 222);
		LIGHT_GRAY_COLOR = new Color (display, 211, 211, 211);

		statusLabel = sl;
		editorLabel = el;

		try
		{
			/**
			 * Read the Config file and parse the XML
			 */
			File file = new File (configFileName);
			if (file.exists())
			{
				FileInputStream fileStream = new FileInputStream (file.getAbsoluteFile());
				DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance ();
				DocumentBuilder builder = builderFactory.newDocumentBuilder ();
				document = builder.parse(fileStream);
				document.getDocumentElement().normalize();
				
				readCALExeFileName ();
				readSize ();
				readStartDir ();
				readFileNames ();
			}
			else
				setStatusText("Config File No Loaded");
		}
		catch (Exception e)
		{
			String errorMessage = "Config::Error: " + e;
			System.err.println(errorMessage);
		}
	}

	/**
	 * Reads start dir for opening and saving file
	 */
	public void readCALExeFileName ()
	{
		if (document != null)
		{
			NodeList nodes = document.getElementsByTagName ("CAL_EXE");
			for (int n = 0; n < nodes.getLength(); n++)
			{
				NamedNodeMap attributes = nodes.item(n).getAttributes();
				if (attributes != null && attributes.getLength () > 0)
				{
					try
					{
						String obj = attributes.getNamedItem ("value").getNodeValue().trim();
						if (obj != null)
							CAL_PARSER_EXE_FILE = obj;
					}
					catch (NullPointerException e) { }
				}
			}
		}
	}

	/**
	 * Reads size of the window from XML configuration
	 */
	public void readSize ()
	{
		if (document != null)
		{
			NodeList nodes = document.getElementsByTagName ("Size");
			for (int n = 0; n < nodes.getLength(); n++)
			{
				NamedNodeMap attributes = nodes.item(n).getAttributes();
				if (attributes != null && attributes.getLength () > 0)
				{
					try
					{
						String obj = attributes.getNamedItem ("width").getNodeValue().trim();
						if (obj != null)
							WIN_WIDTH = (new Integer (obj)).intValue();
						obj = attributes.getNamedItem ("height").getNodeValue().trim();
						if (obj != null)
							WIN_HEIGHT = (new Integer (obj)).intValue();
					}
					catch (NullPointerException e) { }
				}
			}
		}
	}

	/**
	 * Reads start dir for opening and saving file
	 */
	public void readStartDir ()
	{
		if (document != null)
		{
			NodeList nodes = document.getElementsByTagName ("StartDir");
			for (int n = 0; n < nodes.getLength(); n++)
			{
				NamedNodeMap attributes = nodes.item(n).getAttributes();
				if (attributes != null && attributes.getLength () > 0)
				{
					try
					{
						String obj = attributes.getNamedItem ("value").getNodeValue().trim();
						if (obj != null)
							START_DIR = obj;
					}
					catch (NullPointerException e) { }
				}
			}
		}
	}

	/**
	 * Reads start dir for opening and saving file
	 */
	public void readFileNames ()
	{
		if (document != null)
		{
			NodeList nodes = document.getElementsByTagName ("Files");
			for (int n = 0; n < nodes.getLength(); n++)
			{
				NamedNodeMap attributes = nodes.item(n).getAttributes();
				if (attributes != null && attributes.getLength () > 0)
				{
					try
					{
//						String obj = attributes.getNamedItem ("xmi").getNodeValue().trim();
//						if (obj != null)
//							xmiFileName = obj;
					}
					catch (NullPointerException e) { }
				}
			}
		}
	}

	/**
	 * Get the application main window width
	 * @return Windows width
	 */
	public int getWindowWidth()
	{
		return WIN_WIDTH;
	}

	/**
	 * Get the application main windows height
	 * @return Windows height
	 */
	public int getWindowHeight()
	{
		return WIN_HEIGHT;
	}

	/**
	 * Get the maximum file size
	 * @return Maximum file size
	 */
	public int getMaxFileSize()
	{
		return MAX_FILE_SIZE;
	}

	/**
	 * Get start dir for opening and saving file
	 * @return Start Dir
	 */
	public String getStartDir()
	{
		return START_DIR;
	}

	/**
	 * Get XMI file name
	 * @return Start Dir
	 */
	public String getXMIFileName()
	{
		return xmiFileName;
	}

	/**
	 * Set VCAL for global use
	 * @param vc VCAL to set as global
	 */
	public void setVCAL (VCAL vc)
	{
		vcal = vc;
	}

	/**
	 * Get VCAL
	 * @return VCAL
	 */
	public VCAL getVCAL ()
	{
		return vcal;
	}

	/**
	 * Set ADAG for global use
	 * @param ad ADAG to be set for global reuse
	 */
	public void setADAG (ADAG ad)
	{
		adag = ad;
	}

	/**
	 * Get ADAG
	 * @return ADAG
	 */
	public ADAG getADAG ()
	{
		return adag;
	}

	/**
	 * Set VDAG for global use
	 * @param vd VDAG to be set for global reuse
	 */
	public void setVDAG (VisualDAG vd)
	{
		vdag = vd;
	}

	/**
	 * Get VDAG
	 * @return VDAG
	 */
	public VisualDAG getVDAG ()
	{
		return vdag;
	}

	/**
	 * Setting the start dir for global use
	 * @param dir New value of start dir
	 */
	public void setStartDir (String dir)
	{
		if (dir != null)
			START_DIR = dir;
	}

	/**
	 * Set XMI file name for global use
	 */
	public void setXMIFileName (String xmiFile)
	{
		xmiFileName = xmiFile;
	}

	/**
	 * Setting the status window for global use
	 * access to set the program status
	 * @param sw StatusWindow
	 */
	public void setStatusWindow (StatusWindow sw)
	{
		statusWindow = sw;
	}
	
	/**
	 * Set the application toolbar for global use
	 * @param tb
	 */
	public void setToolBar (ToolBar tb)
	{
		toolBar = tb;
	}

	/**
	 * Get the application toolbar
	 * @return the application ToolBar
	 */
	public ToolBar getToolBar ()
	{
		return toolBar;
	}

	/**
	 * Check if tool item is enabled or not
	 * @return True if tool item is enabled and vice versa
	 */
	public boolean isToolItemEnabled (String toolItemData)
	{
		boolean enabled = true;
		ToolItem items[] = getToolBar().getItems();
		for (int i = 0; i < items.length; i++)
		{
			if (items[i].getToolTipText().equals(toolItemData))
			{
				enabled = items[i].isEnabled();
				break;
			}
		}
		return enabled;
	}

	/**
	 * Enable tool item in the Tool Bar with specific data. Data stored for tool item
	 * is the name of the tool item
	 * @param tb
	 * @param itemName
	 */
	public void enableToolItem (ToolBar tb, String itemName, boolean enable)
	{
		if (tb != null)
		{
			ToolItem items[] = tb.getItems();
			for (int i = 0; i < items.length; i++)
			{
				Object d = items[i].getToolTipText();
				if (d != null && d.equals(itemName))
				{
					items[i].setEnabled (enable);
					break;
				}
			}
		}
	}

	/**
	 * Setting the text of the status label for global use
	 * @param text Text of status label
	 */
	public void setStatusWindowText (String text)
	{
		if (statusWindow != null)
			statusWindow.setText(text);
	}

	/**
	 * Setting the text of the status label for global use
	 * @param text Text of status label
	 */
	public void setStatusWindowError (String text)
	{
		if (statusWindow != null)
			statusWindow.setError(text);
	}

	/**
	 * Setting the text of the status label for global use
	 * @param text Text of status label
	 */
	public void setStatusText (String text)
	{
		statusLabel.setText(text);
	}

	/**
	 * Setting the text of the editor label for global use
	 * @param text Text of editor label
	 */
	public void setEditorText (String text)
	{
		editorLabel.setText(text);
	}

	/**
	 * 
	 * Write XML configuration to config file
	 * @param shell Shell of the main window created for the application
	 */
	public void writeToFile (Shell shell)
	{
		try
		{
			Point p = shell.getSize();
	    	WIN_WIDTH = p.x;
	    	WIN_HEIGHT = p.y;
			File file = new File (configFileName);
			BufferedWriter bw = new BufferedWriter (new FileWriter (file.getAbsoluteFile()));
			String XMLStr = "<VCAL_SETTINGS>";
			XMLStr += "<CAL_EXE value='" + CAL_PARSER_EXE_FILE + "' />";
			XMLStr += "<Size width='" + WIN_WIDTH + "' height='" + WIN_HEIGHT + "' />";
			XMLStr += "<StartDir value='" + START_DIR + "' />";
			XMLStr += "<Files />";
			XMLStr += "</VCAL_SETTINGS>";
			bw.write(XMLStr);
			bw.close();
		}
		catch (IOException ioe)
		{
			String errorMessage = "Config::writeToFile:IO Error: " + ioe;
			setStatusWindowError(errorMessage);
		}
	}
}
