/* ============================================================================
|
|   Filename:    LexicalAnalysis.java
|   Dated:       12 Dec, 2006
|   By:          Shahid Alam (salam@sce.carleton.ca)
|
|   Description: See below
|
|   TO DO:
|
 ----------------------------------------------------------------------------*/

package org.vcal;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import org.vcal.config.Config;
import org.vcal.window.TextEditorWindow;

/**
 * 
 * 
 * 
 * @author Shahid Alam
 * @version 3.0
 * 
 */
public class LexicalAnalysis
{
	private static Config config = null;

	private static TextEditorWindow window = null;
	private static StyledText text = null;
	private static String FILE_NAME = null;
	private static Table lexicalAnalysisTable = null;
	private static TableItem tableItems[] = null;
	private static int lookaheadPos;
	private static String[] TOKEN;
	private static int[] tokenNUM;
	private static int TN;
	private static boolean START = false;

	public LexicalAnalysis (String filename, TextEditorWindow win, Config con)
	{
		config = con;
		FILE_NAME = filename;
   		lookaheadPos = 0;
   		window = win;
   		TN = 0;
   		START = false;
	}

	public Table createTable ()
	{
		try
		{
			if (lexicalAnalysisTable != null)
				lexicalAnalysisTable.removeAll();
			if (tableItems != null)
			{
				for (int n = 0; n < tableItems.length; n++)
					tableItems[n] = null;
				tableItems = null;
			}
			text = window.getTextWidget ();
			lexicalAnalysisTable = null;
			CTabFolder folder = (CTabFolder)window.getOutputWindowFolder();
			if (folder != null)
			{
		   		char[] cText = new char [config.getMaxFileSize()];
				String token[];
				int size;

				String fileStr = FILE_NAME;
				int i = FILE_NAME.lastIndexOf('.');
				if (i > 0)
					fileStr = FILE_NAME.substring(0, i);
				File file = new File (fileStr + ".num");
				BufferedReader fileNUM = new BufferedReader (new FileReader (file));
				size = fileNUM.read (cText);
				String numText = new String (cText, 0, size);
				token = numText.split (":");
				tokenNUM = new int [token.length];
				for (int tn = 0; tn < token.length; tn++)
					tokenNUM[tn] = Integer.parseInt (token[tn]);
				if (Config.DEBUG)
					config.setStatusWindowText("tokenNUM.length: " + tokenNUM.length + " Content: " + numText);
				numText = null;
				fileNUM.close ();
	
				file = new File (FILE_NAME);
				BufferedReader fileLA = new BufferedReader (new FileReader (file));
				size = fileLA.read (cText);
				String laText = new String (cText, 0, size);
				TOKEN = laText.split (":");

				lexicalAnalysisTable = new Table (folder, SWT.MULTI | SWT.BORDER);
				Font font = new Font (folder.getDisplay(), "Courier New", 8, SWT.NORMAL);
				lexicalAnalysisTable.setFont(font);
				lexicalAnalysisTable.setBackground(Config.WHITE_COLOR);
				lexicalAnalysisTable.setForeground(Config.FOREST_GREEN_COLOR);
				lexicalAnalysisTable.setHeaderVisible(true);
				lexicalAnalysisTable.setLinesVisible(true);

				String columnNames[] = { "Token", "Lexeme" };
				for (i = 0; i < columnNames.length; i++)
				{
					TableColumn leftColumn = new TableColumn (lexicalAnalysisTable, SWT.NONE);
					leftColumn.setResizable(true);
					leftColumn.setText (columnNames[i]);
				}

				int n = 0;
				for (i = 0; i < tokenNUM.length; i++)
				{
					TableItem tableItem = new TableItem (lexicalAnalysisTable, SWT.NONE);
	    			tableItem.setText (0, TOKEN[n]);
	    			tableItem.setText (1, TOKEN[++n]);
	    			n++;
				}

				for (i = 0; i < columnNames.length; i++)
					lexicalAnalysisTable.getColumn(i).pack();
				window.openLexicalAnalysisWindow ("Lexical Analysis", lexicalAnalysisTable);

				/**
		   		 * 
		   		 * This version doesn't support running the lexical anlaysis in a 
		   		 * separate thread. There is no support for setting the speed of scanning.
		   		 * 
		   		 */
				/*tableItems = new TableItem[tokenNUM.length];
				for (i = 0; i < tableItems.length; i++)
				{
					tableItems[i] = new TableItem (lexicalAnalysisTable, SWT.NONE);
	    			tableItems[i].setText (0, "                 .");
	    			tableItems[i].setText (1, "                 .");
				}
				for (i = 0; i < columnNames.length; i++)
					lexicalAnalysisTable.getColumn(i).pack();
				window.openLexicalAnalysisWindow ("Lexical Analysis", lexicalAnalysisTable);
		   		CTabItem items[] = folder.getItems();
		   		for (i = 0; i < items.length; i++)
		   		{
		   			if (items[i].getText().equals("Lexical Analysis"))
		   			{
		   				folder.setSelection(items[i]);
						FontData fData[] = items[i].getFont().getFontData();
						if (fData[0] != null && fData[0].getStyle() != SWT.NORMAL)
						{
							int h = fData[0].getHeight();
							String fontName = fData[0].getName();
							items[i].setFont(new Font(folder.getDisplay(), fontName, h, SWT.NORMAL));
						}
		   			}
		   		}
		   		START = true;

	    		int s = Config.MAX_SPEED - Config.SCANNING_SPEED;
		   		final int SLEEP_AMOUNT = 2 * s;
		   		folder.getDisplay().syncExec(new Runnable ()
    	        {
        	        public void run ()
            	    {
    	    			try { Thread.sleep (1000); }
    	    			catch (InterruptedException e) { return; }
        	    		while (START)
        	    		{
        	    			step ();
        	    			try { Thread.sleep (SLEEP_AMOUNT); }
        	    			catch (InterruptedException e) { break; }
        	    		}
	                }
    	        });*/
			}
			
			return lexicalAnalysisTable;
		}
		catch (Exception e)
		{
			config.setStatusWindowError("LexicalAnalysis::createTable: " + e);
			return null;
		}
	}

	/**
	*
	* This version doesn't support running the lexical anlaysis in a 
	* separate thread. There is no support for setting the speed of scanning.
	*
    *
    * Step through the SC file and move the lookaheadPos pointer according
    * to the positions given in LA file
    *
    */
	public void step ()
	{
		//
		// Lexical Analysis
		//
		if (START)
		{
			if (TN < tokenNUM.length)
			{
				if (lookaheadPos == tokenNUM[TN])
				{
					int tn = 2 * TN;
        			tableItems[TN].setText (new String[] { TOKEN[tn], TOKEN[tn + 1] });
					TN++;
				}
				lookaheadPos++;
				if (text != null)
				{
					text.forceFocus ();
					text.setSelection (0, lookaheadPos);
					text.setCaretOffset (lookaheadPos);
				}
			}
			else
			{
				lookaheadPos = TN = 0;
				START = false;
			}
		}
	}
}
