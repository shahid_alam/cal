/* ============================================================================
|
|   Filename:    RunVCAL.java
|   Dated:       09 Dec, 2006
|   By:          Shahid Alam (salam@sce.carleton.ca)
|
|   Description: To run VCAL
|
|   TO DO:
|
 ----------------------------------------------------------------------------*/

package org.vcal;

/**
 * 
 * Class RunVCAL.
 * 
 */
public class RunVCAL
{
	/**
	 *
	 * Constructor Initializing all the objects
	 *
	 */
    public RunVCAL ()
    {
    	try
    	{
    	}
		catch (Exception e)
		{
			System.out.println ("Error::RunVCAL: " + e);
		}
    }

    /**
     * Print the version of VCAL
     */
	public void printVersion ()
	{
		System.out.println(VCAL.Version);
	}

    /**
     *
     * The standard main method.
     * To run in normal mode: RunVCAL 0 [filename.cal] [filename.xml]
     * To run in debug mode: RunVCAL 1 [filename.cal] [filename.xml]
     *
     */
    public static void main (String[] args)
    {
    	VCAL.main (args);
    }
}
