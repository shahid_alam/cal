/* ============================================================================
|
|   Filename:    CALHelp.java
|   Dated:       12 Dec, 2006
|   By:          Shahid Alam (salam@sce.carleton.ca)
|
|   Description: See below
|
|   TO DO:
|
 ----------------------------------------------------------------------------*/

package org.vcal.help;

import java.io.File;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.ProgressEvent;
import org.eclipse.swt.browser.ProgressListener;
import org.eclipse.swt.browser.StatusTextEvent;
import org.eclipse.swt.browser.StatusTextListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

/**
 * 
 * <p>
 * This class displays the help file
 * <p>
 * 
 * @author Shahid Alam
 * @version 3.0
 * 
 */
public class CALHelp
{
	private static String helpFile = "html/help.html";
	private static Display display;
	private static Browser browser = null;

	/**
	 *
	 * Constructor
	 *
	 */
    public CALHelp (Display d)
    {
		display = d;
    }

	/**
	 * Open a local link of help (HTML) file
	 * @return true if success else false
	 */
	public boolean open ()
	{
		Shell shell = null;
		File file = new File (helpFile);
		String fileStr = "";
		if (file.exists())
			fileStr = file.getAbsoluteFile().toString();
		file = null;

		try
		{
			shell = new Shell (display);
			shell.setText("VCAL Help");
			GridLayout gridLayout = new GridLayout();
			gridLayout.numColumns = 3;
			shell.setLayout(gridLayout);
			ToolBar toolbar = new ToolBar(shell, SWT.NONE);
			ToolItem itemBack = new ToolItem(toolbar, SWT.PUSH);
			itemBack.setText("Back");
			ToolItem itemForward = new ToolItem(toolbar, SWT.PUSH);
			itemForward.setText("Forward");
			ToolItem itemRefresh = new ToolItem(toolbar, SWT.PUSH);
			itemRefresh.setText("Refresh");
			
			GridData data = new GridData();
			data.horizontalSpan = 3;
			toolbar.setLayoutData(data);

			browser = new Browser (shell, SWT.NONE);
			data = new GridData();
			data.horizontalAlignment = GridData.FILL;
			data.verticalAlignment = GridData.FILL;
			data.horizontalSpan = 3;
			data.grabExcessHorizontalSpace = true;
			data.grabExcessVerticalSpace = true;
			browser.setLayoutData(data);

			final Label status = new Label(shell, SWT.NONE);
			data = new GridData(GridData.FILL_HORIZONTAL);
			data.horizontalSpan = 2;
			status.setLayoutData(data);

			final ProgressBar progressBar = new ProgressBar(shell, SWT.NONE);
			data = new GridData();
			data.horizontalAlignment = GridData.END;
			progressBar.setLayoutData(data);

			/* event handling */
			Listener listener = new Listener() {
				public void handleEvent(Event event) {
					ToolItem item = (ToolItem)event.widget;
					String string = item.getText();
					if (string.equals("Back")) browser.back(); 
					else if (string.equals("Forward")) browser.forward();
					else if (string.equals("Refresh")) browser.refresh();
			   }
			};
			browser.addProgressListener(new ProgressListener() {
				public void changed(ProgressEvent event) {
						if (event.total == 0) return;                            
						int ratio = event.current * 100 / event.total;
						progressBar.setSelection(ratio);
				}
				public void completed(ProgressEvent event) {
					progressBar.setSelection(0);
				}
			});
			browser.addStatusTextListener(new StatusTextListener() {
				public void changed(StatusTextEvent event) {
					status.setText(event.text);	
				}
			});
			itemBack.addListener(SWT.Selection, listener);
			itemForward.addListener(SWT.Selection, listener);
			itemRefresh.addListener(SWT.Selection, listener);
		}
		catch (SWTError e)
		{
			System.err.println("BrowserWindow::open:Error: " + e);
		}
		if (browser != null)
		{
			shell.open();
			if (browser.setUrl(fileStr))
			{
				shell.open();
				return true;
			}
			else
			{
				shell.close();
				return false;
			}
		}
		else
			shell.close();

		while (!shell.isDisposed())
		{
			if (!display.readAndDispatch()) display.sleep();
		}
		return false;
	}
}
